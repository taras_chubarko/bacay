<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class DetskiyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        
        //$cityes = \City::where('geo_city.country_id', 3159)->get();
        //foreach($cityes as $city)
        //{
        //    $cityarr[$city->city_id] = $city->city_id;
        //}
        $metros = \Metro::where('geo_metro.city_id', 4400)->get();
        foreach($metros as $metro)
        {
            $metroArr[$metro->id] = $metro->id;
        }
        
        
        foreach(range(1, 10) as $index)
        {
                
                $advert = new \Advert;
		$advert->user_id 	= 1;
		$advert->type 		= 'detskiy-mir';
		$advert->name 		= $faker->sentence(6);
		$advert->status 	= 1;
		$advert->save();
		
		$advert->pivotCity()->attach($advert->id, [
			'type' 	=> 'detskiy-mir',
			'city' 	=> 4400
		]);
                
                $advert->pivotMetro()->attach($advert->id, [
                    'type' 	=> 'detskiy-mir',
                    'metro'     => $faker->randomElement($metroArr),
                ]);
		   
		$advert->pivotCategory()->attach($advert->id, [
			'type' 		=> 'detskiy-mir',
			'category' 	=> 11,
                        'sort'          => 0,
		]);
		
                $el = $faker->numberBetween($min = 261, $max = 271);
                
		$advert->pivotCategory()->attach($advert->id, [
			'type' 		=> 'detskiy-mir',
			'category' 	=> $el,
                        'sort'          => 1,
		]);
                
                if($el == 262)
                {
                    $advert->pivotCategory()->attach($advert->id, [
                            'type' 	=> 'detskiy-mir',
                            'category' 	=> $faker->numberBetween($min = 272, $max = 273),
                            'sort'      => 2,
                    ]);
                }
                
		$advert->pivotPrice()->attach($advert->id, [
			'type' 		=> 'detskiy-mir',
			'price' 	=> $faker->numberBetween($min = 10000, $max = 1000000)
		]);
		
		$advert->pivotBody()->attach($advert->id, [
			'type' 	=> 'detskiy-mir',
			'body' 	=> $faker->realText(500)
		]);
                
                foreach(range(1, 1) as $i)
                {
                    $im = $faker->image($dir = public_path('uploads/').'detskiy-mir', $width = 800, $height = 600);
                    $imex = explode(public_path('uploads/').'detskiy-mir/', $im);
                    
                    $img = new \Img;
                    $img->user_id 	= 1;
                    $img->filename 	= $imex[1];
                    $img->filesize  	= 0;
                    $img->uri 	        = 'detskiy-mir';
                    $img->status 	= 0;
                    $img->save();
                    //$data[] = $img;
                    
                    $advert->pivotImages()->attach($advert->id, [
                        'type' 	=> 'detskiy-mir',
                        'fid' => $img->id,
                        'sort' => $i,
                    ]);
                    \Img::setStatus($img->id);
                }
		
		$now = date('Y-m-d');
		$start_date = strtotime($advert->created_at);
		$end_date = strtotime("+10 day", $start_date);
		
		$advert->pivotEnd()->attach($advert->id, [
			'type' 	=> 'detskiy-mir',
			'end' 	=> date('Y-m-d H:i:s', $end_date)
		]);
        }
    }
}
