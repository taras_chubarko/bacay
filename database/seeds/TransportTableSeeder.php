<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class TransportTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        
        //$cityes = \City::where('geo_city.country_id', 3159)->get();
        //foreach($cityes as $city)
        //{
        //    $cityarr[$city->city_id] = $city->city_id;
        //}
        $metros = \Metro::where('geo_metro.city_id', 4400)->get();
        foreach($metros as $metro)
        {
            $metroArr[$metro->id] = $metro->id;
        }
        
        
        foreach(range(1, 10) as $index)
        {
                
                $advert = new \Advert;
		$advert->user_id 	= 1;
		$advert->type 		= 'transport';
		$advert->name 		= $faker->sentence(6);
		$advert->status 	= 1;
		$advert->save();
		
		$advert->pivotCity()->attach($advert->id, [
			'type' 	=> 'transport',
			'city' 	=> 4400
		]);
                
                $advert->pivotMetro()->attach($advert->id, [
                    'type' 	=> 'transport',
                    'metro'     => $faker->randomElement($metroArr),
                ]);
		   
		$advert->pivotCategory()->attach($advert->id, [
			'type' 		=> 'transport',
			'category' 	=> 2,
                        'sort'          => 0,
		]);
		
		$advert->pivotCategory()->attach($advert->id, [
			'type' 		=> 'transport',
			'category' 	=> 32,
                        'sort'          => 1,
		]);
                
                $advert->pivotCategory()->attach($advert->id, [
			'type' 		=> 'transport',
			'category' 	=> 37,
                        'sort'          => 2,
		]);
                
                $advert->pivotBrand()->attach($advert->id, [
			'type' 		=> 'transport',
			'brand' 	=> $faker->numberBetween($min = 1, $max = 180),
		]);
                
                $advert->pivotModel()->attach($advert->id, [
			'type' 		=> 'transport',
			'model' 	=> $faker->numberBetween($min = 1, $max = 180),
		]);
                
                $advert->pivotTransmission()->attach($advert->id, [
			'type' 		=> 'transport',
			'transmission' 	=> $faker->numberBetween($min = 1, $max = 5),
		]);
                
                $advert->pivotDrive()->attach($advert->id, [
			'type' 		=> 'transport',
			'drive' 	=> $faker->numberBetween($min = 1, $max = 3),
		]);
                
                $advert->pivotFuel()->attach($advert->id, [
			'type' 		=> 'transport',
			'fuel' 	        => $faker->numberBetween($min = 1, $max = 9),
		]);
		
                $advert->pivotYear()->attach($advert->id, [
			'type' 		=> 'transport',
			'year' 	        => $faker->numberBetween($min = 2000, $max = 2016),
		]);
		
		$advert->pivotPrice()->attach($advert->id, [
			'type' 		=> 'transport',
			'price' 	=> $faker->numberBetween($min = 10000, $max = 1000000)
		]);
		
		$advert->pivotBody()->attach($advert->id, [
			'type' 	=> 'transport',
			'body' 	=> $faker->realText(500)
		]);
                
                foreach(range(1, 2) as $i)
                {
                    $im = $faker->image($dir = public_path('uploads/').'transport', $width = 800, $height = 600);
                    $imex = explode(public_path('uploads/').'transport/', $im);
                    
                    $img = new \Img;
                    $img->user_id 	= 1;
                    $img->filename 	= $imex[1];
                    $img->filesize  	= 0;
                    $img->uri 	        = 'transport';
                    $img->status 	= 0;
                    $img->save();
                    //$data[] = $img;
                    
                    $advert->pivotImages()->attach($advert->id, [
                        'type' 	=> 'transport',
                        'fid' => $img->id,
                        'sort' => $i,
                    ]);
                    \Img::setStatus($img->id);
                }
		
		$now = date('Y-m-d');
		$start_date = strtotime($advert->created_at);
		$end_date = strtotime("+10 day", $start_date);
		
		$advert->pivotEnd()->attach($advert->id, [
			'type' 	=> 'transport',
			'end' 	=> date('Y-m-d H:i:s', $end_date)
		]);
        }
    }
}
