<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class RabotaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        
        //$cityes = \City::where('geo_city.country_id', 3159)->get();
        //foreach($cityes as $city)
        //{
        //    $cityarr[$city->city_id] = $city->city_id;
        //}
        $metros = \Metro::where('geo_metro.city_id', 4400)->get();
        foreach($metros as $metro)
        {
            $metroArr[$metro->id] = $metro->id;
        }
        
        
        foreach(range(1, 20) as $index)
        {
                
                $advert = new \Advert;
		$advert->user_id 	= 1;
		$advert->type 		= 'rabota-i-obrazovanie';
		$advert->name 		= $faker->sentence(6);
		$advert->status 	= 1;
		$advert->save();
		
		$advert->pivotCity()->attach($advert->id, [
			'type' 	=> 'rabota-i-obrazovanie',
			'city' 	=> 4400
		]);
                
                $advert->pivotMetro()->attach($advert->id, [
                    'type' 	=> 'rabota-i-obrazovanie',
                    'metro'     => $faker->randomElement($metroArr),
                ]);
		   
		$advert->pivotCategory()->attach($advert->id, [
			'type' 		=> 'rabota-i-obrazovanie',
			'category' 	=> 3,
                        'sort'          => 0,
		]);
		
                $rabota = $faker->randomElement([98, 99]);
                
		$advert->pivotCategory()->attach($advert->id, [
			'type' 		=> 'rabota-i-obrazovanie',
			'category' 	=> $rabota,
                        'sort'          => 1,
		]);
                
                $advert->pivotService()->attach($advert->id, [
			'type' 		=> 'rabota-i-obrazovanie',
			'service' 	=> $faker->numberBetween($min = 74, $max = 97),
		]);
                
                $advert->pivotSchedule()->attach($advert->id, [
			'type' 		=> 'rabota-i-obrazovanie',
			'schedule' 	=> $faker->numberBetween($min = 106, $max = 108),
		]);
                
                $advert->pivotExperience()->attach($advert->id, [
			'type' 		=> 'rabota-i-obrazovanie',
			'experience' 	=> $faker->numberBetween($min = 1, $max = 50),
		]);
                
                $advert->pivotExperience()->attach($advert->id, [
			'type' 		=> 'rabota-i-obrazovanie',
			'experience' 	=> $faker->numberBetween($min = 1, $max = 50),
		]);
                
                if($rabota == 99)
                {
                    $advert->pivotEducation()->attach($advert->id, [
                        'type' 		=> 'rabota-i-obrazovanie',
                        'education' 	=> $faker->numberBetween($min = 102, $max = 105),
                    ]);
                    
                    $advert->pivotGender()->attach($advert->id, [
                        'type' 		=> 'rabota-i-obrazovanie',
                        'gender' 	=> $faker->numberBetween($min = 100, $max = 101),
                    ]);
                    
                    $advert->pivotAge()->attach($advert->id, [
                        'type' 	=> 'rabota-i-obrazovanie',
                        'age' 	=> $faker->numberBetween($min = 18, $max = 60),
                    ]);
                }
		
		$advert->pivotPrice()->attach($advert->id, [
			'type' 		=> 'rabota-i-obrazovanie',
			'price' 	=> $faker->numberBetween($min = 10000, $max = 1000000)
		]);
		
		$advert->pivotBody()->attach($advert->id, [
			'type' 	=> 'rabota-i-obrazovanie',
			'body' 	=> $faker->realText(500)
		]);
                
		$now = date('Y-m-d');
		$start_date = strtotime($advert->created_at);
		$end_date = strtotime("+10 day", $start_date);
		
		$advert->pivotEnd()->attach($advert->id, [
			'type' 	=> 'rabota-i-obrazovanie',
			'end' 	=> date('Y-m-d H:i:s', $end_date)
		]);
        }
    }
}
