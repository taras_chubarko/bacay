<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;


class BiznesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        
        //$cityes = \City::where('geo_city.country_id', 3159)->get();
        //foreach($cityes as $city)
        //{
        //    $cityarr[$city->city_id] = $city->city_id;
        //}
        $metros = \Metro::where('geo_metro.city_id', 4400)->get();
        foreach($metros as $metro)
        {
            $metroArr[$metro->id] = $metro->id;
        }
        
        
        foreach(range(1, 10) as $index)
        {
                
                $advert = new \Advert;
		$advert->user_id 	= 1;
		$advert->type 		= 'biznes-i-partnerstvo';
		$advert->name 		= $faker->sentence(6);
		$advert->status 	= 1;
		$advert->save();
		
		$advert->pivotCity()->attach($advert->id, [
			'type' 	=> 'biznes-i-partnerstvo',
			'city' 	=> 4400
		]);
                
                $advert->pivotMetro()->attach($advert->id, [
                    'type' 	=> 'biznes-i-partnerstvo',
                    'metro'     => $faker->randomElement($metroArr),
                ]);
		   
		$advert->pivotCategory()->attach($advert->id, [
			'type' 		=> 'biznes-i-partnerstvo',
			'category' 	=> 4,
                        'sort'          => 0,
		]);
		
                $uslugi = $faker->randomElement([109, 110, 111]);
                
		$advert->pivotCategory()->attach($advert->id, [
			'type' 		=> 'biznes-i-partnerstvo',
			'category' 	=> $uslugi,
                        'sort'          => 1,
		]);
                
                if($uslugi == 110)
                {
                    $advert->pivotCategory()->attach($advert->id, [
                            'type' 	=> 'biznes-i-partnerstvo',
                            'category' 	=> $faker->numberBetween($min = 112, $max = 134),
                            'sort'      => 2,
                    ]);
                }
                
		$advert->pivotPrice()->attach($advert->id, [
			'type' 		=> 'biznes-i-partnerstvo',
			'price' 	=> $faker->numberBetween($min = 10000, $max = 1000000)
		]);
		
		$advert->pivotBody()->attach($advert->id, [
			'type' 	=> 'uslugi-i-deyatelnost',
			'body' 	=> $faker->realText(500)
		]);
                
                foreach(range(1, 1) as $i)
                {
                    $im = $faker->image($dir = public_path('uploads/').'biznes-i-partnerstvo', $width = 800, $height = 600);
                    $imex = explode(public_path('uploads/').'biznes-i-partnerstvo/', $im);
                    
                    $img = new \Img;
                    $img->user_id 	= 1;
                    $img->filename 	= $imex[1];
                    $img->filesize  	= 0;
                    $img->uri 	        = 'biznes-i-partnerstvo';
                    $img->status 	= 0;
                    $img->save();
                    //$data[] = $img;
                    
                    $advert->pivotImages()->attach($advert->id, [
                        'type' 	=> 'biznes-i-partnerstvo',
                        'fid'   => $img->id,
                        'sort'  => $i,
                    ]);
                    \Img::setStatus($img->id);
                }
		
		$now = date('Y-m-d');
		$start_date = strtotime($advert->created_at);
		$end_date = strtotime("+10 day", $start_date);
		
		$advert->pivotEnd()->attach($advert->id, [
			'type' 	=> 'biznes-i-partnerstvo',
			'end' 	=> date('Y-m-d H:i:s', $end_date)
		]);
        }
    }
}
