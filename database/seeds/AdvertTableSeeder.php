<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class AdvertTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        
        $cityes = \City::where('geo_city.country_id', 3159)->get();
        foreach($cityes as $city)
        {
            $cityarr[$city->city_id] = $city->city_id;
        }
        
        
        foreach(range(1, 10) as $index)
        {
                
                $advert = new \Advert;
		$advert->user_id 	= 1;
		$advert->type 		= 'nedvizhimost';
		$advert->name 		= $faker->sentence(6);
		$advert->status 	= 1;
		$advert->save();
		
		$advert->pivotCity()->attach($advert->id, [
			'type' 	=> 'nedvizhimost',
			'city' 	=> $faker->randomElement($cityarr)
		]);
		   
		$advert->pivotCategory()->attach($advert->id, [
			'type' 		=> 'nedvizhimost',
			'category' 	=> 1,
                        'sort'          => 0,
		]);
		
		$advert->pivotCategory()->attach($advert->id, [
			'type' 		=> 'nedvizhimost',
			'category' 	=> $faker->numberBetween($min = 16, $max = 24),
                        'sort'          => 1,
		]);
		
		$advert->pivotTypadvert()->attach($advert->id, [
			'type' 		=> 'nedvizhimost',
			'typadvert' 	=> $faker->numberBetween($min = 25, $max = 28)
		]);
		
		$advert->pivotTyphouse()->attach($advert->id, [
			'type' 		=> 'nedvizhimost',
			'typhouse' 	=> $faker->numberBetween($min = 29, $max = 31)
		]);
		
		$advert->pivotQtyroom()->attach($advert->id, [
			'type' 		=> 'nedvizhimost',
			'qtyroom' 	=> $faker->numberBetween($min = 1, $max = 10)
		]);
		
		$advert->pivotArea()->attach($advert->id, [
			'type' 	=> 'nedvizhimost',
			'area' 	=> $faker->numberBetween($min = 30, $max = 200)
		]);
		
		$advert->pivotFloor()->attach($advert->id, [
			'type' 		=> 'nedvizhimost',
			'floor' 	=> $faker->numberBetween($min = 1, $max = 20)
		]);
		
		$advert->pivotQtyfloor()->attach($advert->id, [
			'type' 		=> 'nedvizhimost',
			'qtyfloor' 	=> $faker->numberBetween($min = 1, $max = 20)
		]);
		
		$advert->pivotPrice()->attach($advert->id, [
			'type' 		=> 'nedvizhimost',
			'price' 	=> $faker->numberBetween($min = 10000, $max = 1000000)
		]);
		
		$advert->pivotBody()->attach($advert->id, [
			'type' 	=> 'nedvizhimost',
			'body' 	=> $faker->realText(500)
		]);
                
                foreach(range(1, 10) as $i)
                {
                    $im = $faker->image($dir = public_path('uploads/').'nedvizhimost', $width = 1024, $height = 768);
                    $imex = explode(public_path('uploads/').'nedvizhimost/', $im);
                    
                    $img = new \Img;
                    $img->user_id 	= 1;
                    $img->filename 	= $imex[1];
                    $img->filesize  	= 0;
                    $img->uri 	        = 'nedvizhimost';
                    $img->status 	= 0;
                    $img->save();
                    //$data[] = $img;
                    
                    $advert->pivotImages()->attach($advert->id, [
                        'type' 	=> 'nedvizhimost',
                        'fid' => $img->id,
                        'sort' => $i,
                    ]);
                    \Img::setStatus($img->id);
                }
		
		$now = date('Y-m-d');
		$start_date = strtotime($advert->created_at);
		$end_date = strtotime("+10 day", $start_date);
		
		$advert->pivotEnd()->attach($advert->id, [
			'type' 	=> 'nedvizhimost',
			'end' 	=> date('Y-m-d H:i:s', $end_date)
		]);
        }
    }
}
