<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;


class KrasotaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('ru_RU');
        
        //$cityes = \City::where('geo_city.country_id', 3159)->get();
        //foreach($cityes as $city)
        //{
        //    $cityarr[$city->city_id] = $city->city_id;
        //}
        $metros = \Metro::where('geo_metro.city_id', 4400)->get();
        foreach($metros as $metro)
        {
            $metroArr[$metro->id] = $metro->id;
        }
        
        
        foreach(range(1, 10) as $index)
        {
                
                $advert = new \Advert;
		$advert->user_id 	= 1;
		$advert->type 		= 'krasota-i-zdorove';
		$advert->name 		= $faker->sentence(6);
		$advert->status 	= 1;
		$advert->save();
		
		$advert->pivotCity()->attach($advert->id, [
			'type' 	=> 'krasota-i-zdorove',
			'city' 	=> 4400
		]);
                
                $advert->pivotMetro()->attach($advert->id, [
                    'type' 	=> 'zhivotnye-i-rasteniya',
                    'metro'     => $faker->randomElement($metroArr),
                ]);
		   
		$advert->pivotCategory()->attach($advert->id, [
			'type' 		=> 'krasota-i-zdorove',
			'category' 	=> 10,
                        'sort'          => 0,
		]);
		
                $el = $faker->randomElement([255,256,257,258,259,260]);
                
		$advert->pivotCategory()->attach($advert->id, [
			'type' 		=> 'krasota-i-zdorove',
			'category' 	=> $el,
                        'sort'          => 1,
		]);
                
		$advert->pivotPrice()->attach($advert->id, [
			'type' 		=> 'krasota-i-zdorove',
			'price' 	=> $faker->numberBetween($min = 10000, $max = 1000000)
		]);
		
		$advert->pivotBody()->attach($advert->id, [
			'type' 	=> 'krasota-i-zdorove',
			'body' 	=> $faker->realText(500)
		]);
                
                foreach(range(1, 1) as $i)
                {
                    $im = $faker->image($dir = public_path('uploads/').'krasota-i-zdorove', $width = 800, $height = 600);
                    $imex = explode(public_path('uploads/').'krasota-i-zdorove/', $im);
                    
                    $img = new \Img;
                    $img->user_id 	= 1;
                    $img->filename 	= $imex[1];
                    $img->filesize  	= 0;
                    $img->uri 	        = 'krasota-i-zdorove';
                    $img->status 	= 0;
                    $img->save();
                    //$data[] = $img;
                    
                    $advert->pivotImages()->attach($advert->id, [
                        'type' 	=> 'krasota-i-zdorove',
                        'fid' => $img->id,
                        'sort' => $i,
                    ]);
                    \Img::setStatus($img->id);
                }
		
		$now = date('Y-m-d');
		$start_date = strtotime($advert->created_at);
		$end_date = strtotime("+10 day", $start_date);
		
		$advert->pivotEnd()->attach($advert->id, [
			'type' 	=> 'krasota-i-zdorove',
			'end' 	=> date('Y-m-d H:i:s', $end_date)
		]);
        }
    }
}
