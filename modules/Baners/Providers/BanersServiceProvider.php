<?php namespace Modules\Baners\Providers;

use Illuminate\Support\ServiceProvider;

class BanersServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 * 
	 * @return void
	 */
	public function boot()
	{
		$this->registerTranslations();
		$this->registerConfig();
		$this->registerViews();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{		
		//
	}

	/**
	 * Register config.
	 * 
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
		    __DIR__.'/../Config/config.php' => config_path('baners.php'),
		]);
		$this->mergeConfigFrom(
		    __DIR__.'/../Config/config.php', 'baners'
		);
	}

	/**
	 * Register views.
	 * 
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = null; // base_path('resources/views/modules/baners');

		$sourcePath = __DIR__.'/../Resources/views';

		//$this->publishes([
		//	$sourcePath => $viewPath
		//]);

		$this->loadViewsFrom(array_merge(array_map(function ($path) {
			return $path . '/modules/baners';
		}, \Config::get('view.paths')), [$sourcePath]), 'baners');
	}

	/**
	 * Register translations.
	 * 
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = null; // base_path('resources/lang/modules/baners');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'baners');
		} else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'baners');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
