<div id="p2">
    @if($baner)
        <a class="baner-click" data-id="{{ $baner->id }}" href="{{ $baner->link }}"><img src="{{ url('/') }}/uploads/baners/{{ $baner->baner->filename }}" class="img-responsive" alt="{{ $baner->name }}"></a>
    @endif
</div>