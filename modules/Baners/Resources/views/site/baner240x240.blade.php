@if($baner)
<div class="aside-bl-repeat mobile-hidden">
    <div class="aside-banner">
        <a class="baner-click" data-id="{{ $baner->id }}" href="{{ $baner->link }}"><img width="240" height="240" src="{{ url('/') }}/uploads/baners/{{ $baner->baner->filename }}" alt="{{ $baner->name }}"></a>
    </div>
    <span class="link-more-aside">
        <span class="aside-ad-icon"></span>
        <a href="/reklama-na-sayte">Реклама на сайте</a>
    </span>
</div>
@endif