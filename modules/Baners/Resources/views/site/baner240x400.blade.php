@if($baner)
<div class="aside-bl-repeat mobile-hidden">
    <div class="aside-banner aside-banner-border">
        <a class="baner-click" data-id="{{ $baner->id }}" href="{{ $baner->link }}"><img width="240" height="400" src="{{ url('/') }}/uploads/baners/{{ $baner->baner->filename }}" alt="{{ $baner->name }}"></a>
    </div>
</div>
@endif