@if($baner)
<div class="long-banner">
    <a class="baner-click" data-id="{{ $baner->id }}" href="{{ $baner->link }}"><img width="660" height="90" src="{{ url('/') }}/uploads/baners/{{ $baner->baner->filename }}"  alt="{{ $baner->name }}"></a>
</div>
@endif