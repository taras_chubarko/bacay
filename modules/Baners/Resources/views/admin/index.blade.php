@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Банеры') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Банеры</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('baners.create'))
                <a class="btn btn-success btn-flat" href="{{ route('admin.baners.create') }}">Создать</a>
            @endif
        </div>
        
        <div class="box-body">
            @if($baners->count())
            <table class="table table-bordered b-c-222D32">
                <thead>
                    <tr>
                        <th width="10">#</th>
                        <th>Название</th>
                        <th>Категории</th>
                        <th>Регионы</th>
                        <th width="70">Клики</th>
                        <th width="100">Размер</th>
                        <th width="100">Позиция</th>
                        <th width="100">Дата</th>
                        <th width="100">Статус</th>
                        <th width="100">Действие</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($baners as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ str_limit($item->categoryValue, 50, '...') }}</td>
                        <td>{{ str_limit($item->regions->names, 50, '...') }}</td>
                        <td class="text-center">
                            <button type="button" class="btn btn-link" data-container="body" title="Статистика по кликам" data-toggle="popover-hover" data-placement="top">
                                {{ $item->clicks->all }} / {{ $item->clicks->unique }}
                            </button>
                            <div class="popover-content hidden">
                                <p>Всего кликов: {{ $item->clicks->all }}</p>
                                <p>Уникальних кликов: {{ $item->clicks->unique }}</p>
                            </div>
                        </td>
                        <td>{{ $item->size->name }}</td>
                        <td>{{ $item->position->name }}</td>
                        <td>{{ $item->date }}</td>
                        <td>{{ $item->statuss }}</td>
                        <td>
                            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('baners.edit'))
                                <a href="{{ route('admin.baners.edit', $item->id) }}" class="btn btn-sm bg-navy btn-flat"><i class="fa fa-fw fa-edit"></i></a>
                            @endif
                            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('baners.delete'))
                                @include('cms::admin.delete', ['route' => 'admin.baners.destroy', 'id' => $item->id])
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $baners->render() !!}
            @else
                <p>Нет банеров.</p>
            @endif
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop