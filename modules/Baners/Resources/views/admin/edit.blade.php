@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Редактировать банер') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Редактировать банер</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            
        </div>
        
        <div class="box-body">
            {!! Form::open(array('route' => ['admin.baners.update', $baner->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'form')) !!}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Название *</label>
                    {!! Form::text('name', $baner->name, array('class' => 'form-control')) !!}
                </div>
                   
                <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                    <label for="category">Раздел</label>
                    {!! Form::select('category[]', TaxonomyTerm::itemsArray2(1), $baner->categoryDef, array('class' => 'form-control selectpicker', 'data-live-search' => true, 'data-actions-box' => true, 'title' => '- Выбрать -', 'multiple' => 'multiple')) !!}
                </div>
                    
                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                    <label for="city">Регион *</label>
                    {!! Form::select('region_id[]', Region::getArray(), $baner->regions->default, array('class' => 'form-control selectpicker', 'multiple' => 'multiple', 'data-live-search' => true, 'data-actions-box' => true, 'title' => '- Выбрать -')) !!}
                </div>
                    
                <div class="form-group{{ $errors->has('size') ? ' has-error' : '' }}">
                    <label for="size">Рзмер *</label>
                   {!! Form::select('size', TaxonomyTerm::get(9, 0), $baner->size->id, array('class' => 'form-control selectpicker', 'title' => '- Выбрать -')) !!}
                </div>
                
                <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                    <label for="position">Позиция *</label>
                    {!! Form::select('position', TaxonomyTerm::get(8, 0), $baner->position->id, array('class' => 'form-control selectpicker',  'title' => '- Выбрать -')) !!}
                </div>
                    
                <div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
                    <label for="link">Ссылка *</label>
                    {!! Form::text('link', $baner->link, array('class' => 'form-control')) !!}
                </div>
                <div class="form-group{{ $errors->has('baners') ? ' has-error' : '' }}">
                    <label for="baners">Банер *</label>
                    <div id="baners">
                        @include('baners::admin.image-field', ['images' => $baner->images, 'uri' => 'baners'])
                    </div>
                </div>
                    
                <div class="form-group">
                    {!! Form::hidden('all_pages', 0) !!}
                    <label for="all_pages" class="icheck">
                        {!! Form::checkbox('all_pages', 1, $baner->all_pages) !!}
                        Показывать на всех страницах (только для Баннеров-фонов).
                    </label>
                </div>
                
                    
                <div class="form-group">
                    {!! Form::hidden('status', 0) !!}
                    <label for="status" class="icheck">
                        {!! Form::checkbox('status', 1, 1) !!}
                        Опубликовать
                    </label>
                </div>
                    
                <div class="form-group">
                   {!! Form::submit('Сохранить', array('class' => 'btn btn-primary btn-flat')) !!}
                </div>
            {!! Form::close() !!}
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>

@stop