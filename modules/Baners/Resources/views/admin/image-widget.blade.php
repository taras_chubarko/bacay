@if(count($images))
    @foreach($images as $key => $image)
    <li class="drag" data-id="{{ $image->id }}">
        <a href="javascript:" title="Удалить" data-image-id="{{ $image->id }}" class="delim"><i class="fa fa-trash"></i></a>
        <img src="{{ Img::fit($image, 150, 150) }}">
        {!! Form::hidden('image['.$key.'][fid]', $image->id, ['class' => 'fid']) !!}
    </li>
    @endforeach
@endif