<?php namespace Modules\Baners\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Baners extends Model {

    protected $table = 'baners';

    protected $fillable = [];
    
    protected $guarded = ['_token'];
    
    /*
     * Раздел 
     */
    public function pivotCategory()
    {
       return $this->belongsToMany('Baners', 'baners_category')
       ->withPivot('category', 'sort')
       ->leftJoin('taxonomy_term', 'baners_category.category', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     * Размер 
     */
    public function pivotSize()
    {
       return $this->belongsToMany('Baners', 'baners_size')
       ->withPivot('size')
       ->leftJoin('taxonomy_term', 'baners_size.size', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     * Позиция 
     */
    public function pivotPosition()
    {
       return $this->belongsToMany('Baners', 'baners_position')
       ->withPivot('position')
       ->leftJoin('taxonomy_term', 'baners_position.position', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     * Город
     */
    public function pivotLocation()
    {
        return $this->belongsToMany('Baners', 'baners_location')
        ->withPivot('country_id', 'region_id', 'city_id')
        ->leftJoin('geo_region', 'baners_location.region_id', '=', 'geo_region.region_id')
        ->select(
            "geo_region.name_ru as pivot_region"
        );
    }
    /*
     * Клики банеров
     */
    public function pivotClicks()
    {
        return $this->belongsToMany('Baners', 'baners_clicks')
        ->withPivot('ip', 'created_at')->withTimestamps();
    }
     /* public function getStatussAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function getStatussAttribute() {
        
        switch($this->status)
        {
            case 0:
                return 'Не опубликовано';
            break;
        
            case 1:
                return 'Опубликовано';
            break;
        }
        
    }
    /* public function getDateAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function getDateAttribute() {
        return date('d.m.Y', strtotime($this->updated_at));
    }
    /* public function name
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function scopeDeleteOne($query, $id)
    {
        $baner = $query->where('baners.id', $id)->first();
        
        \Img::del($baner->fid, []);
        
        foreach(config('baners.tables') as $table)
        {
            \DB::table($table)->where('baners_id', $id)->delete();
        }
        \DB::table('baners')->where('id', $id)->delete();
    }
    /* public function getSizeAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getSizeAttribute()
    {
        $data = array();
        $data['id'] = ($this->pivotSize->count()) ? $this->pivotSize[0]->pivot->id : null;
        $data['name'] = ($this->pivotSize->count()) ? $this->pivotSize[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getPositionAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getPositionAttribute()
    {
        $data = array();
        $data['id'] = ($this->pivotPosition->count()) ? $this->pivotPosition[0]->pivot->id : null;
        $data['name'] = ($this->pivotPosition->count()) ? $this->pivotPosition[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getCategoryAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCategoryAttribute()
    {
        $data = array();
        if($this->pivotCategory->count())
        {
            foreach($this->pivotCategory as $category)
            {
                $data[$category->pivot->sort] = (object) array(
                    'id'    => $category->pivot->id,
                    'name'  => $category->pivot->name,
                );
            }
        }
        return $data;
    }
    /* public function getCategoryValueAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCategoryValueAttribute()
    {
        $items = array();
        if($this->pivotCategory->count())
        {
            foreach($this->pivotCategory as $category)
            {
                $items[] = $category->pivot->name;
            }
            
        }
        return implode(', ', $items);
    }
    /* public function getCategoryDefAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCategoryDefAttribute()
    {
        $items = array();
        if($this->pivotCategory->count())
        {
            foreach($this->pivotCategory as $category)
            {
                $items[] = $category->pivot->id;
            }
            
        }
        return $items;
    }
    /* public function getCityAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getLocationAttribute()
    {
        $data = array();
        $data['country_id'] = ($this->pivotLocation->count()) ? $this->pivotLocation[0]->pivot->country_id : null;
        $data['region_id']  = ($this->pivotLocation->count()) ? $this->pivotLocation[0]->pivot->region_id : null;
        $data['city_id']    = ($this->pivotLocation->count()) ? $this->pivotLocation[0]->pivot->city_id : null;
        return (object) $data;
    }
    /* public function getRegionsAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getRegionsAttribute()
    {
        $data = array();
        $regions = array();
        $regionsN = array();
        if($this->pivotLocation->count())
        {
            foreach($this->pivotLocation as $location)
            {
                $regions[$location->pivot->region_id] = $location->pivot->region_id;
                $regionsN[$location->pivot->region_id] = $location->pivot->region;
            }
        }
        $data['default'] = $regions;
        $data['names'] = implode(', ', $regionsN);
        
        return (object) $data;
    }
    /* public function name
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getClicksAttribute()
    {
        $data = array();
        $cliksIP = array();
        
        if($this->pivotClicks->count())
        {
            foreach($this->pivotClicks as $click)
            {
                $cliksIP[] = $click->pivot->ip;
            }
        }
        
        $cliksIP = array_unique($cliksIP);
        
        $data['all'] = ($this->pivotClicks->count()) ? $this->pivotClicks->count() : 0;
        $data['unique'] = count($cliksIP);
        return (object) $data;
    }
    /* public function getImagesAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function getImagesAttribute()
    {
        $data = array();
        $data[] = \Img::findOrFail($this->fid);
        return $data;
    }
    /* public function baner
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function baner()
    {
        return $this->hasOne('Img', 'id', 'fid');
    }
    /* public function view
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function view($id)
    {
        $app = app();
        $baners = \Baners::getBaners();
        switch($id)
        {
            case 'p1':
                return $app->make('Modules\Baners\Http\Controllers\BanersController')->p1($baners);
            break;
                
            case 'p2':
                return $app->make('Modules\Baners\Http\Controllers\BanersController')->p2($baners);
            break;
                
            case '240x240':
                return $app->make('Modules\Baners\Http\Controllers\BanersController')->baner240x240($baners);
            break;
                
            case '240x400':
                return $app->make('Modules\Baners\Http\Controllers\BanersController')->baner240x400($baners);
            break;
                
            case 'top660x90':
                return $app->make('Modules\Baners\Http\Controllers\BanersController')->banerTop660x90($baners);
            break;
                
            case 'bottom660x90':
                return $app->make('Modules\Baners\Http\Controllers\BanersController')->banerBottom660x90($baners);
            break;
            
        }
    }
    /* public function scopeGetBaners
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function scopeGetBaners($query)
    {
        $segments = \Request::segments();
       // dd($segments);
        $query->where('baners.status', 1);
        
        
        if(\Session::has('location') && session('location.city.region_id') != 0)
        {
            $query->leftJoin('baners_location', 'baners.id', '=', 'baners_location.baners_id');
            $query->whereIn('baners_location.region_id', [session('location.city.region_id')]);
        }
        
        if(!empty($segments) && $segments[0] == 'adverts' && $segments[1] != 'add')
        {
            $cat = \TaxonomyTerm::findBySlug($segments[1]);
            $query->leftJoin('baners_category', 'baners.id', '=', 'baners_category.baners_id');
            $query->whereIn('baners_category.category', [$cat->id]);
        }
        
        
        $baners = $query->get();
        $baners->load('pivotSize', 'pivotPosition', 'pivotCategory', 'pivotLocation', 'baner');
        return $baners;
    }
}