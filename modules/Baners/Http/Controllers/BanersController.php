<?php namespace Modules\Baners\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class BanersController extends Controller {
	
	/* public function p1
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function p1($baners)
	{
		$filter = $baners->where('pivotPosition.0.pivot.name', 'Лево')->where('pivotSize.0.pivot.name', '220x800');
		
		$baner = $filter->first();
		
		//dd($baner);
		return view('baners::site.p1', compact('baner'));
	}
	/* public function p2
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function p2($baners)
	{
		$filter = $baners->where('pivotPosition.0.pivot.name', 'Право')->where('pivotSize.0.pivot.name', '220x800');
		
		$baner = $filter->first();
		return view('baners::site.p2', compact('baner'));
	}
	/* public function baner240x240
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function baner240x240($baners)
	{
		$filter = $baners->where('pivotPosition.0.pivot.name', 'Право')->where('pivotSize.0.pivot.name', '240x240');
		
		$baner = $filter->first();
		return view('baners::site.baner240x240', compact('baner'));
	}
	/* public function baner240x400
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function baner240x400($baners)
	{
		$filter = $baners->where('pivotPosition.0.pivot.name', 'Право')->where('pivotSize.0.pivot.name', '240x400');
		
		$baner = $filter->first();
		return view('baners::site.baner240x400', compact('baner'));
	}
	/* public function banerTop660x90
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function banerTop660x90($baners)
	{
		$filter = $baners->where('pivotPosition.0.pivot.name', 'Верх')->where('pivotSize.0.pivot.name', '660x90');
		
		$baner = $filter->first();
		return view('baners::site.banerTop660x90', compact('baner'));
	}
	/* public function banerBottom660x90
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function banerBottom660x90($baners)
	{
		$filter = $baners->where('pivotPosition.0.pivot.name', 'Низ')->where('pivotSize.0.pivot.name', '660x90');
		
		$baner = $filter->first();
		return view('baners::site.banerBottom660x90', compact('baner'));
	}
	/* public function click
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function click(Request $request)
	{
		$baner = \Baners::findOrFail($request->id);
		$baner->pivotClicks()->attach($baner->id, [
			'ip' => $request->ip(),
		]);
	}
	
}