<?php namespace Modules\Baners\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class AdminBanersController extends Controller {
	
	public function index()
	{
		$baners = \Baners::orderBy('updated_at', 'DESC')->paginate(20);
		return view('baners::admin.index', compact('baners'));
	}
	/* public function create
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function create()
	{
		return view('baners::admin.create');
	}
	/* public function store
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function store(\Modules\Baners\Http\Requests\BanersReq $request)
	{
		$user = \Sentinel::getUser();
		
		$baners = new \Baners;
		$baners->user_id 	= $user->id;
		$baners->name 		= $request->name;
		$baners->fid 		= $request->image[0]['fid'];
		$baners->link 		= $request->link;
		$baners->all_pages 	= $request->all_pages;
		$baners->status 	= $request->status;
		$baners->save();
		
		if($request->category)
		{
			foreach($request->category as $key => $category)
			{
			    $baners->pivotCategory()->attach($baners->id, [
				'category'  => $category,
				'sort'      => $key
			    ]);
			}
		}
		
		$baners->pivotSize()->attach($baners->id, [
			'size' 	=> $request->size
		]);
		
		$baners->pivotPosition()->attach($baners->id, [
			'position' => $request->position
		]);
		
		if($request->region_id)
		{
			foreach($request->region_id as $region)
			{
				$baners->pivotLocation()->attach($baners->id, [
					'region_id' => $region,
				]);
			}
		}
		
	    	return redirect()->route('admin.baners.index')->with('success', 'Банер создан.');
	}
	/* public function edit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function edit($id)
	{
		$baner = \Baners::findOrFail($id);
		return view('baners::admin.edit', compact('baner'));
	}
	/* public function update
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function update(\Modules\Baners\Http\Requests\BanersReq $request, $id)
	{
		$baners = \Baners::findOrFail($id);
		$baners->name 		= $request->name;
		$baners->fid 		= $request->image[0]['fid'];
		$baners->link 		= $request->link;
		$baners->all_pages 	= $request->all_pages;
		$baners->status 	= $request->status;
		$baners->updated_at 	= date('Y-m-d H:i:s');
		$baners->save();
		
		$baners->pivotCategory()->detach($baners->id);
		if($request->category)
		{
			foreach($request->category as $key => $category)
			{
			    $baners->pivotCategory()->attach($baners->id, [
				'category'  => $category,
				'sort'      => $key
			    ]);
			}
		}
		
		$baners->pivotSize()->detach($baners->id);
		$baners->pivotSize()->attach($baners->id, [
			'size' 	=> $request->size
		]);
		
		$baners->pivotPosition()->detach($baners->id);
		$baners->pivotPosition()->attach($baners->id, [
			'position' => $request->position
		]);
		
		$baners->pivotLocation()->detach($baners->id);
		if($request->region_id)
		{
			foreach($request->region_id as $region)
			{
				$baners->pivotLocation()->attach($baners->id, [
					'region_id' => $region,
				]);
			}
		}
	    	return redirect()->route('admin.baners.index')->with('success', 'Банер обновлен.');
	}
	/* public function destroy
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function destroy($id)
	{
		\Baners::deleteOne($id);
		return redirect()->route('admin.baners.index')->with('success', 'Банер удален.');
	}
	
}