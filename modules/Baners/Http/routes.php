<?php

Route::group(['middleware' => 'web', 'prefix' => 'baners', 'namespace' => 'Modules\Baners\Http\Controllers'], function()
{
	Route::get('/', 'BanersController@index');
	Route::post('click', 'BanersController@click');
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\Baners\Http\Controllers'], function()
{
	Route::resource('baners', 'AdminBanersController');
});