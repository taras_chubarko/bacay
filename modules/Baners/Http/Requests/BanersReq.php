<?php namespace Modules\Baners\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BanersReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'  	=> 'required',
			'link'  	=> 'required',
			//'category' 	=> 'required',
			'size' 		=> 'required',
			'position' 	=> 'required',
			'position' 	=> 'required',
			'images' 	=> 'required',
			'region_id' 	=> 'required',
		];
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		$validator->setAttributeNames([
			'name'  	=> '"Название"',
			'link'  	=> '"Ссылка"',
			'category'  	=> '"Раздел"',
			'size'  	=> '"Рзмер"',
			'position'  	=> '"Позиция"',
			'images'  	=> '"Банер"',
			'region_id'  	=> '"Регион"',
		]);
	 
		return $validator;
	}


}
