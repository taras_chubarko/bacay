<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBanersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baners', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name');
            $table->integer('fid');
            $table->string('link');
            $table->integer('status');
            $table->timestamps();
        });
        
        Schema::create('baners_category', function(Blueprint $table)
        {
            $table->integer('baners_id')->unsigned()->nullable();
            $table->foreign('baners_id')->references('id')->on('baners')->onDelete('cascade');
            $table->integer('category');
        });
        
        Schema::create('baners_position', function(Blueprint $table)
        {
            $table->integer('baners_id')->unsigned()->nullable();
            $table->foreign('baners_id')->references('id')->on('baners')->onDelete('cascade');
            $table->integer('position');
        });
        
        Schema::create('baners_size', function(Blueprint $table)
        {
            $table->integer('baners_id')->unsigned()->nullable();
            $table->foreign('baners_id')->references('id')->on('baners')->onDelete('cascade');
            $table->integer('size');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('baners');
    }

}
