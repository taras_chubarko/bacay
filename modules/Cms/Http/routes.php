<?php

Route::group(['middleware' => ['web']], function () {
  
   Route::get('/', function () {
	 $meta = Meta::getMeta('any', 0);
	 MetaTag::set('title',  $meta->title);
	 MetaTag::set('robots', $meta->robots);
	 MetaTag::set('description', $meta->description);
	 MetaTag::set('keywords', $meta->keywords);
	 return view('cms::site.index');
   });
   
    
});

Route::group(['middleware' => 'web', 'prefix' => 'cms', 'namespace' => 'Modules\Cms\Http\Controllers'], function()
{
	 Route::get('/', 'CmsController@index');
	 Route::get('/clear/cache',
	    array('as' => 'clear.cache',
	    'uses' => 'CmsController@clear_cache'
	 ));
	 
	 Route::get('/test',
	    array('as' => 'test',
	    'uses' => 'CmsController@test'
	 ));
	 
	 Route::get('/test/{key}',
	    array('as' => 'test.key',
	    'uses' => 'CmsController@testKey'
	 ));
	 
	 Route::post('/set/session/city',
	    array('as' => 'set.session.city',
	    'uses' => 'CmsController@setSessionCity'
	 ));
	 
	 Route::get('/run/cron',
	    array('as' => 'run.cron',
	    'uses' => 'CmsController@runCron'
	 ));
	 
	 Route::post('/fedback',
	    array('as' => 'fedback',
	    'uses' => 'CmsController@fedback_submit'
	 ));
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\Cms\Http\Controllers'], function()
{
   Route::get('/', 'AdminController@index');
   
   Route::group(['prefix' => 'cms',], function () {
      
      Route::get('mat', 'AdminController@mat');
      Route::post('mat', [
	 'as' 	=> 'mat',
	 'uses' 	=> 'AdminController@matPost'
      ]);
      
      Route::get('advert', 'AdminController@advert');
      Route::post('advert', [
	 'as' 	=> 'admin.cms.advert',
	 'uses' 	=> 'AdminController@advertPost'
      ]);
      
      Route::get('settings', [
	 'as' 	=> 'admin.cms.settings',
	 'uses' => 'AdminController@settings'
      ]);
      
      Route::post('settings/adminmail', [
	 'as' 	=> 'admin.cms.adminmail',
	 'uses' => 'AdminController@adminmail_submit'
      ]);

   });
   
});


