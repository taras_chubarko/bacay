<?php namespace Modules\Cms\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FedbackReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'  => 'required',
                        'email' => 'required|email',
			'msg' 	=> 'required',
		];
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		$validator->setAttributeNames([
			'name'  => '"Ваше имя"',
			'email' => '"Ваш e-mail"',
                        'msg'   => '"Текст сообщения"',
		]);
	 
		return $validator;
	}

}
