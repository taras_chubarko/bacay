<?php namespace Modules\Cms\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;
use Faker\Factory as Faker;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class CmsController extends Controller {
	
	public function index()
	{
		return view('cms::index');
	}
	/* public function clear_cache
	* @param $id
	*-----------------------------------
	*|
	*-----------------------------------
	*/
	public function clear_cache() {
	    
	    \Cache::flush();
	    return redirect()->back()->with('success', 'Кеш очищен.');
	}
	/* public function runCron
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function runCron()
	{
		\Artisan::call('schedule:run');
		return redirect()->back()->with('success', 'Крон запущен. Внимание! крон запускает событие 1 раз, для повторного запуска события, надо опять запускать крон.');
	}
	/*
	 * function fedback_form
	 * @param $arg
	 */
	
	function fedback_form()
	{
	    return view('cms::site.form-fedback');
	}
	/*
	 * function fedback_submit
	 * @param $arg
	 */
	
	function fedback_submit(\Modules\Cms\Http\Requests\FedbackReq $request)
	{
		$data = $request->all();
	    
		\Mail::later(5, 'cms::site.mail-fedback', $data, function($message)
		{
			$message->to(\Option::get('admin.mail'))->subject('Обратная связь на сайте "'.config('cms.name').'"');
		});
		
		return redirect()->back()->with('success', 'Спасибо за сообщение! Мы обязательно вам ответим.');
	}
	/* public function test
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function test()
	{
//		$csvFile = public_path('uploads').'/freqrnc2011.csv';
//
//                $areas = $this->csv_to_array($csvFile, '	');
//		
//		foreach($areas as $item)
//		{
//			$items[] = "'".$item['Lemma']."'";
//		}
//		shuffle($items);
//		$items = array_slice($items, 10, 800, true);
//		
//		$data = implode(', ', $items);
//		
//		print '<pre>' . htmlspecialchars(print_r($data, true)) . '</pre>';
	//$faker = Faker::create('ru_RU');
	
	//print '<pre>' . htmlspecialchars(print_r(ucfirst(strtolower($faker->paragraph(3))), true)) . '</pre>';
		//$response = json_decode(file_get_contents('https://auto.ria.com/api/categories/8/marks/_active?langId=2&categoryId=1'));
		//
		//foreach($response as $item)
		//{
		//	\DB::table('transport_brand')->insert(
		//	[
		//	 'cid' 		=> $item->value,
		//	 'type_id' 	=> 8,
		//	 'name' 	=> $item->name,
		//	]
		//      );
		//}
		//
//		$string = [
//		'IT, Интернет телеком',
//		'Бытовые услуги',
//		'Деловые услуги',
//		'Искусство',
//		'Красота,здоровье',
//		'Курьерские поручения',
//		'Мастер на час',
//		'Няни ,Сиделки',
//		'Оборудование,производство',
//		'Обучение ,курсы',
//		'Охрана ,безопасность',
//		'Питание ,кайтеринг',
//		'Праздники ,мероприятия',
//		'Реклама,полиграфия',
//		'Ремонт и обслуживание техники',
//		'Ремонт,Строительство',
//		'Сад,благоустройство',
//		'Транспорт ,перевозки',
//		'Уборка',
//		'Установка техники',
//		'Уход за животными',
//		'Фото и Видеосъёмка',
//		'Другое',
//			];
//		foreach($string as $key => $item)
//		{
//			$child = \TaxonomyTerm::create([
//				'taxonomy_id' 	=> 1,
//                                'name' 		=> $item,
//				'sort' 		=> $key,
//				'ico' 		=> '',
//                        ]);
//			$child->makeChildOf(109);
//		}
//		
//		print '<pre>' . htmlspecialchars(print_r($string, true)) . '</pre>';
	
	//$items = array(
	//	'Достоевская',
	//	'Ладожская',
	//	'Лиговский проспект',
	//	'Новочеркасская',
	//	'Площадь Александра Невского',
	//	'Проспект Большевиков',
	//	'Спасская',
	//	'Улица Дыбенко',
	//);
	//
	//foreach($items as $item)
	//{
	//	\DB::table('geo_metro')->insert(
	//	[
	//		'name' 		=> $item,
	//		'city_id' 	=> 4962,
	//		'color' 	=> '#EA7125',
	//		'branch'	=> 17,
	//	]
	//	);
	//}
		//\Advert::deleteAll('otdam-darom');
		
		//$items = \TaxonomyTerm::where('taxonomy_id', 1)->get()->toHierarchy();
		//
		//foreach($items as $item)
		//{
		//	$slug[$item->id] = str_slug($item->name, '-');
		//	if($item->children->count())
		//	{
		//		foreach($item->children as $children1)
		//		{
		//			$slug[$children1->id] = str_slug($item->name, '-').'/'. str_slug($children1->name, '-');
		//			if($children1->children->count())
		//			{
		//				foreach($children1->children as $children2)
		//				{
		//					$slug[$children2->id] = str_slug($item->name, '-').'/'. str_slug($children1->name, '-').'/'. str_slug($children2->name, '-');
		//					if($children2->children->count())
		//					{
		//						foreach($children2->children as $children3)
		//						{
		//							$slug[$children3->id] = str_slug($item->name, '-').'/'. str_slug($children1->name, '-').'/'. str_slug($children2->name, '-').'/'. str_slug($children3->name, '-');
		//						}
		//					}
		//				}
		//			}
		//		}
		//	}
		//}
		//
		//foreach($slug as $key => $slu)
		//{
		//	$tax = \TaxonomyTerm::findOrFail($key);
		//	$tax->slug = $slu;
		//	$tax->save();
		//}
		//
		////$menus = \TaxonomyTerm::parseArray($items);
		//
		//dd($slug);
		//\File::put(public_path('uploads/').'order.txt', "order_num :0;Summ :0;Date :0\n");
		
		//$searchResults = [
		//	'item1',
		//	'item2',
		//	'item3',
		//	'item4',
		//	'item5',
		//	'item6',
		//	'item7',
		//	'item8',
		//	'item9',
		//	'item10'
		//];
		//
		//$currentPage = LengthAwarePaginator::resolveCurrentPage();
		//$collection = new Collection($searchResults);
		//
		//$perPage = 5;
		//
		//$currentPageSearchResults = $collection->slice($currentPage * $perPage, $perPage)->all();
		//$paginatedSearchResults= new LengthAwarePaginator($currentPageSearchResults, count($collection), $perPage);
		//
		//print '<pre>' . htmlspecialchars(print_r($paginatedSearchResults, true)) . '</pre>';
	}
	
	/* public function testKey
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function testKey($key)
	{
		//$bransd = \DB::table('transport_brand')->get();
		//foreach($bransd as $brans)
		//{
		//	$items[] = array($brans->id, $brans->cid);
		//}
		//$items = array_chunk($items, 10);
		//
		//foreach($items[$key] as $item)
		//{
		//	$response = json_decode(file_get_contents('https://auto.ria.com/api/marks/'.$item[1].'/models/_active/_with_count?langId=2'));
		//	
		//	foreach($response as $model)
		//	{
		//		\DB::table('transport_model')->insert([
		//			'brand_id' 	=> $item[0],
		//			'mid' 		=> $model->value,
		//			'name' 		=> $model->name,
		//		]);
		//	}
		//	//print '<pre>' . htmlspecialchars(print_r($response, true)) . '</pre>';
		//}
		//return response()->json(['count' => count($items), 'data' => $items[$key] ], 200);
	}
	
	
	
	public function csv_to_array($filename='', $delimiter=',')
	{
	    if(!file_exists($filename) || !is_readable($filename))
		return FALSE;
	 
	    $header = NULL;
	    $data = array();
	    if (($handle = fopen($filename, 'r')) !== FALSE)
	    {
		while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
		{
		    if(!$header)
			$header = $row;
		    else
			$data[] = array_combine($header, $row);
		}
		fclose($handle);
	    }
	    return $data;
	}
	/* public function setSessionCity
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function setSessionCity(Request $request)
	{
		if($request->city == 0)
		{
			session(['location' => [
				'city' => [
					'country_id'   => 0,
					'region_id'    => 0,
					'city_id'      => 0,
					'name_ru'      => 'Вся Россия',
				]
			]]);
		}
		else
		{
			$city = \City::findOrFail($request->city);
			session(['location' => [
				'city' => [
					'country_id'   => $city->country_id,
					'region_id'    => $city->region_id,
					'city_id'      => $city->city_id,
					'name_ru'      => $city->name_ru,
				]
			]]);
		}
		
	}
	
}