<?php namespace Modules\Cms\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller {
	
	/* public function index
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function index()
	{
		$data = [];
		
		$data['advertCount'] 		= \Advert::count();
		$data['advertCountDay'] 	= \Advert::whereRaw('created_at >= CURRENT_DATE()')->orderBy('created_at', 'DESC')->count();
		$data['advertCountWeek'] 	= \Advert::whereRaw('created_at > DATE_SUB(DATE(NOW()), INTERVAL 1 WEEK)')->orderBy('created_at', 'DESC')->count();
		$data['advertCountMonth'] 	= \Advert::whereRaw('created_at > DATE_SUB(DATE(NOW()), INTERVAL 1 MONTH)')->orderBy('created_at', 'DESC')->count();
		
		$data['usersCount'] 		= \User::count();
		$data['usersCountDay'] 		= \User::whereRaw('created_at >= CURRENT_DATE()')->orderBy('created_at', 'DESC')->count();
		$data['usersCountWeek'] 	= \User::whereRaw('created_at > DATE_SUB(DATE(NOW()), INTERVAL 1 WEEK)')->orderBy('created_at', 'DESC')->count();
		$data['usersCountMonth'] 	= \User::whereRaw('created_at > DATE_SUB(DATE(NOW()), INTERVAL 1 MONTH)')->orderBy('created_at', 'DESC')->count();
		
		$data['visitCount'] 		= \Visitor::count();
		$data['visitCountDay'] 		= \DB::table('visitor_registry')->whereRaw('created_at >= CURRENT_DATE()')->orderBy('created_at', 'DESC')->count();
		$data['visitCountWeek'] 	= \DB::table('visitor_registry')->whereRaw('created_at > DATE_SUB(DATE(NOW()), INTERVAL 1 WEEK)')->orderBy('created_at', 'DESC')->count();
		$data['visitCountMonth'] 	= \DB::table('visitor_registry')->whereRaw('created_at > DATE_SUB(DATE(NOW()), INTERVAL 1 MONTH)')->orderBy('created_at', 'DESC')->count();
		
		$data['walletCount'] 		= \Wallet::select('amount')->where('type', '-')->sum('amount');
		$data['walletCountDay'] 	= \Wallet::select('amount')->where('type', '-')->whereRaw('created_at >= CURRENT_DATE()')->orderBy('created_at', 'DESC')->sum('amount');;
		$data['walletCountWeek'] 	= \Wallet::select('amount')->where('type', '-')->whereRaw('created_at > DATE_SUB(DATE(NOW()), INTERVAL 1 WEEK)')->orderBy('created_at', 'DESC')->sum('amount');;
		$data['walletCountMonth'] 	= \Wallet::select('amount')->where('type', '-')->whereRaw('created_at > DATE_SUB(DATE(NOW()), INTERVAL 1 MONTH)')->orderBy('created_at', 'DESC')->sum('amount');;
		
		$data = (object) $data;
		
		return view('admin::layouts.index', compact('data'));
	}
	/* public function mat
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function mat()
	{
	    return view('cms::admin.mat');
	}
	/* public function matPost
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function matPost(Request $request)
	{
	    \Option::set('mat.mat', $request->mat);
	    
	    return redirect()->back()->with('success', 'Обновлено.');
	}
	/* public function advert
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function advert()
	{
        $categories = \TaxonomyTerm::where('taxonomy_id', 1)->where('parent_id', 0)->get();
		
	    return view('cms::admin.advert', compact('categories'));
	}
	/* public function advertPost
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function advertPost(Request $request)
	{
		\Option::set('advert.endday', $request->endday);
		\Option::set('advert.publics', $request->publics);
		\Option::set('advert.complain_mail', $request->complain_mail);
        
        foreach($request->limit as $key => $limit)
        {
            \Option::set('advert.limit.'.$key, $limit);
        }
		
		return redirect()->back()->with('success', 'Обновлено.');
	}
	/* public function settings
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function settings()
	{
		return view('cms::admin.page-settings');
	}
	/* public function adminmail_submit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function adminmail_submit(Request $request)
	{
		\Option::set('admin.mail', $request->email);
		return redirect()->back()->with('success', 'Обновлено.');
	}
	
}