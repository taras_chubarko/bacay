{!! Form::open(array('route' => 'admin.cms.adminmail', 'role' => 'form', 'class' => 'form', 'id' => 'form-admin-mail')) !!}
    <div class="form-group">
        <label for="email">Email администратора</label>
        {!! Form::text('email', Option::get('admin.mail'), array('class' => 'form-control')) !!}
    </div>
    {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
{!! Form::close() !!}