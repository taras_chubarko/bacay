@if(Sentinel::check())
    @if(Sentinel::getUser()->hasAnyAccess(['admin']))
        <a class="btn btn-info m-b-20 edit-btn" href="{{ route($route, $param) }}"><i class="fa fa-pencil-square"></i></a>
    @endif
@endif