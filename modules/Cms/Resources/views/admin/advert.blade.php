@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Настройка объявлений') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Настройка объявлений</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
           
        </div>
        
        <div class="box-body">
            {!! Form::open(array('route' => 'admin.cms.advert', 'role' => 'form', 'class' => 'form')) !!}
                
                <div class="form-group">
                    <label for="endday">Колличество дней публикации</label>
                    {!! Form::text('endday',  Option::get('advert.endday'), array('class' => 'form-control')) !!}
                </div>
                    
                <div class="form-group">
                    <label for="publics">Публикация по умолчанию</label>
                    {!! Form::radios('publics',  [0 => 'Не опубликовано', 1 => 'Опубликовано', 2 => 'На модерации', 3 => 'Отклонено модератором', 4 => 'Удалено из админки'], Option::get('advert.publics')) !!}
                </div>
                    
                <div class="form-group">
                    <label for="complain_mail">Почта для жалоб</label>
                    {!! Form::email('complain_mail',  Option::get('advert.complain_mail'), array('class' => 'form-control')) !!}
                </div>
                <h3>Ограничение по колличетву поданых объявлений, 0 - без ограничений</h3>
                @foreach($categories as $category)
                    <div class="form-group row">
                        <div class="col-lg-3">
                            {{ $category->name }}
                        </div>
                        <div class="col-lg-9">
                            {!! Form::text('limit['.$category->slug.']', Option::get('advert.limit.'.$category->slug), array('class' => 'form-control')) !!}
                        </div>
                    </div>
                @endforeach
                    
                {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
                
            {!! Form::close() !!}
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop