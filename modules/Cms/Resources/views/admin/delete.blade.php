<a
    href="javascript:" 
    data-href="{{ route($route, $id) }}"
    data-title="Хотите удалить?"
    data-message="Это действие необратимо!"
    data-button1="Да"
    data-button2="Нет"
    data-method="DELETE" 
    class="btn btn-sm bg-maroon btn-flat delete">
    <i class="fa fa-fw fa-trash-o"></i>
</a>