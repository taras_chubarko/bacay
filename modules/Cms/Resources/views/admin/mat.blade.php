@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Запрещенные слова') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Запрещенные слова</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
           
        </div>
        
        <div class="box-body">
            {!! Form::open(array('route' => 'mat', 'role' => 'form', 'class' => 'form')) !!}
                
                <div class="form-group">
                    <label for="mat">Запрещенные слова</label>
                    {!! Form::textarea('mat', Option::get('mat.mat'), array('class' => 'form-control', 'rows' => 3)) !!}
                    <p class="help-block">Пишем слова через запятую и без пробелов.</p>
                </div>
                {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
                
            {!! Form::close() !!}
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop