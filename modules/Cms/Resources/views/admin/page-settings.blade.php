@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Настройки сайта') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Настройки сайта</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
           
        </div>
        
        <div class="box-body">
            @include('cms::admin.form-admin-mail')
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop