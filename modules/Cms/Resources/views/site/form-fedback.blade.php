<div id="form-fedback" class="p-t-20">
    <h2>Напишите нам</h2>
    {!! Form::open(array('route' => 'fedback', 'role' => 'form', 'class' => 'form')) !!}
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Ваше имя</label>
            {!! Form::text('name', null, array('class' => 'form-control')) !!}
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">Ваш e-mail</label>
            {!! Form::email('email', null, array('class' => 'form-control')) !!}
        </div>
        <div class="form-group{{ $errors->has('msg') ? ' has-error' : '' }}">
            <label for="msg">Текст сообщения</label>
            {!! Form::textarea('msg', null, array('class' => 'form-control', 'rows' => 3)) !!}
        </div>
        {!! Form::submit('Отправить', array('class' => 'btn btn-primary')) !!}
    {!! Form::close() !!}
</div>