@extends('site::mail.main')

@section('title')
Обратная связь
@stop

@section('content')
    <p><b>Ваше имя:</b> {!! $name !!}</p>
    <p><b>Ваш e-mail:</b> {!! $email !!}</p>
    <p><b>Текст сообщения:</b><br> {!! $msg !!}</p>
@stop