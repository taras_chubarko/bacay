<?php namespace Modules\Wallet\Providers;

use Illuminate\Support\ServiceProvider;

class WalletServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 * 
	 * @return void
	 */
	public function boot()
	{
		$this->registerTranslations();
		$this->registerConfig();
		$this->registerViews();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{		
		//
	}

	/**
	 * Register config.
	 * 
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
		    __DIR__.'/../Config/config.php' => config_path('wallet.php'),
		]);
		$this->mergeConfigFrom(
		    __DIR__.'/../Config/config.php', 'wallet'
		);
	}

	/**
	 * Register views.
	 * 
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = null;//base_path('resources/views/modules/wallet');

		$sourcePath = __DIR__.'/../Resources/views';

		//$this->publishes([
		//	$sourcePath => $viewPath
		//]);

		$this->loadViewsFrom(array_merge(array_map(function ($path) {
			return $path . '/modules/wallet';
		}, \Config::get('view.paths')), [$sourcePath]), 'wallet');
	}

	/**
	 * Register translations.
	 * 
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath =null; // base_path('resources/lang/modules/wallet');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'wallet');
		} else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'wallet');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
