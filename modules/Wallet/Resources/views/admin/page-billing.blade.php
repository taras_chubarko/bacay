@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Билинг') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Билинг</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            @include('wallet::admin.filter')
        </div>
        
        <div class="box-body">
            @if($wallet->count())
                <table class="table table-bordered b-c-222D32">
                    <thead>
                        <tr>
                            <th>№</th>
                            <th width="150">Дата платежа</th>
                            <th width="115">Тип плетежа</th>
                            <th width="115">Сума</th>
                            <th>Описание</th>
                            <th>Пользователь</th>
                            <th>Объявление</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($wallet as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->date }}</td>
                            <td>{!! $item->typeOp !!}</td>
                            <td>{!! $item->amount !!} руб.</td>
                            <td>{{ $item->description }}</td>
                            <td><a href="{{ route('user.slug', $item->user->slug) }}">{{ $item->user->username }}</a></td>
                            <td>
                                @if($item->advert)
                                    <a href="{{ route($item->advert->type.'.show', $item->advert->id) }}">{{ $item->advert->name }}</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $wallet->render() !!}
            @else
                <p>Нет плетежей.</p>
            @endif
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop