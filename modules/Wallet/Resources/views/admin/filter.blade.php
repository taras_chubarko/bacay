<div id="admin-wallet-filter">
    {!! Form::open(array('url' => Request::url(), 'role' => 'form', 'class' => 'form', 'method' => 'GET')) !!}
        <div class="row">
            <div class="col-lg-2">
                <div class="form-group">
                    <label for="type">Тип плетежа</label>
                    {!! Form::select('type', array('+' => '+ Пополнение', '-' => '- Списание'), Request::get('type'), array('class' => 'form-control selectpicker', 'title' => '- Выбрать -')) !!}
                </div>
            </div>
                
            <div class="col-lg-3">
                <div class="form-group">
                    <label for="services_id">Услуга</label>
                    {!! Form::select('services_id', Services::getArray(), Request::get('services_id'), array('class' => 'form-control selectpicker', 'title' => '- Выбрать -')) !!}
                </div>
            </div>
                
            <div class="col-lg-2">
                <div class="form-group">
                    <label for="city">Город</label>
                    {!! Form::text('city', Request::get('city'), array('class' => 'form-control typeahead-city')) !!}
                </div>
            </div>
                
            <div class="col-lg-2">
                <div class="form-group">
                    <label for="user">Пользователь</label>
                    {!! Form::text('user', Request::get('user'), array('class' => 'form-control')) !!}
                </div>
            </div>
            
            <div class="col-lg-3" style="padding-top:25px;">
                {!! Form::submit('Применить', array('class' => 'btn btn-primary')) !!}
                <a class="btn btn-info" href="{{ Request::url() }}">Сбросить</a>
            </div>
        </div>
    {!! Form::close() !!}
</div>