@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Настройки робокассы') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Настройки робокассы</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            <p>Пишем эти настройки в карточку магазина. Везде ставим метод POST.</p>
            <p><label>Result Url:</label> {{ route('wallet.robokassa.result') }}</p>
            <p><label>Success Url:</label> {{ route('wallet.robokassa.success') }} </p>
            <p><label>Fail Url:</label> {{ route('wallet.robokassa.fail') }}</p>
        </div>
        
        <div class="box-body">
            {!! Form::open(array('route' => 'admin.wallet.robokassa', 'role' => 'form', 'class' => 'form')) !!}
                <div class="form-group">
                    <label for="test" class="icheck">{!! Form::checkbox('test', '1', $settings->test) !!} Использовать тестовый сервер.</label>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Боевой сервер</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="login">Логин</label>
                                    {!! Form::text('login', $settings->login, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group">
                                    <label for="password1">Пароль 1</label>
                                    {!! Form::text('password1', $settings->password1, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group">
                                    <label for="password2">Пароль 2</label>
                                    {!! Form::text('password2', $settings->password2, array('class' => 'form-control')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">Тестовый сервер</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="test_login">Логин</label>
                                    {!! Form::text('test_login', $settings->test_login, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group">
                                    <label for="test_password1">Пароль 1</label>
                                    {!! Form::text('test_password1', $settings->test_password1, array('class' => 'form-control')) !!}
                                </div>
                                <div class="form-group">
                                    <label for="test_password2">Пароль 2</label>
                                    {!! Form::text('test_password2', $settings->test_password2, array('class' => 'form-control')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
            {!! Form::close() !!}
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop