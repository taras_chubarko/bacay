@extends('site::layouts.master')

@section('content')
<div class="group usr-group">
	<div class="row">
		<div class="col-md-3">
			@include('user::site.menu-user', $user)
			@include('advert::site.block-info')
		</div>
		<div class="col-md-9">
			<h1>Ваш {{ $user->balance }}</h1>
			<div id="pay">
				{!! Form::open(array('route' => 'wallet.robokassa.pay', 'method' => 'POST', 'role' => 'form', 'class' => 'form')) !!}
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<div class="input-group">
									{!! Form::text('OutSum', null, array('class' => 'form-control', 'placeholder' => '0,00')) !!}
									<span class="input-group-addon">руб.</span>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							{!! Form::submit('Пополнить', array('class' => 'btn btn-primary')) !!}
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div id="block-wallet-history">
				<h3>История платежей:</h3>
				@if($user->walletHistory->count())
					<table class="table table-striped">
						<thead>
							<tr class="info">
								<th width="140">Дата</th>
								<th width="140">Тип операции</th>
								<th>Описание операции</th>
								<th width="120" class="text-right">Сума</th>
							</tr>
						</thead>
						<tbody>
							@foreach($user->walletHistory as $wallet)
							<tr>
								<td>{{ $wallet->date }}</td>
								<td>{!! $wallet->typeOp !!}</td>
								<td>{{ $wallet->description }}</td>
								<td class="text-right">{{ $wallet->amounts }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				@else
					<p>Нет платежей.</p>
				@endif
			</div>
		</div>
	</div>
</div>
@stop