<!-- Modal -->
<div class="modal fade" id="modal-services" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center text-r">Внимание!</h4>
            </div>
            <div class="modal-body">
                <p class="text-center text-y"><i class="fa fa-credit-card f-sz-100"></i></p>
                @if($user->firstDiscount->discount)
                    <p class="text-center m-b-20">Стоимость услуги: {{ $services->price }} руб.</p>
                    <p class="text-center f-sz-20">Вам начислена скидка {{ $user->firstDiscount->format }}</p>
                    <p class="text-center m-b-20">С Вашего счета будет списано:</p>
                    <p class="text-center f-sz-48">{{ $services->price-($services->price*$user->firstDiscount->discount) }} руб.</p> 
                @else
                    @if($user->timeDiscount->discount)
                        <p class="text-center m-b-20">Стоимость услуги: {{ $services->price }} руб.</p>
                        <p class="text-center f-sz-20">Вам начислена скидка {{ $user->timeDiscount->format }}</p>
                        <p class="text-center m-b-20">С Вашего счета будет списано:</p>
                        <p class="text-center f-sz-48">{{ $services->price-($services->price*$user->timeDiscount->discount) }} руб.</p> 
                    @else
                        <p class="text-center m-b-20">С Вашего счета будет списано:</p>
                        <p class="text-center f-sz-48">{{ $services->price }} руб.</p>
                    @endif
                @endif
            </div>
            <div class="modal-footer">
                {!! Form::open(array('route' => ['wallet.bay.services', $advert->id, $services->id], 'role' => 'form', 'class' => 'form')) !!}
                    {!! Form::submit('Оплатить', array('class' => 'btn btn-primary')) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>