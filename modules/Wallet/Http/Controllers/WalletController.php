<?php namespace Modules\Wallet\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class WalletController extends Controller {
	
	public function index()
	{
		$user = \Sentinel::getUser();
		return view('wallet::site.page-user-wallet', compact('user'));
	}
	/* public function robokassa
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function robokassa()
	{
		$settings = \Wallet::getSettings();
		
		return view('wallet::admin.page-robokassa-settings', compact('settings'));
	}
	/* public function robokassaSave
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function robokassaSave(Request $request)
	{
		\DB::table('wallet_robokassa_settings')->where('id', 1)
		->update([
			'login' 		=> $request->login,
			'password1' 		=> $request->password1,
			'password2' 		=> $request->password2,
			'test' 			=> $request->test,
			'test_login' 		=> $request->test_login,
			'test_password1' 	=> $request->test_password1,
			'test_password2' 	=> $request->test_password2,
		]);
		
		return redirect()->back()->with('success', 'Настройки сохранены.');
	}
	/* public function robokassaPay
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function robokassaPay(\Modules\Wallet\Http\Requests\PayReq $request)
	{
		$settings = \Wallet::getSettings();
		$user = \Sentinel::getUser();
		
		$mrh_login = ($settings->test = 1) ? $settings->test_login : $settings->login;   
		$mrh_pass1 = ($settings->test = 1) ? $settings->test_password1 : $settings->password1;
		
		$inv_id    = \Wallet::lastID();
		$userID	   = $user->id;
				 
		$inv_desc  = ($settings->test = 1) ? 'Тестовое пополнение личного счета' : 'Пополнение счета ROBOKASSA';  
		$out_summ  = $request->OutSum;   
		// build CRC value
		$crc  = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1:Shp_userID=$userID");
		// build URL
		$data['MrchLogin'] 	= $mrh_login;
		$data['OutSum'] 	= $out_summ;
		$data['InvId'] 		= $inv_id;
		$data['Desc'] 		= $inv_desc;
		$data['Shp_userID'] 	= $userID;
		$data['SignatureValue'] = $crc;
		if($settings->test = 1)
		{
			$data['IsTest'] 	= 1;
		}
		$url = 'https://auth.robokassa.ru/Merchant/Index.aspx?'.http_build_query($data);
		
		return redirect($url);
	}
	/* public function bayServicesModal
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function bayServicesModal($advert_id, $services_id)
	{
		$services = \Services::findOrFail($services_id);
		$advert = \Advert::findOrFail($advert_id);
		$user = \Sentinel::getUser();
		$view = view('wallet::site.modal-services', compact('advert', 'services', 'user'))->render();
		return response()->json(['result' => $view]);
	}
	/* public function bayServices
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function bayServices($advert_id, $services_id)
	{
		$advert = \Advert::findOrFail($advert_id);
		$services = \Services::findOrFail($services_id);
		$user = \Sentinel::getUser();
		
		if(!$advert->user->wallet)
		{
			return redirect('/wallet')->with('error', 'На вашем счету недостаточно средств для завершения операции. Пожалуйста пополните счет.');
		}
		
		if($advert->user->wallet->count())
		{
			if($advert->user->wallet->totalValue < $services->price)
			{
				return redirect('/wallet')->with('error', 'На вашем счету недостаточно средств для завершения операции. Пожалуйста пополните счет.');
			}
		}
		
		$price = $services->price;
		
		if($user->firstDiscount->discount)
		{
			$price = $services->price-($services->price*$user->firstDiscount->discount);
		}
		else
		{
			if($user->timeDiscount->discount)
			{
				$price = $services->price-($services->price*$user->timeDiscount->discount);
			}
			else
			{
				$price = $services->price;
			}
		}
		
		$wallet = new \Wallet;
		$wallet->user_id 	= $user->id;
		$wallet->advert_id 	= $advert->id;
		$wallet->city_id 	= $advert->city->id;
		$wallet->services_id 	= $services->id;
		$wallet->type 		= '-';
		$wallet->description	= 'Списание средств за "'.$services->name.'"';
		$wallet->amount		= -$price;
		$wallet->save();
		
		$user->discount_first()->attach($user->id, ['flag' => 1]);
		
		$now = date('Y-m-d');
		$start_date = strtotime($now);
		$end_date = strtotime('+10 day', $start_date);
		
		switch($services->id)
		{
			case 1:
			case 2:
			case 3:
				$advert->pivotUslugi()->attach($advert->id, [
					'type' 		=> $advert->type,
					'services_id' 	=> $services->id,
					'end' 		=> date('Y-m-d H:i:s', $end_date),
					'flag' 		=> 1,
				]);
			break;
			
			case 4:
				$advert->pivotUslugi()->attach($advert->id, [
					'type' 		=> $advert->type,
					'services_id' 	=> $services->id,
					'end' 		=> date('Y-m-d H:i:s', $end_date),
					'flag' 		=> 1,
				]);
				$advert->updated_at = date('Y-m-d H:i:s');
				$advert->save();
			break;
		}
		
		
		
		
		return redirect('/wallet')->with('success', 'С вашего счета было списано '.$price.' руб.');
	}
	
	/* public function robokassaSuccess
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function robokassaSuccess(Request $reqest)
	{
		// регистрационная информация (пароль #1)
		// registration info (password #1)
		$settings = \Wallet::getSettings();
		$mrh_pass1 = ($settings->test = 1) ? $settings->test_password1 : $settings->password1;
		//
		// чтение параметров
		// read parameters
		$out_summ = $reqest->OutSum;
		$inv_id = $reqest->InvId;
		$Shp_userID = $reqest->Shp_userID;
		$crc = $reqest->SignatureValue;
		
		$crc = strtoupper($crc);
		//
		$my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass1:Shp_userID=$Shp_userID"));
		//
		// проверка корректности подписи
		// check signature
		if ($my_crc != $crc)
		{
		  echo "bad sign\n";
		  exit();
		}
		//
		// проверка наличия номера счета в истории операций
		$wallet = \Wallet::findOrFail($inv_id);
		if($wallet)
		{
			return redirect('/wallet')->with('success', 'Вы пополнили свой счет на '.$out_summ.' руб.');
		}
	}
	/* public function robokassaResult
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function robokassaResult(Request $reqest)
	{
		$settings = \Wallet::getSettings();
		$mrh_pass2 = ($settings->test = 1) ? $settings->test_password2 : $settings->password2;
		
		//установка текущего времени
		//current date
		$tm=getdate(time()+9*3600);
		$date="$tm[year]-$tm[mon]-$tm[mday] $tm[hours]:$tm[minutes]:$tm[seconds]";
		
		// чтение параметров
		// read parameters
		$out_summ = $reqest->OutSum;
		$inv_id =  $reqest->InvId;
		$Shp_userID = $reqest->Shp_userID;
		$crc =  $reqest->SignatureValue;
		
		$crc = strtoupper($crc);
		
		$my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass2:Shp_userID=$Shp_userID"));
		
		// проверка корректности подписи
		// check signature
		if ($my_crc !=$crc)
		{
		  echo "bad sign\n";
		  exit();
		}
		
		// признак успешно проведенной операции
		// success
		echo "OK$inv_id\n";
		
		$user = \Sentinel::findById($Shp_userID);
		
		$wallet = new \Wallet;
		$wallet->user_id 	= $user->id;
		$wallet->city_id 	= $user->location->city_id;
		$wallet->type 		= '+';
		$wallet->description	= 'Пополнение счета ROBOKASSA';
		$wallet->amount		= $out_summ;
		$wallet->save();
		
	}
	/* public function robokassaFail
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function robokassaFail(Request $reqest)
	{
		return redirect('/wallet')->with('error', 'Не удачный платеж. Попробуйте снова.');
	}
	/* public function billing
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function billing()
	{
		$wallet = \Wallet::billing();
		return view('wallet::admin.page-billing', compact('wallet'));
	}
	
	
	
}