<?php

Route::group(['middleware' => ['web', 'auth'], 'prefix' => 'wallet', 'namespace' => 'Modules\Wallet\Http\Controllers'], function()
{
	Route::get('/', [
		'as' 	=> 'wallet',
		'uses' 	=> 'WalletController@index'
	]);

	Route::post('robokassa/pay', [
		'as' 	=> 'wallet.robokassa.pay',
		'uses' 	=> 'WalletController@robokassaPay'
	]);
	
	Route::get('bay/{advert_id}/{services_id}', [
		'as' 	=> 'wallet.bay.services',
		'uses' 	=> 'WalletController@bayServicesModal'
	]);
	
	Route::post('bay/{advert_id}/{services_id}', [
		'as' 	=> 'wallet.bay.services',
		'uses' 	=> 'WalletController@bayServices'
	]);
});

Route::group(['middleware' => 'web', 'prefix' => 'wallet', 'namespace' => 'Modules\Wallet\Http\Controllers'], function()
{
	Route::post('robokassa/result', [
		'as' 	=> 'wallet.robokassa.result',
		'uses' 	=> 'WalletController@robokassaResult'
	]);
	
	Route::post('robokassa/success', [
		'as' 	=> 'wallet.robokassa.success',
		'uses' 	=> 'WalletController@robokassaSuccess'
	]);
	
	Route::post('robokassa/fail', [
		'as' 	=> 'wallet.robokassa.fail',
		'uses' 	=> 'WalletController@robokassaFail'
	]);
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin/wallet', 'namespace' => 'Modules\Wallet\Http\Controllers'], function()
{
	Route::get('robokassa', [
		'as' 	=> 'admin.wallet.robokassa',
		'uses' 	=> 'WalletController@robokassa'
	]);
	
	Route::post('robokassa', [
		'as' 	=> 'admin.wallet.robokassa',
		'uses' 	=> 'WalletController@robokassaSave'
	]);
	
	Route::get('billing', [
		'as' 	=> 'admin.wallet.billing',
		'uses' 	=> 'WalletController@billing'
	]);
	
	
	
});