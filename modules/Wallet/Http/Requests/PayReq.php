<?php namespace Modules\Wallet\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PayReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'OutSum' => 'required|numeric',
		];
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
		$validator->setAttributeNames([
			'OutSum' 	=> '"Сумма пополнения"',
		]);
		
		return $validator;
	}
}
