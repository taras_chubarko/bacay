<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletRobokassaSettingsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_robokassa_settings', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('login');
            $table->string('password1');
            $table->string('password2');
            $table->integer('test');
            $table->string('test_login');
            $table->string('test_password1');
            $table->string('test_password2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wallet_robokassa_settings');
    }

}
