<?php namespace Modules\Wallet\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model {

    protected $table = 'wallet';

    protected $fillable = [];
    
    protected $guarded = ['_token'];
    
    /* public function getTotalAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getTotalAttribute()
    {
        $total = $this->where('user_id', \Sentinel::getUser()->id)->sum('amount');
        return $total.' руб.';
    }
    public function getTotalValueAttribute()
    {
        $total = $this->where('user_id', \Sentinel::getUser()->id)->sum('amount');
        return $total;
    }
    /* public function getDateAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getDateAttribute()
    {
        return date('d.m.Y в H:i', strtotime($this->created_at));
    }
    /* public function getTypeOpAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getTypeOpAttribute()
    {
        if($this->type == '+')
        {
            $op = '<i class="fa fa-plus text-g"></i> Пополнение';
        }
        
        if($this->type == '-')
        {
            $op = '<i class="fa fa-minus text-y"></i> Списание';
        }
        return $op;
    }
    /* public function getAmountAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getAmountsAttribute()
    {
        return $this->amount.' руб.';
    }
    /* public function getSerrings
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getSettings()
    {
        $settings = \DB::table('wallet_robokassa_settings')->where('id', 1)->first();
        return $settings;
    }
    /* public function lastID
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function lastID()
    {
        $last = \Wallet::orderBy('id', 'DESC')->first();
        if($last)
        {
            return $last->id + 1;
        }
        else
        {
            return 1;
        }
    }
    /*
    *
    */
    public function user()
    {
        return $this->hasOne('User', 'id', 'user_id');
    }
    
    /*
    *
    */
    public function advert()
    {
        return $this->hasOne('Advert', 'id', 'advert_id');
    }
    /* public function scopeBilling
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function scopeBilling($query)
    {
        $query->orderBy('created_at', 'DESC');
        
        if(\Request::has('type'))
        {
            $query->where('type', \Request::get('type'));
        }
        
        if(\Request::has('services_id'))
        {
            $query->where('services_id', \Request::get('services_id'));
        }
        
        if(\Request::has('city'))
        {
            $city = \City::where('name_ru', \Request::get('city'))->first();
            $query->where('city_id', $city->city_id);
        }
        
        if(\Request::has('user'))
        {
            $user = \User::where('name', \Request::get('user'))->first();
            $query->where('user_id', $user->id);
        }
        
        
        $billing = $query->paginate(20);
        
        return $billing;
    }
    
}