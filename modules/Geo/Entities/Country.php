<?php namespace Modules\Geo\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Country extends Model {

    protected $fillable = [];
    
    protected $guarded = ['_token'];
    
    protected $table = 'geo_country';
    
    protected $primaryKey = 'country_id';
    
    public $timestamps = false;
    
    /* public function name
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function arr()
    {
        $countryes = \Country::all();
        foreach($countryes as $country)
        {
            $data[$country->name_ru] = $country->name_ru;
        }
        return $data;
    }

}