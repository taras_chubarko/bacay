<?php namespace Modules\Geo\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Metro extends Model {

    protected $fillable = [];
    
    protected $guarded = ['_token'];
    
    protected $table = 'geo_metro';
    
    protected $primaryKey = 'id';
    
    public $timestamps = false;
    
    /* public function getArr
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getArr($city_id)
    {
        $items = Metro::where('city_id', $city_id)->orderBy('branch', 'ASC')->get();
        $data = array();
        foreach($items as $item)
        {
            $data[$item->id] = (object) array(
                'name'  => $item->name,
                'color' => $item->color,
            );
        }
        //print '<pre>' . htmlspecialchars(print_r($items, true)) . '</pre>';
        return $data;
    }

}