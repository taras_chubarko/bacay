<?php namespace Modules\Geo\Entities;
   
use Illuminate\Database\Eloquent\Model;

class City extends Model {

    protected $fillable = [];
    
    protected $guarded = ['_token'];
    
    protected $table = 'geo_city';
    
    protected $primaryKey = 'city_id';
    
    public $timestamps = false;
    
    /* public function scopeGet
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function scopeGetOne($query, $id)
    {
        $city = $query->where('city_id', $id)->first();
        
        return array($city->city_id => $city->name_ru);
    }
    /* public function scopeGetFavorite
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getFavorite($default = null)
    {
        $data = array();
        $cities = \City::where('country_id', 3159)->where('sort', '>', 0)->orderBy('sort', 'ASC')->get();
        $data[0] = 'Вся Россия';
        
        if($default)
        {
            $city_default = \City::where('city_id', $default)->first();
            $data[$city_default->city_id] = $city_default->name_ru;
        }
        
        foreach($cities as $city)
        {
            $data[$city->city_id] = $city->name_ru;
        }

        return $data;
    }
    /* public function scopeGetAll
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getAll()
    {
        $data[0] = 'Вся Россия';
        
        $regions = \Region::where('country_id', 3159)->get();
        
        foreach($regions as $region)
        {
            $cities = \City::where('region_id', $region->region_id)->get();
            foreach($cities as $city)
            {
                $items[$city->city_id] = $city->name_ru;
            }
            $data[$region->name_ru] = $items;
        }

        return $data;
    }
    

}