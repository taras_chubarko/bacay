<?php namespace Modules\Geo\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Region extends Model {

    protected $fillable = [];
    
    protected $guarded = ['_token'];
    
    protected $table = 'geo_region';
    
    protected $primaryKey = 'region_id';
    
    public $timestamps = false;
    
    /* public function getArray
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getArray()
    {
        $regions = array();
        
        $items = \Region::where('country_id', 3159)->get();
        
        //$regions[0] = 'Вся Россия';
        
        foreach($items as $item)
        {
            $regions[$item->region_id] = $item->name_ru;
        }
        return $regions;
    }

}