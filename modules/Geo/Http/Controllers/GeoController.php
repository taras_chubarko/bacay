<?php namespace Modules\Geo\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;


class GeoController extends Controller {
	
	/* public function test
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function test() {
		return true;
	}
	
	/*
	 * function get
	 * @param $arg
	 */
	
	function get($id) {
		
		$country = \City::all();
		
		foreach($country as $item)
		{
			
			$items[] = array(
				'city_id' => $item->city_id,
			);
		}
		
		$items = array_chunk($items, 10);
		
		foreach($items[$id] as $country_id)
		{
			
			$co = \City::find($country_id['city_id']);
			
			$items1['text'] = $co->name_ru;
			$items1['lang'] = 'ru-en';
			$items1['key'] = 'trnsl.1.1.20151227T152335Z.96d605387f41f8c4.cebc84259410c26457742b6a0ce28099c8ec595e';
			$data = http_build_query($items1);
			$response = json_decode(file_get_contents('https://translate.yandex.net/api/v1.5/tr.json/translate?'.$data));
			
			$co->name_en = $response->text[0];
			$co->save();
			//print '<pre>' . htmlspecialchars(print_r($data, true)) . '</pre>';
		}
		
		return response()->json(['count' => count($items), 'data' => $items[$id] ], 200);
		
	}
	
	/* public function city
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function city(Request $request) {
		
		$cities = \City::where('geo_city.name_ru', 'like', '%'.$request->q.'%')->where('geo_city.country_id', 3159)
		->leftJoin('geo_region as region', 'geo_city.region_id', '=', 'region.region_id')
		->select(
			"region.name_ru as region",
			"geo_city.name_ru as city",
			"geo_city.city_id as city_id"
		)
		->take(10)->get();
		
		return response()->json($cities, 200);
	}
	
	/* public function region
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function region(Request $request) {
		
		$regions = \Region::where('name_ru', 'like', '%'.$request->q.'%')->where('country_id', 3159)->take(10)->get();
		
		return response()->json($regions, 200);
	}
	/* public function getMetro
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function getMetro(Request $request)
	{
		$metro = \Metro::getArr($request->city_id);
		
		return response()->json(['result' => view('geo::site.select-metro', compact('metro'))->render() ], 200);
	}
    /* public function getCities
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCities(Request $request)
    {
        if( !\Cache::has('geo-gities') )
        {
            $regions = \Region::where('country_id', 3159)->get();
            foreach($regions as $region)
            {
                $items[$region->region_id] = (object) array(
                    'name' => $region->name_ru,
                    'cities' => \City::where('region_id', $region->region_id)->get(),
                );
            }
            \Cache::put('geo-gities', $items, 60);
        }
        $items =  \Cache::get('geo-gities');
        $cities = unserialize(\NewsLatter::find($request->id)->region_id);
        print '<pre>' . htmlspecialchars(print_r($cities, true)) . '</pre>';
        return response()->json(view('geo::modal.modal-cities', compact('items', 'cities'))->render(), 200);
    }
	/* public function getCities2
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCities2(Request $request)
    {
        $regions = \Region::where('country_id', 3159)->get();
        foreach($regions as $region)
        {
            $items[$region->name_ru] = $this->ctt($region->region_id);
        }
        
        $out = '';
        foreach($items as $key => $item)
        {
            $out .= '<optgroup label="'.$key.'">';
            foreach($item as $k => $v)
            {
                $out .= '<option value="'.$k.'">'.$v.'</option>';
            }
        }
        
        return response()->json($out, 200);
    }
	/* public function ctt
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function ctt($region_id)
    {
        $cities = \City::where('region_id', $region_id)->get();
        foreach($cities as $city)
        {
            $items[$city->city_id] = $city->name_ru;  
        }
        return $items;
    }
}