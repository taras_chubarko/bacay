<?php

Route::group(['prefix' => 'geo', 'namespace' => 'Modules\Geo\Http\Controllers'], function()
{
	Route::get('test', [
		'as' => 'geo.test',
		'uses' => 'GeoController@test'
	]);
	
	Route::get('get/{id}', [
		'as' => 'geo.get',
		'uses' => 'GeoController@get'
	]);
	
	Route::post('city', [
		'as' => 'geo.city',
		'uses' => 'GeoController@city'
	]);
	
	Route::post('region', [
		'as' => 'geo.region',
		'uses' => 'GeoController@region'
	]);
	
	Route::post('get/metro', [
		'as' => 'geo.get.metro',
		'uses' => 'GeoController@getMetro'
	]);
	
    Route::post('get/cities', [
		'as' => 'geo.get.cities',
		'uses' => 'GeoController@getCities'
	]);
    Route::post('get/cities2', [
		'as' => 'geo.get.cities2',
		'uses' => 'GeoController@getCities2'
	]);
	
});


Form::macro('region', function($name, $values_name = null, $values_id = array())
{
    $out = '';
    $out .= Form::text($name.'[name]', $values_name, array('class' => 'form-control', 'id' => 'field-cities'));
   
    $out .= '<div id="field-cities-id">';
    if(!empty($values_id))
    {
        foreach($values_id as $key => $id)
        {
            $out .= Form::hidden($name.'[id]['.$key.']', $id);
        }
    }
    $out .= '</div>';
    return $out;
});