<div class="modal fade" id="modal-cities" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выбрать город</h4>
            </div>
            <div class="modal-body" style="max-height: 400px; overflow-y: scroll;">
                <div class="panel-group" id="accordion-cities">
                @foreach($items as $key => $item)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion-cities" href="#collapse-{{ $key }}">
                                    {{ $item->name }}
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-{{ $key }}" class="panel-collapse collapse">
                            <div class="panel-body">
                                <label>{!! Form::checkbox('all', 'all') !!} Выбрать все</label>
                                <div class="row">
                                    @foreach($item->cities->chunk(2) as $chunk)
                                        @foreach ($chunk as $city)
                                            <div class="col-lg-6">
                                                <label style="font-weight: normal;">
                                                    {!! Form::checkbox('city['.$city->city_id.']', $city->city_id) !!}
                                                    {{ $city->name_ru }}
                                                </label>
                                            </div>
                                        @endforeach
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-primary apply">Применить</button>
            </div>
        </div>
    </div>
</div>