<label for="metro" class="col-lg-3 label-30">Метро *</label>
<div class="col-lg-9">
   {!! Form::selectMetro('metro', $metro, null, array('class' => 'form-control selectpicker geometro', 'title' => 'Выбрать метро')) !!}
</div>