<?php namespace Modules\Meta\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Meta extends Model {

    protected $table = 'meta';

    protected $fillable = [];
    
    protected $guarded = ['_token'];
    
    /* public function getDateAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function getDateAttribute() {
        return date('d.m.Y', strtotime($this->created_at));
    }
    
     /* public function get
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function get($field, $entity = null, $entity_type = null) {
        
        if(is_numeric($entity))
        {
            $meta = Meta::findOrFail($entity);
            
            if($meta)
            {
                $title = ($meta->$field != '') ? $meta->$field : $entity_type->name;
            }
            else
            {
                $title = ($entity_type) ? $entity_type->name : '';
            }
            
        }
        else
        {
            $meta = Meta::where('entity', $entity)->where('entity_id', $entity_type->id)->first();
        
            if($meta)
            {
                $title = ($meta->$field != '') ? $meta->$field : $entity_type->name;
            }
            else
            {
                $title = ($entity_type) ? $entity_type->name : '';
            }
        }
        
        return $title;
    }
    
    /* public function set
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function set($id, $meta) {
        
        $meta['entity_id'] = $id;
        
        Meta::create($meta);
        
    }
    
    /* public function getMeta
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function getMeta($entity, $entity_id) {
        return Meta::where('entity', $entity)->where('entity_id', $entity_id)->first();
    }
    
    /* public function updateMeta
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function updateMeta($meta = array()) {
        
        $metas = Meta::where('entity', $meta['entity'])->where('entity_id', $meta['entity_id'])->first();
        
        if($metas)
        {
            $metas->update($meta);
        }
        else
        {
            Meta::set($meta['entity_id'], $meta);
        }
        
    }
    
    /* public function deleteMeta
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function deleteMeta($entity, $entity_id) {
        Meta::where('entity', $entity)->where('entity_id', $entity_id)->delete();
    }

}