{!! Form::open(array('url' => '/admin/meta', 'method' => 'GET', 'role' => 'form', 'class' => 'form')) !!}
    
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="entity">Тип материала</label>
                {!! Form::select('entity', config('meta.entity'), Request::get('entity'), array('class' => 'form-control selectpicker', 'title' => '- Выбрать -')) !!}
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group p-t-25">
               {!! Form::submit('Применить', array('class' => 'btn btn-primary')) !!}
            </div>
        </div>
    </div>
    
{!! Form::close() !!}