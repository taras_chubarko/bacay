@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Редактировать мета теги') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Редактировать мета теги</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            
        </div>
        
        {!! Form::open(array('route' => ['admin.meta.update', $meta->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'form', 'id' => 'page-form')) !!}
            <div class="box-body">
              
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group{{ $errors->has('entity') ? ' has-error' : '' }}">
                      <label for="entity">Тип материала*</label>
                      {!! Form::select('entity', config('meta.entity'), $meta->entity, array('class' => 'form-control selectpicker', 'title' => '- Выбрать -')) !!}
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                      <label for="entity_id">ID материала</label>
                      {!! Form::text('entity_id', $meta->entity_id, array('class' => 'form-control')) !!}
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group{{ $errors->has('robots') ? ' has-error' : '' }}">
                      <label for="robots">Индексация*</label>
                      {!! Form::select('robots', config('meta.robots'), $meta->robots, array('class' => 'form-control selectpicker', 'title' => '- Выбрать -')) !!}
                  </div>
                </div>
              </div>
              
              <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                  <label for="title">Мета - title*</label>
                  {!! Form::text('title', $meta->title, array('class' => 'form-control')) !!}
              </div>
                
              <div class="form-group{{ $errors->has('keywords') ? ' has-error' : '' }}">
                  <label for="keywords">Мета - keywords*</label>
                  {!! Form::text('keywords', $meta->keywords, array('class' => 'form-control')) !!}
              </div>
                
              <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                  <label for="description">Мета - description*</label>
                  {!! Form::textarea('description', $meta->description, array('class' => 'form-control', 'rows' => 5)) !!}
              </div>
              
            </div><!-- /.box-body -->
            <div class="box-footer">
              {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
            </div><!-- /.box-footer-->
        {!! Form::close() !!}
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop