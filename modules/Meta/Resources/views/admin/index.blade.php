@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Мета теги') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Мета теги</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            <a href="{{ route('admin.meta.create') }}" class="btn btn-success btn-flat">Добавить</a>
        </div>
        
        <div class="box-body">
            @include('meta::admin.form-filter')
            @if($meta->count())
                <table class="table table-bordered b-c-222D32">
                  <thead>
                    <tr>
                        <th width="20">#</th>
                        <th>Тип материала</th>
                        <th>id</th>
                        <th>Мета - title</th>
                        <th>Дата</th>
                        <th width="100">Действие</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($meta as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ config('meta.entity.'.$item->entity) }}</td>
                        <td>{{ $item->entity_id }}</td>
                        <td>{{ $item->title }}</td> 
                        <td>{{ $item->date }}</td>
                      
                        <td>
                            <a href="{{ route('admin.meta.edit', $item->id) }}" class="btn btn-sm bg-navy btn-flat"><i class="fa fa-fw fa-edit"></i></a>
                            @include('cms::admin.delete', ['route' => 'admin.meta.destroy', 'id' => $item->id])
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {!! $meta->render() !!}
              @else
                Нет мета тегов.
              @endif

        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop