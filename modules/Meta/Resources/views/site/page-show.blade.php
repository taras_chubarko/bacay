@extends('site::layouts.master')

@section('content')
	<div id="page" class="page-{{ $page->id }} m-b-50">
          
          <div class="row">
               <div class="col-md-12">
                    <div class="body">
                        <h1 class="title">{{ $page->name }}</h1>
                        {!! $body !!}
                    </div>
                    @region('content')
               </div>  
          </div>
          
     </div>
@stop