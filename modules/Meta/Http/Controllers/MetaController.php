<?php namespace Modules\Meta\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class MetaController extends Controller {
	
	/* public function index
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function index(Request $request) {
		
		$q = \Meta::orderBy('created_at', 'DESC');
		
		if($request->entity)
		{
			$q->where('entity', $request->entity);
		}
		
		$meta = $q->paginate(20);
		
		return view('meta::admin.index', compact('meta'));
		
	}
	
	/* public function create
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function create() {
		return view('meta::admin.create');
	}
	
	/* public function store
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function store(\Modules\Meta\Http\Requests\CreateMetaReq $request) {
		
		\Meta::create($request->all());
		
		return redirect()->route('admin.meta.index')->with('success', 'Мета-тег создан.');
	}
	
	/* public function edit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function edit($id) {
		
		$meta = \Meta::findOrFail($id);
		
		return view('meta::admin.edit', compact('meta'));
		
	}
	
	/* public function update
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function update(\Modules\Meta\Http\Requests\CreateMetaReq $request, $id) {
		
		$meta = \Meta::findOrFail($id);
		$meta->update($request->all());
		
		return redirect()->route('admin.meta.index')->with('success', 'Мета-тег обновлен.');
	}
	
	/* public function destroy
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function destroy($id) {
		
		$meta = \Meta::findOrFail($id);
		$meta->delete();
		
		return redirect()->route('admin.meta.index')->with('success', 'Мета-тег удален.');
	}
	
}