<?php namespace Modules\Menu\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Menu extends Model {

    protected $fillable = [];
    
    protected $table = 'menu';
    
    protected $guarded = ['_token'];
    
    public function items()
    {
        return $this->hasMany('MenuItem')->orderBy('sort', 'ASC');
    }
    
    /* public function scopeAdmin
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function admin($key) {
        
        $user = \Sentinel::getUser();
        
        //if( !\Cache::has('admin-menu') )
        //{
            $items = Menu::where('id', 1)->first()->items->toHierarchy();
        //    \Cache::put('admin-menu', $items, 60);
        //}
        //$items =  \Cache::get('admin-menu');
        
        \Menus::create('adminLeft', function($menu) use ($items)
        {
            if(\Sentinel::check())
            {
                $user = \Sentinel::getUser();
                if($user->inRole('admin'))
                {
                    foreach($items as $item)
                    {
                        if($item->children->count())
                        {
                            $menu->dropdown($item->name, function ($sub) use ($item) {
                                foreach($item->children as $children)
                                {
                                    $sub->url($children->link, $children->name);
                                }
                            }, ['icon' => $item->ico]);
                        }
                        else
                        {
                            $menu->url($item->link, $item->name, ['icon' => $item->ico]);
                        }
                    }
                }
                else
                {
                    //dd($items);
                    foreach($items as $item)
                    {
                        
                        if($item->children->count())
                        {
                            if($user->hasAccess([$item->access]))
                            {
                                $menu->dropdown($item->name, function ($sub) use ($item, $user) {
                                    foreach($item->children as $children)
                                    {
                                        if(isset($children->access) && $user->hasAccess([$children->access]))
                                        {
                                            $sub->url($children->link, $children->name);
                                        }
                                        else
                                        {
                                            $sub->header('');
                                        }
                                    }
                                }, ['icon' => $item->ico]);
                            }
                        }
                        else
                        {
                            //dd($item);
                            if($user->hasAccess([$item->access]))
                            {
                                $menu->url($item->link, $item->name, ['icon' => $item->ico]);
                            }
                        }
                    }
                }
            }
            $menu->style('adminLeft');
        });
        
        switch($key)
        {
            case 'left':
                if(\Sentinel::check())
                {
                    if(\Sentinel::getUser()->hasAccess('admin'))
                    {
                        return \Menus::get('adminLeft');
                    }
                }
                return '';
            break;
                
            case 'top':
                if(\Sentinel::check())
                {
                    if(\Sentinel::getUser()->hasAccess('admin'))
                    {
                        return \Menus::render('adminLeft', 'admin-top');
                    }
                }
                return '';
            break;
        }
        
    }
    
    /* public function menu
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function menuBottom()
    {
        if( !\Cache::has('menuBottom') )
        {
            $items = Menu::where('id', 3)->first()->items->toHierarchy();
            \Cache::put('menuBottom', $items, 60);
        }
        $items =  \Cache::get('menuBottom');
        
        \Menus::create('top', function($menu) use ($items)
        {
            foreach($items as $item)
            {
                $menu->url($item->link, $item->name);
                
            }
            $menu->style('top');
        });
        
        return \Menus::get('top');
    }
    
    
    
}