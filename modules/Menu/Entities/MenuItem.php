<?php namespace Modules\Menu\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Baum;

class MenuItem extends Baum\Node {

    protected $fillable = [];
    
    protected $table = 'menu_item';
    
    protected $scoped = array('menu_id');
    
    protected $orderColumn = 'sort';
    
    protected $guarded = ['_token'];
    
    
    public static function itemsArray($menu) {
        
        $items = MenuItem::where('menu_id', $menu)->get()->toHierarchy()->toArray();
        
        $menus = MenuItem::parseArray($items);
        
        $arr = [];
        $arr[0] = '- Корень -';
        
        foreach($menus as $item)
        {
            $depth = str_repeat('-', $item['depth']);
            
            $arr[$item['id']] = $depth . $item['name'];
        }
        
        return $arr;
    }
    
    public static function parseArray($parseArray, $parentID = 0)
    {
        $return = array();
        foreach ($parseArray as $subArray) {
            $returnSubSubArray = array();
            if (isset($subArray['children'])) {
                $returnSubSubArray = MenuItem::parseArray($subArray['children'], $subArray['id']);
            }
            $return[] = array('id' => $subArray['id'],
                              'parent_id' => $parentID,
                              'name' => $subArray['name'],
                              'depth' => $subArray['depth'],
                              );
            $return = array_merge($return, $returnSubSubArray);
        }
        return $return;
    }

}