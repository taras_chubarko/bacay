<?php namespace Modules\Menu\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class MenuItemController extends Controller {
	
	/* public function create
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function create() {
		return view('menu::admin.items.create');
	}
	
	/* public function store
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function store(\Modules\Menu\Http\Requests\CreateItemReq $request) {
		
		if($request->parent_id == 0)
		{
			$root = \MenuItem::create([
				'menu_id' 	=> $request->menu_id,
                                'name' 		=> $request->name,
                                'link' 		=> $request->link,
				'sort' 		=> $request->sort,
				'ico' 		=> $request->ico,
                        ]);
		}
		else
		{
			$child = \MenuItem::create([
				'menu_id' 	=> $request->menu_id,
                                'name' 		=> $request->name,
                                'link' 		=> $request->link,
				'sort' 		=> $request->sort,
				'ico' 		=> $request->ico,
                        ]);
			$child->makeChildOf($request->parent_id);
		}
		
		return redirect()->route('admin.menu.show', $request->menu_id)->with('success', 'Ссылка меню добавлена.');
		
		
	}
	
	/* public function edit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function edit($id) {
		
		$menu_item = \MenuItem::findOrFail($id);
		
		return view('menu::admin.items.edit', compact('menu_item'));
	}
	
	/* public function update
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function update(\Modules\Menu\Http\Requests\CreateItemReq $request, $id) {
		
		$menu_items = \MenuItem::findOrFail($id);
		$menu_items->name 	= $request->name;
		$menu_items->link 	= $request->link;
		$menu_items->parent_id 	= $request->parent_id;
		$menu_items->sort 	= $request->sort;
		$menu_items->ico 	= $request->ico;
		$menu_items->save();
		
		return redirect()->route('admin.menu.show', $menu_items->menu_id)->with('success', 'Ссылка меню обновлена.');
	}
	
	/* public function destroy
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function destroy($id) {
		
		$menu_item = \MenuItem::findOrFail($id);
		$menu_item->delete();
		
		return redirect()->route('admin.menu.show', $menu_item->menu_id)->with('success', 'Ссылка меню удалена.');
	}
	
	
	/* public function sort
	 * @param 
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function sort(Request $request) {
		
		$items = $this->parseJsonArray($request->sort);
		
		foreach($items as $key => $item)
		{
			$menu = \MenuItem::findOrFail($item['id']);
			
			if($item['parent_id'] == 0)
			{
				$menu->makeRoot();
				$menu->sort = $key;
				$menu->save();
			}
			else
			{
				$menu->sort = $key;
				$menu->parent_id = $item['parent_id'];
				$menu->save();
			}
			
		}
		
		return 'OK'; // All the Input fields
		
	}
	
	
	public function parseJsonArray($jsonArray, $parentID = 0)
	{
	    $return = array();
	    foreach ($jsonArray as $subArray) {
		$returnSubSubArray = array();
		if (isset($subArray['children'])) {
		    $returnSubSubArray = $this->parseJsonArray($subArray['children'], $subArray['id']);
		}
		$return[] = array('id' => $subArray['id'], 'parent_id' => $parentID);
		$return = array_merge($return, $returnSubSubArray);
	    }
	    return $return;
	}
	
}