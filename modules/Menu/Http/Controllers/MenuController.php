<?php namespace Modules\Menu\Http\Controllers;

use Pingpong\Modules\Routing\Controller;


class MenuController extends Controller {
	
	public function index()
	{
		$menu = \Menu::all();
		
		return view('menu::admin.index', compact('menu'));
	}
	
	/* public function create
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function create() {
		return view('menu::admin.create');
	}
	
	/* public function store
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function store(\Modules\Menu\Http\Requests\CreateReq $request) {
		
		\Menu::create($request->all());
		
		return redirect()->route('admin.menu.index')->with('success', 'Новое меню создано.');
	}
	
	/* public function edit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function edit($id) {
		
		$menu = \Menu::findOrFail($id);
		
		return view('menu::admin.edit', compact('menu'));
	}
	
	/* public function update
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function update(\Modules\Menu\Http\Requests\CreateReq $request, $id) {
		
		$menu = \Menu::findOrFail($id);
		$menu->update($request->all());
		
		return redirect()->route('admin.menu.index')->with('success', 'Меню обновлено.');
	}
	
	/* public function destroy
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function destroy($id) {
		
		$menu = \Menu::findOrFail($id);
		\MenuItem::where('menu_id', $id)->delete();
		$menu->delete();
		
		return redirect()->route('admin.menu.index')->with('success', 'Меню удалено.');
	}
	
	/* public function show
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function show($id) {
		
		$menu = \Menu::findOrFail($id);
		
		return view('menu::admin.show', compact('menu'));
	}
	
}