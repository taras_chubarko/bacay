<?php namespace Modules\Menu\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateItemReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'  	 => 'required',
			'link'		 => 'required',
			'parent_id' 	 => 'required',
		];
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		$validator->setAttributeNames([
			'name'  	 => '"Название"',
			'link'		 => '"Ссылка"',
			'parent_id' 	 => '"Родитель"',
		]);
	 
		return $validator;
	}

}
