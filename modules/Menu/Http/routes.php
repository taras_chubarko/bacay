<?php

Route::group(['middleware' => 'web', 'prefix' => 'menu', 'namespace' => 'Modules\Menu\Http\Controllers'], function()
{
	Route::get('/', 'MenuController@index');
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\Menu\Http\Controllers'], function()
{
	Route::resource('menu', 'MenuController');
	Route::resource('menu/item', 'MenuItemController');
	Route::post('menu/item/sort', 'MenuItemController@sort');
});