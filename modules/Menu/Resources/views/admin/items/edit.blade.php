@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Редактировать ссылку') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Редактировать ссылку</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            
        </div>
        
        <div class="box-body">
            
            {!! Form::open(array('route' => ['admin.menu.item.update', $menu_item->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'form')) !!}
    
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Название *</label>
                    {!! Form::text('name', $menu_item->name, array('class' => 'form-control')) !!}
                </div>
                    
                <div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
                    <label for="link">Ссылка *</label>
                    {!! Form::text('link', $menu_item->link, array('class' => 'form-control')) !!}
                </div>
                    
                <div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
                    <label for="parent_id">Родитель *</label>
                    {!! Form::select('parent_id', MenuItem::itemsArray($menu_item->menu_id), $menu_item->parent_id, array('class' => 'form-control selectpicker')) !!}
                </div>
                    
                <div class="form-group">
                    <label for="order">Позиция в списке</label>
                    {!! Form::selectRange('sort', 0, 50, $menu_item->sort, array('class' => 'form-control selectpicker')) !!}
                </div>
                    
                <div class="form-group">
                    <label for="ico">Иконка</label>
                    {!! Form::text('ico', $menu_item->ico, array('class' => 'form-control', 'placeholder' => 'fa fa-pencil')) !!}
                </div>
                    
                <div class="form-group">
                    <label for="access">Код разделения прав</label>
                    {!! Form::text('access', $menu_item->access, array('class' => 'form-control')) !!}
                </div>
                     
                <div class="form-group">
                    {!! Form::hidden('menu_id', $menu_item->menu_id) !!}
                    {!! Form::submit('Сохранить', array('class' => 'btn btn-primary btn-flat')) !!}
                </div>
                
            {!! Form::close() !!}
            
            
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>

@stop