@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Создать ссылку') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Создать ссылку</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            
        </div>
        
        <div class="box-body">
            
            {!! Form::open(array('route' => 'admin.menu.item.store', 'role' => 'form', 'class' => 'form')) !!}
    
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Название *</label>
                    {!! Form::text('name', null, array('class' => 'form-control')) !!}
                </div>
                    
                <div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
                    <label for="link">Ссылка *</label>
                    {!! Form::text('link', 'javascript:', array('class' => 'form-control')) !!}
                </div>
                    
                <div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
                    <label for="parent_id">Родитель *</label>
                    {!! Form::select('parent_id', MenuItem::itemsArray(Request::get('menu_id')), (Request::get('item')) ? Request::get('item') : null, array('class' => 'form-control selectpicker')) !!}
                </div>
                    
                <div class="form-group">
                    <label for="order">Позиция в списке</label>
                    {!! Form::selectRange('sort', 0, 50, 0, array('class' => 'form-control selectpicker')) !!}
                </div>
                    
                <div class="form-group">
                    <label for="ico">Иконка</label>
                    {!! Form::text('ico', null, array('class' => 'form-control', 'placeholder' => 'fa fa-pencil')) !!}
                </div>
                    
                <div class="form-group">
                    {!! Form::hidden('menu_id', Request::get('menu_id')) !!}
                    {!! Form::submit('Сохранить', array('class' => 'btn btn-primary btn-flat')) !!}
                </div>
                
            {!! Form::close() !!}
            
            
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>

@stop