@extends('admin::layouts.master')

@section('content')

<section class="content-header">
	<h1>{{ MetaTag::set('title', 'Меню') }}</h1>
	<ol class="breadcrumb">
		<li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
		<li class="active">Меню</li>
	</ol>
</section>
  
<section class="content">

	<div class="box">
		<div class="box-header with-border">
			@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('menu.create'))
				<a class="btn btn-success btn-flat" href="{{ route('admin.menu.create') }}">Создать</a>
			@endif
		</div>
		
		<div class="box-body">
			@if($menu->count())
			<table class="table table-bordered b-c-222D32">
				<thead>
					<tr>
						<th width="10">#</th>
						<th>Название меню</th>
						<th width="100">Действие</th>
					</tr>
				</thead>
				<tbody>
					@foreach($menu as $item)
					<tr>
						<td>{{ $item->id }}</td>
						<td>
						@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('menu.create'))
							<a href="{{ route('admin.menu.show', $item->id) }}">{{ $item->name }}</a>
						@else
							{{ $item->name }}
						@endif
						</td>
						
						<td>
							@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('menu.edit'))
								<a href="{{ route('admin.menu.edit', $item->id) }}" class="btn btn-sm bg-navy btn-flat"><i class="fa fa-fw fa-edit"></i></a>
							@endif
							@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('menu.delete'))
								@include('cms::admin.delete', ['route' => 'admin.menu.destroy', 'id' => $item->id])
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@else
				<p>Нет меню.</p>
			@endif
		  
		</div><!-- /.box-body -->
		
		<div class="box-footer">
		 
		</div><!-- /.box-footer-->
	</div>

</section>

@stop