@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', $menu->name) }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">{{ $menu->name }}</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            <a href="{{ route('admin.menu.item.create') }}?menu_id={{ $menu->id }}" class="btn btn-success btn-flat">Добавить линк</a>
            <a href="javascript:" data-action="collapseAll" class="btn btn-flat btn-s-md btn-danger m-r-20 anes"><i class="fa fa-arrow-circle-o-up"></i> Свернуть</a>
            <a href="javascript:" data-action="expandAll" class="btn btn-flat btn-s-md btn-warning anes"><i class="fa fa-arrow-circle-o-down"></i> Развернуть</a>
        </div>
        
        <div class="box-body">
            @if($menu->items->count())
                <div id="tree-admin" class="dd dd-menu">
                    @include('menu::admin.items.lists', ['lists' => $menu->items->toHierarchy()])
                </div>
            @else
                <p>Нет ссылок.</p>
            @endif
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>

@stop