<?php namespace Modules\Menu\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MenuDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$menu = \Menu::create(['name' => 'Меню администратора']);
		
		$names[] = 'Документы';
		$names[] = 'Структура';
		$names[] = 'Пользователи';
		$names[] = 'Настройки';
		
		$ico[] = 'fa fa-file-text';
		$ico[] = 'fa fa-cube';
		$ico[] = 'fa fa-users';
		$ico[] = 'fa fa-cogs';
		
		foreach($names as $k => $name)
		{
			$root = \MenuItem::create([
				'menu_id' => $menu->id,
				'name' => $name,
				'link' => 'javascript:',
				'sort' => $k,
				'ico' => $ico[$k],
			]);
		}
		
	}

}