<?php namespace Modules\Menu\Presenter;

use Pingpong\Menus\Presenters\Presenter;

class AdminTopMenuPresenter extends Presenter
{
    /**
     * {@inheritdoc }
     */
    public function getOpenTagWrapper()
    {
        return  PHP_EOL . '<nav id="admin-menu" class="navbar-inverse navbar-fixed-top hidden-xs hidden-xs" role="navigation"><div class="container-fluid"><ul class="nav navbar-nav"><li><a  href="'.url('/').'"><i class="fa fa-home"></i> Главная</a></li>' . PHP_EOL;
    }

    /**
     * {@inheritdoc }
     */
    public function getCloseTagWrapper()
    {
        return  PHP_EOL . '<li class="pull-right"><a  href="'.route('user.logout').'"><i class="fa fa-sign-out"></i> Выход</a></li></ul></div></nav>' . PHP_EOL;
    }

    /**
     * {@inheritdoc }
     */
    public function getMenuWithoutDropdownWrapper($item)
    {
        $url = ($item->url == 'javascript:') ? $item->url : $item->getUrl();
        return '<li><a href="'. $url .'">'.$item->getIcon().' <span>'.$item->title.'</span></a></li>';
       //print '<pre>' . htmlspecialchars(print_r(, true)) . '</pre>';
    }

    /**
     * {@inheritdoc }
     */
    public function getActiveState($item)
    {
        return \Request::is($item->getRequest()) ? ' class="active"' : null;
    }

    /**
     * {@inheritdoc }
     */
    public function getDividerWrapper()
    {
        return '<li class="divider"></li>';
    }

    /**
     * {@inheritdoc }
     */
    public function getMenuWithDropDownWrapper($item)
    {
        return '<li class="dropdown">
            <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">'.$item->getIcon().' <span>'.$item->title.'</span></a>
            <ul class="dropdown-menu">
                '.$this->getChildMenuItems($item).'
            </ul>
            </li>' . PHP_EOL;
        ;
    }
}