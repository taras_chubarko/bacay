<?php namespace Modules\Menu\Presenter;

use Pingpong\Menus\Presenters\Presenter;

class TopMenuPresenter extends Presenter
{
    /**
     * {@inheritdoc }
     */
    public function getOpenTagWrapper()
    {
        return  PHP_EOL . '<ul class="footer-nav">' . PHP_EOL;
    }

    /**
     * {@inheritdoc }
     */
    public function getCloseTagWrapper()
    {
        return  PHP_EOL . '</ul>' . PHP_EOL;
    }

    /**
     * {@inheritdoc }
     */
    public function getMenuWithoutDropdownWrapper($item)
    {
       //print '<pre>' . htmlspecialchars(print_r(, true)) . '</pre>';
        $url = ($item->url == 'javascript:') ? $item->url : $item->getUrl();
       
        return '<li><a href="'. $url .'">'.$item->title.'</a></li>';
    }

    /**
     * {@inheritdoc }
     */
    public function getActiveState($item)
    {
        return \Request::is($item->getRequest()) ? ' class="active"' : null;
    }

    /**
     * {@inheritdoc }
     */
    public function getDividerWrapper()
    {
        return '<li class="divider"></li>';
    }

    /**
     * {@inheritdoc }
     */
    public function getMenuWithDropDownWrapper($item)
    {
        return '<li class="dropdown">
                <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">'.$item->getIcon().' <span>'.$item->title.'</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="dropdown-menu">
                  '.$this->getChildMenuItems($item).'
                </ul>
              </li>' . PHP_EOL;
        ;
    }
}