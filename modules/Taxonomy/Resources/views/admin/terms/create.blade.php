@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Создать термин') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Создать термин</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            
        </div>
        
        <div class="box-body">
            
            {!! Form::open(array('route' => 'admin.taxonomy.term.store', 'role' => 'form', 'class' => 'form')) !!}
    
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Название *</label>
                    {!! Form::text('name', null, array('class' => 'form-control')) !!}
                </div>
                    
                <div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
                    <label for="parent_id">Родитель *</label>
                    {!! Form::select('parent_id', TaxonomyTerm::itemsArray(Request::get('taxonomy_id')), (Request::get('term')) ? Request::get('term') : null, array('class' => 'form-control selectpicker')) !!}
                </div>
                    
                <div class="form-group">
                    <label for="ico">Класс иконки</label>
                    {!! Form::text('ico', null, array('class' => 'form-control')) !!}
                </div>
                    
                <div class="form-group">
                    <label for="order">Позиция в списке</label>
                    {!! Form::selectRange('sort', 0, 50, 0, array('class' => 'form-control selectpicker')) !!}
                </div>
                    
                <div class="form-group">
                    {!! Form::hidden('taxonomy_id', Request::get('taxonomy_id')) !!}
                    {!! Form::submit('Сохранить', array('class' => 'btn btn-primary btn-flat')) !!}
                </div>
                
            {!! Form::close() !!}
            
            
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>

@stop