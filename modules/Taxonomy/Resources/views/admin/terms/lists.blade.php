<ol class="dd-list">
    @foreach($lists as $list)
        @if($list->children->count())
            <li class="dd-item" data-id="{{ $list->id }}">
                <div class="dd-handle dd3-handle"><i class="fa fa-bars"></i></div>
                <div class="dd-content">
                    <a href="">{{ $list->name }} :: {{ $list->id }}</a>
                    @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('taxonomy.delete'))
                    <a
                    href="javascript:" 
                    data-href="{{ route('admin.taxonomy.term.destroy', $list->id) }}"
                    data-title="Удалить ссылку?"
                    data-message="Это действие не обратимо."
                    data-method="DELETE"
                    data-toggle="tooltip"
                    data-placement="top"
                    data-button1="Да"
                    data-button2="Нет"
                    title="Удалить"
                    class="eac delete"><i class="fa fa-fw fa-trash-o"></i></a>
                    @endif
                    @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('taxonomy.edit'))
                    <a href="{{ route('admin.taxonomy.term.edit', $list->id) }}" class="eac" data-toggle="tooltip" data-placement="top" title="Редактировать"><i class="fa fa-fw fa-edit"></i></a>    
                    @endif
                    <a href="{{ route('admin.taxonomy.term.create') }}?taxonomy_id={{ $list->taxonomy_id }}&term={{ $list->id }}" class="eac" data-toggle="tooltip" data-placement="top" title="Новый"><i class="fa fa-fw fa-file-text-o"></i></a>
                </div>
                @include('taxonomy::admin.terms.lists', ['lists' => $list->children])
            </li>
        @else
            <li class="dd-item" data-id="{{ $list->id }}">
                <div class="dd-handle dd3-handle"><i class="fa fa-bars"></i></div>
                <div class="dd-content">
                    <a href="">{{ $list->name }} :: {{ $list->id }}</a>
                    @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('taxonomy.delete'))
                    <a
                    href="javascript:" 
                    data-href="{{ route('admin.taxonomy.term.destroy', $list->id) }}"
                    data-title="Удалить ссылку?"
                    data-message="Это действие не обратимо."
                    data-method="DELETE"
                    data-toggle="tooltip"
                    data-placement="top"
                    data-button1="Да"
                    data-button2="Нет"
                    title="Удалить"
                    class="eac delete"><i class="fa fa-fw fa-trash-o"></i></a>
                    @endif
                    @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('taxonomy.edit'))
                    <a href="{{ route('admin.taxonomy.term.edit', $list->id) }}" class="eac" data-toggle="tooltip" data-placement="top" title="Редактировать"><i class="fa fa-fw fa-edit"></i></a>    
                    @endif
                    <a href="{{ route('admin.taxonomy.term.create') }}?taxonomy_id={{ $list->taxonomy_id }}&term={{ $list->id }}" class="eac" data-toggle="tooltip" data-placement="top" title="Новый"><i class="fa fa-fw fa-file-text-o"></i></a>
                </div>
            </li>
        @endif
    @endforeach
</ol>