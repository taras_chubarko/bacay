@extends('admin::layouts.master')

@section('content')

<section class="content-header">
	<h1>{{ MetaTag::set('title', 'Словари таксономии') }}</h1>
	<ol class="breadcrumb">
		<li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
		<li class="active">Словари таксономи</li>
	</ol>
</section>
  
<section class="content">

	<div class="box">
		<div class="box-header with-border">
			@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('taxonomy.create'))
				<a class="btn btn-success btn-flat" href="{{ route('admin.taxonomy.create') }}">Создать</a>
			@endif
		</div>
		
		<div class="box-body">
			@if($taxonomy->count())
			<table class="table table-bordered b-c-222D32">
				<thead>
					<tr>
						<th width="10">#</th>
						<th>Название меню</th>
						<th width="100">Действие</th>
					</tr>
				</thead>
				<tbody>
					@foreach($taxonomy as $item)
					<tr>
						<td>{{ $item->id }}</td>
						<td>
						@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('taxonomy.create'))
							<a href="{{ route('admin.taxonomy.show', $item->id) }}">{{ $item->name }}</a>
						@else
							{{ $item->name }}
						@endif
						</td>
						<td>
							@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('taxonomy.edit'))
								<a href="{{ route('admin.taxonomy.edit', $item->id) }}" class="btn btn-sm bg-navy btn-flat"><i class="fa fa-fw fa-edit"></i></a>
							@endif
							@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('taxonomy.delete'))
								@include('cms::admin.delete', ['route' => 'admin.taxonomy.destroy', 'id' => $item->id])
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@else
				<p>Нет словарей.</p>
			@endif
		  
		</div><!-- /.box-body -->
		
		<div class="box-footer">
		 
		</div><!-- /.box-footer-->
	</div>

</section>

@stop