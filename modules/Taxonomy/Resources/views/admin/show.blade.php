@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', $taxonomy->name) }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">{{ $taxonomy->name }}</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            <a href="{{ route('admin.taxonomy.term.create') }}?taxonomy_id={{ $taxonomy->id }}" class="btn btn-success btn-flat">Добавить термин</a>
            <a href="javascript:" data-action="collapseAll" class="btn btn-flat btn-s-md btn-danger m-r-20 anes"><i class="fa fa-arrow-circle-o-up"></i> Свернуть</a>
            <a href="javascript:" data-action="expandAll" class="btn btn-flat btn-s-md btn-warning anes"><i class="fa fa-arrow-circle-o-down"></i> Развернуть</a>
        </div>
        
        <div class="box-body">
            @if($taxonomy->terms->count())
                <div id="tree-admin" class="dd dd-term">
                    @include('taxonomy::admin.terms.lists', ['lists' => $taxonomy->terms->toHierarchy()])
                </div>
            @else
                <p>Нет терминов таксономии.</p>
            @endif
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>

@stop