<?php namespace Modules\Taxonomy\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Baum;

class TaxonomyTerm extends Baum\Node implements SluggableInterface {
    
    use SluggableTrait;
    
    protected $fillable = [];
    
    protected $table = 'taxonomy_term';
    
    protected $scoped = array('taxonomy_id');
    
    protected $orderColumn = 'sort';
    
    protected $depthColumn = 'depth';
    
    protected $guarded = ['_token'];
    
    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
        'on_update'  => true,
    ];
    
    
    public static function itemsArray($taxonomy_id) {
       // TaxonomyTerm::rebuild();
        $items = TaxonomyTerm::where('taxonomy_id', $taxonomy_id)->get()->toHierarchy()->toArray();
        
        $menus = TaxonomyTerm::parseArray($items);
        
        $arr = [];
        $arr[0] = '- Корень -';
        
        foreach($menus as $item)
        {
            $depth = str_repeat('-', $item['depth']);
            
            $arr[$item['id']] = $depth . $item['name'];
        }
        
        return $arr;
    }
    
    public static function parseArray($parseArray, $parentID = 0)
    {
        $return = array();
       
        $depth = 0;
        foreach ($parseArray as $subArray) {
            $returnSubSubArray = array();
            
            if (isset($subArray['children'])) {
                //$depth =
                $depth += 1;
                $returnSubSubArray = TaxonomyTerm::parseArray($subArray['children'], $subArray['id']);
            }
            
            $return[] = array('id' => $subArray['id'],
                              'parent_id' => $parentID,
                              'name' => $subArray['name'],
                              'depth' => ($subArray['taxonomy_id'] == 1) ? $subArray['depth']-320 : $subArray['depth'],
                              );
            $return = array_merge($return, $returnSubSubArray);
            //$depth++;
        }
        return $return;
    }
    /* public function get
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function get($taxonomy_id, $parent_id)
    {
        $items = TaxonomyTerm::where('taxonomy_id', $taxonomy_id)->where('parent_id', $parent_id)->get();
        
        $terms = array();
        
        foreach($items as $item)
        {
            $terms[$item->id] = $item->name;
        }
        
        return $terms;
    }
    
    /* public function allSlug
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function allSlug() {
        
        $slugs = TaxonomyTerm::all();
        
        $items = [];
        
        
        if($slugs->count())
        {
            foreach($slugs as $slug)
            {
                $items[] = $slug->slug;
            }
            
            $route = implode('|', $items);
            
        }
        else
        {
            $route = '[a-z]+';
        }
        
        return $route;
        
    }
    
    /* public function getCountCategoryAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCountCategoryAttribute()
    {
        $count = \DB::table('advert')->where('status', 1)
        ->leftJoin('advert_category', 'advert.id', '=', 'advert_category.advert_id')
        ->where('advert_category.category', $this->id)
        ->count();
        return $count;
    }
    
    public static function itemsArray2($taxonomy_id) {
       // TaxonomyTerm::rebuild();
        $items = TaxonomyTerm::where('taxonomy_id', $taxonomy_id)->get()->toHierarchy()->toArray();
        
        $menus = TaxonomyTerm::parseArray($items);
        
        $arr = [];
        
        foreach($menus as $item)
        {
            $depth = str_repeat('-', $item['depth']);
            
            $arr[$item['id']] = $depth . $item['name'];
        }
        
        return $arr;
    }
   

}