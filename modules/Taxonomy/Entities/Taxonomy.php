<?php namespace Modules\Taxonomy\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Taxonomy extends Model {

    protected $fillable = [];
    
    protected $table = 'taxonomy';
    
    protected $guarded = ['_token'];
    
    public function terms()
    {
        return $this->hasMany('TaxonomyTerm')->orderBy('sort', 'ASC');
    }

}