<?php

Route::group(['middleware' => 'web', 'prefix' => 'taxonomy', 'namespace' => 'Modules\Taxonomy\Http\Controllers'], function()
{
	Route::get('/', 'TaxonomyController@index');
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\Taxonomy\Http\Controllers'], function()
{
	Route::resource('taxonomy', 'TaxonomyController');
	Route::resource('taxonomy/term', 'TaxonomyTermController');
	Route::post('taxonomy/term/sort', 'TaxonomyTermController@sort');
});