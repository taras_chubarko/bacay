<?php namespace Modules\Taxonomy\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class TaxonomyTermController extends Controller {
	
	/* public function create
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function create() {
		return view('taxonomy::admin.terms.create');
	}
	
	/* public function store
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function store(\Modules\Taxonomy\Http\Requests\CreateItemReq $request) {
		
		if($request->parent_id == 0)
		{
			$root = \TaxonomyTerm::create([
				'taxonomy_id' 	=> $request->taxonomy_id,
                'name' 		=> $request->name,
				'sort' 		=> $request->sort,
				'ico' 		=> $request->ico,
            ]);
		}
		else
		{
			$child = \TaxonomyTerm::create([
				'taxonomy_id' 	=> $request->taxonomy_id,
                'name' 		=> $request->name,
				'sort' 		=> $request->sort,
				'ico' 		=> $request->ico,
            ]);
			$child->makeChildOf($request->parent_id);
		}
		
		return redirect()->route('admin.taxonomy.show', $request->taxonomy_id)->with('success', 'Термин добавлен.');
		
		
	}
	
	/* public function edit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function edit($id) {
		
		$taxonomy_term = \TaxonomyTerm::findOrFail($id);
		
		return view('taxonomy::admin.terms.edit', compact('taxonomy_term'));
	}
	
	/* public function update
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function update(\Modules\Taxonomy\Http\Requests\CreateItemReq $request, $id) {
		
		$taxonomy_term = \TaxonomyTerm::findOrFail($id);
		$taxonomy_term->name 		= $request->name;
		$taxonomy_term->parent_id 	= $request->parent_id;
		$taxonomy_term->sort 		= $request->sort;
		$taxonomy_term->ico 		= $request->ico;
		$taxonomy_term->save();
		
		return redirect()->route('admin.taxonomy.show', $taxonomy_term->taxonomy_id)->with('success', 'Термин обновлен.');
	}
	
	/* public function destroy
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function destroy($id) {
		
		$taxonomy_term = \TaxonomyTerm::findOrFail($id);
		$taxonomy_term->delete();
		
		return redirect()->route('admin.taxonomy.show', $taxonomy_term->taxonomy_id)->with('success', 'Термин удален.');
	}
	
	
	/* public function sort
	 * @param 
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function sort(Request $request) {
		
		$items = $this->parseJsonArray($request->sort);
        
		foreach($items as $key => $item)
		{
			$menu = \TaxonomyTerm::findOrFail($item['id']);
			
			if($item['parent_id'] == 0)
			{
				$menu->makeRoot();
				$menu->sort = $key;
				$menu->save();
			}
			else
			{
				$menu->sort = $key;
				$menu->parent_id = $item['parent_id'];
				$menu->save();
			}
			
		}
		
		return 'OK'; // All the Input fields
		
	}
	
	
	public function parseJsonArray($jsonArray, $parentID = 0)
	{
	    $return = array();
	    foreach ($jsonArray as $subArray) {
		$returnSubSubArray = array();
		if (isset($subArray['children'])) {
		    $returnSubSubArray = $this->parseJsonArray($subArray['children'], $subArray['id']);
		}
		$return[] = array('id' => $subArray['id'], 'parent_id' => $parentID);
		$return = array_merge($return, $returnSubSubArray);
	    }
	    return $return;
	}
	
}