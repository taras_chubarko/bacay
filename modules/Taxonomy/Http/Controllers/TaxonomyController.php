<?php namespace Modules\Taxonomy\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class TaxonomyController extends Controller {
	
	public function index()
	{
		$taxonomy = \Taxonomy::all();
		
		return view('taxonomy::admin.index', compact('taxonomy'));
	}
	
	/* public function create
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function create() {
		return view('taxonomy::admin.create');
	}
	
	/* public function store
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function store(\Modules\Taxonomy\Http\Requests\CreateReq $request) {
		
		\Taxonomy::create($request->all());
		
		return redirect()->route('admin.taxonomy.index')->with('success', 'Новый словарь создан.');
	}
	
	/* public function edit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function edit($id) {
		
		$taxonomy = \Taxonomy::findOrFail($id);
		
		return view('taxonomy::admin.edit', compact('taxonomy'));
	}
	
	/* public function update
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function update(\Modules\Taxonomy\Http\Requests\CreateReq $request, $id) {
		
		$taxonomy = \Taxonomy::findOrFail($id);
		$taxonomy->update($request->all());
		
		return redirect()->route('admin.taxonomy.index')->with('success', 'Словарь таксономии обновлено.');
	}
	
	/* public function destroy
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function destroy($id) {
		
		$taxonomy = \Taxonomy::findOrFail($id);
		\TaxonomyTerm::where('taxonomy_id', $id)->delete();
		$taxonomy->delete();
		
		return redirect()->route('admin.taxonomy.index')->with('success', 'Словарь таксономии удалено.');
	}
	
	/* public function show
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function show($id) {
		
		$taxonomy = \Taxonomy::findOrFail($id);
		
		return view('taxonomy::admin.show', compact('taxonomy'));
	}
	
}