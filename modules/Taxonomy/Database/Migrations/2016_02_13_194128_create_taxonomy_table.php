<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxonomyTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taxonomy', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });
        
        Schema::create('taxonomy_term', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('taxonomy_id')->unsigned()->nullable();
            $table->foreign('taxonomy_id')->references('id')->on('taxonomy')->onDelete('cascade');
            $table->string('name');
            $table->integer('parent_id');
            $table->integer('lft');
            $table->integer('rgt');
            $table->integer('depth');
            $table->integer('sort');
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('taxonomy');
        Schema::drop('taxonomy_term');
    }

}
