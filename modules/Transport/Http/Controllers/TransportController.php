<?php namespace Modules\Transport\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class TransportController extends Controller {
	
	/* public function getType
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function getType(Request $request)
	{
		$types = \TaxonomyTerm::get(1, $request->category);
		$form = view('transport::site.select-type', compact('types'))->render();
		return response()->json(['result' => $form],200);
	}
	/* public function getBrands
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function getBrands(Request $request)
	{
	    $brands = \Transport::getBrands($request->type);
	    $form = view('transport::site.select-brand', compact('brands'))->render();
	    return response()->json(['result' => $form],200);
	}
	/* public function getModels
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function getModels(Request $request)
	{
	    $models = \Transport::getModels($request->brand);
	    $form = view('transport::site.select-model', compact('models'))->render();
	    return response()->json(['result' => $form],200);
	}
	
}