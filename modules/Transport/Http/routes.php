<?php

Route::group(['middleware' => 'web', 'prefix' => 'transport', 'namespace' => 'Modules\Transport\Http\Controllers'], function()
{
	Route::post('get/type', [
		'as' 	=> 'transport.get.type',
		'uses' 	=> 'TransportController@getType'
	]);
	Route::post('get/brands', [
		'as' 	=> 'transport.get.brands',
		'uses' 	=> 'TransportController@getBrands'
	]);
	Route::post('get/models', [
		'as' 	=> 'transport.get.models',
		'uses' 	=> 'TransportController@getModels'
	]);
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\Transport\Http\Controllers'], function()
{
	Route::resource('transport', 'TransportController');
});