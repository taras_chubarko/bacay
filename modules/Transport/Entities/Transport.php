<?php namespace Modules\Transport\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Transport extends Model {

    protected $fillable = [];
    
    /* public function scopeGetBrands
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getBrands($type_id = null)
    {
        switch($type_id)
        {
            case 37:
                $type = 1;
            break;
        
            case 38:
                $type = 6;
            break;
                
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
                $type = 2;
            break;
            
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
                $type = 3;
            break;
            
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
                $type = 4;
            break;
        
            case 51: //Автобусы
                $type = 7;
            break;
        
            case 52: //Автодома :: 52
                $type = 8;
            break;
        
            case 58: //Прицепы :: 58
                $type = 5;
            break;
            
            case 64:
            case 65:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
            case 71:
            case 72:
            case 73:
                $type = 1;
            break;
            
            default:
                
            $type = $type_id;
        }
        
        $brands = \DB::table('transport_brand')->where('type_id', $type)->get();
        $items = array();
        foreach($brands as $brand)
        {
            $items[$brand->id] = $brand->name;
        }
        return $items;
    }
    /* public function scopeGetModels
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getModels($brand_id = null)
    {
        $models = \DB::table('transport_model')->where('brand_id', $brand_id)->get();
        $items = array();
        foreach($models as $model)
        {
            $items[$model->id] = $model->name;
        }
        return $items;
    }
    /* public function getTransmission
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getTransmission()
    {
        $transmissions = \DB::table('transport_transmission')->get();
        $items = array();
        foreach($transmissions as $transmission)
        {
            $items[$transmission->id] = $transmission->name;
        }
        return $items;
    }
    /* public function getDrive
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getDrive()
    {
        $drives = \DB::table('transport_drive')->get();
        $items = array();
        foreach($drives as $drive)
        {
            $items[$drive->id] = $drive->name;
        }
        return $items;
    }
    /* public function getFuel
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getFuel()
    {
        $fuels = \DB::table('transport_fuel')->get();
        $items = array();
        foreach($fuels as $fuel)
        {
            $items[$fuel->id] = $fuel->name;
        }
        return $items;
    }
    

}