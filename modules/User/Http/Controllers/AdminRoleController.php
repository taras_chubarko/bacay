<?php namespace Modules\User\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class AdminRoleController extends Controller {
	
	/* public function __construct
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function __construct()
	{
	    $this->middleware('\Modules\User\Http\Middleware\RolesControlMiddleware', ['only' => ['index']]);
	}
	
	public function index()
	{
		$roles = \DB::table('roles')->get();
		return view('user::admin.roles.index', compact('roles'));
		
	}
	/* public function create
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function create()
	{
		return view('user::admin.roles.create');
	}
	/* public function store
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function store(\Modules\User\Http\Requests\RoleReq $request)
	{
		$role_user = \Sentinel::getRoleRepository()->createModel()->create([
			'name' => $request->name,
			'slug' => str_slug($request->name),
			'permissions' => ['admin' => false]
		]);
		return redirect()->route('admin.roles.index')->with('success', 'Роль создана, можно настроить ее права.');
	}
	/* public function edit
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function edit($id)
	{
		$role = \Sentinel::findRoleById($id);
		return view('user::admin.roles.edit', compact('role'));
	}
	/* public function update
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function update(Request $request, $id)
	{
		$role = \Sentinel::findRoleById($id);
		$role->name = $request->name;
		if(isset($request->permissions))
		{
			foreach($request->permissions as $key => $permission)
			{
				$permissions[$key] = true;
			}
			$role->permissions = $permissions;
		}
		else
		{
			$role->permissions = array('admin' => false);
		}
		$role->save();
		return redirect()->route('admin.roles.index')->with('success', 'Роль обновлена.');
	}
	/* public function destroy
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function destroy($id)
	{
		\DB::table('role_users')
		->where('role_id', $id)
		->update(['role_id' => 2]);
		
		\Sentinel::findRoleById($id)->delete();
		return redirect()->route('admin.roles.index')->with('success', 'Роль удалена.');
	}
	
}