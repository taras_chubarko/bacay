<?php namespace Modules\User\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;
use Sentinel;
use Activation;
use Mail;

class UserController extends Controller {
	
	public function pageUser()
	{
		if(Sentinel::check())
		{
			$user = Sentinel::getUser();
			
			return view('user::site.page-user', compact('user'));
		}
		else
		{
			return view('user::site.page-login');
		}
	}
	
	/* public function pageLogin
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function pageLogin() {
		
		return view('user::site.page-login');
	}
	
	/* public function login
	 * @param $id
	 *-----------------------------------
	 *| catch(\Exception $e) dd(get_class($e));
	 *-----------------------------------
	 */
	
	public function login(\Modules\User\Http\Requests\LoginReq $request) {
		
		try
		{
			$credentials = [
				'email'    => $request->email,
				'password' => $request->password,
			];
			
			$user = Sentinel::authenticate($credentials);
			
			if($user)
			{
				if($user->ban == 1)
				{
					Sentinel::logout(null, true);
					return redirect()->back()->with('error', 'Ваш аккаунт заблокирован администратором.');
				}
				
				session(['location' => [
					'city' => [
					   'country_id'   => $user->location->country_id,
					   'region_id'    => $user->location->region_id,
					   'city_id'      => $user->location->city_id,
					   'name_ru'      => $user->location->name_ru,
					]
				]]);
				
				\Event::fire('user.login', array($user));
				
				if($user->hasAnyAccess(['admin']))
				{
				    return redirect('/admin')->with('success', 'Вы успешно вошли в админку.');
				}
				return redirect()->back()->with('success', 'Вы успешно вошли на сайт.');
			}
			
			return redirect()->back()->with('error', 'Не верный логин или пароль');
		}
		catch(\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e){
			return redirect()->back()->with('error', 'Пользователь не активирован.');
		}
		catch(\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e){
			return redirect()->back()->with('error', 'Пользователь заблокирован.');
		}
		
	}
	
	/* public function pageRegister
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function pageRegister() {
		
		return view('user::site.page-register');
	}
	
	/* public function register
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function register(\Modules\User\Http\Requests\RegisterReq $request) {
		
		
		$credentials = [
			'name'		=> $request->name,	
			'email'    	=> $request->email,
			'password' 	=> $request->password,
		];
		
		$user = Sentinel::register($credentials);
		
		$role = Sentinel::findRoleById(2); /*2 - Регистрируем обычного пользователя*/
		$role->users()->attach($user);
		
		$user->pivotProfile()->attach($user->id, [
			'first_name' 	=> $request->name,
			'last_name' 	=> '',
			'country_id'	=> session('location.city.country_id'),
			'region_id'	=> session('location.city.region_id'),
			'city_id'	=> session('location.city.city_id'),
			'city' 		=> session('location.city.name_ru'),
			'phone' 	=> '',
			'photo_big' 	=> '',
			'news' 	        => 0,
		]);
		    
		$activation = Activation::create($user);
		
		$data = array();
		$data['code'] = $activation->code;
		$data['id'] = $user->id;
		
		
		
		Mail::later(5, 'user::site.mail-register-user', $data, function($message) use ($credentials)
		{
		    $message->to($credentials['email'])->subject('Регистрация на сайте "'.config('cms.name').'"');
		});
		
		Mail::later(10, 'user::site.mail-register-admin', $credentials, function($message)
		{
		    $message->to(\Option::get('admin.mail'))->subject('Новая регистрация на сайте "'.config('cms.name').'"');
		});
		
		return redirect('/')->with('success', 'Спасибо за регистрацию! На вашу почту отправлено письмо с дальнейшими инструкциями.');
		
	}
	
	/* public function logout
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function logout() {
		Sentinel::logout(null, true);
		\Session::forget('user');
		return redirect()->back();
	}
	
	/* public function pagePassword
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function pagePassword() {
		
		if (\Session::has('reminder'))
		{
		  return view('user::site.page-newpassword');
		}
		return view('user::site.page-password');
	}
	
	/* public function password
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function password(\Modules\User\Http\Requests\PasswordReq $request) {
		
		$credentials = [
			'email' => $request->email,
		];
		    
		$user = Sentinel::findByCredentials($credentials);
		    
		if($user)
		{
		    $reminder = \Reminder::create($user);
		    $code = ['code' => $reminder->code];
		    
		    Mail::later(10, 'user::site.mail-reminder', $code, function($message) use ($user)
		    {
			$message->to($user->email)->subject('Восстановление пароля на сайте "'.config('cms.name').'"');
		    });
		    
		    \Session::put('reminder', $request->email);
		    return redirect()->back()->with('success', 'Код для восстановления пароля отправлен вам на почту.');
		}
		else
		{
		    return redirect()->back()->with('error', 'Пользователь с такой почтой не зарегистрирован.');
		}
	}
	
	/* public function passwordNew
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function passwordNew(\Modules\User\Http\Requests\PasswordNewReq $request) {
		
		$credentials = [
			'email' => \Session::get('reminder'),
		];
		
		$user = Sentinel::findByCredentials($credentials);
		
		if(isset($user))
		{
		    if ($reminder = \Reminder::complete($user, $request->code, $request->password))
		    {
			\Session::forget('reminder');
			Sentinel::login($user);
			return redirect('/')->with('success', 'Изминения сохранены.');
		    }
		    else
		    {
			return redirect('/')->with('error', 'Нельзя восстановить пароль для этого пользователя. Свяжитесь с администратором сайта.');
		    }
		    
		}
		else
		{
		    return redirect()->back()->with('error', 'Пользователь с такой почтой не зарегистрирован.');
		}
	}
	
	/* public function activation
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function activation($id, $code) {
		
		$user = Sentinel::findById($id);
		
		if (Activation::complete($user, $code))
		{
		    Sentinel::login($user);
		    
		    return redirect('/user')->with('success', 'Вы успешно активировали свой аккаунт.');
		}
		else
		{
		    return redirect('/')->with('info', 'Код активации просрочен или использован.');
		}
	}
	
	/* public function ulogin
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function ulogin(\Modules\User\Http\Requests\UloginReq $request)
	{
		$ulogin = json_decode(
			file_get_contents('http://ulogin.ru/token.php?token=' . $request->arg . '&host=' . $_SERVER['HTTP_HOST']),
			true
		);
		
		$valid = ['email' => $ulogin['email']];
		$userValidate = Sentinel::findByCredentials($valid);
		
		if($userValidate)
		{
			return response()->json(['msg' => 'Пользователь с таким e-mail уже зарегистрирован. Попробуйте возпользоваться формой входа.'], 422);
		}
		else
		{
			$password = str_random(8);
			$credentials = [
				'name' 	   => $ulogin['first_name'],
				'email'    => $ulogin['email'],
				'password' => $password,
			];
			
			$user = Sentinel::registerAndActivate($credentials);
			
			$role = Sentinel::findRoleById(2); /*2 - Регистрируем обычного пользователя*/
			$role->users()->attach($user);
			
			$user->pivotProfile()->attach($user->id, [
				'first_name' 	=> $ulogin['first_name'],
				'last_name' 	=> $ulogin['last_name'],
				'photo_big' 	=> $ulogin['photo_big'],
				'network'	=> $ulogin['network'],
				'city' 		=> $ulogin['city'],
				'phone' 	=> $ulogin['phone'],
				'uid' 		=> $ulogin['uid'],
				'profile' 	=> $ulogin['profile'],
				'identity' 	=> $ulogin['identity'],
				'news'      => 1,
			]);
			
			Sentinel::login($user);
			
			\Session::flash('info', 'Советуэм вам установить пароль для дальнейшего пользвания вашим профилем.');
		}
	}
	/* public function pageUserEdit
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function pageUserEdit($id)
	{
	    $user = Sentinel::findById($id);
	    return view('user::site.page-user-edit', compact('user'));
	}
	/* public function pageUserEditPost
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function pageUserEditPost(\Modules\User\Http\Requests\UserEditReq $request, $id)
	{
		$user = Sentinel::findById($id);
		
		$credentials['email'] = $request->email;
		if($request->password)
		{
			$credentials['password'] = $request->password;
			Mail::later(5, 'user::site.mail-new-password', $credentials, function($message) use ($credentials)
			{
			    $message->to($credentials['email'])->subject('Смена пароля на сайте "'.config('cms.name').'"');
			});
			
		}
		$user = Sentinel::update($user, $credentials);
		
		$city = \City::where('name_ru', $request->city)->first();
		session(['location' => [
			'city' => [
			   'country_id'   => $city->country_id,
			   'region_id'    => $city->region_id,
			   'city_id'      => $city->city_id,
			   'name_ru'      => $city->name_ru,
			]
		]]);
		
		
		$user->pivotProfile()->detach($user->id);
		$user->pivotProfile()->attach($user->id, [
			'first_name' 	=> $request->first_name,
			'last_name' 	=> $request->last_name,
			'country_id'	=> $city->country_id,
			'region_id'	=> $city->region_id,
			'city_id'	=> $city->city_id,
			'city' 		=> $city->name_ru,
			'phone' 	=> $request->phone,
			'photo_big' 	=> $request->photo_big,
			'news' 	        => isset($request->news) ? $request->news : 0,
		]);
		
		return redirect('/user')->with('success', 'Профиль обновлен.');
	}
	
}