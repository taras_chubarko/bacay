<?php namespace Modules\User\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class AdminUserController extends Controller {
	
	public function index()
	{
		$users = \User::adminUsers([2]);
		return view('user::admin.users.index', compact('users'));
	}
    /* public function userRoleId
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function userRoleId($id)
    {
        $users = \User::adminUsers([$id]);
		return view('user::admin.users.index', compact('users'));
    }
    /* public function userBan
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function userBan()
    {
        $users = \User::adminUsers([], 1);
		return view('user::admin.users.index', compact('users'));
    }
	/* public function edit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function create()
	{
		return view('user::admin.users.create');
	}
	/* public function store
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function store(\Modules\User\Http\Requests\CreateUserReq $request)
	{
		$name = !empty($request->last_name) ? $request->first_name.' '.$request->last_name : $request->first_name;
		
		$credentials = [
			'name'		=> $name,	
			'email'    	=> $request->email,
			'password' 	=> $request->password,
		];
		
		$user = \Sentinel::registerAndActivate($credentials);
		
		$role = \Sentinel::findRoleById($request->roles);
		$role->users()->attach($user);
		
		$user->pivotProfile()->attach($user->id, [
			'first_name' 	=> $request->first_name,
			'last_name' 	=> $request->last_name,
		]);
		
		return redirect()->route('admin.users.index')->with('success', 'Пользователь создан.');
	}
	/* public function edit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function edit($id)
	{
		$user = \Sentinel::findById($id);
		return view('user::admin.users.edit', compact('user'));
	}
	/* public function update
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function update(\Modules\User\Http\Requests\UpdateUserReq $request, $id)
	{
            $user = \Sentinel::findById($id);
            $credentials['email'] = $request->email;
            if($request->password)
            {
                $credentials['password'] = $request->password;
            }
            $user = \Sentinel::update($user, $credentials);
            
            $roleDel = \Sentinel::findRoleById($user->roles[0]->id);
            $roleDel->users()->detach($user);
            
            $role = \Sentinel::findRoleById($request->roles);
            $role->users()->attach($user);
            
            \DB::table('profile')
            ->where('user_id', $id)
            ->update([
                    'first_name' 	=> $request->first_name,
                    'last_name' 	=> $request->last_name,   
            ]);
            
            return redirect()->route('admin.users.index')->with('success', 'Пользователь обновлен.');
	}
	/* public function destroy
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function destroy($id)
	{
	    $adverts = \Advert::where('user_id', $id)->get();
	    foreach($adverts as $advert)
	    {
		\Advert::deleteOne($advert->id);
	    }
	    \Wallet::where('user_id', $id)->delete();
	    
	    $baners = \Baners::where('user_id', $id)->get();
	    foreach($baners as $baner)
	    {
		\Baners::deleteOne($baner->id);
	    }
	    
	    
	    $user = \Sentinel::findById($id);
	    $user->pivotProfile()->detach($user->id);
	    $user->delete();
	    
	    return redirect()->route('admin.users.index')->with('success', 'Пользователь удален.');
	}
	/* public function ban
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function ban($id)
	{
		$user = \User::find($id);
		$user->ban = 1;
		$user->save();
		return redirect()->back()->with('success', 'Пользователь заблокирован.');
	}
	/* public function unban
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function unban($id)
	{
		$user = \User::find($id);
		$user->ban = 0;
		$user->save();
		return redirect()->back()->with('success', 'Пользователь разблокирован.');
	}
	
	
	
}