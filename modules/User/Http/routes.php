<?php

Route::group(['middleware' => 'web', 'prefix' => 'user', 'namespace' => 'Modules\User\Http\Controllers'], function()
{
	Route::get('/', [
		'as' => 'user',
		'uses' => 'UserController@pageUser'
	]);
	
	Route::get('{slug}', [
		'as' => 'user.slug',
		'uses' => 'UserController@pageUser'
	])->where('slug', User::allSlug());
	
	Route::get('login', [
		'middleware' => 'guest',
		'as' => 'user.login',
		'uses' => 'UserController@pageLogin'
	]);
	
	Route::post('login', [
		'middleware' => 'guest',
		'as' => 'user.login',
		'uses' => 'UserController@login'
	]);
	
	Route::get('register', [
		'middleware' => 'guest',
		'as' => 'user.register',
		'uses' => 'UserController@pageRegister'
	]);
	
	Route::post('register', [
		'middleware' => 'guest',
		'as' => 'user.register',
		'uses' => 'UserController@register'
	]);
	
	Route::get('logout', [
		'as' => 'user.logout',
		'uses' => 'UserController@logout'
	]);
	
	Route::get('password', [
		'middleware' => 'guest',
		'as' => 'user.password',
		'uses' => 'UserController@pagePassword'
	]);
	
	Route::post('password', [
		'middleware' => 'guest',
		'as' => 'user.password',
		'uses' => 'UserController@password'
	]);
	
	Route::put('password', [
		'middleware' => 'guest',
		'as' => 'user.password',
		'uses' => 'UserController@passwordNew'
	]);
	
	Route::get('activation/{id}/{code}', [
		'middleware' => 'guest',
		'as' => 'user.activation',
		'uses' => 'UserController@activation'
	]);
	
	Route::post('ulogin', [
		'middleware' => 'guest',
		'as' => 'user.ulogin',
		'uses' => 'UserController@ulogin'
	]);
	
	Route::get('{id}/edit', [
		'as' => 'user.edit',
		'uses' => 'UserController@pageUserEdit'
	]);
	
	Route::post('{id}/edit', [
		'as' => 'user.edit.post',
		'uses' => 'UserController@pageUserEditPost'
	]);
	
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\User\Http\Controllers'], function()
{
	Route::resource('users', 'AdminUserController');
	Route::resource('roles', 'AdminRoleController');
	
	Route::post('users/{id}/ban', [
		'as' => 'admin.users.ban',
		'uses' => 'AdminUserController@ban'
	]);
	
	Route::post('users/{id}/unban', [
		'as' => 'admin.users.unban',
		'uses' => 'AdminUserController@unban'
	]);
    
    Route::get('users/role/{id}', [
		'as' => 'admin.users.role.id',
		'uses' => 'AdminUserController@userRoleId'
	]);
    
    Route::get('users/ban/ban', [
		'as' => 'admin.users.role.ban',
		'uses' => 'AdminUserController@userBan'
	]);
	
	
});

