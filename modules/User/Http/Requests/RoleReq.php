<?php namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' 	=> 'required',
			//'slug'  => 'required',
		];
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		$validator->setAttributeNames([
		    'name' => '"Название"',
		    //'slug' => '"Slug"',
		]);
	 
		return $validator;
	}

}
