<?php namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name'  		=> 'required',
			'email' 		=> 'required|email|unique:users',
			'password'  		=> 'required|min:6|confirmed',
			'password_confirmation' => 'required|min:6',
			'rules' 		=> 'required',
			//'captcha' 		=> 'required|captcha',
		];
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		$validator->setAttributeNames([
			'name' 			=> '"Ваше имя"',
			'email' 		=> '"Ваш e-mail"',
			'password' 		=> '"Пароль"',
			'password_confirmation' => '"Повтор пароля"',
			'rules' 		=> '"Вы должны принять правила"',
			//'captcha' 	=> '"Код с картинки"',
		]);
	 
		return $validator;
	}

}
