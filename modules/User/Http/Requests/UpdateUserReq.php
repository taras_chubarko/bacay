<?php namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'first_name'  		    => 'required',
			'email' 		        => 'required|email',
			'password'  		    => 'min:6|confirmed',
			'password_confirmation' => 'min:6',
			'roles' 		=> 'required',
		];
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		$validator->setAttributeNames([
			'name' 			=> '"Имя"',
			'email' 		=> '"Почта"',
			'password' 		=> '"Пароль"',
			'password_confirmation' => '"Повтор пароля"',
			'roles' 		=> '"Роль"',
		]);
	 
		return $validator;
	}

}
