<?php namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserEditReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'first_name'  	=> 'required',
			'email' 	=> 'required|email',
			'phone'  	=> 'required',
			'city'  	=> 'required',
		];
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		$validator->setAttributeNames([
			'first_name' 	=> '"Имя"',
			'email' 	=> '"E-mail"',
			'phone' 	=> '"Телефон"',
			'city' 		=> '"Город"',
		]);
	 
		return $validator;
	}

}
