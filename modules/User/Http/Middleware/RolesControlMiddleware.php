<?php namespace Modules\User\Http\Middleware; 

use Closure;

class RolesControlMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if (\Sentinel::guest())
        {
            return redirect()->guest('/');
        }
        else
        {
            $user = \Sentinel::getUser();
            
            if($user->inRole('admin') || $user->hasAccess('roles.control'))
            {
                return $next($request);
            }
            else
	    {
		abort(403, 'У вас нет доступа к управлению ролями.');
	    }
        }
    }
    
}
