<?php namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$rore_admin = \Sentinel::getRoleRepository()->createModel()->create([
			'name' => 'Админ',
			'slug' => 'admin',
			'permissions' => ['admin' => true]
		]);
		
		$role_user = \Sentinel::getRoleRepository()->createModel()->create([
			'name' => 'Пользователь',
			'slug' => 'user',
			'permissions' => ['admin' => false]
		]);
		
		//---------------------------------------------
		$credentials_admin = [
			'email'    => 'tchubarko@gmail.com',
			'password' => '159753',
			'name' 	   => 'admin',
		];
		    
		$admin = \Sentinel::create($credentials_admin);
		$rore_admin->users()->attach($admin);
		
		$activation = \Activation::create($admin);
		\Activation::complete($admin, $activation->code);
		
		
		//---------------------------------------------
		$credentials_user = [
			'email'    => 'user@user.com',
			'password' => '123456',
			'name' 	   => 'user',
		];
		    
		$user = \Sentinel::create($credentials_user);
		$role_user->users()->attach($user);
		
		$activation1 = \Activation::create($user);
		\Activation::complete($user, $activation1->code);
	}

}