@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Создать пользователя') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Создать пользователя</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
	    
        </div><!-- /.box-header-->
        
        <div class="box-body">
            {!! Form::open(array('route' => 'admin.users.store', 'role' => 'form', 'class' => 'form')) !!}
                
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">Почта</label>
                    {!! Form::email('email', null, array('class' => 'form-control')) !!}
                </div>
                
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name">Имя</label>
                            {!! Form::text('first_name', null, array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name">Фамилия</label>
                            {!! Form::text('last_name', null, array('class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>
                    
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password">Пароль</label>
                            {!! Form::password('password', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password_confirmation">Повтор пароля</label>
                            {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}
                        </div>
                    </div>
                </div>
                    
                <div class="form-group{{ $errors->has('roles') ? ' has-error' : '' }}">
                    <label for="roles">Роль</label>
                    {!! Form::select('roles', User::getRolesArr(), null, array('class' => 'form-control selectpicker', 'title' => '- Выбрать роль -')) !!}
                </div> 
                
                    
                {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
            
            {!! Form::close() !!}
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div><!-- /.box-->

</section>
@stop