<div id="admin-advert-filter">
    {!! Form::open(array('url' => Request::url(), 'role' => 'form', 'class' => 'form', 'method' => 'GET')) !!}
        <div class="row">
            <div class="col-lg-3">
                <div class="form-group">
                    <label for="name">Имя</label>
                    {!! Form::text('name', Request::get('name'), array('class' => 'form-control')) !!}
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label for="email">Почта</label>
                    {!! Form::email('email', Request::get('email'), array('class' => 'form-control')) !!}
                </div>
            </div>
            <div class="col-lg-3" style="padding-top:25px;">
                {!! Form::submit('Применить', array('class' => 'btn btn-primary')) !!}
                <a class="btn btn-info" href="{{ Request::url() }}">Сбросить</a>
            </div>
        </div>
    {!! Form::close() !!}
</div>