@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Пользователи') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Пользователи</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('users.create'))
            <a class="btn btn-success btn-flat m-b-20" href="{{ route('admin.users.create') }}">Добавить пользователя</a>
            @endif
            @include('user::admin.users.tabs')
            @include('user::admin.users.filter')
        </div>
        
        <div class="box-body">
            @if($users->count())
            <table class="table table-bordered b-c-222D32">
                <thead>
                    <tr>
                        <th width="10">#</th>
                        <th>Почта / Логин</th>
                        <th>Имя</th>
                        <th width="150">Дата регистрации</th>
                        <th>Роль</th>
                        <th>Статус</th>
                        <th width="140">Действие</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td><a href="{{ route('user.slug', $item->slug) }}">{{ $item->email }}</a></td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->date }}</td>
                        <td>{{ $item->roles[0]->name }}</td>
                        <td>{!! $item->statuss !!}</td>
                        <td>
                            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('users.ban'))
                            @if($item->ban == 0)
                                <a class="btn btn-sm bg-orange btn-flat delete" data-method="POST" data-button2="Нет" data-button1="Да" data-message="Вы действительно хотете заблокировать пользователя?" data-title="Блокировка пользователя" data-href="{{ route('admin.users.ban', $item->id) }}" href="javascript:">
                                    <i class="fa fa-lock"></i>
                                </a>
                            @endif
                            
                            @if($item->ban == 1)
                                <a class="btn btn-sm bg-orange btn-flat delete" data-method="POST" data-button2="Нет" data-button1="Да" data-message="Вы действительно хотете разблокировать пользователя?" data-title="Разблокировка пользователя" data-href="{{ route('admin.users.unban', $item->id) }}" href="javascript:">
                                    <i class="fa fa-unlock"></i>
                                </a>
                            @endif
                            @endif
                                
                            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('users.edit'))
                                <a href="{{ route('admin.users.edit', $item->id) }}" class="btn btn-sm bg-navy btn-flat"><i class="fa fa-fw fa-edit"></i></a>
                            @endif
                            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('users.delete'))
                                @include('cms::admin.delete', ['route' => 'admin.users.destroy', 'id' => $item->id])
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $users->render() !!}
            @else
                <p>Нет пользователей.</p>
            @endif
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop