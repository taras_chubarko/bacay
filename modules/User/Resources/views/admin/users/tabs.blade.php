<ul class="nav nav-tabs m-b-20">
    <li class="{{ active_class(if_uri(['admin/users/role/2', 'admin/users']), 'active') }}"><a href="{{ route('admin.users.role.id', 2) }}">Пользователи <span class="badge">{{ User::countUsersRole(2) }}</span></a></li>
    <li class="{{ active_class(if_uri(['admin/users/role/1']), 'active') }}"><a href="{{ route('admin.users.role.id', 1) }}">Администраторы <span class="badge">{{ User::countUsersRole(1) }}</span></a></li>
    <li class="{{ active_class(if_uri(['admin/users/role/4']), 'active') }}"><a href="{{ route('admin.users.role.id', 4) }}">Модераторы <span class="badge">{{ User::countUsersRole(4) }}</span></a></li>
    <li class="{{ active_class(if_uri(['admin/users/role/3']), 'active') }}"><a href="{{ route('admin.users.role.id', 3) }}">Руководители <span class="badge">{{ User::countUsersRole(3) }}</span></a></li>
    <li class="{{ active_class(if_uri(['admin/users/ban/ban']), 'active') }}"><a href="{{ route('admin.users.role.ban') }}">Заблокированые <span class="badge">{{ User::countUserBan() }}</span></a></li>
</ul>