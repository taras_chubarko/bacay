@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Редактировать роль') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Редактировать роль</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
	    
        </div><!-- /.box-header-->
        
        <div class="box-body">
            {!! Form::open(array('route' => ['admin.roles.update', $role->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'form')) !!}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Название</label>
                    {!! Form::text('name', $role->name, array('class' => 'form-control')) !!}
                </div>
                    
                @foreach(config('user.permissions') as $permission)
                    <div class="box box-primary collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{ $permission['name'] }}</h3>
                            <div class="box-tools pull-right">
                                <button data-widget="collapse" class="btn btn-box-tool" type="button"><i class="fa fa-minus"></i></button>
                            </div>
                          <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <ul class="list-group">
                                @foreach($permission['permissions'] as $key => $perm)
                                <li class="list-group-item">
                                    <label for="{{ $key }}" class="icheck" style="font-weight: normal">
                                        {!! Form::checkbox('permissions['.$key.']', true, isset($role->permissions[$key]) ? true : false) !!}
                                        {{ $perm }}
                                    </label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                @endforeach
                    
                    
                {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
            
            {!! Form::close() !!}
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div><!-- /.box-->

</section>
@stop