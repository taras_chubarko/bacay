@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Роли') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Роли</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
	    @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('roles.create'))
		<a class="btn btn-success btn-flat" href="{{ route('admin.roles.create') }}">Добавить роль</a>
	    @endif
	</div>
        
        <div class="box-body">
            <table class="table table-bordered b-c-222D32">
                <thead>
                    <tr>
                        <th width="10">#</th>
                        <th>Название</th>
                        <th>Slug</th>
                        <th width="120">Действие</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($roles as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
			<td>{{ $item->name }}</td>
			<td>{{ $item->slug }}</td>
                        <td>
			@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('roles.edit'))
			    @if($item->id !=1)
			    <a href="{{ route('admin.roles.edit', $item->id) }}" class="btn btn-sm bg-navy btn-flat"><i class="fa fa-fw fa-edit"></i></a>
			    @endif
			@endif
			@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('roles.delete'))
			    @if($item->id !=1 && $item->id != 2 && $item->slug != 'anonim')
				@include('cms::admin.delete', ['route' => 'admin.roles.destroy', 'id' => $item->id])
			    @endif
			@endif
			</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop