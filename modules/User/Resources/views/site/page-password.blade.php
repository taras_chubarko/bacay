@extends('site::layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <h1 class="text-center">{!! MetaTag::set('title', 'Восстановить пароль') !!}</h1>
                
            {!! Form::open(array('route' => 'user.password', 'role' => 'form', 'class' => 'form m-b-20 p-b-50')) !!}
                
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">Почта</label>
                    {!! Form::email('email', null, array('class' => 'form-control')) !!}
                </div>
                    
                <div class="form-group text-center">
                    {!! Form::submit('Восстановить', array('class' => 'btn btn-default')) !!}
                </div>
            
            {!! Form::close() !!}
        </div>
    </div>
@stop