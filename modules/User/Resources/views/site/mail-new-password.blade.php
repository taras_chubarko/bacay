@extends('site::mail.main')

@section('title')
Вы изменили пароль
@stop

@section('content')
<p><b>Новый пароль:</b> {{ $password }}</p>
@stop