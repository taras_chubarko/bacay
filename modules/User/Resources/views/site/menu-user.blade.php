<div class="aside-nav">
    <ul>
        <li class="aside-icon-1"><a href="{{ route('user.adverts', $user->id) }}">Мои объявления</a></li>
        <li class="aside-icon-2"><a href="{{ route('user') }}">Мой профиль</a></li>
        <li class="aside-icon-3"><a href="{{ route('adverts.favorites') }}">Избранное</a></li>
        <li class="aside-icon-4"><a href="{{ route('wallet') }}">Кошелек</a></li>
    </ul>
</div>