<div class="head-cabinet-panel head-cabinet-close">
    @if(Sentinel::check())
    <ul>
        <li class="head-cabinet-balance"><a href="{{ route('wallet') }}">{{ Sentinel::getUser()->balance }}</a></li>
        <li class="head-cabinet-user"><a href="{{ route('user') }}">{{ Sentinel::getUser()->username }}</a></li>
        <li class="head-cabinet-exit"><a href="{{ route('user.logout') }}">Выйти</a></li>
    </ul>
    @else
        <div class="head-cabiner-link"><a href="{{ route('user') }}">Личный кабинет</a></div>
    @endif
</div>