@extends('site::layouts.master')

@section('content')
<div class="group">
    <div class="row">
        <div class="col-md-3">
            @include('user::site.menu-user', $user)
        </div>
    <div class="col-md-9">
        <div class="push-main-bl m-l-0">
            <div class="row">
                <div class="col-md-4">
                    <div class="user-img">
                        <img src="{{ $user->avatar->image }}" class="img-responsive" alt="{{ $user->username }}">
                    </div>
                    @include('filem::site.user.avatar-field', ['uri' => 'avatar'])
                </div>
                {!! Form::open(array('route' => ['user.edit.post', $user->id], 'role' => 'form', 'class' => 'form')) !!}
                    <div class="col-md-8">
                        <h2>{{ $user->username }}</h2>
                        
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name">Имя</label>
                            {!! Form::text('first_name', $user->firstname, array('class' => 'form-control')) !!}
                        </div>
                            
                        <div class="form-group">
                            <label for="last_name{{ $errors->has('last_name') ? ' has-error' : '' }}">Фамилия</label>
                            {!! Form::text('last_name', $user->lastname, array('class' => 'form-control')) !!}
                        </div>
                        
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">E-mail</label>
                            {!! Form::email('email', $user->email, array('class' => 'form-control')) !!}
                        </div>
                            
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone">Телефон</label>
                            {!! Form::text('phone', $user->phone, array('class' => 'form-control phone')) !!}
                        </div>
                            
                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="phone">Город</label>
                            {!! Form::text('city', $user->city, array('class' => 'form-control typeahead-city')) !!}
                        </div>
                            
                        <div class="form-group">
                            <label for="password">Новый пароль</label>
                            {!! Form::password('password', array('class' => 'form-control')) !!}
                        </div>
                            
                        <div class="form-group">
                            <label for="news">{!! Form::checkbox('news', 1, $user->news, ['class' => 'styler']) !!} Подписка на раcсылку</label>
                        </div>
                            
                        {!! Form::hidden('photo_big', $user->avatar->image) !!}
                        {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@stop