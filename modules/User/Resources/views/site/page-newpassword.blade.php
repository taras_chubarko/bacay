@extends('site::layouts.master')

@section('content')
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <h1 class="text-center">{!! MetaTag::set('title', 'Восстановить пароль') !!}</h1>
                
            {!! Form::open(array('route' => 'user.password', 'method' => 'PUT', 'role' => 'form', 'class' => 'form', 'id' => 'user-password-new-form')) !!}
            
            <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                <label for="code">Код</label>
                {!! Form::text('code', null, array('class' => 'form-control')) !!}
            </div>
                
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password">Новый пароль</label>
                {!! Form::password('password', array('class' => 'form-control')) !!}
            </div>
                
            <div class="form-group">
                {!! Form::submit('Сохранить', array('class' => 'btn big btn-default')) !!}
            </div>
            
            {!! Form::close() !!}
            
        </div>
    </div>
@stop