@extends('site::layouts.master')

@section('content')
    <div class="dotted-bg wrapper-login-form">
        {!! Form::open(array('route' => 'user.login', 'role' => 'form', 'class' => 'form')) !!}
            <div class="login-form">
                <span class="form-group-tt">{!! MetaTag::set('title', 'Вход в личный кабинет') !!}</span>
                <div class="form-group-element{{ $errors->has('email') ? ' has-error' : '' }}">
                  <div class="user-icon-input icon-input">
                    <span></span>
                    {!! Form::text('email', null, array('placeholder' => 'Электронная почта')) !!}
                  </div>
                </div>
                <div class="form-group-element{{ $errors->has('password') ? ' has-error' : '' }}">
                  <div class="pass-icon-input icon-input">
                    <span></span>
                    {!! Form::password('password', ['placeholder' => 'Пароль']) !!}
                  </div>
                </div>
                <div class="form-group-element group">
                  <div class="check-login-form">
                    <input type="checkbox" id="l-ch-1" class="styler">
                    <label for="l-ch-1">Чужой компьютер</label>
                  </div>
                  <a class="aside-login-link" href="{{ route('user.password') }}">Забыли пароль?</a>
                </div>
                <div class="form-group-element">
                  <div class="text-center">
                    {!! Html::decode(Form::button('Войти', array('class' => 'btn', 'type' => 'submit'))) !!}
                  </div>
                </div>
            </div>
            <div class="reg-login-f">
                <span>Еще не зарегистрированы на Bacay.ru?</span>
                <a href="{{ route('user.register') }}">Зарегистрироваться</a>
            </div>
        {!! Form::close() !!}
    </div>
@stop