@extends('site::layouts.master')

@section('content')
<div class="group">
    <div class="row">
        <div class="col-md-3">
            @include('user::site.menu-user', $user)
        </div>
        <div class="col-md-9">
            <div class="push-main-bl m-l-0">
                <div class="row">
                    <div class="col-md-4">
                        <div class="user-img">
                            <img src="{{ $user->avatar->image }}" class="img-responsive" alt="{{ $user->username }}">
                        </div>
                    </div>
                    <div class="col-md-7">
                        <h2>{{ $user->username }}</h2>
                        <p><label class="w-100">E-mail:</label> {{ $user->email }}</p>
                        <p><label class="w-100">Телефон:</label> {{ $user->phone }}</p>
                        <p><label class="w-100">Город:</label> {{ $user->city }}</p>
                    </div>
                    <div class="col-md-1">
                        <a href="{{ route('user.edit', $user->id) }}" title="Редактировать"><i class="fa fa-pencil-square-o f-sz-28"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop