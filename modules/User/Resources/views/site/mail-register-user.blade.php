@extends('site::mail.main')

@section('title')
Спасибо за регистрацию!
@stop

@section('content')
    <p>Для активации аккаунта нажмите ссылку.</p>
    <p><a href="{{ route('user.activation', [$id, $code]) }}">{{ route('user.activation', [$id, $code]) }}</a></p>
@stop