@extends('site::layouts.master')

@section('content')

<div class="dotted-bg wrapper-registration-form">
    {!! Form::open(array('route' => 'user.register', 'role' => 'form', 'class' => 'form')) !!}
      <div class="registration-form">
        <div class="head-line-form">
          <span class="form-group-tt">{!! MetaTag::set('title', 'Регистрация') !!} <span class="mobile-hidden">нового</span> пользователя</span>
          <p class="text-center">Регистрация на нашем сайте позволит Вам быть его полноценным участником. Вы сможете добавлять объявления, отвечать на вопросы и многое другое.</p>
        </div>
        <div class="reg-group-bl">
          <span class="reg-group-tt">Быстрая регистрация</span>
          <div class="group">
            <div class="form-col">
                <div class="form-wrap-input true-input{{ $errors->has('name') ? ' has-error' : '' }}">
                    {!! Form::text('name', null, array('placeholder' => 'Ваше имя')) !!}
                </div>
                <div class="form-wrap-input true-input{{ $errors->has('email') ? ' has-error' : '' }}">
                    {!! Form::text('email', null, array('placeholder' => 'Ваш e-mail')) !!}
                </div>
            </div>
            <div class="form-col">
                <div class="form-wrap-input false-input{{ $errors->has('password') ? ' has-error' : '' }}">
                    {!! Form::password('password', array('placeholder' => 'Пароль')) !!}
                </div>
                <div class="form-wrap-input false-input{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    {!! Form::password('password_confirmation', array('placeholder' => 'Повторите пароль')) !!}
                </div>
            </div>
          </div>
          <div class="form-group-element{{ $errors->has('rules') ? ' has-error' : '' }}">
            {!! Form::checkbox('rules', 1, true, ['id' => 'r-ch-1']) !!}
            <label for="r-ch-1">Я ознакомился с</label>
            <a href="/pravila">правилами сайта</a>
          </div>
        </div>
        <div class="reg-group-bl">
            <span class="reg-group-tt">Можно через профиль</span>
            <script src="//ulogin.ru/js/ulogin.js"></script>
            <div id="uLogin" data-ulogin="display=buttons;fields=first_name,last_name,email,photo_big,photo,phone,city;callback=ulogins">
                <ul class="reg-soc">
                    <li><a href="#"><img alt="" src="/themes/site/assets/img/reg-vk.png" data-uloginbutton="vkontakte"><img alt="" src="/themes/site/assets/img/reg-mob-vk.png" data-uloginbutton="vkontakte"></a></li>
                    <li><a href="#"><img alt="" src="/themes/site/assets/img/reg-fb.png" data-uloginbutton="facebook"><img alt="" src="/themes/site/assets/img/reg-mob-fb.png" data-uloginbutton="facebook"></a></li>
                    <li><a href="#"><img alt="" src="/themes/site/assets/img/reg-gp.png" data-uloginbutton="google"><img alt="" src="/themes/site/assets/img/reg-mob-gp.png" data-uloginbutton="google"></a></li>
                </ul>
            </div>
        </div>
            
        
        
        <div class="reg-group-bl">
          <div class="group reg-reverse-float">
            <div class="form-col">
                <span class="reg-info">
                  После регистрации зайдите на свой профиль и заполните данные.
                </span>
            </div>
            <div class="form-col">
                {!! Html::decode(Form::button('Зарегистрироваться', array('class' => 'btn btn-st', 'type' => 'submit'))) !!}
            </div>
          </div>
        </div>
      </div>
    {!! Form::close() !!}
</div>

@stop