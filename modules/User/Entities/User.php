<?php namespace Modules\User\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Sentinel\Users\EloquentUser as CartalystUser;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class User extends CartalystUser implements SluggableInterface {
    
    use SluggableTrait;
    
    protected $fillable = ['name', 'email', 'password', 'permissions'];
    
    protected $table = 'users';
    
    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
        'on_update'  => true,
    ];
    
    /* public function allSlug
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function allSlug() {
        
        $slugs = User::all();
        
        $items = [];
        
        
        if($slugs->count())
        {
            foreach($slugs as $slug)
            {
                $items[] = $slug->slug;
            }
            
            $route = implode('|', $items);
            
        }
        else
        {
            $route = '[a-z]+';
        }
        
        return $route;
        
    }
    
    public function pivotProfile()
    {
       return $this->belongsToMany('User', 'profile')
       ->withPivot('first_name',
                   'last_name',
                   'photo_big',
                   'network',
                   'country_id',
                   'region_id',
                   'city_id',
                   'city',
                   'phone',
                   'uid',
                   'profile',
                   'identity',
                   'news'
        );
    }
    /* public function discount_first
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function discount_first()
    {
        return $this->belongsToMany('User', 'users_discount_first')
        ->withPivot('flag');
    }
    /* public function discount
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function discount()
    {
        return $this->belongsToMany('User', 'users_discount')
        ->withPivot('discount_id', 'discount', 'date_end');
    }
    /* public function getUserNmaeAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function getUsernameAttribute()
    {
        //$this->load('pivotProfile');
        $name = array();
        $name['first_name'] = ($this->pivotProfile->count()) ? $this->pivotProfile[0]->pivot->first_name : $this->name;
        $name['last_name']  = ($this->pivotProfile->count()) ? $this->pivotProfile[0]->pivot->last_name : '';
        return implode(' ', $name);
    }
    /* public function getAvatarAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getAvatarAttribute()
    {
        $data = array();
        $data['image'] = ($this->pivotProfile->count() && $this->pivotProfile[0]->pivot->photo_big != '') ? $this->pivotProfile[0]->pivot->photo_big : url('/').'/uploads/user.png';
        return (object) $data;
    }
    /* public function getPhoneAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getPhoneAttribute()
    {
        return ($this->pivotProfile->count() && $this->pivotProfile[0]->pivot->phone != '') ? $this->pivotProfile[0]->pivot->phone : '-не указан-';
    }
    /* public function getCityAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCityAttribute()
    {
        return ($this->pivotProfile->count() && $this->pivotProfile[0]->pivot->city != '') ? $this->pivotProfile[0]->pivot->city : '-не указан-';
    }
    /* public function getLoactionAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getLocationAttribute()
    {
        $data = array();
        if($this->pivotProfile->count())
        {
            $data['country_id']  =   $this->pivotProfile[0]->pivot->country_id;
            $data['region_id']  =   $this->pivotProfile[0]->pivot->region_id;
            $data['city_id']    =   $this->pivotProfile[0]->pivot->city_id;
            $data['name_ru']    =   $this->pivotProfile[0]->pivot->city;
        }
        return (object) $data;
    }
    /* public function getFirstnameAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFirstnameAttribute()
    {
        return ($this->pivotProfile->count() && $this->pivotProfile[0]->pivot->first_name != '') ? $this->pivotProfile[0]->pivot->first_name : null;
    }
    /* public function getFirstnameAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getLastnameAttribute()
    {
        return ($this->pivotProfile->count() && $this->pivotProfile[0]->pivot->last_name != '') ? $this->pivotProfile[0]->pivot->last_name : null;
    }
    /* public function getNewsAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getNewsAttribute()
    {
        return ($this->pivotProfile->count() && $this->pivotProfile[0]->pivot->news == 1) ? true : false;
    }
    /*
     *
     */
    public function wallet()
    {
        return $this->hasOne('Wallet', 'user_id');
    }
    /*
     *
     */
    public function walletHistory()
    {
        return $this->hasMany('Wallet', 'user_id')->orderBy('created_at', 'DESC');
    }
    /* public function getBalanceAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getBalanceAttribute()
    {
        return ($this->wallet) ? 'Баланс: '. $this->wallet->total : 'Баланс: 0 руб.';
    }
    /*
     *
     */
    public function getDateAttribute()
    {
        return date('d.m.Y', strtotime($this->created_at));
    }
    /* public function getDateAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getStatussAttribute()
    {
        $user = \Sentinel::findById($this->id);

        $activation = \Activation::exists($user);
        
        if($activation && $activation->completed == 0)
        {
            return '<span class="text-warning"><i class="fa fa-hourglass-start"></i> Не активиный</span>';
        }
        
        if($user->ban === 1)
        {
            return '<span class="text-danger"><i class="fa fa-lock"></i> Заблокирован</span>';
        }
        
        return '<span class="text-success"><i class="fa fa-check"></i> Активный</span>';
    }
    /* public function getRolesArr
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getRolesArr()
    {
        $roles = \DB::table('roles')->get();
        foreach($roles as $role)
        {
            $data[$role->id] = $role->name;
        }
        return $data;
    }
    /* public function name
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCountAdvertAttribute()
    {
        $user = \Sentinel::getUser();
        $categories = \TaxonomyTerm::where('taxonomy_id', 1)->where('parent_id', 0)->get();
        $data = array();
        foreach($categories as $category)
        {
            $data[$category->slug] = \Advert::where('user_id', $user->id)->where('type',$category->slug)->count();
        }
        return $data;
    }
    /* public function scopeAdminUsers
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function scopeAdminUsers($query, $role = array(), $ban = 0)
    {
        $query->orderBy('users.created_at', 'DESC');
        $query->where('users.ban', $ban);
        if(!empty($role))
        {
            $query->leftJoin('role_users', 'users.id', '=', 'role_users.user_id');
            $query->whereIn('role_id', $role);
        }
        if(\Request::has('name'))
        {
            $query->where('name', 'like', '%'.\Request::get('name').'%');
        }
        if(\Request::has('email'))
        {
            $query->where('email', \Request::get('email'));
        }
        $users = $query->paginate(20);
        return $users;
    }
    /* public function countUser
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function countUserBan()
    {
        $count = \User::where('ban', 1)->count();
        return $count;
    }
    /* public function countUsersRole
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function countUsersRole($id)
    {
        $count = \DB::table('role_users')->where('role_id', $id)->count();
        return $count;
    }
    /* public function set_location
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function set_location()
    {
       // \Session::flush();
        if (\Sentinel::guest())
        {
            if (!\Session::has('location'))
            {
                $response = json_decode(file_get_contents('http://api.sypexgeo.net/json/'.\Request::ip()));
                
                if($response->country && $response->country->name_ru == 'Россия')
                {
                    $city = \City::where('name_ru', $response->city->name_ru)->first();
                    session(['location' => [
                       'city' => [
                          'country_id'   => $city->country_id,
                          'region_id'    => $city->region_id,
                          'city_id'      => $city->city_id,
                          'name_ru'      => $city->name_ru,
                       ]
                    ]]);
                }
                else
                {
                    session(['location' => [
                        'city' => [
                           'country_id'   => 0,
                           'region_id'    => 0,
                           'city_id'      => 0,
                           'name_ru'      => 'Вся Россия',
                        ]
                    ]]);
                }
            }
        }
        
        if($user = \Sentinel::check())
        {
            if($user->profile->location->country_id == 0)
            {
                $response = json_decode(file_get_contents('http://api.sypexgeo.net/json/'.Request::ip()));
                $city = \City::where('name_ru', 'like', '%'.$response->city->name_ru.'%')->first();
                session(['location' => [
                    'city' => [
                       'country_id'   => $city->country_id,
                       'region_id'    => $city->region_id,
                       'city_id'      => $city->city_id,
                       'name_ru'      => $city->name_ru,
                    ]
                ]]);
            }
            else
            {
                session(['location' => [
                    'city' => [
                       'country_id'   => $user->location->country_id,
                       'region_id'    => $user->location->region_id,
                       'city_id'      => $user->location->city_id,
                       'name_ru'      => $user->location->name_ru,
                    ]
                ]]);
            }
        }
        //dd(\Session::all());
    }
    /* public function scopeGetByRegion
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function scopeGetByRegion($query, $region_id)
    {
        $query->leftJoin('profile', 'users.id', '=', 'profile.user_id');
        $query->where('region_id', $region_id);
        $users = $query->get();
        return $users;
    }
    /* public function getFirstDiscountAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFirstDiscountAttribute()
    {
        $data = array();
        $data['discount'] = null;
        $data['format'] = '';
        if(!empty($this->location->region_id))
        {
            if(!$this->discount_first->count())
            {
                $discount = \Discount::getByRegion($this->location->region_id, 373);
                $data['discount'] = ($discount->count()) ? $discount->discount : null;
                $data['format'] = ($discount->count()) ? number_format(($discount->discount*100), 0, '.', '').'%' : '';
            }
        }
        return (object) $data;
    }
    /* public function getTimeDiscountAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getTimeDiscountAttribute()
    {
        $data = array();
        $data['discount'] = null;
        $data['format'] = '';
        $data['time'] = '';
        if(!empty($this->location->region_id))
        {
            $discount = \Discount::getByRegion($this->location->region_id, 374);
            $data['discount'] = ($discount->count()) ? $discount->discount : null;
            $data['format'] = ($discount->count()) ? number_format(($discount->discount*100), 0, '.', '').'%' : '';
            $data['time']   = ($discount->count()) ? date('d.m.Y', strtotime($discount->dayend)) : null;
        }
        return (object) $data;
    }
    
}