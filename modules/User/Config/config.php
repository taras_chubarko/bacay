<?php

return [
    'name' => 'User',
    'permissions' => [
        
        'admin' => [
            'name' => 'Панель администратора',
            'permissions' => [
                'admin' => 'Доступ к панели администратора',
            ],
        ],
        'pages' => [
            'name' => 'Страници',
            'permissions' => [
                'pages.create'  => 'Создание',
                'pages.edit'    => 'Редактирование',
                'pages.delete'  => 'Удаление',
                'pages.view'    => 'Просмотр',
            ],
        ],
        'advert' => [
            'name' => 'Обявления',
            'permissions' => [
                'advert.create'  => 'Создание',
                'advert.edit'    => 'Редактирование',
                'advert.delete'  => 'Удаление',
                'advert.view'    => 'Просмотр',
            ],
        ],
        'advert' => [
            'name' => 'Обявления',
            'permissions' => [
                'advert.create'  => 'Создание',
                'advert.edit'    => 'Редактирование',
                'advert.delete'  => 'Удаление',
                'advert.view'    => 'Просмотр',
            ],
        ],
        'users' => [
            'name' => 'Пользователи',
            'permissions' => [
                'users.create'  => 'Создание',
                'users.edit'    => 'Редактирование',
                'users.delete'  => 'Удаление',
                'users.view'    => 'Просмотр',
                'users.ban'     => 'Блокировка / Разблокировка',
            ],
        ],
    ],
];