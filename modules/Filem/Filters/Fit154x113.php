<?php

namespace Modules\Filem\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Fit154x113 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(154, 113);
    }
}