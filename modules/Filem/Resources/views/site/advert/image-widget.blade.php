@if(count($images))
        @foreach($images as $key => $image)
        <li class="drag col-md-4" data-id="{{ $image->id }}">
            <a href="javascript:" title="Удалить" data-image-id="{{ $image->id }}" class="delim"><i class="fa fa-trash"></i></a>
            <img src="{{ Img::fit($image, 115, 115) }}" class="img-thumbnail img-responsive" alt="" title="">
            {!! Form::hidden('image['.$key.'][fid]', $image->id, ['class' => 'fid']) !!}
            {!! Form::hidden('image['.$key.'][sort]', ($image->sort) ? $image->sort : $key, ['class' => 'sort']) !!}
        </li>
        @endforeach
@endif