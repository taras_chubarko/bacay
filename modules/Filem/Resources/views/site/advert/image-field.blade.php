<div class="row">
    <div class="col-md-4 m-b-15">
        {!! Form::file('images[]', array('class' => 'files', 'id' => 'input-image-'.$uri, 'multiple' => true, 'data-uri' => $uri)) !!}
    </div>
    <div id="image-widget">
        <ul id="image-widget-drag">
            @include('filem::site.advert.image-widget', ['images' => isset($images) ? $images : null]) 
        </ul>
        <div class="clearfix"></div>
    </div>
</div>

