<?php namespace Modules\Filem\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Img extends Model {

    protected $table = 'filemanager';

    protected $fillable = [];
    
    protected $guarded = ['_token'];
    
    
    /* public function randomString
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function randomString($length)
    {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));
        
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        
        return $key;
    }
    
    /* public function setStatus
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function setStatus($id, $status = 1)
    {
        $image = Img::findOrFail($id);
        $image->status = $status;
        $image->save();
    }
    
    /* public function del
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function del($id, $tables = array())
    {
        $images = Img::findOrFail($id);
        $manuals = \File::allFiles('uploads');
        foreach($manuals as $manual)
        {
                $files[] = pathinfo($manual);
        }
        foreach($files as $file)
        {
                if($file['basename'] == $images->filename)
                {
                        \File::delete($file['dirname'].'/'.$file['basename']);
                }
        }
        if(!empty($tables))
        {
            foreach($tables as $table)
            {
                \DB::table($table)->where('fid', $id)->delete();
            }
        }
        $images->delete();
    }
    
    /* public function fit
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function fit($image, $w, $h)
    {
        if($image)
        {
            $uri        = $image->uri;
            $filename   = $image->filename;
            
            $destinationPath = public_path('uploads/').$uri.'/';
            $newFolder       = $w.'x'.$h.'/';
            $path            = $destinationPath.$newFolder;
            
            if(!\File::exists($path))
            {
                \File::makeDirectory($path, 0777, true);
            }
            
            $img = \Image::make($destinationPath.$filename)->fit($w, $h, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            
            if(!\File::exists($path.$filename))
            {
                $img->save($path.$filename, 60);
            }
            
            return url('/').'/uploads/'.$uri.'/'.$newFolder.$filename;
        }
        else
        {
            $destinationPath = public_path('uploads/');
            $newFolder       = $w.'x'.$h.'/';
            $path            = $destinationPath.$newFolder;
            
            if(!\File::exists($path))
            {
                \File::makeDirectory($path, 0777, true);
            }
            
            $img = \Image::make($destinationPath.'noim.jpg')->fit($w, $h, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            
            if(!\File::exists($path.'noim.jpg'))
            {
                $img->save($path.'noim.jpg', 80);
            }
            
            return url('/').'/uploads/'.$newFolder.'noim.jpg';
        }
        
    }
    /* public function original
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function original($image)
    {
        if($image)
        {
            return url('/').'/uploads/'.$image->uri.'/'.$image->filename;
        }
        else
        {
            return url('/').'/uploads/noim.jpg';
        }
    }
    

}