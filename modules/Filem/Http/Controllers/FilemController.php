<?php namespace Modules\Filem\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class FilemController extends Controller {
	
	/* public function uploadImages
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function uploadImages(Request $request)
	{
		$user = \Sentinel::getUser();
		
		$uri = $request->uri;
		
		$destinationPath = public_path('uploads/').$uri.'/';
		
		$images = [];
		       
		if($request->hasFile('images')){
			
			$files = $request->file('images');
			
			foreach($files as $key => $file)
			{
				$filename = \Img::randomString(12). '.' . $file->getClientOriginalExtension();
				
				$uploadSuccess = $file->move($destinationPath, $filename);
				
				$img = new \Img;
				$img->user_id 	= $user->id;
				$img->filename 	= $filename;
				$img->uri 	= $uri;
				$img->filemime 	= $uploadSuccess->getMimeType();
				$img->filesize 	= filesize($uploadSuccess);
				$img->status 	= 0;
				$img->save();
				
				$images[$key] = $img;
				
			}
		}
		
		return response()->json(['result' => view('filem::admin.image-widget', compact('images'))->render()]);
	}
	/* public function name
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function uploadImagesAdvert(Request $request)
	{
		$user = (\Sentinel::check()) ? \Sentinel::getUser()->id : 0;
		
		$uri = $request->uri;
		
		$destinationPath = public_path('uploads/').$uri.'/';
		
		$images = [];
		       
		if($request->hasFile('images')){
			
			$files = $request->file('images');
			
			foreach($files as $key => $file)
			{
				$filename = \Img::randomString(12). '.' . $file->getClientOriginalExtension();
				
				$uploadSuccess = $file->move($destinationPath, $filename);
				
				$img = new \Img;
				$img->user_id 	= $user;
				$img->filename 	= $filename;
				$img->uri 	= $uri;
				$img->filemime 	= $uploadSuccess->getMimeType();
				$img->filesize 	= filesize($uploadSuccess);
				$img->status 	= 0;
				$img->save();
				
				$images[$key] = $img;
				
			}
			session(['images' => $images]);
		}
		
		return response()->json(['result' => view('filem::site.advert.image-widget', compact('images'))->render()]);
	}
	
	
	/* public function deleteImages
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function deleteImages(Request $request)
	{
		\Img::del($request->image_id);
	}
	/* public function uploadImagesAvatar
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function uploadImagesAvatar(Request $request)
	{
		if(\Sentinel::check())
		{
			$user = \Sentinel::getUser();
			$uri = $request->uri;
			
			$delim = \Img::where('user_id', $user->id)->where('uri', 'like', '%'.$uri.'%')->first();
			if($delim)
			{
				\Img::del($delim->id);
			}
			
			
			$destinationPath = public_path('uploads/').$uri.'/';
			
			$images = [];
			       
			if($request->hasFile('avatar')){
				
				$file = $request->file('avatar');
				
				$filename = \Img::randomString(12). '.' . $file->getClientOriginalExtension();
					
				$uploadSuccess = $file->move($destinationPath, $filename);
				
				$img = new \Img;
				$img->user_id 	= $user->id;
				$img->filename 	= $filename;
				$img->uri 	= $uri;
				$img->filemime 	= $uploadSuccess->getMimeType();
				$img->filesize 	= filesize($uploadSuccess);
				$img->status 	= 1;
				$img->save();
				
				$item = url('/').'/uploads/'.$img->uri.'/'.$img->filename;
				
				return response()->json(['result' => '<img src="'.$item.'" class="img-responsive" alt="">', 'item' => $item ]);
			}
		}
	}
    /* public function uploadFilem
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function uploadFilem(Request $request)
    {
        $destinationPath = public_path('uploads/filem/');
        if($request->hasFile('file'))
        {
            $file = $request->file('file');
            $filename = \Img::randomString(12). '.' . $file->getClientOriginalExtension();
            $uploadSuccess = $file->move($destinationPath, $filename);
            $item = url('/').'/uploads/filem/'.$filename;
            return response()->json(['item' => $item, 'filename' => $filename]);
        }
    }
	
}