<?php

Route::group(['middleware' => 'web', 'prefix' => 'filem', 'namespace' => 'Modules\Filem\Http\Controllers'], function()
{
	Route::get('/', 'FilemController@index');
	Route::post('upload/images/advert', 'FilemController@uploadImagesAdvert');
	Route::post('delete/images', 'FilemController@deleteImages');
	Route::post('upload/images/avatar', 'FilemController@uploadImagesAvatar');
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\Filem\Http\Controllers'], function()
{
	Route::resource('filem', 'FilemController');
	Route::post('upload/images', 'FilemController@uploadImages');
	Route::post('delete/images', 'FilemController@deleteImages');
	Route::post('upload/filem', 'FilemController@uploadFilem');
});