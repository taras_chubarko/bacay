@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Добарить услугу') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Добарить услугу</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            
        </div>
        
        <div class="box-body">
            {!! Form::open(array('route' => 'admin.services.store', 'role' => 'form', 'class' => 'form')) !!}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Название *</label>
                    {!! Form::text('name', null, array('class' => 'form-control')) !!}
                </div>
                    
                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                    <label for="price">Цена *</label>
                    <div class="input-group col-md-4">
                        {!! Form::text('price', null, array('class' => 'form-control', 'placeholder' => '0,00')) !!}
                        <span class="input-group-addon">руб.</span>
                    </div>
                </div>
                    
                <div class="form-group">
                    {!! Form::hidden('status', 0) !!}
                    <label for="status" class="icheck">
                        {!! Form::checkbox('status', 1, 1) !!}
                        Активная
                    </label>
                </div>
                    
                <div class="form-group">
                   {!! Form::submit('Сохранить', array('class' => 'btn btn-primary btn-flat')) !!}
                </div>
            {!! Form::close() !!}
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>

@stop