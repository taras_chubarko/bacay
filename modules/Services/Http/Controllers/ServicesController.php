<?php namespace Modules\Services\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class ServicesController extends Controller {
	
	/* public function index
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function index()
	{
		$services = \Services::all();
		return view('services::admin.index', compact('services'));
	}
	/* public function create
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function create()
	{
		return view('services::admin.create');
	}
	/* public function store
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function store(\Modules\Services\Http\Requests\ServicesReq $request)
	{
		$services = \Services::create($request->all());
		return redirect()->route('admin.services.index')->with('success', 'Услуга создана.');
	}
	/* public function edit
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function edit($id)
	{
		$services = \Services::findOrFAil($id);
		return view('services::admin.edit', compact('services'));
	}
	/* public function update
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function update(\Modules\Services\Http\Requests\ServicesReq $request, $id)
	{
		$services = \Services::findOrFAil($id);
		$services->update($request->all());
		return redirect()->route('admin.services.index')->with('success', 'Услуга обновлена.');
	}
	/* public function destroy
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function destroy($id)
	{
		$services = \Services::findOrFAil($id);
		$services->delete();
		return redirect()->route('admin.services.index')->with('success', 'Услуга удалена.');
	}
}