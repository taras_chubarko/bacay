<?php

Route::group(['middleware' => 'web', 'prefix' => 'services', 'namespace' => 'Modules\Services\Http\Controllers'], function()
{
	Route::get('/', 'ServicesController@index');
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\Services\Http\Controllers'], function()
{
	Route::resource('services', 'ServicesController');
});