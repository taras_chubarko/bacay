<?php namespace Modules\Services\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServicesReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' 	=> 'required',
			'price' => 'required|numeric',
		];
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
		$validator->setAttributeNames([
			'name' 	=> '"Название"',
			'price' => '"Цена"',
		]);
		
		return $validator;
	}

}
