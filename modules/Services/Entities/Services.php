<?php namespace Modules\Services\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Services extends Model {

    protected $table = 'services';

    protected $fillable = [];
    
    protected $guarded = ['_token'];
    
    /* public function getStatussAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function getStatussAttribute() {
        
        switch($this->status)
        {
            case 0:
                return 'Не активно';
            break;
        
            case 1:
                return 'Активно';
            break;
        }
        
    }
    
    /* public function getDateAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function getDateAttribute() {
        return date('d.m.Y', strtotime($this->updated_at));
    }
    /* public function getArray()
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getArray()
    {
        $services = \Services::all();
        $items = array();
        foreach($services as $item)
        {
            $items[$item->id] = $item->name;
        }
        return $items;
    }

}