<?php namespace Modules\Newslatter\Entities;
   
use Illuminate\Database\Eloquent\Model;

class NewsLatter extends Model {

    protected $fillable = [];
    
    protected $table = 'newslatter';
    
    public function getDateAttribute()
    {
        return date('d.m.Y', strtotime($this->created_at));
    }
    /* public function getDateAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getStatussAttribute()
    {
       switch($this->status)
       {
            case 0:
                return 'Не активная';
            break;
                
            case 1:
                return 'Активная';
            break;
                
            case 2:
                return 'Завершена';
            break;
       }
    }
    /* public function getWorkPersentAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getWorkPersentAttribute()
    {
        $subscibers = unserialize($this->subscibers);
        $count = count($subscibers);
        $total = $this->total;
        
        $tp = ($count * 100) / $total;
        $it = round(100 - $tp);
        return $it.'%';
    }
    /* public function send
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function send()
    {
        $newslatter = \NewsLatter::orderBy('created_at', 'DESC')->where('status', 1)->first();
        
        if($newslatter)
        {
            $subscribers = unserialize($newslatter->subscibers);
            
            $data = $newslatter->toArray();
            
            foreach($subscribers[0] as $subscriber)
            {
                \Mail::later(5, 'newslatter::site.mail-newslatter', $data, function($message) use ($subscriber, $data)
                {
                    $message->to($subscriber)->subject($data['name'].'"'.config('cms.name').'"');
                });
            }
            unset($subscribers[0]);
            
            $newslatter->subscibers = serialize(array_values($subscribers));
            
            if(count($subscribers) == 0)
            {
                $newslatter->status = 2;
            }
            
            $newslatter->save();
        }
    }
    
}