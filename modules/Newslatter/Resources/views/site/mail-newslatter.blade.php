@extends('site::mail.main')

@section('title')
{{ $name }}
@stop

@section('content')
{!! $body !!}
@stop