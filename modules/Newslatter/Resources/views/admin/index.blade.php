@extends('admin::layouts.master')

@section('content')

<section class="content-header">
	<h1>{{ MetaTag::set('title', 'Рассылка') }}</h1>
	<ol class="breadcrumb">
		<li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
		<li class="active">Рассылка</li>
	</ol>
</section>
  
<section class="content">

	<div class="box">
		<div class="box-header with-border">
			@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('newslatter.create'))
				<a class="btn btn-success btn-flat" href="{{ route('admin.newslatter.create') }}">Создать</a>
			@endif
		</div>
            
		<div class="box-body">
			@if($newslatters->count())
			<table class="table table-bordered b-c-222D32">
				<thead>
					<tr>
						<th width="10">#</th>
						<th>Название</th>
						<th width="150">Выполнено</th>
						<th width="150">Дата</th>
						<th width="150">Статус</th>
						<th width="100">Действие</th>
					</tr>
				</thead>
				<tbody>
					@foreach($newslatters as $item)
					<tr>
						<td>{{ $item->id }}</td>
						<td>{{ $item->name }}</td>
                        <td>{{ $item->workPersent }}</td>
						<td>{{ $item->date }}</td>
						<td>{{ $item->statuss }}</td>
						<td>
							@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('newslatter.edit'))
								<a href="{{ route('admin.newslatter.edit', $item->id) }}" class="btn btn-sm bg-navy btn-flat"><i class="fa fa-fw fa-edit"></i></a>
							@endif
							
							@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('newslatter.delete'))
								@include('cms::admin.delete', ['route' => 'admin.newslatter.destroy', 'id' => $item->id])
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{!! $newslatters->render() !!}
			@else
				<p>Нет рассылок.</p>
			@endif
		  
		</div><!-- /.box-body -->
		
		<div class="box-footer">
		 
		</div><!-- /.box-footer-->
	</div>

</section>

@stop