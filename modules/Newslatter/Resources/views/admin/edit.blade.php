@extends('admin::layouts.master')

@section('script')
    <script src="{{ url('/') }}/themes/admin/assets/js/module.geo.city.js"></script>
@stop
    
@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Редактировать рассылку') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Редактировать рассылку</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            
        </div>
        
        <div class="box-body">
            {!! Form::open(array('route' => ['admin.newslatter.update', $newslatter->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'form')) !!}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Название *</label>
                    {!! Form::text('name', $newslatter->name, array('class' => 'form-control')) !!}
                </div>
                    
                <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                    <label for="body">Содержимое *</label>
                    {!! Form::textarea('body', $newslatter->body, array('class' => 'form-control wredaktor', 'rows' => 10)) !!}
                </div>
                    
                <div class="form-group">
                    {!! Form::hidden('status', 0) !!}
                    <label for="status" class="icheck">
                        {!! Form::checkbox('status', 1, $newslatter->status) !!}
                        Активировать
                    </label>
                </div>
                    
                <div class="form-group">
                    {!! Form::hidden('update', 0) !!}
                    <label for="status" class="icheck">
                        {!! Form::checkbox('update', 1, 0) !!}
                        Обновить подписчиков
                    </label>
                </div>
                    
                {!! Form::hidden('id', $newslatter->id) !!}
                    
                <div class="form-group">
                   {!! Form::submit('Сохранить', array('class' => 'btn btn-primary btn-flat')) !!}
                </div>
            {!! Form::close() !!}
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>

@stop