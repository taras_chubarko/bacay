<?php

Route::group(['middleware' => 'web', 'prefix' => 'newslatter', 'namespace' => 'Modules\Newslatter\Http\Controllers'], function()
{
	//Route::get('/', 'NewslatterController@index');
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\Newslatter\Http\Controllers'], function()
{
	Route::resource('newslatter', 'NewslatterController');
});