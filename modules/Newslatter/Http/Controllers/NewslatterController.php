<?php namespace Modules\Newslatter\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class NewslatterController extends Controller {
	
	/* public function index
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function index()
    {
        $newslatters = \NewsLatter::orderBy('created_at', 'DESC')->paginate(20);
        return view('newslatter::admin.index', compact('newslatters'));
    }
	/* public function create
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function create()
    {
        return view('newslatter::admin.create');
    }
    /* public function store
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function store(\Modules\Newslatter\Http\Requests\CreateReq $request)
    {
        $newslatter = new \NewsLatter;
        $newslatter->name   = $request->name;
        $newslatter->body   = $request->body;
        $newslatter->status = $request->status;
        
        //$newslatter->city = serialize($request->city);
        
        $subscribers = array();
        $users = \User::all();
        $users->load('pivotProfile');
        foreach($users as $user)
        {
            if($user->news == true)
            {
                $subscribers[] = $user->email;
            }
        }
        $subscribers = array_chunk($subscribers, 10);
        $newslatter->subscibers = serialize($subscribers);
        $newslatter->total = count($subscribers);
        $newslatter->save();
        
        return redirect()->route('admin.newslatter.index')->with('success', 'Рассылка создана.');
    }
    /* public function edit
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function edit($id)
    {
        $newslatter = \NewsLatter::findOrFail($id);
        return view('newslatter::admin.edit', compact('newslatter'));
    }
    /* public function update
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function update(\Modules\Newslatter\Http\Requests\CreateReq $request, $id)
    {
        $newslatter = \NewsLatter::findOrFail($id);
        $newslatter->name   = $request->name;
        $newslatter->body   = $request->body;
        $newslatter->status = $request->status;
        
        //$newslatter->city = serialize($request->city);
       
        if($request->update == 1)
        {
            $subscribers = array();
            $users = \User::all();
            $users->load('pivotProfile');
            foreach($users as $user)
            {
                if($user->news == true)
                {
                    $subscribers[] = $user->email;
                }
            }
            $subscribers = array_chunk($subscribers, 10);
            $newslatter->subscibers = serialize($subscribers);
            $newslatter->total = count($subscribers);
        }
        $newslatter->save();
        
        return redirect()->route('admin.newslatter.index')->with('success', 'Рассылка обновленная.');
    }
    /* public function destroy
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function destroy($id)
    {
        $newslatter = \NewsLatter::findOrFail($id);
        $newslatter->delete();
        return redirect()->route('admin.newslatter.index')->with('success', 'Рассылка удалена.');
    }
    
    
    
    
}