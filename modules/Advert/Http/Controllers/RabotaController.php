<?php namespace Modules\Advert\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class RabotaController extends Controller {
	
	/* public function store
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function store(\Modules\Advert\Http\Requests\RabotaReq $request)
	{
		$user = \Sentinel::getUser();
		
		$advert = new \Advert;
		$advert->user_id 	= $request->user_id;
		$advert->type 		= $request->type;
		$advert->name 		= $request->name;
		$advert->status 	= \Option::get('advert.publics');
		$advert->save();
		
		if($request->city)
		{
			$advert->pivotCity()->attach($advert->id, [
				'type' 	=> $request->type,
				'city' 	=> $request->city
			]);
		}
		
		if($request->metro)
		{
			$advert->pivotMetro()->attach($advert->id, [
				'type' 	=> $request->type,
				'metro' => $request->metro
			]);
		}
		   
		foreach($request->category as $key => $category)
		{
		    $advert->pivotCategory()->attach($advert->id, [
			'type' 	    => $request->type,
			'category'  => $category,
			'sort'      => $key
		    ]);
		}
		
		if($request->service)
		{
			$advert->pivotService()->attach($advert->id, [
				'type' 		=> $request->type,
				'service' 	=> $request->service
			]);
		}
		
		if($request->schedule)
		{
			$advert->pivotSchedule()->attach($advert->id, [
				'type' 		=> $request->type,
				'schedule' 	=> $request->schedule
			]);
		}
		
		if($request->experience)
		{
			$advert->pivotExperience()->attach($advert->id, [
				'type' 		=> $request->type,
				'experience' 	=> $request->experience
			]);
		}
		
		if($request->typ_education)
		{
			$advert->pivotTypEducation()->attach($advert->id, [
				'type' 		=> $request->type,
				'typ_education' => $request->typ_education
			]);
		}
		
		if($request->education)
		{
			$advert->pivotEducation()->attach($advert->id, [
				'type' 		=> $request->type,
				'education' 	=> $request->education
			]);
		}
		
		if($request->gender)
		{
			$advert->pivotGender()->attach($advert->id, [
				'type' 		=> $request->type,
				'gender' 	=> $request->gender
			]);
		}
		
		if($request->age)
		{
			$advert->pivotAge()->attach($advert->id, [
				'type' 	=> $request->type,
				'age' 	=> $request->age
			]);
		}
		
		if($request->price)
		{
			$advert->pivotPrice()->attach($advert->id, [
				'type' 		=> $request->type,
				'price' 	=> $request->price
			]);
		}
		
		if($request->body)
		{
			$advert->pivotBody()->attach($advert->id, [
				'type' 	=> $request->type,
				'body' 	=> $request->body
			]);
		}
		
		if($request->city_raj)
		{
			$advert->pivotCityRaj()->attach($advert->id, [
				'type' 		=> $request->type,
				'city_raj' 	=> $request->city_raj
			]);
		}
		
		if($request->address)
		{
			$advert->pivotAddress()->attach($advert->id, [
				'type' 		=> $request->type,
				'address' 	=> $request->address
			]);
		}
		
		if($request->image)
		{
			foreach($request->image as $image)
			{
				$advert->pivotImages()->attach($advert->id, [
					'type' 	=> $request->type,
					'fid' 	=> $image['fid'],
					'sort' 	=> $image['sort']
				]);
				\Img::setStatus($image['fid']);
			}
		}
		
		if($request->user)
		{
			$advert->pivotUserInfo()->attach($advert->id, [
				'type' 	=> $request->type,
				'name' 	=> $request->user['name'],
				'email' => $request->user['email'],
				'phone' => $request->user['phone'],
			]);
		}
		
		\Session::forget('images');
		\Event::fire('advert.presave', array($request, $advert));
		
		return redirect()->route('user.adverts', $advert->user_id)->with('success', 'Объявление создано.');
	}
	/* public function edit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function edit($id)
	{
		$advert = \Advert::findOrFail($id);
		$advert->load(
			'pivotBody', 'pivotCity', 'pivotMetro', 'pivotCategory',
			'pivotService', 'pivotSchedule', 'pivotExperience', 'pivotCityRaj', 'pivotAddress',
			'pivotEducation', 'pivotGender', 'pivotAge'
		);
		
		return view('advert::site.rabota-i-obrazovanie.edit', compact('advert'));
	}
	/* public function update
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function update(\Modules\Advert\Http\Requests\RabotaReq $request, $id)
	{
		$advert = \Advert::findOrFail($id);
		$advert->type 		= $request->type;
		$advert->name 		= $request->name;
		$advert->status 	= 2;
		$advert->save();
		
		$advert->pivotCity()->detach($advert->id);
		if($request->city)
		{
			$advert->pivotCity()->attach($advert->id, [
				'type' 	=> $request->type,
				'city' 	=> $request->city
			]);
		}
		
		$advert->pivotMetro()->detach($advert->id);
		if($request->metro)
		{
			$advert->pivotMetro()->attach($advert->id, [
				'type' 	=> $request->type,
				'metro' => $request->metro
			]);
		}
		   
		$advert->pivotCategory()->detach($advert->id);
		foreach($request->category as $key => $category)
		{
		    $advert->pivotCategory()->attach($advert->id, [
			'type' 	    => $request->type,
			'category'  => $category,
			'sort'      => $key
		    ]);
		}
		
		$advert->pivotService()->detach($advert->id);
		if($request->service)
		{
			$advert->pivotService()->attach($advert->id, [
				'type' 		=> $request->type,
				'service' 	=> $request->service
			]);
		}
		
		$advert->pivotSchedule()->detach($advert->id);
		if($request->schedule)
		{
			$advert->pivotSchedule()->attach($advert->id, [
				'type' 		=> $request->type,
				'schedule' 	=> $request->schedule
			]);
		}
		
		$advert->pivotExperience()->detach($advert->id);
		if($request->experience)
		{
			$advert->pivotExperience()->attach($advert->id, [
				'type' 		=> $request->type,
				'experience' 	=> $request->experience
			]);
		}
		
		$advert->pivotTypEducation()->detach($advert->id);
		if($request->typ_education)
		{
			$advert->pivotTypEducation()->attach($advert->id, [
				'type' 		=> $request->type,
				'typ_education' => $request->typ_education
			]);
		}
		
		$advert->pivotEducation()->detach($advert->id);
		if($request->education)
		{
			$advert->pivotEducation()->attach($advert->id, [
				'type' 		=> $request->type,
				'education' 	=> $request->education
			]);
		}
		
		$advert->pivotGender()->detach($advert->id);
		if($request->gender)
		{
			$advert->pivotGender()->attach($advert->id, [
				'type' 		=> $request->type,
				'gender' 	=> $request->gender
			]);
		}
		
		$advert->pivotAge()->detach($advert->id);
		if($request->age)
		{
			$advert->pivotAge()->attach($advert->id, [
				'type' 	=> $request->type,
				'age' 	=> $request->age
			]);
		}
		
		$advert->pivotPrice()->detach($advert->id);
		if($request->price)
		{
			$advert->pivotPrice()->attach($advert->id, [
				'type' 		=> $request->type,
				'price' 	=> $request->price
			]);
		}
		
		$advert->pivotBody()->detach($advert->id);
		if($request->body)
		{
			$advert->pivotBody()->attach($advert->id, [
				'type' 	=> $request->type,
				'body' 	=> $request->body
			]);
		}
		
		$advert->pivotCityRaj()->detach($advert->id);
		if($request->city_raj)
		{
			$advert->pivotCityRaj()->attach($advert->id, [
				'type' 		=> $request->type,
				'city_raj' 	=> $request->city_raj
			]);
		}
		
		$advert->pivotAddress()->detach($advert->id);
		if($request->address)
		{
			$advert->pivotAddress()->attach($advert->id, [
				'type' 		=> $request->type,
				'address' 	=> $request->address
			]);
		}
		
		$advert->pivotImages()->detach($advert->id);
		if($request->image)
		{
			foreach($request->image as $image)
			{
				$advert->pivotImages()->attach($advert->id, [
					'type' 	=> $request->type,
					'fid' 	=> $image['fid'],
					'sort' 	=> $image['sort']
				]);
				\Img::setStatus($image['fid']);
			}
		}
		
		$advert->pivotUserInfo()->detach($advert->id);
		if($request->user)
		{
			$advert->pivotUserInfo()->attach($advert->id, [
				'type' 	=> $request->type,
				'name' 	=> $request->user['name'],
				'email' => $request->user['email'],
				'phone' => $request->user['phone'],
			]);
		}
		
		\Session::forget('images');
		return redirect()->route('user.adverts', $advert->user_id)->with('success', 'Объявление обновлено.');
	}
	/* public function show
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function show($id)
	{
		$advert = \Advert::findOrFail($id);
		$advert->load(
			'pivotBody', 'pivotCity', 'pivotMetro', 'pivotCategory',
			'pivotService', 'pivotSchedule', 'pivotExperience', 'pivotCityRaj', 'pivotAddress',
			'pivotEducation', 'pivotGender', 'pivotAge'
		);
		
		if (!$advert || $advert->status == 0 || $advert->status == 2 || $advert->status == 3 || $advert->status == 4) {
		    abort(404);
		}
		
		\MetaTag::set('title', $advert->name);
        \MetaTag::set('keywords', $advert->categoryKeywords);
        \MetaTag::set('description', $advert->metaDescription);
		\Event::fire('advert.views', array($advert));
		
		return view('advert::site.rabota-i-obrazovanie.show', compact('advert'));
	}
}