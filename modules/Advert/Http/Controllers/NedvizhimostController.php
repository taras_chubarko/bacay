<?php namespace Modules\Advert\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class NedvizhimostController extends Controller {
	
	public function __construct()
	{
		$this->middleware('Modules\Advert\Http\Middleware\ViewMiddleware', ['only' => ['show']]);
	}
	
	/* public function store
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function store(\Modules\Advert\Http\Requests\NedvizhimostReq $request)
	{
		//$user = \Sentinel::getUser();
		
		$advert = new \Advert;
		$advert->user_id 	= $request->user_id;
		$advert->type 		= $request->type;
		$advert->name 		= $request->name;
		$advert->status 	= \Option::get('advert.publics');
		$advert->save();
		
		if($request->city)
		{
			$advert->pivotCity()->attach($advert->id, [
				'type' 	=> $request->type,
				'city' 	=> $request->city
			]);
		}
		
		if($request->metro)
		{
			$advert->pivotMetro()->attach($advert->id, [
				'type' 	=> $request->type,
				'metro' => $request->metro
			]);
		}
			
		foreach($request->category as $key => $category)
		{
		    $advert->pivotCategory()->attach($advert->id, [
			'type' 	    => $request->type,
			'category'  => $category,
			'sort'      => $key
		    ]);
		}
		
		if($request->typadvert)
		{
			$advert->pivotTypadvert()->attach($advert->id, [
				'type' 		=> $request->type,
				'typadvert' 	=> $request->typadvert
			]);
		}
		
		if($request->typhouse)
		{
			$advert->pivotTyphouse()->attach($advert->id, [
				'type' 		=> $request->type,
				'typhouse' 	=> $request->typhouse
			]);
		}
		
		if($request->qtyroom)
		{
			$advert->pivotQtyroom()->attach($advert->id, [
				'type' 		=> $request->type,
				'qtyroom' 	=> $request->qtyroom
			]);
		}
		
		if($request->area)
		{
			$advert->pivotArea()->attach($advert->id, [
				'type' 	=> $request->type,
				'area' 	=> $request->area
			]);
		}
		
		if($request->floor)
		{
			$advert->pivotFloor()->attach($advert->id, [
				'type' 		=> $request->type,
				'floor' 	=> $request->floor
			]);
		}
		
		if($request->qtyfloor)
		{
			$advert->pivotQtyfloor()->attach($advert->id, [
				'type' 		=> $request->type,
				'qtyfloor' 	=> $request->qtyfloor
			]);
		}
		
		if($request->price)
		{
			$advert->pivotPrice()->attach($advert->id, [
				'type' 		=> $request->type,
				'price' 	=> $request->price
			]);
		}
		
		if($request->body)
		{
			$advert->pivotBody()->attach($advert->id, [
				'type' 	=> $request->type,
				'body' 	=> $request->body
			]);
		}
		
		if($request->city_raj)
		{
			$advert->pivotCityRaj()->attach($advert->id, [
				'type' 		=> $request->type,
				'city_raj' 	=> $request->city_raj
			]);
		}
		
		if($request->address)
		{
			$advert->pivotAddress()->attach($advert->id, [
				'type' 		=> $request->type,
				'address' 	=> $request->address
			]);
		}
		
		if($request->vid_object)
		{
			$advert->pivotVidObject()->attach($advert->id, [
				'type' 		=> $request->type,
				'vid_object' 	=> $request->vid_object
			]);
		}
		
		if($request->area_sot)
		{
			$advert->pivotAreaSot()->attach($advert->id, [
				'type' 		=> $request->type,
				'area_sot' 	=> $request->area_sot
			]);
		}
		
		if($request->vid_build)
		{
			$advert->pivotVidBuild()->attach($advert->id, [
				'type' 		=> $request->type,
				'vid_build' 	=> $request->vid_build
			]);
		}
		
		if($request->material_walls)
		{
			$advert->pivotMaterialWalls()->attach($advert->id, [
				'type' 		=> $request->type,
				'material_walls'=> $request->material_walls
			]);
		}
		
		if($request->car_places)
		{
			$advert->pivotCarPlaces()->attach($advert->id, [
				'type' 		=> $request->type,
				'car_places'	=> $request->car_places
			]);
		}
		
		if($request->country)
		{
			$advert->pivotCountry()->attach($advert->id, [
				'type' 		=> $request->type,
				'country'	=> $request->country
			]);
		}
		
		
		if($request->image)
		{
			foreach($request->image as $image)
			{
				$advert->pivotImages()->attach($advert->id, [
					'type' 	=> $request->type,
					'fid' 	=> $image['fid'],
					'sort' 	=> $image['sort']
				]);
				\Img::setStatus($image['fid']);
			}
		}
		
		if($request->user)
		{
			$advert->pivotUserInfo()->attach($advert->id, [
				'type' 	=> $request->type,
				'name' 	=> $request->user['name'],
				'email' => $request->user['email'],
				'phone' => $request->user['phone'],
			]);
		}
		
		\Session::forget('images');
		\Event::fire('advert.presave', array($request, $advert));
		
		return redirect()->route('user.adverts', $advert->user_id)->with('success', 'Объявление создано.');
	}
	/* public function edit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function edit($id)
	{
		$advert = \Advert::findOrFail($id);
		$advert->load(
			'pivotBody', 'pivotCity', 'pivotMetro', 'pivotCategory',
			'pivotTypadvert', 'pivotTyphouse', 'pivotQtyroom', 'pivotCityRaj', 'pivotAddress',
			'pivotArea', 'pivotFloor', 'pivotQtyfloor', 'pivotImages'
		);
		
		return view('advert::site.nedvizhimost.edit', compact('advert'));
	}
	/* public function update
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function update(\Modules\Advert\Http\Requests\NedvizhimostReq $request, $id)
	{
		$advert = \Advert::findOrFail($id);
		$advert->type 		= $request->type;
		$advert->name 		= $request->name;
		$advert->status 	= 2;
		$advert->save();
		
		$advert->pivotCity()->detach($advert->id);
		if($request->city)
		{
			$advert->pivotCity()->attach($advert->id, [
				'type' 	=> $request->type,
				'city' 	=> $request->city
			]);
		}
		
		$advert->pivotMetro()->detach($advert->id);
		if($request->metro)
		{
			$advert->pivotMetro()->attach($advert->id, [
				'type' 	=> $request->type,
				'metro' => $request->metro
			]);
		}
			
		$advert->pivotCategory()->detach($advert->id);
		foreach($request->category as $key => $category)
		{
		    $advert->pivotCategory()->attach($advert->id, [
			'type' 	    => $request->type,
			'category'  => $category,
			'sort'      => $key
		    ]);
		}
		
		$advert->pivotTypadvert()->detach($advert->id);
		if($request->typadvert)
		{
			$advert->pivotTypadvert()->attach($advert->id, [
				'type' 		=> $request->type,
				'typadvert' 	=> $request->typadvert
			]);
		}
		
		$advert->pivotTyphouse()->detach($advert->id);
		if($request->typhouse)
		{
			$advert->pivotTyphouse()->attach($advert->id, [
				'type' 		=> $request->type,
				'typhouse' 	=> $request->typhouse
			]);
		}
		
		$advert->pivotQtyroom()->detach($advert->id);
		if($request->qtyroom)
		{
			$advert->pivotQtyroom()->attach($advert->id, [
				'type' 		=> $request->type,
				'qtyroom' 	=> $request->qtyroom
			]);
		}
		
		$advert->pivotArea()->detach($advert->id);
		if($request->area)
		{
			$advert->pivotArea()->attach($advert->id, [
				'type' 	=> $request->type,
				'area' 	=> $request->area
			]);
		}
		
		$advert->pivotFloor()->detach($advert->id);
		if($request->floor)
		{
			$advert->pivotFloor()->attach($advert->id, [
				'type' 		=> $request->type,
				'floor' 	=> $request->floor
			]);
		}
		
		$advert->pivotQtyfloor()->detach($advert->id);
		if($request->qtyfloor)
		{
			$advert->pivotQtyfloor()->attach($advert->id, [
				'type' 		=> $request->type,
				'qtyfloor' 	=> $request->qtyfloor
			]);
		}
		
		$advert->pivotPrice()->detach($advert->id);
		if($request->price)
		{
			$advert->pivotPrice()->attach($advert->id, [
				'type' 		=> $request->type,
				'price' 	=> $request->price
			]);
		}
		
		$advert->pivotBody()->detach($advert->id);
		if($request->body)
		{
			$advert->pivotBody()->attach($advert->id, [
				'type' 	=> $request->type,
				'body' 	=> $request->body
			]);
		}
		
		$advert->pivotCityRaj()->detach($advert->id);
		if($request->city_raj)
		{
			$advert->pivotCityRaj()->attach($advert->id, [
				'type' 		=> $request->type,
				'city_raj' 	=> $request->city_raj
			]);
		}
		
		$advert->pivotAddress()->detach($advert->id);
		if($request->address)
		{
			$advert->pivotAddress()->attach($advert->id, [
				'type' 		=> $request->type,
				'address' 	=> $request->address
			]);
		}
		
		$advert->pivotVidObject()->detach($advert->id);
		if($request->vid_object)
		{
			$advert->pivotVidObject()->attach($advert->id, [
				'type' 		=> $request->type,
				'vid_object' 	=> $request->vid_object
			]);
		}
		
		$advert->pivotAreaSot()->detach($advert->id);
		if($request->area_sot)
		{
			$advert->pivotAreaSot()->attach($advert->id, [
				'type' 		=> $request->type,
				'area_sot' 	=> $request->area_sot
			]);
		}
		
		$advert->pivotVidBuild()->detach($advert->id);
		if($request->vid_build)
		{
			$advert->pivotVidBuild()->attach($advert->id, [
				'type' 		=> $request->type,
				'vid_build' 	=> $request->vid_build
			]);
		}
		
		$advert->pivotMaterialWalls()->detach($advert->id);
		if($request->material_walls)
		{
			$advert->pivotMaterialWalls()->attach($advert->id, [
				'type' 		=> $request->type,
				'material_walls'=> $request->material_walls
			]);
		}
		
		$advert->pivotCarPlaces()->detach($advert->id);
		if($request->car_places)
		{
			$advert->pivotCarPlaces()->attach($advert->id, [
				'type' 		=> $request->type,
				'car_places'	=> $request->car_places
			]);
		}
		
		$advert->pivotCountry()->detach($advert->id);
		if($request->country)
		{
			$advert->pivotCountry()->attach($advert->id, [
				'type' 		=> $request->type,
				'country'	=> $request->country
			]);
		}
		
		$advert->pivotImages()->detach($advert->id);
		if($request->image)
		{
			foreach($request->image as $image)
			{
				$advert->pivotImages()->attach($advert->id, [
					'type' 	=> $request->type,
					'fid' 	=> $image['fid'],
					'sort' 	=> $image['sort']
				]);
				\Img::setStatus($image['fid']);
			}
		}
		
		$advert->pivotUserInfo()->detach($advert->id);
		if($request->user)
		{
			$advert->pivotUserInfo()->attach($advert->id, [
				'type' 	=> $request->type,
				'name' 	=> $request->user['name'],
				'email' => $request->user['email'],
				'phone' => $request->user['phone'],
			]);
		}
		\Session::forget('images');
		return redirect()->route('user.adverts', $advert->user_id)->with('success', 'Объявление обновлено.');
	}
	/* public function show
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function show($id)
	{
		$advert = \Advert::findOrFail($id);
		$advert->load(
			'pivotBody', 'pivotCity', 'pivotMetro', 'pivotCategory',
			'pivotTypadvert', 'pivotTyphouse', 'pivotQtyroom', 'pivotCityRaj', 'pivotAddress',
			'pivotArea', 'pivotFloor', 'pivotQtyfloor', 'pivotImages'
		);
		
		if (!$advert || $advert->status == 0 || $advert->status == 2 || $advert->status == 3 || $advert->status == 4) {
		    abort(404);
		}
		
		\MetaTag::set('title', $advert->name);
		\MetaTag::set('keywords', $advert->categoryKeywords);
		\MetaTag::set('description', $advert->metaDescription);
		
		\Event::fire('advert.views', array($advert));
		
		return view('advert::site.nedvizhimost.show', compact('advert'));
	}
	
	
}