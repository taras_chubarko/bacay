<?php namespace Modules\Advert\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class TransportController extends Controller {
    
    /* public function store
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function store(\Modules\Advert\Http\Requests\TransportReq $request)
    {
        $user = \Sentinel::getUser();
        
        $advert = new \Advert;
        $advert->user_id 	= $request->user_id;
        $advert->type 		= $request->type;
        $advert->name 		= $request->name;
        $advert->status 	= \Option::get('advert.publics');
        $advert->save();
        
        $advert->pivotCity()->attach($advert->id, [
            'type' 	=> $request->type,
            'city' 	=> $request->city
        ]);
        
        if($request->metro)
        {
            $advert->pivotMetro()->attach($advert->id, [
            'type' 	=> $request->type,
            'metro' => $request->metro
            ]);
        }
           
        foreach($request->category as $key => $category)
        {
            $advert->pivotCategory()->attach($advert->id, [
                'type' 	    => $request->type,
                'category'  => $category,
                'sort'      => $key
            ]);
        }
        
        $advert->pivotBrand()->attach($advert->id, [
            'type' 	=> $request->type,
            'brand' 	=> $request->brand
        ]);
        
        $advert->pivotModel()->attach($advert->id, [
            'type' 	=> $request->type,
            'model' 	=> $request->model
        ]);
        
        $advert->pivotTransmission()->attach($advert->id, [
            'type' 	    => $request->type,
            'transmission'  => $request->transmission
        ]);
        
        $advert->pivotDrive()->attach($advert->id, [
            'type' 	=> $request->type,
            'drive' 	=> $request->drive
        ]);
        
        $advert->pivotFuel()->attach($advert->id, [
            'type' 	=> $request->type,
            'fuel' 	=> $request->fuel
        ]);
        
        $advert->pivotYear()->attach($advert->id, [
            'type' 	=> $request->type,
            'year' 	=> $request->year
        ]);
        
        $advert->pivotBody()->attach($advert->id, [
            'type' => $request->type,
            'body' => $request->body
        ]);
        
        $advert->pivotPrice()->attach($advert->id, [
	    'type' 	=> $request->type,
	    'price' 	=> $request->price
        ]);
        
        $advert->pivotCityRaj()->attach($advert->id, [
            'type' 		=> $request->type,
            'city_raj' 	=> $request->city_raj
        ]);
        
        $advert->pivotAddress()->attach($advert->id, [
            'type' 		=> $request->type,
            'address' 	=> $request->address
        ]);
        
        if($request->image)
        {
            foreach($request->image as $image)
            {
                $advert->pivotImages()->attach($advert->id, [
                    'type' 	=> $request->type,
                    'fid' 	=> $image['fid'],
                    'sort' 	=> $image['sort']
                ]);
                \Img::setStatus($image['fid']);
            }
        }
	
	$advert->pivotUserInfo()->attach($advert->id, [
			'type' 	=> $request->type,
			'name' 	=> $request->user['name'],
			'email' => $request->user['email'],
			'phone' => $request->user['phone'],
		]);
        
        //$now = date('Y-m-d');
        //$start_date = strtotime($advert->created_at);
        //$end_date = strtotime('+'.\Option::get('advert.endday').' day', $start_date);
        //
        //$advert->pivotEnd()->attach($advert->id, [
        //    'type' 	=> $request->type,
        //    'end' 	=> date('Y-m-d H:i:s', $end_date)
        //]);
        
        \Event::fire('advert.presave', array($request, $advert));
        
        return redirect()->route('user.adverts', $advert->user_id)->with('success', 'Объявление создано.');
    }
    /* public function edit
    * @param $id
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function edit($id)
    {
        $advert = \Advert::findOrFail($id);
        $advert->load(
            'pivotBody', 'pivotCity', 'pivotCategory', 'pivotImages', 'pivotPrice',
            'pivotBrand', 'pivotModel', 'pivotTransmission', 'pivotDrive', 'pivotCityRaj', 'pivotAddress',
            'pivotFuel', 'pivotYear'
        );
        
        return view('advert::site.transport.edit', compact('advert'));
    }
    /* public function update
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function update(\Modules\Advert\Http\Requests\TransportReq $request, $id)
    {
        $advert = \Advert::findOrFail($id);
        //$advert->user_id 	= $user->id;
        $advert->type 		= $request->type;
        $advert->name 		= $request->name;
        $advert->status 	= 2;
        $advert->save();
        
        $advert->pivotCity()->detach($advert->id);
        $advert->pivotCity()->attach($advert->id, [
            'type' 	=> $request->type,
            'city' 	=> $request->city
        ]);
        
        $advert->pivotMetro()->detach($advert->id);
        if($request->metro)
        {
            $advert->pivotMetro()->attach($advert->id, [
                'type' 	=> $request->type,
                'metro' => $request->metro
            ]);
        }
           
        $advert->pivotCategory()->detach($advert->id);
        foreach($request->category as $key => $category)
        {
            $advert->pivotCategory()->attach($advert->id, [
                'type' 	    => $request->type,
                'category'  => $category,
                'sort'      => $key
            ]);
        }
        
        $advert->pivotBrand()->detach($advert->id);
        $advert->pivotBrand()->attach($advert->id, [
            'type' 	=> $request->type,
            'brand' 	=> $request->brand
        ]);
        
        $advert->pivotModel()->detach($advert->id);
        $advert->pivotModel()->attach($advert->id, [
            'type' 	=> $request->type,
            'model' 	=> $request->model
        ]);
        
        $advert->pivotTransmission()->detach($advert->id);
        $advert->pivotTransmission()->attach($advert->id, [
            'type' 	    => $request->type,
            'transmission'  => $request->transmission
        ]);
        
        $advert->pivotDrive()->detach($advert->id);
        $advert->pivotDrive()->attach($advert->id, [
            'type' 	=> $request->type,
            'drive' 	=> $request->drive
        ]);
        
        $advert->pivotFuel()->detach($advert->id);
        $advert->pivotFuel()->attach($advert->id, [
            'type' 	=> $request->type,
            'fuel' 	=> $request->fuel
        ]);
        
        $advert->pivotYear()->detach($advert->id);
        $advert->pivotYear()->attach($advert->id, [
            'type' 	=> $request->type,
            'year' 	=> $request->year
        ]);
        
        $advert->pivotBody()->detach($advert->id);
        $advert->pivotBody()->attach($advert->id, [
            'type' => $request->type,
            'body' => $request->body
        ]);
        
        $advert->pivotPrice()->detach($advert->id);
        $advert->pivotPrice()->attach($advert->id, [
	    'type' 	=> $request->type,
	    'price' 	=> $request->price
	]);
	
	$advert->pivotCityRaj()->detach($advert->id);
	$advert->pivotCityRaj()->attach($advert->id, [
		'type' 		=> $request->type,
		'city_raj' 	=> $request->city_raj
	]);
	
	$advert->pivotAddress()->detach($advert->id);
	$advert->pivotAddress()->attach($advert->id, [
		'type' 		=> $request->type,
		'address' 	=> $request->address
	]);
        
        $advert->pivotImages()->detach($advert->id);
        if($request->image)
        {
            foreach($request->image as $image)
            {
                $advert->pivotImages()->attach($advert->id, [
                    'type' 	=> $request->type,
                    'fid' 	=> $image['fid'],
                    'sort' 	=> $image['sort']
                ]);
                \Img::setStatus($image['fid']);
            }
        }
	
	$advert->pivotUserInfo()->detach($advert->id);
		$advert->pivotUserInfo()->attach($advert->id, [
			'type' 	=> $request->type,
			'name' 	=> $request->user['name'],
			'email' => $request->user['email'],
			'phone' => $request->user['phone'],
		]);
        
        return redirect()->route('user.adverts', $advert->user_id)->with('success', 'Объявление обновлено.');
    }
    /* public function show
    * @param $id
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function show($id)
    {
        $advert = \Advert::findOrFail($id);
        $advert->load(
            'pivotBody', 'pivotCity', 'pivotMetro', 'pivotCategory', 'pivotImages', 'pivotPrice',
            'pivotBrand', 'pivotModel', 'pivotTransmission', 'pivotDrive', 'pivotCityRaj', 'pivotAddress',
            'pivotFuel', 'pivotYear'
        );
        
        if (!$advert || $advert->status == 0 || $advert->status == 2 || $advert->status == 3 || $advert->status == 4) {
		    abort(404);
		}
        
        \MetaTag::set('title', $advert->name);
        \MetaTag::set('keywords', $advert->categoryKeywords);
        \MetaTag::set('description', $advert->metaDescription);
        
        \Event::fire('advert.views', array($advert));
        
        return view('advert::site.transport.show', compact('advert'));
    }
     
    
}