<?php namespace Modules\Advert\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;
use Former\Facades\Former;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class AdvertController extends Controller {
	
	/* public function category
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function category(Request $request, $category)
	{
		$items = \Advert::category($category)->toArray();
		$page = ($request->page) ? ($request->page - 1) : 0;
		
		$subcategory = $this->loadCategory($category);
		$filter = 'advert::site.'.$category.'.form-filter';
		
		$adverts = $this->getAdverts($items);
		$advertsCount = count($items);
		//$adverts = array_chunk($adverts, 10);
		//$adverts = !empty($adverts[$page]) ? $adverts[$page] : null;
		$currentPage = LengthAwarePaginator::resolveCurrentPage()-1;
		$collection = new Collection($adverts);
		
		$perPage = !empty($request->pages) ? $request->pages : 25;
		
		$currentAdvertsResults = $collection->slice($currentPage * $perPage, $perPage)->all();
		$adverts= new LengthAwarePaginator($currentAdvertsResults, count($collection), $perPage);
		
		//dd($adverts);
		
		//$premium = null;
		$premium = $this->getPremium($items);
		$premium = array_chunk($premium, 3);
		$premium = !empty($premium[$page]) ? $premium[$page] : null;
		
		//dd($items);
		//dd($premium);
		

		if($request->ajax())
		{
			$filterData = $this->filterParam($request, $category);
			
			$result = view('advert::site.category-adverts', compact('adverts', 'premium', 'category'))->render();
			return response()->json(['result' => $result, 'total' => $advertsCount, 'filterData' => $filterData], 200);
		}
		else
		{
			return view('advert::site.category', compact('adverts', 'premium', 'category', 'subcategory', 'filter', 'advertsCount'));
		}
	}
	/* public function add
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function add()
	{
		$categories = \TaxonomyTerm::where('taxonomy_id', 1)->where('parent_id', 0)->get();
		$result = view('advert::site.add', compact('categories'))->render();
		
		return response()->json(['result' => $result], 200);
	}
	/* public function addCategory
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function addCategory($category)
	{
		$category = \TaxonomyTerm::findBySlugOrIdOrFail($category);
		
		$title = 'Добавить объявление "'.$category->name.'"';
		
		\MetaTag::set('title', $title);
		
		$add = view('advert::site.'.$category->slug.'.create');

		return view('advert::site.add-category', compact('title', 'add'));
	}
	/* public function userAdverts
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function userAdverts($id)
	{
		$user = \Sentinel::findById($id);
		$adverts = \Advert::where('user_id', $id)->orderBy('created_at', 'DESC')->paginate(10);
		$adverts->load('pivotImages', 'pivotPrice', 'pivotEnd');
		
		return view('advert::site.page-uder-adverts', compact('adverts', 'user'));
	}
	/* public function getPhone
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function getPhone(Request $request)
	{
		$advert = \Advert::findOrFail($request->advert);
		
		$phone = isset($advert->userInfo->phone) ? $advert->userInfo->phone : $advert->user->phone;
		
		return response()->json(['result' => $phone]);
	}
	/* public function sendmessage
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *| Написать пользователю обявления
	 *-----------------------------------
	 */
	public function sendmessage(Request $request)
	{
		$advert = \Advert::findOrFail($request->advert);
		return response()->json(['result' => view('advert::modal.sendmessage', compact('advert'))->render()]);
	}
	/* public function sendmessagePost
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *| Написать пользователю обявления
	 *-----------------------------------
	 */
	public function sendmessagePost(\Modules\Advert\Http\Requests\SensMessageReq $request)
	{
		$data = $request->all();
		$advert =  \Advert::findOrFail($request->advert);
		$data['advert'] = $advert;
		$data['sendto'] = isset($advert->userInfo->email) ? $advert->userInfo->email : $advert->user->email;
		
		\Mail::later(10, 'advert::modal.mail-sendmessage', $data, function($message) use ($data)
		{
			$message->to($data['sendto'])->subject('Сообщение от пользователя "'.$data['username'].'"');
		});
	}
	/* public function complain
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *| Жалоба на обявление
	 *-----------------------------------
	 */
	public function complain(Request $request)
	{
		$advert = \Advert::findOrFail($request->advert);
		return response()->json(['result' => view('advert::modal.complain', compact('advert'))->render()]);
	}
	/* public function complainPost
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *| Жалоба на обявление
	 *-----------------------------------
	 */
	public function complainPost(\Modules\Advert\Http\Requests\ComplainReq $request)
	{
		$data = $request->all();
		$advert =  \Advert::findOrFail($request->advert);
		$data['advert'] = $advert;
		$data['sendto'] = \Option::get('advert.complain_mail');
		
		\Mail::later(10, 'advert::modal.mail-complain', $data, function($message) use ($data)
		{
			$message->to($data['sendto'])->subject('Жалоба от пользователя "'.$data['username'].'"');
		});
	}
	/* public function similar
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function similar($data)
	{
        //$adverts = collect();
	    $adverts = \Advert::where('type', $data->type)->take(10)->get();
         //dd($adverts);
        // print '<pre>' . htmlspecialchars(print_r($adverts, true)) . '</pre>';
	    return view('advert::site.similar', compact('adverts'));
	}
	/* public function vip
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function vip()
	{
		$arg = (\Request::segment(1) == 'adverts') ? \Request::segment(2) : \Request::segment(1);
		$query = \Advert::where('advert.type', $arg);
		$query->leftJoin('advert_uslugi', 'advert_uslugi.advert_id', '=', 'advert.id');
		$query->where('advert_uslugi.flag', 1);
		$query->where('advert_uslugi.services_id', 2);
		$adverts = $query->orderBy(\DB::raw('RAND()'))->limit(2)->get();
		$adverts->load('pivotImages', 'pivotMetro', 'pivotPrice', 'pivotCategory');
		
		return view('advert::site.vip', compact('adverts'));
	}
	/* public function search
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function search(\Modules\Advert\Http\Requests\ShearchReq $request)
	{
	    $razdel = \TaxonomyTerm::findOrFail($request->category);
	    $data['search'] = $request->search;
	    if($request->city)
	    {
		session(['city' => $request->city]);
	    }
	    $data['city'] = $request->city;
	    
	    $link = url('/').'/adverts/'.$razdel->slug.'?'.http_build_query($data);
	    return response()->json(['link' => $link]);
	}
	/* public function favoritesAdd
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function favoritesAdd(Request $request)
	{
		$user_id = (\Sentinel::check()) ? \Sentinel::getUser()->id : \Session::getId();
		
		$advert =  \Advert::findOrFail($request->id);
		$favorites = \DB::table('advert_favorites')->where('advert_id', $advert->id)->where('user_id', $user_id)->count();
		
		if($favorites == 0)
		{
			$advert->pivotFavorites()->attach($advert->id, [
				'user_id' => $user_id,
			]);
		}
	}
	/* public function favorites
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function favorites()
	{
		$user = (\Sentinel::check()) ? \Sentinel::getUser() : null;
		$adverts = \Advert::getFavorites();
		return view('advert::site.page-favorites', compact('adverts', 'user'));
	}
	/* public function favoritesDel
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function favoritesDel(Request $request)
	{
		$user_id = (\Sentinel::check()) ? \Sentinel::getUser()->id : \Session::getId();
		
		foreach($request->del as $del)
		{
			\DB::table('advert_favorites')->where('advert_id', $del)->where('user_id', $user_id)->delete();
		}
		return redirect()->back();
	}
	/* public function loadCategory
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function loadCategory($category)
	{
		$term = \TaxonomyTerm::findBySlug($category)->getDescendants()->toHierarchy();
		
		$out = '';
		if($term)
		{
		    $out .= '<div class="filter-checked-items">';
		    $out .= '<ul class="filter-ch-list">';
		    foreach($term as $item)
		    {
			$out .= '<li><a href="'.route('adverts.category', $item->slug).'">'.$item->name.' <span>'.$item->countCategory.'</span></a></li>';
		    }
		    $out .= '</ul></div>';
		}
		return $out;
	}
	/* public function checkMail
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function checkMail(Request $request)
	{
	    $user = \User::where('email', $request->email)->first();
	    
	    if($user)
	    {
		return response()->json(['Пользователь с таким e-mail уже зарегестирован. Пожалуйста авторизируйтесь.'], 422);
	    }
	    else{
		return response()->json(['OK'], 200);
	    }
	}
	/* public function advertPublic
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function advertPublic(Request $request, $id)
	{
		$user = \Sentinel::getUser();
		
		$advert = \Advert::findOrFail($id);
		$advert->status = ($user->inRole('user')) ? 2 : 1;
		$advert->save();
		
		return redirect()->back()->with('success', 'Объявление обновлено.');
	}
	/* public function advertUnPublic
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function advertUnPublic(Request $request, $id)
	{
		$advert = \Advert::findOrFail($id);
		$advert->status = 0;
		$advert->save();
		return redirect()->back()->with('success', 'Объявление обновлено.');
	}
	/* public function advertDelete
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function advertDelete(Request $request, $id)
	{
		\Advert::deleteOne($id);
		return redirect()->back()->with('success', 'Объявление удалено навсегда и его невозможно восстановить.');
	}
	/* public function filterParam
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function filterParam($request, $category)
	{
		$filterData = array();
		
		$titles = array(
			'rabota-i-obrazovanie' 	=> 'Вид услуги',
			'uslugi-i-deyatelnost' 	=> 'Вид услуги',
			'biznes-i-partnerstvo' 	=> 'Вид услуги',
			'elektronika' 		=> 'Вид электроники',
			'dom-i-sad' 		=> 'Вид товара',
			'zhivotnye-i-rasteniya' => 'Вид товара',
			'odezhda-i-aksesuary'	=> 'Вид товара',
			'krasota-i-zdorove'	=> 'Вид товара',
			'detskiy-mir'		=> 'Вид товара',
			'sport-otdykh-khobbi'	=> 'Вид товара',
			'otdam-darom'		=> 'Вид товара',
			'drugoe' 		=> 'Вид товара',
			'nedvizhimost' 		=> 'Вид товара',
		);
		
		switch($category)
		{
			case 'transport':
				$filterData['type'] =  '<div class="filter-checked-items">'.\Form::radios2('category[1]', \TaxonomyTerm::get(1, $request->input('category.0')), $request->category, ['class' => 'styler type']).'</div>';
				$filterData['brand'] =  Former::select('brand')->options(\Transport::getBrands($request->input('category.1')))->setAttributes(['class' => 'selectpicker', 'title' => 'Выбрать марку', 'id' => 'brand-filter'])->render();
				$filterData['model'] =	Former::select('model')->options(\Transport::getModels($request->brand))->setAttributes(['class' => 'selectpicker', 'title' => 'Выбрать модель', 'id' => 'model-filter'])->render();
			break;
				
			case 'rabota-i-obrazovanie':
				$filterData['append'] = view('advert::site.filter.append-'.$request->input('category.0'))->render();
			break;
				
			default:
				$options = \TaxonomyTerm::get(1, $request->input('category.0'));
				$title = $titles[$category];
				$filterData['append'] = view('advert::site.filter.append-sub', compact('options', 'title'))->render();
			
			
		}
		
		return $filterData;
	}
	/* public function getAdverts
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function getAdverts($items)
	{
		foreach($items as $key => $item)
		{
			if(isset($item['pivot_uslugi']))
			{
				foreach($item['pivot_uslugi'] as $uslugi)
				{
					if($uslugi['pivot']['services_id'] == 1)
					{
						unset($items[$key]);
					}
				}
			}
		}
		$adverts = array();
		$adverts = $this->dataAdverts($items);
		return $adverts;
	}
	/* public function getPremium
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function getPremium($items)
	{
		$adverts = array();
		foreach($items as $key => $item)
		{
			if(isset($item['pivot_uslugi']))
			{
				foreach($item['pivot_uslugi'] as $uslugi)
				{
					if($uslugi['pivot']['services_id'] == 1 && $uslugi['pivot']['flag'] == 1)
					{
						$adverts[] = $items[$key];
					}
				}
			}
		}
		$adverts = $this->dataAdverts($adverts);
		return $adverts;
	}
	/* public function dataAdverts
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function dataAdverts($items)
	{
		$adverts = array();
		$data = array();
		
		foreach($items as $item)
		{
			$data['id'] 	= $item['id'];
			$data['type'] 	= $item['type'];
			$data['name'] 	= $item['name'];
			$data['price'] 	= !empty($item['pivot_price']) ? number_format($item['pivot_price'][0]['pivot']['price'], 0, '.', ' ') . ' руб.': null;
			$data['image'] 	= !empty($item['pivot_images']) ? (object) $item['pivot_images'][0]['pivot'] : null;
			$data['date'] 	= cuteDate($item['updated_at']);
			$data['metro']  = !empty($item['pivot_metro']) ? '<i class="glyph-icon flaticon-symbols" style="color:'.$item['pivot_metro'][0]['pivot']['color'].'"></i> '.$item['pivot_metro'][0]['pivot']['name'] : null;
			
			$data['premium'] = null;
			if(isset($item['pivot_uslugi']))
			{
				foreach($item['pivot_uslugi'] as $uslugi)
				{
					if($uslugi['pivot']['services_id'] == 1 && $uslugi['pivot']['flag'] == 1)
					{
						$data['premium'] = '<span class="label label-warning premium">Премиум объявление</span>';
					}
				}
			}
			
			$data['color'] = null;
			if(isset($item['pivot_uslugi']))
			{
				foreach($item['pivot_uslugi'] as $uslugi)
				{
					if($uslugi['pivot']['services_id'] == 3 && $uslugi['pivot']['flag'] == 1)
					{
						$data['color'] = ' catalog-fav';
					}
				}
			}
			
			$data['favorites']  = !empty($item['pivot_favorites']) ? '<i class="fa fa-star"></i>' : '<i class="fa fa-star-o"></i>';
			
			$data['category'] = null;
			$category = array();
			if(isset($item['pivot_category']))
			{
				foreach($item['pivot_category'] as $uslugi)
				{
					$category[] = '<li>'.$uslugi['pivot']['name'].'</li>';
				}
			}
			krsort($category);
			$data['category'] = implode('', $category);
			
			$adverts[] = (object) $data;
		}
		return $adverts;
	}
	
	
}