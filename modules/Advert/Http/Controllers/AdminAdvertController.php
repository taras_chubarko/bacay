<?php namespace Modules\Advert\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class AdminAdvertController extends Controller {
	
	public function index()
	{
		$adverts = \Advert::getAdminAdverts(2);
		$categories = \TaxonomyTerm::where('taxonomy_id', 1)->where('parent_id', 0)->get();
		$types = array();
		foreach($categories as $category)
		{
		    $types[$category->slug] = $category->name;
		}
		return view('advert::admin.index', compact('adverts', 'categories', 'types'));
	}
    /* public function advertStatus
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function advertStatus($status)
    {
        $adverts = \Advert::getAdminAdverts($status);
        $categories = \TaxonomyTerm::where('taxonomy_id', 1)->where('parent_id', 0)->get();
        $types = array();
        foreach($categories as $category)
        {
            $types[$category->slug] = $category->name;
        }
		return view('advert::admin.index', compact('adverts', 'categories', 'types'));
    }
	/* public function destroy
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function destroy($id)
	{
		\Advert::deleteOne($id);
		return redirect()->back()->with('success', 'Объявление удалено навсегда и его невозможно восстановить.');
	}
	/* public function advertPublic
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function advertPublic(Request $request, $id)
	{
		$advert = \Advert::findOrFail($id);
		$advert->status = 1;
		$advert->public_at = \Carbon::now();
		$advert->save();
	
		$advert->pivotEnd()->attach($advert->id, [
			'type' 	=> $advert->type,
			'end' 	=> \Carbon::now()->addDays(\Option::get('advert.endday'))
		]);
		
		return redirect()->back()->with('success', 'Объявление обновлено.');
	}
	/* public function advertUnPublic
	 * @param $id use Illuminate\Http\Request;
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function advertUnPublic(Request $request, $id)
	{
		$advert = \Advert::findOrFail($id);
		$advert->status = 0;
		$advert->save();
		return redirect()->back()->with('success', 'Объявление обновлено.');
	}
	/* public function advertCancel
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function advertCancel(Request $request, $id)
	{
		$advert = \Advert::findOrFail($id);
		$advert->status = 3;
		$advert->save();
		return redirect()->back()->with('success', 'Объявление обновлено.');
	}
	/* public function deladmin
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function deladmin(Request $request, $id)
	{
		$advert = \Advert::findOrFail($id);
		$advert->status = 4;
		$advert->save();
		return redirect()->back()->with('success', 'Объявление удалено из админки.');
	}
    
    
}