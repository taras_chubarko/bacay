<?php namespace Modules\Advert\Http\Middleware; 

use Closure;

class ViewMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if(\Sentinel::check())
        {
            //$user = \Sentinel::getUser();
            //
            //if($user->inRole('admin'))
            //{
            //return $next($request);
            //}
            //
            //if($user->hasAccess(['advert.view']))
            //{
            //return $next($request);
            //}
            //else
            //{
            //abort(403, 'У вас нет доступа для просмотра объявлений.');
            //}
                        return $next($request);
        }
        
        if (\Sentinel::guest())
            {
                $role = \Sentinel::findRoleBySlug('anonim');
            
            if($role->hasAccess(['advert.view']))
            {
            return $next($request);
            }
            else
            {
            if($request->ajax())
            {
                return response()->json(['error' => 'У вас нет доступа для создания объявлений.'], 422);
            }
            abort(403, 'У вас нет доступа для просмотра объявлений.');
            }
            
            }
            
        abort(403, 'У вас нет доступа для просмотра объявлений.');
    }
    
}
