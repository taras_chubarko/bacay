<?php namespace Modules\Advert\Http\Middleware; 

use Closure;

class CreateMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if(\Sentinel::check())
	{
	    $user = \Sentinel::getUser();
	    
	    if($user->inRole('admin'))
	    {
            $segments = $request->segments();
            if(!empty($segments[2]))
            {
                $limit = \Option::get('advert.limit.'.$segments[2]);
                if($limit != 0 && $user->countAdvert[$segments[2]] >= $limit)
                {
                    abort(403, 'Вы изчерпали лимит подачи объявлений.');
                }
            }
            return $next($request);
	    }
	    
	    if($user->hasAccess(['advert.create']))
	    {
            $segments = $request->segments();
            if(!empty($segments[2]))
            {
                $limit = \Option::get('advert.limit.'.$segments[2]);
                if($limit != 0 && $user->countAdvert[$segments[2]] >= $limit)
                {
                    abort(403, 'Вы изчерпали лимит подачи объявлений.');
                }
            }
            return $next($request);
	    }
	    else
	    {
            abort(403, 'У вас нет доступа для создания объявлений.');
	    }
	}
	
	if (\Sentinel::guest())
        {
        $role = \Sentinel::findRoleBySlug('anonim');
	    
	    if($role->hasAccess(['advert.create']))
	    {
		return $next($request);
	    }
	    else
	    {
		if($request->ajax())
		{
		    return response()->json(['error' => 'У вас нет доступа для создания объявлений.'], 422);
		}
		abort(403, 'У вас нет доступа для создания объявлений.');
	    }
	    
        }
	
	abort(404);
    }
    
}
