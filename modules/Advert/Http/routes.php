<?php

Route::group(['middleware' => 'web', 'prefix' => 'adverts', 'namespace' => 'Modules\Advert\Http\Controllers'], function()
{
	Route::get('{category}', [
		'middleware' 	=> ['Modules\Advert\Http\Middleware\ViewMiddleware'],
		'as' 		=> 'adverts.category',
		'uses' 		=> 'AdvertController@category'
	])->where('category', TaxonomyTerm::allSlug());
	
	Route::get('add', [
		'middleware' 	=> ['Modules\Advert\Http\Middleware\CreateMiddleware'],
		'as' 		=> 'adverts.add',
		'uses' 		=> 'AdvertController@add'
	]);
	
	Route::get('add/{category}', [
		'middleware' 	=> ['Modules\Advert\Http\Middleware\CreateMiddleware'],
		'as' 		=> 'adverts.add.category',
		'uses' 		=> 'AdvertController@addCategory'
	]);
	
	Route::post('get/phone', [
		'as' 	=> 'adverts.get.phone',
		'uses' 	=> 'AdvertController@getPhone'
	]);
	
	Route::get('sendmessage', [
		'as' 	=> 'adverts.sendmessage',
		'uses' 	=> 'AdvertController@sendmessage'
	]);
	
	Route::post('sendmessage', [
		'as' 	=> 'adverts.sendmessage',
		'uses' 	=> 'AdvertController@sendmessagePost'
	]);
	
	Route::get('complain', [
		'as' 	=> 'adverts.complain',
		'uses' 	=> 'AdvertController@complain'
	]);
	
	Route::post('complain', [
		'as' 	=> 'adverts.complain',
		'uses' 	=> 'AdvertController@complainPost'
	]);
	
	Route::get('load/{category}', [
		'as' 	=> 'adverts.load.category',
		'uses' 	=> 'AdvertController@loadCategory'
	]);
	
	Route::post('check/mail', [
		'as' 	=> 'adverts.check.mail',
		'uses' 	=> 'AdvertController@checkMail'
	]);
	
	
});

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Advert\Http\Controllers'], function()
{
	Route::resource('nedvizhimost', 'NedvizhimostController');
	Route::resource('transport', 'TransportController');
	Route::resource('rabota-i-obrazovanie', 'RabotaController');
	Route::resource('uslugi-i-deyatelnost', 'UslugiController');
	Route::resource('biznes-i-partnerstvo', 'BiznesController');
	Route::resource('elektronika', 'ElektronikaController');
	Route::resource('dom-i-sad', 'DomsadController');
	Route::resource('zhivotnye-i-rasteniya', 'ZhivotnyeController');
	Route::resource('odezhda-i-aksesuary', 'OdezhdaController');
	Route::resource('krasota-i-zdorove', 'KrasotaController');
	Route::resource('detskiy-mir', 'DetskiyController');
	Route::resource('sport-otdykh-khobbi', 'SportController');
	Route::resource('otdam-darom', 'OtdamController');
	Route::resource('obmen-i-barakholka', 'ObmenController');
	Route::resource('drugoe', 'DrugoeController');
	
	
	Route::get('user/{id}/adverts', [
		'middleware' 	=> 'auth',
		'as' 		=> 'user.adverts',
		'uses' 		=> 'AdvertController@userAdverts'
	]);
	
	Route::post('search', [
		'as' 	=> 'adverts.search',
		'uses' 	=> 'AdvertController@search'
	]);
	
	Route::post('favorites/add', [
		'as' 	=> 'adverts.favorites.add',
		'uses' 	=> 'AdvertController@favoritesAdd'
	]);
	
	Route::get('favorites', [
		'as' 	=> 'adverts.favorites',
		'uses' 	=> 'AdvertController@favorites'
	]);
	
	Route::post('favorites/del', [
		'as' 	=> 'adverts.favorites.del',
		'uses' 	=> 'AdvertController@favoritesDel'
	]);
	
	Route::post('advert/{id}/public', [
		'as' 	=> 'advert.id.public',
		'uses' 	=> 'AdvertController@advertPublic'
	]);
	
	Route::post('advert/{id}/unpublic', [
		'as' 	=> 'advert.id.unpublic',
		'uses' 	=> 'AdvertController@advertUnPublic'
	]);
	
	Route::delete('advert/{id}/delete', [
		'as' 	=> 'advert.id.delete',
		'uses' 	=> 'AdvertController@advertDelete'
	]);
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\Advert\Http\Controllers'], function()
{
	Route::resource('adverts', 'AdminAdvertController');
    
	Route::post('advert/{id}/public', [
		'as' 	=> 'admin.advert.id.public',
		'uses' 	=> 'AdminAdvertController@advertPublic'
	]);
	    
	Route::post('advert/{id}/unpublic', [
		'as' 	=> 'admin.advert.id.unpublic',
		'uses' 	=> 'AdminAdvertController@advertUnPublic'
	]);
	
	Route::post('advert/{id}/cancel', [
		'as' 	=> 'admin.advert.id.cancel',
		'uses' 	=> 'AdminAdvertController@advertCancel'
	]);
	
	Route::get('adverts/status/{status}', [
		'as' 	=> 'admin.adverts.status',
		'uses' 	=> 'AdminAdvertController@advertStatus'
	]);
	
	Route::delete('advert/{id}/deladmin', [
		'as' 	=> 'admin.advert.id.deladmin',
		'uses' 	=> 'AdminAdvertController@deladmin'
	]);
    
});