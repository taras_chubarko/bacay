<?php namespace Modules\Advert\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShearchReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'category' 	=> 'required',
			'search'  	=> 'required',
		];
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		$validator->setAttributeNames([
			'category' 	=> '"Выбирите категрию"',
			'search'  	=> '"Поиск по сайту"',
		]);
	 
		return $validator;
	}

}
