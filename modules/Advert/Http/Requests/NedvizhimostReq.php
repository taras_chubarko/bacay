<?php namespace Modules\Advert\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NedvizhimostReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		if(\Sentinel::check())
		{
			return [
				'name' 		=> 'required',
				'category.1'  	=> 'required',
				'city'  	=> 'required',
				'typadvert' 	=> 'required',
				'price' 	=> 'required|numeric',
				
				'typhouse' 	=> 'required_if:category.1,16,17',
				'qtyroom' 	=> 'required_if:category.1,16,17,20|integer',
				'floor' 	=> 'required_if:category.1,16,17|integer',
				'qtyfloor' 	=> 'required_if:category.1,16,17,20|integer',
				'area' 		=> 'required_if:category.1,16,17,19,20,21,22,23,24|numeric',
				'area_sot'	=> 'required_if:category.1,18,20|numeric',
				'vid_object'	=> 'required_if:category.1,19,24',
				'vid_build'	=> 'required_if:category.1,20',
				'material_walls'=> 'required_if:category.1,20',
				'car_places'	=> 'required_if:category.1,21,22,23',
				'country'	=> 'required_if:category.1,24',
			];
		}
		else
		{
			return [
				'user.name' 	=> 'required',
				'user.phone' 	=> 'required|phone',
				'user.email' 	=> 'required|email|unique:users,email',
				//'user.password' => 'required|min:6|confirmed',
				//'user.password_confirmation' => 'required|min:6',
				'name' 		=> 'required',
				'category.1'  	=> 'required',
				'city'  	=> 'required',
				'typadvert' 	=> 'required',
				'price' 	=> 'required|numeric',
				
				'typhouse' 	=> 'required_if:category.1,16,17',
				'qtyroom' 	=> 'required_if:category.1,16,17,20|integer',
				'floor' 	=> 'required_if:category.1,16,17|integer',
				'qtyfloor' 	=> 'required_if:category.1,16,17,20|integer',
				'area' 		=> 'required_if:category.1,16,17,19,20,21,22,23,24|numeric',
				'area_sot'	=> 'required_if:category.1,18,20|numeric',
				'vid_object'	=> 'required_if:category.1,19,24',
				'vid_build'	=> 'required_if:category.1,20',
				'material_walls'=> 'required_if:category.1,20',
				'car_places'	=> 'required_if:category.1,21,22,23',
				'country'	=> 'required_if:category.1,24',
			];
		}
		
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		if(\Sentinel::check())
		{
			$validator->setAttributeNames([
				'name' 		=> '"Заголовок"',
				'city'  	=> '"Город"',
				'category.1' 	=> '"Категория"',
				'typadvert' 	=> '"Тип объявления"',
				'typhouse' 	=> '"Тип дома"',
				'price' 	=> '"Цена, Р"',
				'qtyroom' 	=> '"Кол-во комнат"',
				'area' 		=> '"Площадь"',
				'floor' 	=> '"Этаж"',
				'qtyfloor' 	=> '"Этажей в доме"',
				'vid_object' 	=> '"Вид объекта"',
				'area_sot' 	=> '"Площадь участка в сотках"',
				'vid_build' 	=> '"Вид строения"',
				'material_walls'=> '"Материал стен"',
				'car_places'	=> '"Машиноместо"',
				'country'	=> '"Страна"',
			]);
		}
		else
		{
			$validator->setAttributeNames([
				'user.name' 	=> '"Ваше имя"',
				'user.phone' 	=> '"Ваш телефон"',
				'user.email' 	=> '"Ваш e-mail"',
				'user.password' => '"Пароль"',
				'user.password_confirmation' 	=> '"Повтор пароля"',
				'name' 		=> '"Заголовок"',
				'city'  	=> '"Город"',
				'category.1' 	=> '"Категория"',
				'typadvert' 	=> '"Тип объявления"',
				'typhouse' 	=> '"Тип дома"',
				'price' 	=> '"Цена, Р"',
				'qtyroom' 	=> '"Кол-во комнат"',
				'area' 		=> '"Площадь"',
				'floor' 	=> '"Этаж"',
				'qtyfloor' 	=> '"Этажей в доме"',
				'vid_object' 	=> '"Вид объекта"',
				'area_sot' 	=> '"Площадь участка в сотках"',
				'vid_build' 	=> '"Вид строения"',
				'material_walls'=> '"Материал стен"',
				'car_places'	=> '"Машиноместо"',
				'country'	=> '"Страна"',
			]);
		}
		
		return $validator;
	}

}
