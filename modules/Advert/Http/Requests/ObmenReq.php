<?php namespace Modules\Advert\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ObmenReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		if(\Sentinel::check())
		{
			return [
				'name' 		=> 'required',
				'city'  	=> 'required',
			];
		}
		else
		{
			return [
				'user.name' 	=> 'required',
				'user.phone' 	=> 'required|phone',
				'user.email' 	=> 'required|email|unique:users,email',
				//'user.password' => 'required|min:6|confirmed',
				//'user.password_confirmation' => 'required|min:6',
				'name' 		=> 'required',
				'city'  	=> 'required',
			];
		}
		
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		if(\Sentinel::check())
		{
			$validator->setAttributeNames([
				'name' 		=> '"Заголовок"',
				'city'  	=> '"Город"',
			]);
		}
		else
		{
			$validator->setAttributeNames([
				'user.name' 	=> '"Ваше имя"',
				'user.phone' 	=> '"Ваш телефон"',
				'user.email' 	=> '"Ваш e-mail"',
				'user.password' => '"Пароль"',
				'user.password_confirmation' 	=> '"Повтор пароля"',
				'name' 		=> '"Заголовок"',
				'city'  	=> '"Город"',
			]);
		}
		
	 
		return $validator;
	}

}
