<?php namespace Modules\Advert\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ComplainReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'complain' 	=> 'required',
			'username'  	=> 'required',
			'usermail'  	=> 'required|email',
			'messages' 	=> 'required',
		];
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		$validator->setAttributeNames([
			'complain' 	=> '"Причина жалобы"',
			'username'  	=> '"Ваше имя"',
			'usermail'  	=> '"Ваша почта"',
			'message' 	=> '"Текст жалобы"',
		]);
	 
		return $validator;
	}

}
