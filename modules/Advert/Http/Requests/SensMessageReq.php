<?php namespace Modules\Advert\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SensMessageReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'subject' 	=> 'required',
			'username'  	=> 'required',
			'usermail'  	=> 'required|email',
			'messages' 	=> 'required',
		];
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		$validator->setAttributeNames([
			'subject' 	=> '"Тема сообщения"',
			'username'  	=> '"Ваше имя"',
			'usermail'  	=> '"Ваша почта"',
			'message' 	=> '"Текст сообщения"',
		]);
	 
		return $validator;
	}

}
