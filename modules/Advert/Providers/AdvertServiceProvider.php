<?php namespace Modules\Advert\Providers;

use Illuminate\Support\ServiceProvider;

class AdvertServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 * 
	 * @return void
	 */
	public function boot()
	{
		$this->registerTranslations();
		$this->registerConfig();
		$this->registerViews();
		
		\Event::listen('advert.presave', function ($request, $advert) {
			\Session::forget('images');
            if(!\Sentinel::check())
            {
                if(is_string($request->user_id))
                {
                    $password = str_random(8);
                    $credentials = [
                        'name' 	   => $request->user['name'],
                        'email'    => $request->user['email'],
                        'password' => $password,
                    ];
                    
                    $user = \Sentinel::registerAndActivate($credentials);
                    
                    $role = \Sentinel::findRoleById(2); /*2 - Регистрируем обычного пользователя*/
                    $role->users()->attach($user);
                    
                    $user->pivotProfile()->attach($user->id, [
                        'first_name' 	=> $request->user['name'],
                        'last_name' 	=> '',
                        'photo_big' 	=> '',
                        'network'	=> '',
                        'city' 		=> $advert->city->name,
                        'phone' 	=> $request->user['phone'],
                        'uid' 		=> '',
                        'profile' 	=> '',
                        'identity' 	=> '',
                    ]);
                    
                    \Sentinel::login($user);
                    
                    $advert->user_id 	= $user->id;
                    $advert->status 	= \Option::get('advert.publics');
                    $advert->save();
                    
                    $credentials['advertLink'] = route($advert->type.'.show', $advert->id);
                    
                    \Mail::later(5, 'advert::mail.new-user', $credentials, function($message) use ($credentials)
                    {
                        $message->to($credentials['email'])->subject('Ваше объявление на сайте "'.config('cms.name').'"');
                    });
                    
                    \Session::flash('warning', 'Ваш пароль на вход <strong>'.$password.'</strong> Советуем вам сменить пароль на свой в целях безопасности, для дальнейшего пользвания вашим профилем.');
                }
            }	
		});
		
		\Event::listen('advert.views', function ($advert)
		{
			$today = new \DateTime('today');
			$ip = \Request::ip();
			
			$views = \DB::table('advert_views')->where('advert_id', $advert->id)->where('ip', $ip)->where('view_at', '>=', $today)->count();
			
			if($views == 0)
			{
				$advert->pivotViews()->attach($advert->id, [
					'ip' => $ip,
				]);
			}
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{		
		//
	}

	/**
	 * Register config.
	 * 
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
		    __DIR__.'/../Config/config.php' => config_path('advert.php'),
		]);
		$this->mergeConfigFrom(
		    __DIR__.'/../Config/config.php', 'advert'
		);
	}

	/**
	 * Register views.
	 * 
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = null;//base_path('resources/views/modules/advert');

		$sourcePath = __DIR__.'/../Resources/views';

		//$this->publishes([
		//	$sourcePath => $viewPath
		//]);

		$this->loadViewsFrom(array_merge(array_map(function ($path) {
			return $path . '/modules/advert';
		}, \Config::get('view.paths')), [$sourcePath]), 'advert');
	}

	/**
	 * Register translations.
	 * 
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = null;//base_path('resources/lang/modules/advert');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'advert');
		} else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'advert');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
