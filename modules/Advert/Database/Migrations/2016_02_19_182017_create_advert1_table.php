<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvert1Table extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advert_category', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->integer('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('advert1');
    }

}
