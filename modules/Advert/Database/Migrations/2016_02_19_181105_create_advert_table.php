<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advert', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('name');
            $table->integer('status');
            $table->string('slug');
            $table->timestamps();
        });
        
        Schema::create('advert_body', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->text('body');
        });
        
        Schema::create('advert_images', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->integer('fid');
        });
        
        Schema::create('advert_price', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->decimal('price', 10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('advert');
    }

}
