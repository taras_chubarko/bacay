<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvtoTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advert_brand', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->string('type');
            $table->integer('brand');
        });
        
        Schema::create('advert_model', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->string('type');
            $table->integer('model');
        });
        
        Schema::create('advert_transmission', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->string('type');
            $table->integer('transmission');
        });
        
        Schema::create('advert_drive', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->string('type');
            $table->integer('drive');
        });
        
        Schema::create('advert_fuel', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->string('type');
            $table->integer('fuel');
        });
        
        Schema::create('advert_year', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->string('type');
            $table->integer('year');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('avto');
    }

}
