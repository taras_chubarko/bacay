<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvert2Table extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advert_typadvert', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->string('type');
            $table->integer('typadvert');
        });
        
        Schema::create('advert_typhouse', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->string('type');
            $table->integer('typhouse');
        });
        
        Schema::create('advert_qtyroom', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->string('type');
            $table->integer('qtyroom');
        });
        
        Schema::create('advert_area', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->string('type');
            $table->integer('area');
        });
        
        Schema::create('advert_floor', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->string('type');
            $table->integer('floor');
        });
        
        Schema::create('advert_qtyfloort', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->string('type');
            $table->integer('qtyfloor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('advert2');
    }

}
