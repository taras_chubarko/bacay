<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRabotaTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advert_service', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->integer('service');
        });
        
        Schema::create('advert_schedule', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->integer('schedule');
        });
        
        Schema::create('advert_experience', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->integer('experience');
        });
        
        Schema::create('advert_education', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->integer('education');
        });
        
        Schema::create('advert_gender', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->integer('gender');
        });
        
        Schema::create('advert_age', function(Blueprint $table)
        {
            $table->integer('advert_id')->unsigned()->nullable();
            $table->foreign('advert_id')->references('id')->on('advert')->onDelete('cascade');
            $table->integer('age');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rabota');
    }

}
