<div class="btn-group btn-group-100">
    <button type="button" class="btn btn-e btn-us dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i> Редактировать <span class="caret"></span></button>
    <ul class="dropdown-menu" role="menu">
        @if($advert->status == 0 || $advert->status == 4)
            <li><a class="ajax ajax-form" data-method="POST" href="{{ route('advert.id.public', $advert->id) }}">Активировать</a></li>
        @endif
        @if($advert->status == 1)
            <li><a class="ajax ajax-form" data-method="POST" href="{{ route('advert.id.unpublic', $advert->id) }}">Снять с публикации</a></li>
        @endif
        
        @if($advert->status != 2)
        <li><a href="{{ route($advert->type.'.edit', $advert->id) }}">Редактировать</a></li>
        @endif
        
        <li><a class="ajax ajax-form" data-method="DELETE" href="{{ route('advert.id.delete', $advert->id) }}">Удалить навсегда</a></li>
    </ul>
</div>