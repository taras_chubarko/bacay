@extends('site::layouts.master')

@section('content')
<div class="group">
    <div class="row">
            @if($user)
		<div class="col-md-3">
		    @include('user::site.menu-user', $user)
		</div>
	    @endif
    <div class="col-md-{{ (isset($user)) ? 9 : 12 }}">
    <div class="push-main-bl m-l-0">
        <div class="catalog-wrap">
            @if($adverts->count())
                {!! Form::open(array('route' => 'adverts.favorites.del', 'role' => 'form', 'class' => 'form-favorites')) !!}
                <div class="row h-50">
                    <div class="col-md-3">
                        <label class="p-l-7 p-t-10 icheck-flat-blue">{!! Form::checkbox('checkall', 'all', false, ['class' => 'checkall']) !!} Выбрать все</label>
                    </div>
                    <div class="col-md-9">
                        {!! Form::submit('Удалить из избраного', array('class' => 'btn btn-primary', 'disabled' => true)) !!}
                    </div>
                </div>
                <ul class="catalog-list" {{ empty($user) ? 'style=max-width:none' : '' }}>
                    @foreach($adverts as $advert)
                        <li>
                            <div class="col-md-1 p-0 w-20">
                                <div class="form-group icheck-flat-blue">
                                    {!! Form::checkbox('del[]', $advert->id, false, ['class' => 'del']) !!}
                                </div>
                            </div>
                            <div class="col-md-11">
                                <a class="catalog-img" href="{{ route($advert->type.'.show', $advert->id) }}">
                                    <img alt="{{ $advert->name }}" src="{{ Img::fit($advert->oneImage, 154, 113) }}">
                                </a>
                                <div class="catalog-descript">
                                    <div class="catalog-d-head">
                                        <span class="catalog-name">
                                            <a class="fav-add" data-toggle="tooltip" data-placement="top" title="В избраное" href="javascript:" data-id="{{ $advert->id }}">{!! $advert->favorite->star !!}</a>
                                            <a href="{{ route($advert->type.'.show', $advert->id) }}" class="title">{{ $advert->name }}</a>
                                        </span>
                                        <span class="catalog-price">{{ $advert->price->format }}</span>
                                    </div>
                                    <ul class="catalog-tags">
                                        <li>{{ $advert->category[1]->name }}</li>
                                        <li>{{ $advert->category[0]->name }}</li>
                                    </ul>
                                    <span class="catalog-metro">{!! $advert->metro->value !!}</span>
                                    <span class="catalog-date">{{ $advert->dateHuman }}</span>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
                {!! Form::close() !!}
                {!! (new App\Pagination($adverts))->render() !!}
            @else
                <p>У вас нет объявлений в избраном.</p>
            @endif
	</div>
       </div>
    </div>
</div>
@stop