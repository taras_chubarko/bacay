{!! Form::open(array('route' => 'dom-i-sad.store', 'role' => 'form', 'class' => 'form')) !!}
    <div class="row">
        <div class="col-lg-6">
            @include('advert::site.add.saler-info', ['advert' => null])
        </div>
        <div class="col-lg-6">
            @include('advert::site.add.saler-location', ['advert' => null])
        </div>
    </div>
        
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4>Основная информация</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
                        <label for="name" class="col-lg-4 label-30">Заголовок *</label>
                        <div class="col-lg-8">
                            {!! Form::text('name', null, array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    
                    <div class="form-group h-34{{ $errors->has('category.1') ? ' has-error' : '' }} row">
                        <label for="name" class="col-lg-4 label-30">Категория *</label>
                        <div class="col-lg-8">
                            {!! Form::hidden('category[0]', 7) !!}
                            {!! Form::select('category[1]', TaxonomyTerm::get(1, 7), null, array('class' => 'form-control selectpicker category', 'title' => 'Выбрать категорию')) !!}
                        </div>
                    </div>
                        
                    <div id="type" class="form-group{{ $errors->has('category.2') ? ' has-error' : '' }} row">
                        <label for="name" class="col-lg-4 label-30">Тип *</label>
                        <div class="col-lg-8">
                            {!! Form::select('category[2]', [], null, array('class' => 'form-control selectpicker', 'title' => 'Выбрать тип')) !!}
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="body" class="col-lg-4 label-30">Описание</label>
                        <div class="col-lg-8">
                            {!! Form::textarea('body', null, array('class' => 'form-control', 'rows' => 3)) !!}
                        </div>
                    </div>
                        
                </div><!--.col-lg-6-->
                <div class="col-lg-6">
                    
                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }} row">
                        <label for="price" class="col-lg-1 label-30">Цена</label>
                        <div class="col-lg-11">
                            <div class="input-group">
                                {!! Form::text('price', null, array('class' => 'form-control')) !!}
                                <span class="input-group-addon">руб.</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="fotos" class="col-lg-1 label-30">Фото</label>
                        <div class="col-lg-11">
                            <div id="fotos">
                                @include('filem::site.advert.image-field', ['images' => null, 'uri' => 'dom'])
                            </div>
                        </div>
                    </div>
                     
                </div><!--.col-lg-6-->
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-3">
            {!! Form::hidden('user_id', (Sentinel::check()) ? Sentinel::check()->id : Session::getId()) !!}
            {!! Form::hidden('type', 'dom-i-sad') !!}
            {!! Form::submit('Добавить', array('class' => 'btn btn-primary wp-100')) !!}
        </div>
    </div>
        
{!! Form::close() !!}