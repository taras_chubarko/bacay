{!! Former::setOption('automatic_label', false) !!}
<div class="filter-select">
    {!! Former::multiselect('service')->options(TaxonomyTerm::get(4, 0))->setAttributes(['class' => 'selectpicker', 'title' => 'Сфера дефтельности', 'id' => 'select-filter']) !!}
</div>
    
<div class="filter-select">
    {!! Former::multiselect('schedule')->options(TaxonomyTerm::get(5, 0))->setAttributes(['class' => 'selectpicker', 'title' => 'График работы', 'id' => 'select-filter']) !!}
</div>
    
<div class="filter-select">
    <div class="btn-group bootstrap-ddown filter-experience">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">{{ (Request::get('experience') && Request::input('experience.min') != '') ? Request::input('experience.min').'-'.Request::input('experience.max').' лет' : 'Опыт работы' }} <span class="caret"></span></button>
        <div class="dropdown-menu" role="menu" style="width:220px;">
            <div class="row">
                <div class="col-lg-6">
                    {!! Form::text('experience[min]', Request::input('experience.min'), ['class' => 'form-control', 'placeholder' => 'от']) !!}
                </div>
                <div class="col-lg-6">
                    {!! Form::text('experience[max]', Request::input('experience.max'), ['class' => 'form-control', 'placeholder' => 'до']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <button type="button" class="btn btn-info experience-apply">Применить</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="filter-select">
    <div class="btn-group bootstrap-ddown filter-price">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">{{ (Request::get('price') && Request::input('price.min') != '') ? Request::input('price.min').'-'.Request::input('price.max').' Р' : 'Цена, Р' }} <span class="caret"></span></button>
        <div class="dropdown-menu" role="menu" style="width:220px;">
            <div class="row">
                <div class="col-lg-6">
                    {!! Form::text('price[min]', Request::input('price.min'), ['class' => 'form-control', 'placeholder' => 'от']) !!}
                </div>
                <div class="col-lg-6">
                    {!! Form::text('price[max]', Request::input('price.max'), ['class' => 'form-control', 'placeholder' => 'до']) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <button type="button" class="btn btn-info price-apply">Применить</button>
                </div>
            </div>
        </div>
    </div>
</div>