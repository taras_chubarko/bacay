<div id="block-info">
    <div class="row">
        <div class="col-md-12">
            <p>Вы можете сделать объявление заметным и популярным, воспользовавшись услугами:</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <a href="/premium-obyavlenie"><i class="glyph-icon flaticon-signs"></i> Премиум-объявление</a>
        </div>
        <div class="col-md-6">
            <a href="/chto-takoe-vip-obyavleniya"><i class="fa fa-diamond"></i> VIP-объявление</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <a href="/vydelennye-obyavleniya-tsvetom"><i class="fa fa-paint-brush"></i> Выделенные объявления цветом</a>
        </div>
        <div class="col-md-6">
            <a href="/podnyatie-obyavleniya-vverkh-spiska"><i class="glyph-icon flaticon-arrow"></i> Поднятие объявления вверх списка</a>
        </div>
    </div>
</div>