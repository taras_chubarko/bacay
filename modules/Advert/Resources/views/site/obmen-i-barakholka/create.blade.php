{!! Form::open(array('route' => 'obmen-i-barakholka.store', 'role' => 'form', 'class' => 'form')) !!}
    <div class="row">
        <div class="col-lg-6">
            @include('advert::site.add.saler-info', ['advert' => null])
        </div>
        <div class="col-lg-6">
            @include('advert::site.add.saler-location', ['advert' => null])
        </div>
    </div>
        
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4>Основная информация</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
                        <label for="name" class="col-lg-4 label-30">Заголовок *</label>
                        <div class="col-lg-8">
                            {!! Form::text('name', null, array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    
                    {!! Form::hidden('category[0]', 14) !!}
                    
                    <div class="form-group row">
                        <label for="body" class="col-lg-4 label-30">Описание</label>
                        <div class="col-lg-8">
                            {!! Form::textarea('body', null, array('class' => 'form-control', 'rows' => 4)) !!}
                        </div>
                    </div>
                        
                </div><!--.col-lg-6-->
                <div class="col-lg-6">
                    
                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }} row">
                        <label for="price" class="col-lg-1 label-30">Цена</label>
                        <div class="col-lg-11">
                            <div class="input-group">
                                {!! Form::text('price', null, array('class' => 'form-control')) !!}
                                <span class="input-group-addon">руб.</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="fotos" class="col-lg-1 label-30">Фото</label>
                        <div class="col-lg-11">
                            <div id="fotos">
                                @include('filem::site.advert.image-field', ['images' => null, 'uri' => 'obmen-i-barakholka'])
                            </div>
                        </div>
                    </div>
                     
                </div><!--.col-lg-6-->
            </div>
        </div>
    </div>
        
    <div class="row">
        <div class="col-md-3">
            {!! Form::hidden('user_id', (Sentinel::check()) ? Sentinel::check()->id : Session::getId()) !!}
            {!! Form::hidden('type', 'obmen-i-barakholka') !!}
            {!! Form::submit('Добавить', array('class' => 'btn btn-primary wp-100')) !!}
        </div>
    </div>
        
{!! Form::close() !!}