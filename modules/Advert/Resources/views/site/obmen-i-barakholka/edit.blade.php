@extends('site::layouts.master')

@section('content')
<h1>Редактировать объявление "Обмен и барахолка"</h1>

{!! Form::open(array('route' => ['obmen-i-barakholka.update', $advert->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'form')) !!}
    <div class="row">
        <div class="col-lg-6">
            @include('advert::site.add.saler-info', ['advert' => $advert])
        </div>
        <div class="col-lg-6">
            @include('advert::site.add.saler-location', ['advert' => $advert])
        </div>
    </div>
        
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4>Основная информация</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
                        <label for="name" class="col-lg-4 label-30">Заголовок *</label>
                        <div class="col-lg-8">
                            {!! Form::text('name', $advert->name, array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    
                    {!! Form::hidden('category[0]', 14) !!}
                    
                    <div class="form-group row">
                        <label for="body" class="col-lg-4 label-30">Описание</label>
                        <div class="col-lg-8">
                            {!! Form::textarea('body', $advert->body, array('class' => 'form-control', 'rows' => 4)) !!}
                        </div>
                    </div>
                        
                </div><!--.col-lg-6-->
                <div class="col-lg-6">
                    
                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }} row">
                        <label for="price" class="col-lg-1 label-30">Цена</label>
                        <div class="col-lg-11">
                            <div class="input-group">
                                {!! Form::text('price', $advert->price->value, array('class' => 'form-control')) !!}
                                <span class="input-group-addon">руб.</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="fotos" class="col-lg-1 label-30">Фото</label>
                        <div class="col-lg-11">
                            <div id="fotos">
                                @include('filem::site.advert.image-field', ['images' => $advert->images, 'uri' => 'obmen-i-barakholka'])
                            </div>
                        </div>
                    </div>
                     
                </div><!--.col-lg-6-->
            </div>
        </div>
    </div>
        
    <div class="row">
        <div class="col-md-3">
            {!! Form::hidden('type', 'obmen-i-barakholka') !!}
            {!! Form::submit('Сохранить', array('class' => 'btn btn-primary wp-100')) !!}
        </div>
    </div>
        
{!! Form::close() !!}

@stop