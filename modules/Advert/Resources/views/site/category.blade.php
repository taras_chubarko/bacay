@extends('site::layouts.master')

@section('breadcrumbs')
{!! Breadcrumbs::render('adverts', $advertsCount) !!}
@stop

@section('content')
<div class="push-right-main-bl">
	<!--{!! $subcategory !!}-->
	@include($filter)
	{!! Baners::view('top660x90') !!}
	<div class="catalog-wrap">
		@include('advert::site.category-adverts', ['adverts' => $adverts, 'premium' => $premium])
	</div>
	{!! Baners::view('bottom660x90') !!}
</div>
<div class="aside-column">
	{!! Block::view('vip') !!}
	{!! Baners::view('240x400') !!}
	{!! Baners::view('240x240') !!}
</div>
@stop