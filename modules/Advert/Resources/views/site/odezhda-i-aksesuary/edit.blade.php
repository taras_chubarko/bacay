@extends('site::layouts.master')

@section('content')
<h1>Редактировать объявление "Одежда и Аксесуары"</h1>

{!! Form::open(array('route' => ['odezhda-i-aksesuary.update', $advert->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'form')) !!}
    <div class="row">
        <div class="col-lg-6">
            @include('advert::site.add.saler-info', ['advert' => $advert])
        </div>
        <div class="col-lg-6">
            @include('advert::site.add.saler-location', ['advert' => $advert])
        </div>
    </div>
        
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4>Основная информация</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
                        <label for="name" class="col-lg-4 label-30">Заголовок *</label>
                        <div class="col-lg-8">
                            {!! Form::text('name', $advert->name, array('class' => 'form-control')) !!}
                        </div>
                    </div>
                    
                    <div class="form-group h-34{{ $errors->has('category.1') ? ' has-error' : '' }} row">
                        <label for="category[1]" class="col-lg-4 label-30">Категория *</label>
                        <div class="col-lg-8">
                            {!! Form::hidden('category[0]', 9) !!}
                            {!! Form::select('category[1]', TaxonomyTerm::get(1, 9), $advert->category[1]->id, array('class' => 'form-control selectpicker category', 'title' => 'Выбрать категорию')) !!}
                        </div>
                    </div>
                        
                    <div id="type" class="form-group{{ $errors->has('category.2') ? ' has-error' : '' }} row">
                        <label for="category[2]" class="col-lg-4 label-30">Тип *</label>
                        <div class="col-lg-8">
                            {!! Form::select('category[2]', TaxonomyTerm::get(1, $advert->category[1]->id), (isset($advert->category[2])) ? $advert->category[2]->id : null, array('class' => 'form-control selectpicker', 'title' => 'Выбрать тип')) !!}
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="body" class="col-lg-4 label-30">Описание</label>
                        <div class="col-lg-8">
                            {!! Form::textarea('body', $advert->body, array('class' => 'form-control', 'rows' => 3)) !!}
                        </div>
                    </div>
                        
                </div><!--.col-lg-6-->
                <div class="col-lg-6">
                    
                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }} row">
                        <label for="price" class="col-lg-1 label-30">Цена</label>
                        <div class="col-lg-11">
                            <div class="input-group">
                                {!! Form::text('price', $advert->price->value, array('class' => 'form-control')) !!}
                                <span class="input-group-addon">руб.</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="fotos" class="col-lg-1 label-30">Фото</label>
                        <div class="col-lg-11">
                            <div id="fotos">
                                @include('filem::site.advert.image-field', ['images' => $advert->images, 'uri' => 'odezhda'])
                            </div>
                        </div>
                    </div>
                     
                </div><!--.col-lg-6-->
            </div>
        </div>
    </div>
        
    <div class="row">
        <div class="col-md-3">
            {!! Form::hidden('type', 'odezhda-i-aksesuary') !!}
            {!! Form::submit('Сохранить', array('class' => 'btn btn-primary wp-100')) !!}
        </div>
    </div>
        
{!! Form::close() !!}

@stop