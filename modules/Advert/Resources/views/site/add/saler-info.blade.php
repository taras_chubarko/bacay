<div class="panel panel-info info">
    <div class="panel-heading">
        <h4>Информация о продавце</h4>
    </div>
    <div class="panel-body">
        @if(Sentinel::guest())
            <div class="form-group{{ $errors->has('user.name') ? ' has-error' : '' }} row">
                <label for="user[name]" class="col-lg-4 label-30">Ваше имя *</label>
                <div class="col-lg-8">
                    {!! Form::text('user[name]', null, array('class' => 'form-control')) !!}
                </div>
            </div>
                
            <div class="form-group{{ $errors->has('user.email') ? ' has-error' : '' }} row">
                <label for="user[email]" class="col-lg-4 label-30">Ваш e-mail *</label>
                <div class="col-lg-8">
                    @var $checkMail = (!Sentinel::check()) ? ' checkMail' : null;
                    {!! Form::email('user[email]', null, array('class' => 'form-control'.$checkMail)) !!}
                </div>
            </div>
                
            <div class="form-group{{ $errors->has('user.phone') ? ' has-error' : '' }} row">
                <label for="user[phone]" class="col-lg-4 label-30">Ваш телефон *</label>
                <div class="col-lg-8">
                    {!! Form::text('user[phone]', null, array('class' => 'form-control phone')) !!}
                </div>
            </div>
        @endif
        
        @if(Sentinel::check())
            @var $disabled = (Sentinel::check()) ? 'disabled' : null;
            <div class="form-group{{ $errors->has('user.name') ? ' has-error' : '' }} row">
                <label for="user[name]" class="col-lg-4 label-30">Ваше имя *</label>
                <div class="col-lg-8">
                    {!! Form::text('user[name]', ($advert) ? $advert->userInfo->name : Sentinel::getUser()->username, array('class' => 'form-control', 'disabled' => 'disabled')) !!}
                    {!! Form::hidden('user[name]', ($advert) ? $advert->userInfo->name : Sentinel::getUser()->username) !!}
                </div>
            </div>
                
            <div class="form-group{{ $errors->has('user.email') ? ' has-error' : '' }} row">
                <label for="user[email]" class="col-lg-4 label-30">Ваш e-mail *</label>
                <div class="col-lg-8">
                    @var $checkMail = (!Sentinel::check()) ? ' checkMail' : null;
                    {!! Form::email('user[email]', ($advert) ? $advert->userInfo->email : Sentinel::getUser()->email, array('class' => 'form-control'.$checkMail, 'disabled' => 'disabled')) !!}
                    {!! Form::hidden('user[email]', ($advert) ? $advert->userInfo->email : Sentinel::getUser()->email) !!}
                </div>
            </div>
                
            <div class="form-group{{ $errors->has('user.phone') ? ' has-error' : '' }} row">
                <label for="user[phone]" class="col-lg-4 label-30">Ваш телефон *</label>
                <div class="col-lg-8">
                    {!! Form::text('user[phone]', ($advert) ? $advert->userInfo->phone : Sentinel::getUser()->phone, array('class' => 'form-control phone')) !!}
                </div>
            </div>
        @endif
    </div>
</div>