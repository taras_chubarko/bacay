<div class="panel panel-info info">
    <div class="panel-heading">
        <h4>Местоположение объявления</h4>
    </div>
    <div class="panel-body">
        
        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }} row">
            <label for="city" class="col-lg-3 label-30">Город *</label>
            <div class="col-lg-9">
                {!! Form::select('city', City::getFavorite(isset($advert) ? $advert->city->id : null), isset($advert) ? $advert->city->id : null, array('class' => 'form-control geocity', 'title' => 'Выбрать город')) !!}
            </div>
        </div>
            
        <div class="form-group{{ $errors->has('metro') ? ' has-error' : '' }} metro row">
            <label for="metro" class="col-lg-3 label-30">Метро *</label>
            <div class="col-lg-9">
                @if(isset($advert))
                    {!! Form::selectMetro('metro', Metro::getArr($advert->city->metro), $advert->metro->id, array('class' => 'form-control selectpicker geometro', 'title' => 'Выбрать метро')) !!}
                @else
                    {!! Form::selectMetro('metro', Metro::getArr(4400), Request::get('city'), array('class' => 'form-control selectpicker geometro', 'title' => 'Выбрать метро')) !!}
                @endif
            </div>
        </div>
            
        <div class="form-group row">
            <label for="city_raj" class="col-lg-3 label-30">Район города</label>
            <div class="col-lg-9">
                {!! Form::text('city_raj', isset($advert) ? $advert->city_raj->value : null, array('class' => 'form-control')) !!}
            </div>
        </div>
            
        <div class="form-group row">
            <label for="address" class="col-lg-3 label-30">Адрес</label>
            <div class="col-lg-9">
                {!! Form::text('address', isset($advert) ? $advert->address->value : null, array('class' => 'form-control')) !!}
            </div>
        </div>
            
    </div>
</div>