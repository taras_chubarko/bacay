@if($advert->images)
<!-- Start photosgallery-vertical -->
<div class="sliderkit photosgallery-vertical" style="display: block;">
       
    <div class="sliderkit-nav">
        <div class="sliderkit-nav-clip">
            <ul>
                @foreach($advert->images as $key => $image)
                    <li {{ ($key == 0) ? 'class="sliderkit-selected"' : '' }}><a title="[link title]" rel="nofollow" href="#"><img alt="{{ $advert->name }}" src="{{ Img::fit($image, 80, 60) }}"></a></li>
                @endforeach
            </ul>
        </div>
        <div class="sliderkit-btn sliderkit-nav-btn sliderkit-nav-prev"><a title="Previous line" href="#" rel="nofollow"><span>Previous</span></a></div>
        <div class="sliderkit-btn sliderkit-nav-btn sliderkit-nav-next"><a title="Next line" href="#" rel="nofollow"><span>Next</span></a></div>
    </div>
    <div class="sliderkit-panels">
        @foreach($advert->images as $k => $image)
            <div class="sliderkit-panel{{ ($k == 0) ? ' sliderkit-panel-active' : '' }}">
                <a class="gallery-zoom fancyslider" href="{{ Img::original($image) }}" rel="gallery1"></a>
                <img alt="{{ $advert->name }}" src="{{ Img::fit($image, 560, 430) }}">
            </div>
        @endforeach
    </div>
</div>
@endif