@if(isset($adverts))
<ul class="catalog-list">
    @include('advert::site.category-premium', ['premium' => $premium])
    @foreach($adverts as $advert)
        <li class="adv{{ $advert->color }}">
            <a class="catalog-img" href="{{ route($advert->type.'.show', $advert->id) }}">
                {!! $advert->premium !!}
                <img alt="{{ $advert->name }}" src="/imagecache/154x113/{{ $advert->image->filename or 'noim.jpg' }}">
            </a>
            <div class="catalog-descript">
                <div class="catalog-d-head">
                    <span class="catalog-name">
                        <a class="fav-add" data-toggle="tooltip" data-placement="top" title="В избраное" href="javascript:" data-id="{{ $advert->id }}">{!! $advert->favorites !!}</a>
                        <a href="{{ route($advert->type.'.show', $advert->id) }}" class="title">{{ $advert->name }}</a>
                    </span>
                    <span class="catalog-price">
                        {{ $advert->price }}
                    </span>
                </div>
                <ul class="catalog-tags">
                   {!! $advert->category !!}
                </ul>
                <span class="catalog-metro">{!! $advert->metro !!}</span>
                <span class="catalog-date">{{ $advert->date }}</span>
            </div>
        </li>
    @endforeach
</ul>
{!! (new App\Pagination($adverts->appends(Request::all())->setPath(route('adverts.category', $category))))->render() !!}
@else
<p>По вашему запросу объявлений не найдено.</p>
@endif
