<div class="modal fade" id="modal-advert-add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Тип объявления для подачи</h4>
            </div>
            <div class="modal-body">
                @if($categories->count())
                    <div class="main-tiles-list m-b-0">
                        <ul>
                            @foreach($categories as $category)
                            <li>
                                <a href="{{ route('adverts.add.category', $category->slug) }}">
                                    <span class="{{ $category->ico }}"></span>
                                    {!! $category->name !!}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>