@if($adverts->count())
<ul class="catalog-list">
    @foreach($adverts as $advert)
        <li class="adv{{ $advert->colorAdvert->value }}">
            <a class="catalog-img" href="{{ route($advert->type.'.show', $advert->id) }}">
                <img alt="{{ $advert->name }}" src="{{ Img::fit($advert->oneImage, 154, 113) }}">
            </a>
            <div class="catalog-descript">
                <div class="catalog-d-head">
                    <span class="catalog-name">
                        <a class="fav-add" data-toggle="tooltip" data-placement="top" title="В избраное" href="javascript:" data-id="{{ $advert->id }}">{!! $advert->favorite->star !!}</a>
                        <a href="{{ route($advert->type.'.show', $advert->id) }}" class="title">{{ $advert->name }}</a>
                    </span>
                    <span class="catalog-price">{{ $advert->price->format }}</span>
                </div>
                <ul class="catalog-tags">
                    @if(isset($advert->category[1]))
                    <li>{{ $advert->category[1]->name }}</li>
                    @endif
                    @if(isset($advert->category[0]))
                    <li>{{ $advert->category[0]->name }}</li>
                    @endif
                </ul>
                <span class="catalog-metro">{!! $advert->metro->value !!}</span>
                <span class="catalog-date">{{ $advert->dateHuman }}</span>
            </div>
        </li>
    @endforeach
</ul>
{!! (new App\Pagination($adverts->appends(Request::all())))->render() !!}
@else
<p>По вашему запросу объявлений не найдено.</p>
@endif