{!! Form::open(array('url' => Request::url(), 'role' => 'form', 'class' => 'form-filter', 'method' => 'GET')) !!}
    {!! Former::setOption('automatic_label', false) !!}
    <div class="filter-wrapp">
        
        <div class="filter-checked-items">
            {!! Form::radios2('category[0]', TaxonomyTerm::get(1, 2), Request::get('category'), ['class' => 'styler category']) !!}
        </div>
            
        <div class="filter-type">
            {!! Form::radios2('category[1]', TaxonomyTerm::get(1, Request::get('category')), Request::get('category'), ['class' => 'styler type']) !!}
        </div>
    
        <div class="more hidden">
            <div class="filter-group-ad">
                <div class="filter-wrap-select">  
                    <div class="filter-select w170 brand-filter">
                        {!! Former::select('brand')->options(\Transport::getBrands(Request::input('category.1')))->setAttributes(['class' => 'selectpicker', 'title' => 'Выбрать марку', 'id' => 'brand-filter']) !!}
                    </div>
                            
                    <div class="filter-select w-170 model-filter">
                        {!! Former::select('model')->options(\Transport::getModels(Request::input('brand')))->setAttributes(['class' => 'selectpicker', 'title' => 'Выбрать модель', 'id' => 'model-filter']) !!}
                    </div>
                        
                    <div class="filter-select filter-year w-170">
                        {!! Form::selectRange('year[]', 1910, date('Y'), Request::get('year'), array('class' => 'selectpicker', 'title' => 'Год', 'multiple' => true)) !!}
                    </div>
                        
                    <div class="filter-select">
                        <div class="btn-group bootstrap-ddown filter-price">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">{{ (Request::get('price') && Request::input('price.min') != '') ? Request::input('price.min').'-'.Request::input('price.max').' Р' : 'Цена, Р' }} <span class="caret"></span></button>
                            <div class="dropdown-menu" role="menu" style="width:220px;">
                                <div class="row">
                                    <div class="col-lg-6">
                                        {!! Form::text('price[min]', Request::input('price.min'), ['class' => 'form-control', 'placeholder' => 'от']) !!}
                                    </div>
                                    <div class="col-lg-6">
                                        {!! Form::text('price[max]', Request::input('price.max'), ['class' => 'form-control', 'placeholder' => 'до']) !!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="button" class="btn btn-info price-apply">Применить</button>
                                    </div>
                                </div>
                            </div>
                      </div>
                    </div>
                </div>
            </div>
            
            <div class="filter-group-ad p-b-11">
                <a class="any-param" href="javascript:">Дополнительные параметры</a>
                <div class="any-param">
                    <div class="filter-checked-items">
                        <span>Выбрать коробку передач</span>
                        {!! Form::checkboxes('transmission', Transport::getTransmission(), Request::get('transmission'), ['class' => 'styler transmission']) !!}
                    </div>
                        
                    <div class="filter-checked-items">
                        <span>Выбрать тип привода</span>
                        {!! Form::checkboxes('drive', Transport::getDrive(), Request::get('drive'), ['class' => 'styler drive']) !!}
                    </div>
                        
                    <div class="filter-checked-items">
                        <span>Выбрать топливо</span>
                        {!! Form::checkboxes('fuel', Transport::getFuel(), Request::get('fuel'), ['class' => 'styler fuel']) !!}
                    </div>
                </div>
            </div>
        </div>
            
        <div class="filter-select filter-pages">
            {!! Form::select('pages', [25 => 25, 50 => 50, 100 => 100],  (Request::get('pages')) ? Request::get('pages') : 25, array('class' => 'selectpicker', 'title' => 'Показать', 'data-width' => '120px')) !!}
        </div>
            
    </div><!--.filter-wrapp-->
{!! Form::close() !!}