@extends('site::layouts.master')

@section('content')
<h1>Редактировать объявление "Транспорт"</h1>

{!! Form::open(array('route' => ['transport.update', $advert->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'form')) !!}
    <div class="row">
        <div class="col-lg-6">
            @include('advert::site.add.saler-info', ['advert' => $advert])
        </div>
        <div class="col-lg-6">
            @include('advert::site.add.saler-location', ['advert' => $advert])
        </div>
    </div>
        
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4>Основная информация</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
                        <label for="name" class="col-lg-4 label-30">Заголовок *</label>
                        <div class="col-lg-8">
                            {!! Form::text('name', $advert->name, array('class' => 'form-control')) !!}
                        </div>
                    </div>
                        
                    <div class="form-group{{ $errors->has('category.1') ? ' has-error' : '' }} row">
                        <label for="category[1]" class="col-lg-4 label-30">Категория *</label>
                        <div class="col-lg-8">
                            {!! Form::hidden('category[0]', $advert->category[0]->id) !!}
                            {!! Form::select('category[1]', TaxonomyTerm::get(1, $advert->category[0]->id), $advert->category[1]->id, array('class' => 'form-control selectpicker category', 'title' => 'Выбрать категорию')) !!}
                        </div>
                    </div>
                        
                    <div id="type" class="form-group{{ $errors->has('category.2') ? ' has-error' : '' }} row">
                        <label for="category[2]" class="col-lg-4 label-30">Тип *</label>
                        <div class="col-lg-8">
                            {!! Form::select('category[2]', TaxonomyTerm::get(1, $advert->category[1]->id), $advert->category[2]->id, array('class' => 'form-control selectpicker type', 'title' => 'Выбрать тип')) !!}
                        </div>
                    </div>
                        
                    <div id="brand" class="form-group{{ $errors->has('brand') ? ' has-error' : '' }} row">
                        <label for="brand" class="col-lg-4 label-30">Марка *</label>
                        <div class="col-lg-8">
                            {!! Form::select('brand', Transport::getBrands($advert->category[2]->id), $advert->brand->id, array('class' => 'form-control selectpicker', 'title' => 'Выбрать марку')) !!}
                        </div>
                    </div>
                        
                    <div id="model" class="form-group{{ $errors->has('model') ? ' has-error' : '' }} row">
                        <label for="model" class="col-lg-4 label-30">Модель *</label>
                        <div class="col-lg-8">
                            {!! Form::select('model', Transport::getModels($advert->brand->id), $advert->model->id, array('class' => 'form-control selectpicker', 'title' => 'Выбрать модель')) !!}
                        </div>
                    </div>
                        
                    <div id="transmission" class="form-group{{ $errors->has('transmission') ? ' has-error' : '' }} row">
                        <label for="transmission" class="col-lg-4 label-30">Коробка передач</label>
                        <div class="col-lg-8">
                            {!! Form::select('transmission', Transport::getTransmission(), $advert->transmission->id, array('class' => 'form-control selectpicker', 'title' => 'Выбрать коробку передач')) !!}
                        </div>
                    </div>
                        
                    <div id="drive" class="form-group{{ $errors->has('drive') ? ' has-error' : '' }} row">
                        <label for="drive" class="col-lg-4 label-30">Тип привода</label>
                        <div class="col-lg-8">
                            {!! Form::select('drive', Transport::getDrive(), $advert->drive->id, array('class' => 'form-control selectpicker', 'title' => 'Выбрать тип привода')) !!}
                        </div>
                    </div>
                        
                    <div id="fuel" class="form-group{{ $errors->has('fuel') ? ' has-error' : '' }} row">
                        <label for="fuel" class="col-lg-4 label-30">Топливо</label>
                        <div class="col-lg-8">
                            {!! Form::select('fuel', Transport::getFuel(), $advert->fuel->id, array('class' => 'form-control selectpicker', 'title' => 'Выбрать топливо')) !!}
                        </div>
                    </div>
                        
                    <div id="year" class="form-group{{ $errors->has('year') ? ' has-error' : '' }} row">
                        <label for="year" class="col-lg-4 label-30">Год</label>
                        <div class="col-lg-8">
                            {!! Form::selectRange('year', 2016, 1910, $advert->year->value, array('class' => 'form-control selectpicker', 'title' => 'Выбрать год')) !!}
                        </div>
                    </div>
                        
                    <div class="form-group row">
                        <label for="body" class="col-lg-4 label-30">Описание</label>
                        <div class="col-lg-8">
                            {!! Form::textarea('body', $advert->body, array('class' => 'form-control', 'rows' => 3)) !!}
                        </div>
                    </div>
                        
                </div><!--.col-lg-6-->
                <div class="col-lg-6">
                    
                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }} row">
                        <label for="price" class="col-lg-1 label-30">Цена</label>
                        <div class="col-lg-11">
                            <div class="input-group">
                                {!! Form::text('price', $advert->price->value, array('class' => 'form-control')) !!}
                                <span class="input-group-addon">руб.</span>
                            </div>
                        </div>
                    </div>
                        
                    <div class="form-group row">
                        <label for="fotos" class="col-lg-1 label-30">Фото</label>
                        <div class="col-lg-11">
                            <div id="fotos">
                                @include('filem::site.advert.image-field', ['images' => $advert->images, 'uri' => 'transport'])
                            </div>
                        </div>
                    </div>
                        
                </div><!--.col-lg-6-->
            </div>
        </div>
    </div>
        
    <div class="row">
        <div class="col-md-3">
            {!! Form::hidden('type', 'transport') !!}
            {!! Form::submit('Сохранить', array('class' => 'btn btn-primary wp-100')) !!}
        </div>
    </div>
        
{!! Form::close() !!}

@stop