@if(isset($premium))
    @foreach($premium as $advert)
        <li class="adv{{ $advert->color }} premium">
            <a class="catalog-img" href="{{ route($advert->type.'.show', $advert->id) }}">
                {!! $advert->premium !!}
                <img alt="{{ $advert->name }}" src="{{ Img::fit($advert->image, 154, 113) }}">
            </a>
            <div class="catalog-descript">
                <div class="catalog-d-head">
                    <span class="catalog-name">
                        <a class="fav-add" data-toggle="tooltip" data-placement="top" title="В избраное" href="javascript:" data-id="{{ $advert->id }}">{!! $advert->favorites !!}</a>
                        <a href="{{ route($advert->type.'.show', $advert->id) }}" class="title">{{ $advert->name }}</a>
                    </span>
                    <span class="catalog-price">
                        {{ $advert->price }}
                    </span>
                </div>
                <ul class="catalog-tags">
                   {!! $advert->category !!}
                </ul>
                <span class="catalog-metro">{!! $advert->metro !!}</span>
                <span class="catalog-date">{{ $advert->date }}</span>
            </div>
        </li>
    @endforeach
@endif
