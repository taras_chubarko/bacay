@extends('site::layouts.master')

@section('content')
<div class="group usr-group">
    <div class="row">
        <div class="col-md-3">
            @include('user::site.menu-user', $user)
            @include('advert::site.block-info')
            
            @if($user->firstDiscount->discount || $user->timeDiscount->discount)
            <div class="discount-block">
                <h2>Скидка на услуги</h2>
                @if($user->firstDiscount->discount)
                    <p class="text-center">Мы дарим вам одноразовую скидку на услуги в размере <b>{{ $user->firstDiscount->format }}</b></p>
                @endif
                
                @if($user->timeDiscount->discount)
                    <p class="text-center">Мы дарим вам скидку на услуги в размере <b>{{ $user->timeDiscount->format }}</b></p>
                    <p class="text-center">Скидка действует до: <b>{{ $user->timeDiscount->time }}</b></p>
                @endif
            </div>
            @endif
           
            
        </div>
        <div class="col-md-9">
            <div class="push-main-bl m-l-0">
                <div class="user-list-items">
                    @if($adverts->count())
                        @foreach($adverts as $advert)
                        <div class="row">
                                <div class="col-md-3{{ ($advert->status == 0 || $advert->status == 2 || $advert->status == 3 || $advert->status == 4) ? ' unpublic' : '' }}">
                                    <div class="ad-img">
                                        <a href="{{ route($advert->type.'.show', $advert->id) }}"><img alt="" class="img-responsive" src="/imagecache/167x127/{{ $advert->pivotImages->first()->pivot->filename or 'noim.jpg' }}"></a>
                                        @foreach($advert->usluga as $usl)
                                            <i class="{{ $usl->ico }} f-sz-20 m-t-10"></i>
                                        @endforeach
                                        @if($advert->status == 2)
                                            <span class="label label-primary">На модерации</span>
                                        @endif
                                        @if($advert->status == 3)
                                            <span class="label label-danger">Отклонено модератором</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6{{ ($advert->status == 0 || $advert->status == 2 || $advert->status == 3 || $advert->status == 4) ? ' unpublic' : '' }}">
                                    <div class="ad-desctipt m-0">
                                        <a href="{{ route($advert->type.'.show', $advert->id) }}">{{ $advert->name }}</a>
                                        <span class="ad-price">{{ $advert->price->format }}</span>
                                        <div class="ad-date-wrap">
                                            {!! $advert->endLine !!}
                                            <span>Размещено с {{ $advert->date }}  по {{ $advert->end }}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    @include('advert::site.button-edit', $advert)
                                    @include('advert::site.button-rekl', $advert)
                                </div>
                        </div>
                        @endforeach
                    @else
                        <p>У вас нет объявлений.</p>
                    @endif
                </div>
                {!! (new App\Pagination($adverts))->render() !!}
            </div>
        </div>
    </div>
</div>
@stop