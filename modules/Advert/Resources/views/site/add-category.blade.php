@extends('site::layouts.master')

@section('script')
    <script>
        var mat = '{{ Option::get('mat.mat') }}';
    </script>
@stop

@section('content')
<h1>{{ $title }}</h1>
{!! $add !!}
@stop