@if($adverts->count())
<div class="aside-bl-repeat">
    <div class="vip-ad">
        <span class="vip-tt">VIP-объявления</span>
        <ul class="vip-ad-list">
            @foreach($adverts as $key => $advert)
            <li {{ ($key == 2) ? 'class=mobile-visible' : '' }}>
                <div>
                    <a href="{{ route($advert->type.'.show', $advert->id) }}"><img alt="{{ $advert->name }}" src="{{ Img::fit($advert->oneImage, 210, 138) }}"></a>
                    <span class="vip-name"><a href="{{ route($advert->type.'.show', $advert->id) }}">{{ $advert->name }}</a></span>
                    <span class="vip-price">{{ $advert->price->format }}</span>
                   <span class="vip-adress">{!! $advert->metro->value !!}</span>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
    <span class="link-more-aside"><a href="/chto-takoe-vip-obyavleniya">Что такое VIP-объявления?</a></span>
</div>
@endif