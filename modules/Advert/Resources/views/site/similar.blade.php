@if($adverts->count())
<div class="more-product-gallery">
    <span class="more-prod-tt">Похожие объявления:</span>
    <div class="more-prod-carousel">
        <ul class="slides">
            @foreach($adverts as $advert)
            <li>
                <div class="more-item">
                    <a href="{{ route($advert->type.'.show', $advert->id) }}">
                        <img src="{{ Img::fit($advert->oneImage, 154, 113) }}" alt="{{ $advert->name }}">
                        <b class="more-item-name">{{ $advert->name }}</b>
                        <!--<span>Пятницкое шоссе, 5 км</span>-->
                        <b class="more-item-price">{{ $advert->price->format }}</b>
                    </a>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</div>
@endif

