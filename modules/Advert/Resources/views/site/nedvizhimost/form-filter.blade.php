{!! Form::open(array('url' => Request::url(), 'role' => 'form', 'class' => 'form-filter', 'method' => 'GET')) !!}
    <div class="filter-wrapp">
        <div class="filter-checked-items category-checked">
            {!! Form::radios2('category[0]', TaxonomyTerm::get(1,1), Request::get('category'), ['class' => 'styler category']) !!}
        </div>
            
        <div class="filter-group-ad typadverts hidden">
            <div class="filter-wrap-btn typadvert">
                <a class="filter-btn{{ (Request::get('typadvert') == 25) ? ' active' : '' }}" data-typadvert="25" href="javascript:">Продам <span></span></a>
                <a class="filter-btn{{ (Request::get('typadvert') == 26) ? ' active' : '' }}" data-typadvert="26" href="javascript:">Сдам <span></span></a>
                <a class="filter-btn{{ (Request::get('typadvert') == 27) ? ' active' : '' }}" data-typadvert="27" href="javascript:">Куплю <span></span></a>
                <a class="filter-btn{{ (Request::get('typadvert') == 28) ? ' active' : '' }}" data-typadvert="28" href="javascript:">Сниму <span></span></a>
                {!! Form::hidden('typadvert', Request::get('typadvert')) !!}
            </div>
        </div>
            
        @include('advert::site.nedvizhimost.filter.filter-16-17')
        @include('advert::site.nedvizhimost.filter.filter-18')
        @include('advert::site.nedvizhimost.filter.filter-19')
        @include('advert::site.nedvizhimost.filter.filter-20')
        @include('advert::site.nedvizhimost.filter.filter-21-22-23')
        @include('advert::site.nedvizhimost.filter.filter-24')
            
        <div class="filter-select filter-pages">
            {!! Form::select('pages', [25 => 25, 50 => 50, 100 => 100],  (Request::get('pages')) ? Request::get('pages') : 25, array('class' => 'selectpicker', 'title' => 'Показать', 'data-width' => '120px')) !!}
        </div>
        
    </div>
{!! Form::close() !!}