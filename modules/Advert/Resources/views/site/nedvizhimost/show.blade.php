@extends('site::layouts.master')

@section('breadcrumbs')
{!! Breadcrumbs::render('advert', $advert) !!}
@stop

@section('content')
<div class="push-right-main-bl">
    @include('advert::site.block-info')
    <div class="product-card">
        <h1 class="large-title">{{ $advert->name }}</h1>
        <div class="gallery-wrapper">
            @include('advert::site.sliderkit', $advert)
        </div>
        <div class="product-slide-wrapper"></div>
        
        <div class="p-inline-bl">
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Цена:</span>
                <span class="p-price-bl">{{ $advert->price->format }}</span>
            </div>
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Контактное лицо:</span>
                <div class="p-inline-more-info">
                    <span>{{ $advert->user->username }}</span>
                    <a class="btn-icon btn-ph" data-advert="{{ $advert->id }}" href="javascript:">Показать телефон</a>
                    <a class="btn-icon btn-mess" data-advert="{{ $advert->id }}" href="javascript:">Написать сообщение</a>
                </div>
            </div>
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Город:</span>
                <span>{{ $advert->city->name }}</span>
            </div>
            @if($advert->metro->value)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Метро:</span>
                <span>{!! $advert->metro->value !!}</span>
            </div>
            @endif
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt"></span>
                <span><b>{{ $advert->category[1]->name }} | {{ $advert->category[0]->name }}</b></span>
            </div>
                
            @if($advert->typadvert->name)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Тип объявления:</span>
                <span>{{ $advert->typadvert->name }}</span>
            </div>
            @endif
            
            @if($advert->typhouse->name)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Тип дома:</span>
                <span>{{ $advert->typhouse->name }}</span>
            </div>
            @endif
            
            @if($advert->vidObject->name)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Вид объекта:</span>
                <span>{{ $advert->vidObject->name }}</span>
            </div>
            @endif
            
            @if($advert->vidBuild->name)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Вид строения:</span>
                <span>{{ $advert->vidBuild->name }}</span>
            </div>
            @endif
            
            @if($advert->materialWalls->name)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Материал стен:</span>
                <span>{{ $advert->materialWalls->name }}</span>
            </div>
            @endif
            
            @if($advert->carPlaces->name)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Машиноместо:</span>
                <span>{{ $advert->carPlaces->name }}</span>
            </div>
            @endif
            
            @if($advert->country->value)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Страна:</span>
                <span>{{ $advert->country->value }}</span>
            </div>
            @endif
            
            @if($advert->qtyroom->value)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Кол-во комнат:</span>
                <span>{{ $advert->qtyroom->value }}</span>
            </div>
            @endif
            
            @if($advert->area->value)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Площадь, м²:</span>
                <span>{{ $advert->area->value }}</span>
            </div>
            @endif
            
            @if($advert->areaSot->value)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Площадь участка:</span>
                <span>{{ $advert->areaSot->value }} соток</span>
            </div>
            @endif
            
            @if($advert->floor->value)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Этаж:</span>
                <span>{{ $advert->floor->value }}</span>
            </div>
            @endif
            
            @if($advert->qtyfloor->value)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Этажей в доме:</span>
                <span>{{ $advert->qtyfloor->value }}</span>
            </div>
            @endif
            
            
        </div>
        
        @if($advert->body)
        <div class="p-descript-info">
            <b>Описание:</b>
           {!! $advert->body !!}
        </div>
        @endif
        
        <div class="date-info">
            <span>№ {{ $advert->id }}</span>
            <span>{{ $advert->dateHuman2 }}.</span>
        </div>
        
        <div class="post-info-line">
            <div class="row">
                <div class="col-xs-6">
                    <a class="icon-link-post ic-l-p1 complain" data-advert="{{ $advert->id }}" href="javascript:">Пожаловаться</a>
                    <a class="icon-link-post ic-l-p2" data-container="body" data-toggle="popover" data-placement="bottom" data-content="" href="javascript:">Поделиться</a>
                    <div id="popover-social">
                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <div class="addthis_sharing_toolbox"></div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="icon-post-show">
                        <span>
                            Просмотров: <a href="#">всего {{ $advert->views->all }}, сегодня {{ $advert->views->today }}</a>
                        </span>
                    </div>
                </div>
            </div>
        </div><!--.post-info-line-->
    </div>
    
    {!! Block::view('similar', $advert) !!}
</div>
    
<div class="aside-column">
    @include('advert::site.region-right', $advert)
</div>
@stop