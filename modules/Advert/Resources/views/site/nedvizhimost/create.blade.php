{!! Form::open(array('route' => 'nedvizhimost.store', 'role' => 'form', 'class' => 'form')) !!}
    <div class="row">
        <div class="col-lg-6">
            @include('advert::site.add.saler-info', ['advert' => null])
        </div>
        <div class="col-lg-6">
            @include('advert::site.add.saler-location', ['advert' => null])
        </div>
    </div>
    
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4>Основная информация</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} row">
                        <label for="name" class="col-lg-4 label-30">Заголовок *</label>
                        <div class="col-lg-8">
                            {!! Form::text('name', null, array('class' => 'form-control')) !!}
                        </div>
                    </div>
                        
                    <div class="form-group{{ $errors->has('typadvert') ? ' has-error' : '' }} row">
                        <label for="typadvert" class="col-lg-4 label-30">Тип объявления *</label>
                        <div class="col-lg-8">
                            {!! Form::select('typadvert', TaxonomyTerm::get(2,0), null, array('class' => 'form-control selectpicker', 'title' => 'Тип объявления')) !!}
                        </div>
                    </div>
                        
                    <div class="form-group{{ $errors->has('category.1') ? ' has-error' : '' }} row">
                        <label for="category[1]" class="col-lg-4 label-30">Категория *</label>
                        <div class="col-lg-8">
                            {!! Form::hidden('category[0]', 1) !!}
                            {!! Form::select('category[1]', TaxonomyTerm::get(1,1), null, array('class' => 'form-control selectpicker', 'title' => 'Выбрать категорию', 'id' => 'select-category')) !!}
                        </div>
                    </div>
                        
                    @include('advert::site.nedvizhimost.form.form-16-17', ['advert' => null])
                    @include('advert::site.nedvizhimost.form.form-18', ['advert' => null])
                    @include('advert::site.nedvizhimost.form.form-19', ['advert' => null])
                    @include('advert::site.nedvizhimost.form.form-20', ['advert' => null])
                    @include('advert::site.nedvizhimost.form.form-21-22-23', ['advert' => null])
                    @include('advert::site.nedvizhimost.form.form-24', ['advert' => null])
                        
                    <div class="form-group row">
                        <label for="body" class="col-lg-4 label-30">Описание</label>
                        <div class="col-lg-8">
                            {!! Form::textarea('body', null, array('class' => 'form-control', 'rows' => 3)) !!}
                        </div>
                    </div>
                        
                </div><!--.col-lg-6-->
                <div class="col-lg-6">
                    
                    <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }} row">
                        <label for="price" class="col-lg-1 label-30">Цена</label>
                        <div class="col-lg-11">
                            <div class="input-group">
                                {!! Form::text('price', null, array('class' => 'form-control')) !!}
                                <span class="input-group-addon">руб.</span>
                            </div>
                        </div>
                    </div>
                        
                    <div class="form-group row">
                        <label for="fotos" class="col-lg-1 label-30">Фото</label>
                        <div class="col-lg-11">
                            <div id="fotos">
                                @include('filem::site.advert.image-field', ['images' => (Session::has('images')) ? session('images') : null, 'uri' => 'nedvizhimost'])
                            </div>
                        </div>
                    </div>
                        
                </div><!--.col-lg-6-->
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-3">
            {!! Form::hidden('user_id', (Sentinel::check()) ? Sentinel::check()->id : Session::getId()) !!}
            {!! Form::hidden('type', 'nedvizhimost') !!}
            {!! Form::submit('Добавить', array('class' => 'btn btn-primary wp-100')) !!}
        </div>
    </div>
        
{!! Form::close() !!}