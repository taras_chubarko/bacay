<div id="form-18" class="hidden">
    <div class="form-group{{ $errors->has('area_sot') ? ' has-error' : '' }} row">
        <label for="area_sot" class="col-lg-4 label-30">Площадь участка *</label>
        <div class="col-lg-8">
            <div class="input-group">
                {!! Form::text('area_sot', ($advert) ? $advert->areaSot->value : null, array('class' => 'form-control')) !!}
                <span class="input-group-addon">соток</span>
            </div>
        </div>
    </div>
</div>