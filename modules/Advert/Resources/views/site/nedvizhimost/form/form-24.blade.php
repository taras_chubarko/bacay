<div id="form-24" class="hidden">
    
    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }} row">
        <label for="area" class="col-lg-4 label-30">Страна *</label>
        <div class="col-lg-8">
            {!! Form::text('country', ($advert) ? $advert->country->value : null, array('class' => 'form-control')) !!}
        </div>
    </div>
        
    <div class="form-group{{ $errors->has('vid_object') ? ' has-error' : '' }} row">
        <label for="typhouse" class="col-lg-4 label-30">Вид объекта *</label>
        <div class="col-lg-8">
            {!! Form::select('vid_object', TaxonomyTerm::get(15,0), ($advert) ? $advert->vidObject->id : null, array('class' => 'form-control selectpicker', 'title' => 'Вид объекта')) !!}
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('area') ? ' has-error' : '' }} row">
        <label for="area" class="col-lg-4 label-30">Площадь, м² *</label>
        <div class="col-lg-8">
            {!! Form::text('area', ($advert) ? $advert->area->value : null, array('class' => 'form-control')) !!}
        </div>
    </div>
   
</div>