<div id="form-21-22-23" class="hidden">
   
   <div class="form-group{{ $errors->has('car_places') ? ' has-error' : '' }} row">
        <label for="typhouse" class="col-lg-4 label-30">Машиноместо *</label>
        <div class="col-lg-8">
            {!! Form::select('car_places', TaxonomyTerm::get(14,0), ($advert) ? $advert->carPlaces->id : null, array('class' => 'form-control selectpicker', 'title' => 'Машиноместо')) !!}
        </div>
    </div>
   
    <div class="form-group{{ $errors->has('area') ? ' has-error' : '' }} row">
        <label for="area" class="col-lg-4 label-30">Площадь, м² *</label>
        <div class="col-lg-8">
            {!! Form::text('area', ($advert) ? $advert->area->value : null, array('class' => 'form-control')) !!}
        </div>
    </div>
    
</div>