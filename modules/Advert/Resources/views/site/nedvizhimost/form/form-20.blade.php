<div id="form-20" class="hidden">
    
    <div class="form-group{{ $errors->has('vid_build') ? ' has-error' : '' }} row">
        <label for="typhouse" class="col-lg-4 label-30">Вид строения *</label>
        <div class="col-lg-8">
            {!! Form::select('vid_build', TaxonomyTerm::get(12,0), ($advert) ? $advert->vidBuild->id : null, array('class' => 'form-control selectpicker', 'title' => 'Вид строения')) !!}
        </div>
    </div>
        
    <div class="form-group{{ $errors->has('material_walls') ? ' has-error' : '' }} row">
        <label for="typhouse" class="col-lg-4 label-30">Материал стен *</label>
        <div class="col-lg-8">
            {!! Form::select('material_walls', TaxonomyTerm::get(13,0), ($advert) ? $advert->materialWalls->id : null, array('class' => 'form-control selectpicker', 'title' => 'Материал стен')) !!}
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('qtyroom') ? ' has-error' : '' }} row">
        <label for="qtyroom" class="col-lg-4 label-30">Кол-во комнат *</label>
        <div class="col-lg-8">
            {!! Form::text('qtyroom', ($advert) ? $advert->qtyroom->value : null, array('class' => 'form-control')) !!}
        </div>
    </div>
        
    <div class="form-group{{ $errors->has('area') ? ' has-error' : '' }} row">
        <label for="area" class="col-lg-4 label-30">Площадь, м² *</label>
        <div class="col-lg-8">
            {!! Form::text('area', ($advert) ? $advert->area->value : null, array('class' => 'form-control')) !!}
        </div>
    </div>
        
    <div class="form-group{{ $errors->has('qtyfloor') ? ' has-error' : '' }} row">
        <label for="qtyfloor" class="col-lg-4 label-30">Этажей в доме *</label>
        <div class="col-lg-8">
            {!! Form::text('qtyfloor', ($advert) ? $advert->qtyfloor->value : null, array('class' => 'form-control')) !!}
        </div>
    </div>
        
    <div class="form-group{{ $errors->has('area_sot') ? ' has-error' : '' }} row">
        <label for="area" class="col-lg-4 label-30">Площадь участка *</label>
        <div class="col-lg-8">
            <div class="input-group">
                {!! Form::text('area_sot', ($advert) ? $advert->areaSot->value : null, array('class' => 'form-control')) !!}
                <span class="input-group-addon">соток</span>
            </div>
        </div>
    </div>
        
</div>