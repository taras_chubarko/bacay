<div id="form-16-17" class="hidden">
    <div class="form-group{{ $errors->has('typhouse') ? ' has-error' : '' }} row">
        <label for="typhouse" class="col-lg-4 label-30">Тип дома *</label>
        <div class="col-lg-8">
            {!! Form::select('typhouse', TaxonomyTerm::get(3,0), ($advert) ? $advert->typhouse->id : null, array('class' => 'form-control selectpicker', 'title' => 'Тип дома')) !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('qtyroom') ? ' has-error' : '' }} row">
        <label for="qtyroom" class="col-lg-4 label-30">Кол-во комнат *</label>
        <div class="col-lg-8">
            {!! Form::text('qtyroom', ($advert) ? $advert->qtyroom->value : null, array('class' => 'form-control')) !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('area') ? ' has-error' : '' }} row">
        <label for="area" class="col-lg-4 label-30">Площадь, м² *</label>
        <div class="col-lg-8">
            {!! Form::text('area', ($advert) ? $advert->area->value : null, array('class' => 'form-control')) !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('floor') ? ' has-error' : '' }} row">
        <label for="floor" class="col-lg-4 label-30">Этаж *</label>
        <div class="col-lg-8">
            {!! Form::text('floor', ($advert) ? $advert->floor->value : null, array('class' => 'form-control')) !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('qtyfloor') ? ' has-error' : '' }} row">
        <label for="qtyfloor" class="col-lg-4 label-30">Этажей в доме *</label>
        <div class="col-lg-8">
            {!! Form::text('qtyfloor', ($advert) ? $advert->qtyfloor->value : null, array('class' => 'form-control')) !!}
        </div>
    </div>
</div>