<div id="filter-18" class="subfilter hidden">
    <div class="filter-group-ad more">
        <div class="filter-wrap-select">
            
            <div class="filter-select">
                <div class="btn-group bootstrap-select filter-area">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">{{ (Request::get('area_sot') && Request::input('area_sot.min') !='') ? Request::input('area_sot.min').'-'.Request::input('area_sot.max').' соток' : 'Площадь, соток' }} <span class="caret"></span></button>
                    <div class="dropdown-menu" role="menu" style="width:220px;">
                        <div class="row">
                            <div class="col-lg-6">
                                {!! Form::text('area_sot[min]', Request::input('area_sot.min'), ['class' => 'form-control', 'placeholder' => 'от']) !!}
                            </div>
                            <div class="col-lg-6">
                                {!! Form::text('area_sot[max]', Request::input('area_sot.max'), ['class' => 'form-control', 'placeholder' => 'до']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-info area-apply">Применить</button>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
                
            <div class="filter-select">
                <div class="btn-group bootstrap-ddown filter-price">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">{{ (Request::get('price') && Request::input('price.min') != '') ? Request::input('price.min').'-'.Request::input('price.max').' Р' : 'Цена, Р' }} <span class="caret"></span></button>
                    <div class="dropdown-menu" role="menu" style="width:220px;">
                        <div class="row">
                            <div class="col-lg-6">
                                {!! Form::text('price[min]', Request::input('price.min'), ['class' => 'form-control', 'placeholder' => 'от']) !!}
                            </div>
                            <div class="col-lg-6">
                                {!! Form::text('price[max]', Request::input('price.max'), ['class' => 'form-control', 'placeholder' => 'до']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-info price-apply">Применить</button>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
        </div>
    </div>
</div>