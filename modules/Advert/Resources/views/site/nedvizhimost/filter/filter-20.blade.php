<div id="filter-20" class="subfilter hidden">
    <div class="filter-group-ad more">
        <div class="filter-wrap-select">
        
            <div class="filter-select filter-vid_build">
                {!! Form::select('vid_build', TaxonomyTerm::get(12,0), Request::get('vid_build'), array('class' => 'form-control selectpicker', 'title' => 'Вид строения', 'data-width' => '120px')) !!}
            </div>
                
            <div class="filter-select filter-material_walls">
                {!! Form::select('material_walls', TaxonomyTerm::get(13,0), Request::get('material_walls'), array('class' => 'form-control selectpicker', 'title' => 'Материал стен', 'data-width' => '120px')) !!}
            </div>
                
            <div class="filter-select filter-qtyroom">
                {!! Form::selectRange('qtyroom[]', 1, 10, Request::get('qtyroom'), array('class' => 'selectpicker', 'title' => 'Кол-во комнат', 'data-width' => '120px', 'multiple' => true, 'data-val' => 'qtyroom')) !!}
            </div>
            
            <div class="filter-select">
                <div class="btn-group bootstrap-select filter-area">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">{{ (Request::get('area') && Request::input('area.min') !='') ? Request::input('area.min').'-'.Request::input('area.max').' м²' : 'Площадь, м²' }} <span class="caret"></span></button>
                    <div class="dropdown-menu" role="menu" style="width:220px;">
                        <div class="row">
                            <div class="col-lg-6">
                                {!! Form::text('area[min]', Request::input('area.min'), ['class' => 'form-control', 'placeholder' => 'от']) !!}
                            </div>
                            <div class="col-lg-6">
                                {!! Form::text('area[max]', Request::input('area.max'), ['class' => 'form-control', 'placeholder' => 'до']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-info area-apply">Применить</button>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
                
            <div class="filter-select filter-qtyfloor">
                {!! Form::selectRange('qtyfloor[]', 1, 30, Request::get('qtyfloor'), array('class' => 'selectpicker', 'title' => 'Этажей в доме', 'data-width' => '120px', 'multiple' => true)) !!}
            </div>
                
            <div class="filter-select">
                <div class="btn-group bootstrap-select filter-area">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">{{ (Request::get('area_sot') && Request::input('area_sot.min') !='') ? Request::input('area_sot.min').'-'.Request::input('area_sot.max').' соток' : 'Площадь участка' }} <span class="caret"></span></button>
                    <div class="dropdown-menu" role="menu" style="width:220px;">
                        <div class="row">
                            <div class="col-lg-6">
                                {!! Form::text('area_sot[min]', Request::input('area_sot.min'), ['class' => 'form-control', 'placeholder' => 'от']) !!}
                            </div>
                            <div class="col-lg-6">
                                {!! Form::text('area_sot[max]', Request::input('area_sot.max'), ['class' => 'form-control', 'placeholder' => 'до']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-info area-apply">Применить</button>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
                
            <div class="filter-select">
                <div class="btn-group bootstrap-ddown filter-price">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">{{ (Request::get('price') && Request::input('price.min') != '') ? Request::input('price.min').'-'.Request::input('price.max').' Р' : 'Цена, Р' }} <span class="caret"></span></button>
                    <div class="dropdown-menu" role="menu" style="width:220px;">
                        <div class="row">
                            <div class="col-lg-6">
                                {!! Form::text('price[min]', Request::input('price.min'), ['class' => 'form-control', 'placeholder' => 'от']) !!}
                            </div>
                            <div class="col-lg-6">
                                {!! Form::text('price[max]', Request::input('price.max'), ['class' => 'form-control', 'placeholder' => 'до']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-info price-apply">Применить</button>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
        </div>
    </div>
</div>