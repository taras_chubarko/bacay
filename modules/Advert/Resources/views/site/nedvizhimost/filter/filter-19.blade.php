<div id="filter-19" class="subfilter hidden">
    <div class="filter-group-ad more">
        <div class="filter-wrap-select">
        
            <div class="filter-select filter-vid_object">
                {!! Form::select('vid_object', TaxonomyTerm::get(11,0), Request::get('vid_object'), array('class' => 'form-control selectpicker', 'title' => 'Вид объекта', 'data-width' => '120px',)) !!}
            </div>
            
            <div class="filter-select">
                <div class="btn-group bootstrap-select filter-area">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">{{ (Request::get('area') && Request::input('area.min') !='') ? Request::input('area.min').'-'.Request::input('area.max').' м²' : 'Площадь, м²' }} <span class="caret"></span></button>
                    <div class="dropdown-menu" role="menu" style="width:220px;">
                        <div class="row">
                            <div class="col-lg-6">
                                {!! Form::text('area[min]', Request::input('area.min'), ['class' => 'form-control', 'placeholder' => 'от']) !!}
                            </div>
                            <div class="col-lg-6">
                                {!! Form::text('area[max]', Request::input('area.max'), ['class' => 'form-control', 'placeholder' => 'до']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-info area-apply">Применить</button>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
                
            <div class="filter-select">
                <div class="btn-group bootstrap-ddown filter-price">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">{{ (Request::get('price') && Request::input('price.min') != '') ? Request::input('price.min').'-'.Request::input('price.max').' Р' : 'Цена, Р' }} <span class="caret"></span></button>
                    <div class="dropdown-menu" role="menu" style="width:220px;">
                        <div class="row">
                            <div class="col-lg-6">
                                {!! Form::text('price[min]', Request::input('price.min'), ['class' => 'form-control', 'placeholder' => 'от']) !!}
                            </div>
                            <div class="col-lg-6">
                                {!! Form::text('price[max]', Request::input('price.max'), ['class' => 'form-control', 'placeholder' => 'до']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-info price-apply">Применить</button>
                            </div>
                        </div>
                    </div>
              </div>
            </div>
        </div>
    </div>
</div>