<div class="btn-group btn-group-100">
    <button type="button" class="btn btn-e btn-us dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bacay"></i> Рекламировать <span class="caret"></span></button>
    <ul class="dropdown-menu" role="menu">
        <li><a class="ajax ajax-services" href="{{ route('wallet.bay.services', [$advert->id, 1]) }}"><i class="glyph-icon flaticon-signs"></i> Премиум-объявления</a></li>
        <li><a class="ajax ajax-services" href="{{ route('wallet.bay.services', [$advert->id, 2]) }}"><i class="fa fa-diamond"></i> VIP-объявления</a></li>
        <li><a class="ajax ajax-services" href="{{ route('wallet.bay.services', [$advert->id, 3]) }}"><i class="fa fa-paint-brush"></i> Выделенные объявления</a></li>
        <li><a class="ajax ajax-services" href="{{ route('wallet.bay.services', [$advert->id, 4]) }}"><i class="glyph-icon flaticon-arrow"></i> Поднятие объявления</a></li>
    </ul>
</div>