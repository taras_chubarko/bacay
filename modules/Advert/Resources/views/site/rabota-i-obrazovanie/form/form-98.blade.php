<div id="form-98" class="hidden">
    <div class="form-group h-34{{ $errors->has('service') ? ' has-error' : '' }} row">
        <label for="service" class="col-lg-4 label-30">Сфера деятельности</label>
        <div class="col-lg-8">
            {!! Form::select('service', TaxonomyTerm::get(4, 0), ($advert) ? $advert->service->id : null, array('class' => 'form-control selectpicker', 'title' => 'Выбрать сферу деятельности')) !!}
        </div>
    </div>
    
    <div class="form-group h-34{{ $errors->has('schedule') ? ' has-error' : '' }} row">
        <label for="schedule" class="col-lg-4 label-30">График работы</label>
        <div class="col-lg-8">
            {!! Form::select('schedule', TaxonomyTerm::get(5, 0), ($advert) ? $advert->schedule->id : null, array('class' => 'form-control selectpicker', 'title' => 'Выбрать график работы')) !!}
        </div>
    </div>
    
    <div class="form-group{{ $errors->has('experience') ? ' has-error' : '' }} row">
        <label for="schedule" class="col-lg-4 label-30">Опыт работы</label>
        <div class="col-lg-8">
            <div class="input-group">
                {!! Form::text('experience', ($advert) ? $advert->experience->value : null, array('class' => 'form-control')) !!}
                <span class="input-group-addon">лет</span>
            </div>
        </div>
    </div>
</div>