<div id="form-376" class="hidden">
    <div class="form-group h-34{{ $errors->has('typ_education') ? ' has-error' : '' }} row">
        <label for="typ_education" class="col-lg-4 label-30">Тип обявления</label>
        <div class="col-lg-8">
            {!! Form::select('typ_education', TaxonomyTerm::get(16, 0), ($advert) ? $advert->typEducation->id : null, array('class' => 'form-control selectpicker', 'title' => 'Выбрать')) !!}
        </div>
    </div>
</div>