<div id="form-99" class="hidden">
    <div class="form-group h-34{{ $errors->has('education') ? ' has-error' : '' }} row">
        <label for="body" class="col-lg-4 label-30">Образование</label>
        <div class="col-lg-8">
            {!! Form::select('education', TaxonomyTerm::get(6, 0), ($advert) ? $advert->education->id : null, array('class' => 'form-control selectpicker', 'title' => 'Выбрать образование')) !!}
        </div>
    </div>
        
    <div class="form-group h-34{{ $errors->has('gender') ? ' has-error' : '' }} row">
        <label for="body" class="col-lg-4 label-30">Пол</label>
        <div class="col-lg-8">
            {!! Form::select('gender', TaxonomyTerm::get(7, 0), ($advert) ? $advert->gender->id : null, array('class' => 'form-control selectpicker', 'title' => 'Выбрать пол')) !!}
        </div>
    </div>
        
    <div class="form-group{{ $errors->has('age') ? ' has-error' : '' }} row">
        <label for="body" class="col-lg-4 label-30">Возраст</label>
        <div class="col-lg-8">
            <div class="input-group">
                {!! Form::text('age', ($advert) ? $advert->age->value : null, array('class' => 'form-control')) !!}
                <span class="input-group-addon">лет</span>
            </div>
        </div>
    </div>
</div>