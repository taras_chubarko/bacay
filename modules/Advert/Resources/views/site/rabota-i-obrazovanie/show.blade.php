@extends('site::layouts.master')

@section('breadcrumbs')
{!! Breadcrumbs::render('advert', $advert) !!}
@stop

@section('content')
<div class="push-right-main-bl">
    @include('advert::site.block-info')
    <div class="product-card">
        <h1 class="large-title">{{ $advert->name }}</h1>
        <div class="gallery-wrapper">
            @include('advert::site.sliderkit', $advert)
        </div>
        <div class="product-slide-wrapper"></div>
        
        <div class="p-inline-bl">
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Зарплата:</span>
                <span class="p-price-bl">{{ $advert->price->format }}</span>
            </div>
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Контактное лицо:</span>
                <div class="p-inline-more-info">
                    <span>{{ $advert->user->username }}</span>
                    <a class="btn-icon btn-ph" data-advert="{{ $advert->id }}" href="javascript:">Показать телефон</a>
                    <a class="btn-icon btn-mess" data-advert="{{ $advert->id }}" href="javascript:">Написать сообщение</a>
                </div>
            </div>
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Город:</span>
                <span>{{ $advert->city->name }}</span>
            </div>
            @if($advert->metro->value)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Метро:</span>
                <span>{!! $advert->metro->value !!}</span>
            </div>
            @endif
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt"></span>
                <span><b>{{ $advert->category[1]->name }} | {{ $advert->category[0]->name }}</b></span>
            </div>
            @if($advert->service->name)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Сфера деятельности:</span>
                <span>{{ $advert->service->name }}</span>
            </div>
            @endif
            @if($advert->schedule->name)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">График работы:</span>
                <span>{{ $advert->schedule->name }}</span>
            </div>
            @endif
            @if($advert->experience->value)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Опыт работы:</span>
                <span>{{ $advert->experience->value }} лет</span>
            </div>
            @endif
            @if($advert->education->name)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Образование:</span>
                <span>{{ $advert->education->name }}</span>
            </div>
            @endif
            @if($advert->typEducation->name)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Тип:</span>
                <span>{{ $advert->typEducation->name }}</span>
            </div>
            @endif
            @if($advert->age->value)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Возраст:</span>
                <span>{{ $advert->age->value }} лет</span>
            </div>
            @endif
            @if($advert->gender->name)
            <div class="p-inline-line-wrap">
                <span class="p-inline-tt">Пол:</span>
                <span>{{ $advert->gender->name }}</span>
            </div>
            @endif
        </div>
        
        @if($advert->body)
        <div class="p-descript-info">
            <b>Описание:</b>
           {!! $advert->body !!}
        </div>
        @endif
        
        <div class="date-info">
            <span>№ {{ $advert->id }}</span>
            <span>{{ $advert->dateHuman2 }}.</span>
        </div>
        
        <div class="post-info-line">
            <div class="row">
                <div class="col-xs-6">
                    <a class="icon-link-post ic-l-p1 complain" data-advert="{{ $advert->id }}" href="javascript:">Пожаловаться</a>
                    <a class="icon-link-post ic-l-p2" data-container="body" data-toggle="popover" data-placement="bottom" data-content="" href="javascript:">Поделиться</a>
                    <div id="popover-social">
                        <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <div class="addthis_sharing_toolbox"></div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="icon-post-show">
                        <span>
                            Просмотров: <a href="#">всего {{ $advert->views->all }}, сегодня {{ $advert->views->today }}</a>
                        </span>
                    </div>
                </div>
            </div>
        </div><!--.post-info-line-->
    </div>
    
    {!! Block::view('similar', $advert) !!}
</div>
    
<div class="aside-column">
    @include('advert::site.region-right', $advert)
</div>
@stop