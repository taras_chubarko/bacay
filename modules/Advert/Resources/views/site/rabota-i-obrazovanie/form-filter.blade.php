{!! Form::open(array('url' => Request::url(), 'role' => 'form', 'class' => 'form-filter', 'method' => 'GET')) !!}
    {!! Former::setOption('automatic_label', false) !!}
    <div class="filter-wrapp">
        
        <div class="filter-checked-items">
            {!! Form::radios2('category[0]', TaxonomyTerm::get(1, 3), Request::get('category'), ['class' => 'styler category']) !!}
        </div>
            
        <div class="append">
            
        </div>
            
        <div class="filter-select filter-pages">
            {!! Form::select('pages', [25 => 25, 50 => 50, 100 => 100],  (Request::get('pages')) ? Request::get('pages') : 25, array('class' => 'selectpicker', 'title' => 'Показать', 'data-width' => '120px')) !!}
        </div>
            
    </div><!--.filter-wrapp-->
{!! Form::close() !!}