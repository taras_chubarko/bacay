<div class="modal fade" id="sendComplain" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Пожаловаться</h4>
            </div>
            {!! Form::open(array('route' => 'adverts.complain', 'role' => 'form', 'class' => 'form')) !!}
                <div class="modal-body">
                    <div class="form-group subject">
                        {!! Form::select('complain', config('advert.complain'), null, array('class' => 'form-control selectpicker', 'title' => 'Причина жалобы')) !!}
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 username">
                            {!! Form::text('username', (Sentinel::check()) ? Sentinel::getUser()->username : null, array('class' => 'form-control', 'placeholder' => 'Ваше имя')) !!}
                        </div>
                        <div class="form-group col-md-6 usermail">
                            {!! Form::email('usermail', (Sentinel::check()) ? Sentinel::getUser()->email : null, array('class' => 'form-control', 'placeholder' => 'Ваша почта')) !!}
                        </div>
                    </div>
                    <div class="form-group messages">
                        {!! Form::textarea('messages', null, array('class' => 'form-control', 'rows' => 5, 'placeholder' => 'Текст жалобы')) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="status m-r-20 w-410 pull-left f-sz-20 p-t-10"></div>
                    {!! Form::hidden('advert', $advert->id) !!}
                    {!! Form::submit('Отправить', array('class' => 'btn btn-primary')) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>