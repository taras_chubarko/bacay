@extends('site::mail.main')

@section('title')
Причина жалобы: "{{ $complain }}"
@stop

@section('content')
    <p><b>Обявление: </b><a href="{{ route($advert['type'].'.show', $advert['id']) }}">{{ $advert['name'] }}</a></p>
    <p><b>Имя: </b>{{ $username }}</p>
    <p><b>Почта: </b>{{ $usermail }}</p>
    <p><b>Текст жалобы:</b><br>{{ $messages }}</p>
@stop