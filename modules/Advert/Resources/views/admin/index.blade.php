@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Объявления') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Объявления</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('advert.create'))
            <div class="btn-group m-b-20">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Добавить <span class="caret"></span></button>
                <ul class="dropdown-menu" role="menu">
                    @foreach($categories as $category)
                        <li><a href="{{ route('adverts.add.category', $category->slug) }}">{{ $category->name }}</a></li>
                    @endforeach
                </ul>
            </div>
            @endif
            @include('advert::admin.tabs')
            @include('advert::admin.filter', ['types' => $types])
        </div>
        
        <div class="box-body">
            @if($adverts->count())
            <table class="table table-bordered b-c-222D32">
                <thead>
                    <tr>
                        <th width="10">#</th>
                        <th width="150">Тип</th>
                        <th>Название</th>
                        <th width="200">Автор</th>
                        <th>Опубликовано</th>
                        <th width="150">Статус</th>
                        <th width="150">Действие</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($adverts as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->type }}</td>
                        <td><a href="{{ route($item->type.'.show', $item->id) }}">{{ $item->name }}</a></td>
                        <td><a href="{{ route('user.slug', $item->user->slug) }}">{{ $item->user->name }}</a></td>
                        <td>{{ $item->date }}</td>
                        <td>{{ $item->statuss }}</td>
                        <td>
                            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('advert.edit'))
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Редактировать <span class="caret"></span></button>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a class="ajax ajax-form" data-method="POST" href="{{ route('admin.advert.id.public', $item->id) }}"><i class="fa fa-check-square-o" aria-hidden="true"></i> Опубликовать</a>
                                    </li>
                                    <li>
                                        <a href="{{ route($item->type.'.edit', $item->id) }}"><i class="fa fa-fw fa-edit"></i> Изменить</a>
                                    </li>
                                     @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('advert.delete'))
                                    <li>
                                        <a class="delete" data-method="DELETE" data-button2="Нет" data-button1="Да" data-message="Это действие необратимо!" data-title="Хотите удалить?" data-href="{{ route('admin.adverts.destroy', $item->id) }}" href="javascript:">
                                            <i class="fa fa-fw fa-trash-o"></i> Удалить
                                        </a>
                                    </li>
                                    <li>
                                        <a class="delete" data-method="DELETE" data-button2="Нет" data-button1="Да" data-message="Это действие необратимо!" data-title="Хотите удалить?" data-href="{{ route('admin.advert.id.deladmin', $item->id) }}" href="javascript:">
                                            <i class="fa fa-fw fa-trash-o"></i> Удалить из админки
                                        </a>
                                    </li>
                                    @endif
                                    <li>
                                        <a class="ajax ajax-form" data-method="POST" href="{{ route('admin.advert.id.unpublic', $item->id) }}"><i class="fa fa-square-o" aria-hidden="true"></i> Снять с публикации</a>
                                    </li>
                                    <li>
                                        <a class="ajax ajax-form" data-method="POST" href="{{ route('admin.advert.id.cancel', $item->id) }}"><i class="fa fa-times" aria-hidden="true"></i> Отклонить</a>
                                    </li>
                                </ul>
                            </div>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {!! $adverts->render() !!}
            @else
                <p>Нет объявлений.</p>
            @endif
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop