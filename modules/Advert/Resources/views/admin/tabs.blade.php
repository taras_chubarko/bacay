<ul class="nav nav-tabs m-b-20">
    <li class="{{ active_class(if_uri(['admin/adverts/status/2', 'admin/adverts']), 'active') }}"><a href="{{ route('admin.adverts.status', 2) }}">На модерации <span class="badge">{{ Advert::countStatus(2) }}</span></a></li>
    <li class="{{ active_class(if_uri(['admin/adverts/status/3']), 'active') }}"><a href="{{ route('admin.adverts.status', 3) }}">Отклоненые модератором <span class="badge">{{ Advert::countStatus(3) }}</span></a></li>
    <li class="{{ active_class(if_uri(['admin/adverts/status/1']), 'active') }}"><a href="{{ route('admin.adverts.status', 1) }}">Опубликованые <span class="badge">{{ Advert::countStatus(1) }}</span></a></li>
    <li class="{{ active_class(if_uri(['admin/adverts/status/0']), 'active') }}"><a href="{{ route('admin.adverts.status', 0) }}">Снятые с публикации <span class="badge">{{ Advert::countStatus(0) }}</span></a></li>
</ul>