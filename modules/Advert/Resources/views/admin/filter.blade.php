<div id="admin-advert-filter">
    {!! Form::open(array('url' => Request::url(), 'role' => 'form', 'class' => 'form', 'method' => 'GET')) !!}
        <div class="row">
            <div class="col-lg-3">
                <div class="form-group">
                    <label for="type">Тип объявления</label>
                    {!! Form::select('type', $types, Request::get('type'), array('class' => 'form-control selectpicker', 'title' => '- Выбрать -')) !!}
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label for="name">Название</label>
                    {!! Form::text('name', Request::get('name'), array('class' => 'form-control')) !!}
                </div>
            </div>
            <div class="col-lg-3">
                <div class="form-group">
                    <label for="avtor">Автор</label>
                    {!! Form::text('avtor', Request::get('avtor'), array('class' => 'form-control')) !!}
                </div>
            </div>
            <div class="col-lg-3" style="padding-top:25px;">
                {!! Form::submit('Применить', array('class' => 'btn btn-primary')) !!}
                <a class="btn btn-info" href="{{ Request::url() }}">Сбросить</a>
            </div>
        </div>
    {!! Form::close() !!}
</div>