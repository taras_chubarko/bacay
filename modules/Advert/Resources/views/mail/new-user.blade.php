@extends('site::mail.main')

@section('title')
Вы разместили первое объявление.
@stop

@section('content')
<p><b>Почта / Логин:</b> {{ $email }}</p>
<p><b>Пароль для входа:</b> {{ $password }}</p>
<p>Вы можете войти по ссылке <a href="{{ url('/') }}/user">{{ url('/') }}/user</a></p>
<p>Постояяная ссылка на ваше объявление <a href="{{ $advertLink }}">{{ $advertLink }}</a></p>
@stop