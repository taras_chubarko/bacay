<?php namespace Modules\Advert\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Advert extends Model implements SluggableInterface {

    use SluggableTrait;
    
    protected $table = 'advert';

    protected $fillable = [];
    
    protected $guarded = ['_token', 'meta'];
    
    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
        'on_update'  => false,
    ];
    /*
     * Категория 
     */
    public function pivotCategory()
    {
       return $this->belongsToMany('Advert', 'advert_category')
       ->withPivot('type', 'category', 'sort')
       ->leftJoin('taxonomy_term', 'advert_category.category', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     * Тип объявления
     */
    public function pivotTypadvert()
    {
       return $this->belongsToMany('Advert', 'advert_typadvert')
       ->withPivot('type', 'typadvert')
       ->leftJoin('taxonomy_term', 'advert_typadvert.typadvert', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     * Тип дома
     */
    public function pivotTyphouse()
    {
       return $this->belongsToMany('Advert', 'advert_typhouse')
       ->withPivot('type', 'typhouse')
       ->leftJoin('taxonomy_term', 'advert_typhouse.typhouse', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     * Кол-во комнат
     */
    public function pivotQtyroom()
    {
       return $this->belongsToMany('Advert', 'advert_qtyroom')
       ->withPivot('type', 'qtyroom');
    }
    /*
     * Площадь, м²
     */
    public function pivotArea()
    {
       return $this->belongsToMany('Advert', 'advert_area')
       ->withPivot('type', 'area');
    }
    /*
     * Этаж
     */
    public function pivotFloor()
    {
       return $this->belongsToMany('Advert', 'advert_floor')
       ->withPivot('type', 'floor');
    }
    /*
     * Этажей в доме
     */
    public function pivotQtyfloor()
    {
       return $this->belongsToMany('Advert', 'advert_qtyfloor')
       ->withPivot('type', 'qtyfloor');
    }
    /*
     * Вид объекта
     */
    public function pivotVidObject()
    {
       return $this->belongsToMany('Advert', 'advert_vid_object')
       ->withPivot('type', 'vid_object')
       ->leftJoin('taxonomy_term', 'advert_vid_object.vid_object', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     * Площадь участка в сотках
     */
    public function pivotAreaSot()
    {
       return $this->belongsToMany('Advert', 'advert_area_sot')
       ->withPivot('type', 'area_sot');
    }
    /*
     * Вид строения
     */
    public function pivotVidBuild()
    {
       return $this->belongsToMany('Advert', 'advert_vid_build')
       ->withPivot('type', 'vid_build')
       ->leftJoin('taxonomy_term', 'advert_vid_build.vid_build', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     * Материал стен
     */
    public function pivotMaterialWalls()
    {
       return $this->belongsToMany('Advert', 'advert_material_walls')
       ->withPivot('type', 'material_walls')
       ->leftJoin('taxonomy_term', 'advert_material_walls.material_walls', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     * Машиноместо
     */
    public function pivotCarPlaces()
    {
       return $this->belongsToMany('Advert', 'advert_car_places')
       ->withPivot('type', 'car_places')
       ->leftJoin('taxonomy_term', 'advert_car_places.car_places', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     * Страна
     */
    public function pivotCountry()
    {
       return $this->belongsToMany('Advert', 'advert_country')
       ->withPivot('type', 'country');
    }
    /*
     * Цена, Р
     */
    public function pivotPrice()
    {
       return $this->belongsToMany('Advert', 'advert_price')
       ->withPivot('type', 'price');
    }
    /*
     * Описание
     */
    public function pivotBody()
    {
       return $this->belongsToMany('Advert', 'advert_body')
       ->withPivot('type', 'body');
    }
    /*
     * Картинки 
     */
    public function pivotImages()
    {
       return $this->belongsToMany('Advert', 'advert_images')
       ->withPivot('type', 'fid', 'alt', 'title', 'sort')
       ->leftJoin('filemanager as images', 'advert_images.fid', '=', 'images.id')
       ->orderBy('advert_images.sort', 'ASC')
       ->select(
            "images.id as pivot_id",
            "images.user_id as pivot_user_id",
            "images.filename as pivot_filename",
            "images.uri as pivot_uri",
            "images.filemime as pivot_filemime",
            "images.filesize as pivot_filesize",
            "images.status as pivot_status",
            "images.created_at as pivot_created_at"
        );
    }
    /*
     * Дата окончания публикации
     */
    public function pivotEnd()
    {
       return $this->belongsToMany('Advert', 'advert_end')
       ->withPivot('type', 'end');
    }
    /*
     * Город
     */
    public function pivotCity()
    {
        return $this->belongsToMany('Advert', 'advert_city')
        ->withPivot('type', 'city')
        ->leftJoin('geo_city', 'geo_city.city_id', '=', 'advert_city.city')
        ->select(
            "geo_city.city_id as pivot_city_id",
            "geo_city.name_ru as pivot_name"
        );
    }
    /*
     * Метро
     */
    public function pivotMetro()
    {
        return $this->belongsToMany('Advert', 'advert_metro')
        ->withPivot('type', 'metro')
        ->leftJoin('geo_metro', 'geo_metro.id', '=', 'advert_metro.metro')
        ->select(
            "geo_metro.id as pivot_id",
            "geo_metro.name as pivot_name",
            "geo_metro.color as pivot_color"
        );
    }
    /*
     * Марка Авто
     */
    public function pivotBrand()
    {
        return $this->belongsToMany('Advert', 'advert_brand')
        ->withPivot('type', 'brand')
        ->leftJoin('transport_brand', 'transport_brand.id', '=', 'advert_brand.brand')
        ->select(
            "transport_brand.id as pivot_id",
            "transport_brand.name as pivot_name"
        );
    }
    /*
     * Модель Авто
     */
    public function pivotModel()
    {
        return $this->belongsToMany('Advert', 'advert_model')
        ->withPivot('type', 'model')
        ->leftJoin('transport_model', 'transport_model.id', '=', 'advert_model.model')
        ->select(
            "transport_model.id as pivot_id",
            "transport_model.brand_id as pivot_brand_id",
            "transport_model.name as pivot_name"
        );
    }
    /*
     * Коробка Авто
     */
    public function pivotTransmission()
    {
        return $this->belongsToMany('Advert', 'advert_transmission')
        ->withPivot('type', 'transmission')
        ->leftJoin('transport_transmission', 'transport_transmission.id', '=', 'advert_transmission.transmission')
        ->select(
            "transport_transmission.id as pivot_id",
            "transport_transmission.name as pivot_name"
        );
    }
    /*
     * Тип привода Авто
     */
    public function pivotDrive()
    {
        return $this->belongsToMany('Advert', 'advert_drive')
        ->withPivot('type', 'drive')
        ->leftJoin('transport_drive', 'transport_drive.id', '=', 'advert_drive.drive')
        ->select(
            "transport_drive.id as pivot_id",
            "transport_drive.name as pivot_name"
        );
    }
    /*
     * Топливо Авто
     */
    public function pivotFuel()
    {
        return $this->belongsToMany('Advert', 'advert_fuel')
        ->withPivot('type', 'fuel')
        ->leftJoin('transport_fuel', 'transport_fuel.id', '=', 'advert_fuel.fuel')
        ->select(
            "transport_fuel.id as pivot_id",
            "transport_fuel.name as pivot_name"
        );
    }
    /*
     * Год производства
     */
    public function pivotYear()
    {
        return $this->belongsToMany('Advert', 'advert_year')
        ->withPivot('type', 'year');
    }
    /*
     * Сфера деятельности 
     */
    public function pivotService()
    {
       return $this->belongsToMany('Advert', 'advert_service')
       ->withPivot('type', 'service')
       ->leftJoin('taxonomy_term', 'advert_service.service', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     * график работы
     */
    public function pivotSchedule()
    {
       return $this->belongsToMany('Advert', 'advert_schedule')
       ->withPivot('type', 'schedule')
       ->leftJoin('taxonomy_term', 'advert_schedule.schedule', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     * Опыт работы
     */
    public function pivotExperience()
    {
        return $this->belongsToMany('Advert', 'advert_experience')
        ->withPivot('type', 'experience');
    }
    /*
     * образование
     */
    public function pivotEducation()
    {
       return $this->belongsToMany('Advert', 'advert_education')
       ->withPivot('type', 'education')
       ->leftJoin('taxonomy_term', 'advert_education.education', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    public function pivotTypEducation()
    {
       return $this->belongsToMany('Advert', 'advert_typ_education')
       ->withPivot('type', 'typ_education')
       ->leftJoin('taxonomy_term', 'advert_typ_education.typ_education', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     * пол
     */
    public function pivotGender()
    {
       return $this->belongsToMany('Advert', 'advert_gender')
       ->withPivot('type', 'gender')
       ->leftJoin('taxonomy_term', 'advert_gender.gender', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     * Возраст
     */
    public function pivotAge()
    {
        return $this->belongsToMany('Advert', 'advert_age')
        ->withPivot('type', 'age');
    }
    /*
     * Кол-во просмотров
     */
    public function pivotViews()
    {
       return $this->belongsToMany('Advert', 'advert_views')
       ->withPivot('ip', 'view_at');
    }
    /*
     * Избраные
     */
    public function pivotFavorites()
    {
        $user_id = (\Sentinel::check()) ? \Sentinel::getUser()->id : \Session::getId();
        return $this->belongsToMany('Advert', 'advert_favorites')
        ->withPivot('user_id', 'add_at')->where('advert_favorites.user_id', $user_id);
    }
    /*
     * Платные услуги
     */
    public function pivotUslugi()
    {
       return $this->belongsToMany('Advert', 'advert_uslugi')
       ->withPivot('type', 'services_id', 'created_at', 'end', 'flag')
       ->leftJoin('services', 'advert_uslugi.services_id', '=', 'services.id')
       ->select(
            "services.name as pivot_name",
            "services.ico as pivot_ico",
            "services.price as pivot_price"
        );
    }
    /*
     * Район города
     */
    public function pivotCityRaj()
    {
        return $this->belongsToMany('Advert', 'advert_city_raj')
        ->withPivot('type', 'city_raj');
    }
    /*
     * Адрес
     */
    public function pivotAddress()
    {
        return $this->belongsToMany('Advert', 'advert_address')
        ->withPivot('type', 'address');
    }
    /*
     * Информация о продавце
     */
    public function pivotUserInfo()
    {
        return $this->belongsToMany('Advert', 'advert_user_info')
        ->withPivot('type', 'name', 'email', 'phone');
    }
    public function getUserInfoAttribute()
    {
        $data = array();
     
        $data['name']   = ($this->pivotUserInfo->count()) ? $this->pivotUserInfo[0]->pivot->name : null;
        $data['email']  = ($this->pivotUserInfo->count()) ? $this->pivotUserInfo[0]->pivot->email : null;
        $data['phone']  = ($this->pivotUserInfo->count()) ? $this->pivotUserInfo[0]->pivot->phone : null;
       
        return (object) $data;
    }
    //-----------------------------------------
    // Поля и виды
    //-----------------------------------------
    /* public function getDateAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getPriceAttribute($id)
    {
        $data = array();
        $data['value'] = ($this->pivotPrice->count()) ? $this->pivotPrice[0]->pivot->price : null;
        $data['format'] = ($this->pivotPrice->count()) ? number_format($this->pivotPrice[0]->pivot->price, 0, '.', ' ') . ' руб.': null;
        return (object) $data;
    }
    /* public function getImagesAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function getImagesAttribute()
    {
        $data = array();
        
        if($this->pivotImages->count())
        {
            foreach($this->pivotImages as $images)
            {
                $data[$images->pivot->sort] = $images->pivot;
            }
        }
        
        return $data;
    }
    /* public function getOneImageAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getOneImageAttribute()
    {
        return ($this->pivotImages->count()) ? $this->pivotImages[0]->pivot : null;
    }
    /* public function getDateAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getDateAttribute()
    {
        if($this->public_at)
        {
            return date('d.m.Y', strtotime($this->public_at));
        }
        
    }
    public function getDateHumanAttribute()
    {
        if($this->public_at)
        {
            return cuteDate($this->public_at);
        }
        else
        {
            return cuteDate($this->created_at);
        }
        
    }
    public function getDateHuman2Attribute()
    {
        if($this->public_at)
        {
            return cuteDate2($this->public_at);
        }
        else
        {
            return cuteDate2($this->created_at);
        }
    }
    public function getStatussAttribute()
    {
        if($this->status == 1)
        {
            return 'Опубликовано';
        }
        if($this->status == 0)
        {
           return 'Не опубликовано';
        }
        if($this->status == 2)
        {
           return 'На модерации';
        }
        if($this->status == 3)
        {
           return 'Отклонено модератором';
        }
       
    }
    
    /* public function getEndAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getEndAttribute()
    {
        $data = null;
        if($this->pivotEnd->count())
        {
            $data = date('d.m.Y', strtotime($this->pivotEnd[0]->pivot->end));
        }
        return $data;
    }
    /* public function getBodyAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getBodyAttribute()
    {
        return ($this->pivotBody->count() && $this->pivotBody[0]->pivot->body != '') ? $this->pivotBody[0]->pivot->body : null;
    }
    /* public function getCityAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCityAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotCity->count()) ? $this->pivotCity[0]->pivot->city_id : null;
        $data['name']   = ($this->pivotCity->count()) ? $this->pivotCity[0]->pivot->name : null;
        $data['metro']  = ($this->pivotCity->count()) ? $this->pivotCity[0]->pivot->city_id : 4400;
        return (object) $data;
    }
    /* public function getCityAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getMetroAttribute()
    {
        $data = array();
        $data['id']         = ($this->pivotMetro->count()) ? $this->pivotMetro[0]->pivot->id : null;
        $data['name']       = ($this->pivotMetro->count()) ? $this->pivotMetro[0]->pivot->name : null;
        $data['color']      = ($this->pivotMetro->count()) ? $this->pivotMetro[0]->pivot->color : null;
        $data['value']      = ($this->pivotMetro->count()) ? '<i class="glyph-icon flaticon-symbols" style="color:'.$this->pivotMetro[0]->pivot->color.'"></i> '.$this->pivotMetro[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getCategoryAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCategoryAttribute()
    {
        $data = array();
        if($this->pivotCategory->count())
        {
            foreach($this->pivotCategory as $category)
            {
                $data[$category->pivot->sort] = (object) array(
                    'id'    => $category->pivot->id,
                    'name'  => $category->pivot->name,
                );
            }
        }
        return $data;
    }
    /* public function getTypadvertAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getTypadvertAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotTypadvert->count()) ? $this->pivotTypadvert[0]->pivot->id : null;
        $data['name']   = ($this->pivotTypadvert->count()) ? $this->pivotTypadvert[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getTyphouseAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getTyphouseAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotTyphouse->count()) ? $this->pivotTyphouse[0]->pivot->id : null;
        $data['name']   = ($this->pivotTyphouse->count()) ? $this->pivotTyphouse[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getQtyroomAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getQtyroomAttribute()
    {
        $data = array();
        $data['value'] = ($this->pivotQtyroom->count()) ? $this->pivotQtyroom[0]->pivot->qtyroom : null;
        return (object) $data;
    }
    /* public function getAreaAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getAreaAttribute()
    {
        $data = array();
        $data['value'] = ($this->pivotArea->count()) ? $this->pivotArea[0]->pivot->area : null;
        return (object) $data;
    }
    /* public function getFloorAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFloorAttribute()
    {
        $data = array();
        $data['value'] = ($this->pivotFloor->count()) ? $this->pivotFloor[0]->pivot->floor : null;
        return (object) $data;
    }
    /* public function getFloorAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getQtyfloorAttribute()
    {
        $data = array();
        $data['value'] = ($this->pivotQtyfloor->count()) ? $this->pivotQtyfloor[0]->pivot->qtyfloor : null;
        return (object) $data;
    }
    /* public function allSlug
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function allSlug()
    {
        $slugs = Advert::all();
        
        $items = [];
        
        if($slugs->count())
        {
            foreach($slugs as $slug)
            {
                $items[] = $slug->id;
            }
            
            $route = implode('|', $items);
        }
        else
        {
            $route = '[0-9]+';
        }
        
        return $route;
    }
    /* public function getBrandAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getBrandAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotBrand->count()) ? $this->pivotBrand[0]->pivot->id : null;
        $data['name']   = ($this->pivotBrand->count()) ? $this->pivotBrand[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getModelAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getModelAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotModel->count()) ? $this->pivotModel[0]->pivot->id : null;
        $data['name']   = ($this->pivotModel->count()) ? $this->pivotModel[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getTransmissionAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getTransmissionAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotTransmission->count()) ? $this->pivotTransmission[0]->pivot->id : null;
        $data['name']   = ($this->pivotTransmission->count()) ? $this->pivotTransmission[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getDriveAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getDriveAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotDrive->count()) ? $this->pivotDrive[0]->pivot->id : null;
        $data['name']   = ($this->pivotDrive->count()) ? $this->pivotDrive[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getFuelAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFuelAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotFuel->count()) ? $this->pivotFuel[0]->pivot->id : null;
        $data['name']   = ($this->pivotFuel->count()) ? $this->pivotFuel[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getFuelAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getYearAttribute()
    {
        $data = array();
        $data['value'] = ($this->pivotYear->count()) ? $this->pivotYear[0]->pivot->year : null;
        return (object) $data;
    }
    /* public function getServiceAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getServiceAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotService->count()) ? $this->pivotService[0]->pivot->id : null;
        $data['name']   = ($this->pivotService->count()) ? $this->pivotService[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getScheduleAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getScheduleAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotSchedule->count()) ? $this->pivotSchedule[0]->pivot->id : null;
        $data['name']   = ($this->pivotSchedule->count()) ? $this->pivotSchedule[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getExperienceAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getExperienceAttribute()
    {
        $data = array();
        $data['value'] = ($this->pivotExperience->count()) ? $this->pivotExperience[0]->pivot->experience : null;
        return (object) $data;
    }
    /* public function getEducationAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getEducationAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotEducation->count()) ? $this->pivotEducation[0]->pivot->id : null;
        $data['name']   = ($this->pivotEducation->count()) ? $this->pivotEducation[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getEducationAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getTypEducationAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotTypEducation->count()) ? $this->pivotTypEducation[0]->pivot->id : null;
        $data['name']   = ($this->pivotTypEducation->count()) ? $this->pivotTypEducation[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getGenderAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getGenderAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotGender->count()) ? $this->pivotGender[0]->pivot->id : null;
        $data['name']   = ($this->pivotGender->count()) ? $this->pivotGender[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getExperienceAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getAgeAttribute()
    {
        $data = array();
        $data['value'] = ($this->pivotAge->count() && $this->pivotAge[0]->pivot->age != 0) ? $this->pivotAge[0]->pivot->age : null;
        return (object) $data;
    }
    /* public function getAreaArr
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getAreaArr()
    {
        $area = \DB::table('advert_area')->groupBy('area')->get();
        $items = array();
        
        foreach($area as $item)
        {
            $items[$item->area] = $item->area;
        }
        return $items;
    }
    /* public function getAreaArr
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getQtyRoomArr()
    {
        $area = \DB::table('advert_qtyroom')->groupBy('qtyroom')->get();
        $items = array();
        
        foreach($area as $item)
        {
            $items[$item->qtyroom] = $item->qtyroom;
        }
        return $items;
    }
    /* public function getDriveAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getViewsAttribute()
    {
        $data = array();
        $data['all'] = 0;
        $data['today'] = 0;
        if($this->pivotViews->count())
        {
            $today = new \DateTime('today');
            $data['all'] = $this->pivotViews->count();
            foreach($this->pivotViews as $view)
            {
                $d1 = date('d.m.Y');
                $d2 = date('d.m.Y', strtotime($view->pivot->view_at));
                if($d1 == $d2)
                {
                    $views[] = $view->pivot->view_at;
                }
            }
            $data['today'] = count($views);
        }
        return (object) $data;
    }
    
    /*
     * function scopeCategory
     * @param $arg
     */
    function scopeCategory($query, $category)
    {
        $query->where('advert.status', 1);
        $query->where('advert.type', $category);
        
        //$query->leftJoin('advert_uslugi', 'advert.id', '=', 'advert_uslugi.advert_id');
        //$query->where('advert_uslugi.services_id', 4);
        //$query->orderBy('advert_uslugi.created_at', 'DESC');
        
        $query->orderBy('advert.updated_at', 'DESC');
        
        
        $query->groupBy('advert.id');
        
        /*
         * Общие параметры
         */
        if(\Request::has('search'))
        {
            $query->where('advert.name', 'like', '%'.\Request::get('search').'%');
        }
        
        if(\Request::has('category'))
        {
            $query->leftJoin('advert_category', 'advert.id', '=', 'advert_category.advert_id');
            $query->whereIn('advert_category.category', \Request::get('category'));
        }
        
        if(\Request::has('typadvert'))
        {
            $query->leftJoin('advert_typadvert', 'advert.id', '=', 'advert_typadvert.advert_id');
            $query->where('advert_typadvert.typadvert', \Request::get('typadvert'));
        }
        
        if(\Request::has('price') && \Request::input('price.min') !='')
        {
            $query->leftJoin('advert_price', 'advert.id', '=', 'advert_price.advert_id');
            $query->whereBetween('advert_price.price', \Request::get('price'));
        }
        
        if(\Session::has('location') && session('location.city.city_id') != 0)
        {
            $query->leftJoin('advert_city', 'advert.id', '=', 'advert_city.advert_id');
            $query->where('advert_city.city', session('location.city.city_id'));
        }
        
        /*
         * Недвижимость
         */
        
        if(\Request::has('qtyroom'))
        {
            $query->leftJoin('advert_qtyroom', 'advert.id', '=', 'advert_qtyroom.advert_id');
            $query->whereIn('advert_qtyroom.qtyroom', \Request::get('qtyroom'));
        }
        
        if(\Request::has('area') && \Request::input('area.min') !='')
        {
            $query->leftJoin('advert_area', 'advert.id', '=', 'advert_area.advert_id');
            $query->whereBetween('advert_area.area', \Request::get('area'));
        }
        
        if(\Request::has('floor'))
        {
            $query->leftJoin('advert_floor', 'advert.id', '=', 'advert_floor.advert_id');
            $query->whereIn('advert_floor.floor', \Request::get('floor'));
        }
        
        if(\Request::has('qtyfloor'))
        {
            $query->leftJoin('advert_qtyfloor', 'advert.id', '=', 'advert_qtyfloor.advert_id');
            $query->whereIn('advert_qtyfloor.qtyfloor', \Request::get('qtyfloor'));
        }
        
        if(\Request::has('area_sot') && \Request::input('area_sot.min') !='')
        {
            $query->leftJoin('advert_area_sot', 'advert.id', '=', 'advert_area_sot.advert_id');
            $query->whereBetween('advert_area_sot.area_sot', \Request::get('area_sot'));
        }
        
        if(\Request::has('vid_object'))
        {
            $query->leftJoin('advert_vid_object', 'advert.id', '=', 'advert_vid_object.advert_id');
            $query->where('advert_vid_object.vid_object', \Request::get('vid_object'));
        }
        
        if(\Request::has('vid_build'))
        {
            $query->leftJoin('advert_vid_build', 'advert.id', '=', 'advert_vid_build.advert_id');
            $query->where('advert_vid_build.vid_build', \Request::get('vid_build'));
        }
        
        if(\Request::has('material_walls'))
        {
            $query->leftJoin('advert_material_walls', 'advert.id', '=', 'advert_material_walls.advert_id');
            $query->where('advert_material_walls.material_walls', \Request::get('material_walls'));
        }
        
        if(\Request::has('car_places'))
        {
            $query->leftJoin('advert_car_places', 'advert.id', '=', 'advert_car_places.advert_id');
            $query->where('advert_car_places.car_places', \Request::get('car_places'));
        }
        
        if(\Request::has('country'))
        {
            $query->leftJoin('advert_country', 'advert.id', '=', 'advert_country.advert_id');
            $query->where('advert_country.country', \Request::get('country'));
        }
        /*
         * Транспорт
         */
        
        if(\Request::has('brand'))
        {
            $query->leftJoin('advert_brand', 'advert.id', '=', 'advert_brand.advert_id');
            $query->where('advert_brand.brand', \Request::get('brand'));
        }
        
        if(\Request::has('model'))
        {
            $query->leftJoin('advert_model', 'advert.id', '=', 'advert_model.advert_id');
            $query->where('advert_model.model', \Request::get('model'));
        }
        
        if(\Request::has('year'))
        {
            $query->leftJoin('advert_year', 'advert.id', '=', 'advert_year.advert_id');
            $query->whereIn('advert_year.year', \Request::get('year'));
        }
        
        if(\Request::has('transmission'))
        {
            $query->leftJoin('advert_transmission', 'advert.id', '=', 'advert_transmission.advert_id');
            $query->whereIn('advert_transmission.transmission', \Request::get('transmission'));
        }
        
        if(\Request::has('drive'))
        {
            $query->leftJoin('advert_drive', 'advert.id', '=', 'advert_drive.advert_id');
            $query->whereIn('advert_drive.drive', \Request::get('drive'));
        }
        
        if(\Request::has('fuel'))
        {
            $query->leftJoin('advert_fuel', 'advert.id', '=', 'advert_fuel.advert_id');
            $query->whereIn('advert_fuel.fuel', \Request::get('fuel'));
        }
        
        /*
         * Работа и образование
         */
        
        if(\Request::has('service'))
        {
            $query->leftJoin('advert_service', 'advert.id', '=', 'advert_service.advert_id');
            $query->whereIn('advert_service.service', \Request::get('service'));
        }
        
        if(\Request::has('schedule'))
        {
            $query->leftJoin('advert_schedule', 'advert.id', '=', 'advert_schedule.advert_id');
            $query->whereIn('advert_schedule.schedule', \Request::get('schedule'));
        }
            
        if(\Request::has('experience') && \Request::input('experience.min') !='')
        {
            $query->leftJoin('advert_experience', 'advert.id', '=', 'advert_experience.advert_id');
            $query->whereBetween('advert_experience.experience', \Request::get('experience'));
        }
        
        if(\Request::has('education'))
        {
            $query->leftJoin('advert_education', 'advert.id', '=', 'advert_education.advert_id');
            $query->whereIn('advert_education.education', \Request::get('education'));
        }
        
        if(\Request::has('typ_education'))
        {
            $query->leftJoin('advert_typ_education', 'advert.id', '=', 'advert_typ_education.advert_id');
            $query->whereIn('advert_typ_education.typ_education', [\Request::get('typ_education')]);
        }
        
        if(\Request::has('gender'))
        {
            $query->leftJoin('advert_gender', 'advert.id', '=', 'advert_gender.advert_id');
            $query->whereIn('advert_gender.gender', \Request::get('gender'));
        }
        
        if(\Request::has('age') && \Request::input('age.min') !='')
        {
            $query->leftJoin('advert_age', 'advert.id', '=', 'advert_age.advert_id');
            $query->whereBetween('advert_age.age', \Request::get('age'));
        }
        
        $adverts = $query->get();//->paginate(10);
        
        $adverts->load('pivotImages', 'pivotMetro', 'pivotFavorites', 'pivotPrice', 'pivotCategory', 'pivotUslugi');
        
        //print '<pre>' . htmlspecialchars(print_r(session(), true)) . '</pre>';
        return $adverts;
    }
    /*
     *
     */
    public function user()
    {
        return $this->belongsTo('User');
    }
    /* public function getDefaultCategory
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function getDefaultCategory()
    {
        $seg1 = \Request::segment(1);
        $seg2 = \Request::segment(2);
        $category = null;
        if($seg1 == 'adverts' && $seg2 != 'add')
        {
            $category = \TaxonomyTerm::findBySlug($seg2)->id;
        }
        return $category;
    }
    /* public function getFavoriteAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFavoriteAttribute()
    {
        $data = array();
        $data['star']  = ($this->pivotFavorites->count()) ? '<i class="fa fa-star"></i>' : '<i class="fa fa-star-o"></i>';
        return (object) $data;
    }
    /* public function scopeGetFavorites
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function scopeGetFavorites($query)
    {
        $user_id = (\Sentinel::check()) ? \Sentinel::getUser()->id : \Session::getId();
        $query->leftJoin('advert_favorites', 'advert.id', '=', 'advert_favorites.advert_id');
        $query->where('advert_favorites.user_id', $user_id);
        $adverts = $query->paginate(10);
        $adverts->load('pivotImages', 'pivotPrice', 'pivotCategory');
        return $adverts;
    }
    /* public function name
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function scopeDeleteOne($query, $id)
    {
        $advert = $query->where('advert.id', $id)->first();
        
        $images = \DB::table('advert_images')->where('advert_id', $id)->get();
        foreach($images as $image)
        {
            \Img::del($image->fid, []);
        }
        
        foreach(config('advert.tables') as $table)
        {
            \DB::table($table)->where('advert_id', $id)->delete();
        }
        \DB::table('advert')->where('id', $id)->delete();
        
    }
    /* public function scopeDeleteAll
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function scopeDeleteAll($query, $type)
    {
        $adverts = $query->where('advert.type', $type)->get();
        $items = array();
        foreach($adverts as $advert)
        {
            $items[$advert->id] = $advert->id;
        }
        
        $images = \DB::table('advert_images')->whereIn('advert_id', $items)->get();
        foreach($images as $image)
        {
            \Img::del($image->fid, []);
        }
        
        foreach(config('advert.tables') as $table)
        {
            \DB::table($table)->whereIn('advert_id', $items)->delete();
        }
        \DB::table('advert')->whereIn('id', $items)->delete();
    }
    /* public function getUslugaAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getUslugaAttribute()
    {
        $data = array();
        if($this->pivotUslugi->count())
        {
            foreach($this->pivotUslugi as $key => $usluga)
            {
                $data[$key] = (object) array(
                    'id'    => $usluga->pivot->services_id,
                    'name'  => $usluga->pivot->name,
                    'ico'   => $usluga->pivot->ico,
                    'price' => $usluga->pivot->price,
                );
            }
        }
        return $data;
    }
    /* public function getCityRajAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCityRajAttribute()
    {
        $data = array();
        $data['value'] = ($this->pivotCityRaj->count()) ? $this->pivotCityRaj[0]->pivot->city_raj : null;
        return (object) $data;
    }
    /* public function getAddressAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getAddressAttribute()
    {
        $data = array();
        $data['value'] = ($this->pivotAddress->count()) ? $this->pivotAddress[0]->pivot->address : null;
        return (object) $data;
    }
    /* public function getColorAdvertAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getColorAdvertAttribute()
    {
        $data = array();
        $data['value'] = '';
        if($this->pivotUslugi->count())
        {
            foreach($this->pivotUslugi as $uslugi)
            {
                if($uslugi->pivot->services_id == 3 && $uslugi->pivot->flag == 1)
                {
                    $data['value'] = ' catalog-fav';
                }
            }
        }
        return (object) $data;
    }
    /* public function getPremiunAdvertAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getPremiunAdvertAttribute()
    {
        $data = array();
        $data['id']     = null;
        $data['name']   = null;
        $data['ico']    = null;
        $data['price']  = null;
        if($this->pivotUslugi->count())
        {
            foreach($this->pivotUslugi as $uslugi)
            {
                if($uslugi->pivot->services_id == 1 && $uslugi->pivot->flag == 1)
                {
                    $data['id']     = $uslugi->pivot->services_id;
                    $data['name']   = $uslugi->pivot->name;
                    $data['ico']    = $uslugi->pivot->ico;
                    $data['price']  = $uslugi->pivot->price;
                }
            }
        }
        return (object) $data;
    }
    /* public function getEndLineAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getEndLineAttribute()
    {
        if(isset($this->pivotEnd[0]))
        {
            $end = $this->pivotEnd[0]->pivot->end;
            $days = timeLeft($end);
            
            $endday = \Option::get('advert.endday');
            $persent = 0;
            $day = 0;
            if($days->days > 0)
            {
                $persent = ($days->days * 100) / $endday;
                $day = $days->days;
            }
            return '<div class="ad-ui-line" data-toggle="tooltip" data-placement="top" title="До конца публикации осталось '.$day . num2word($day, array(' день', ' дня', ' дней')).'"><span style="width:'.$persent.'%"></span></div>';  
        }
        
    }
    /* public function getCategoryKeywordsAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCategoryKeywordsAttribute()
    {
        $data = array();
        if($this->pivotCategory->count())
        {
            foreach($this->pivotCategory as $category)
            {
                $data[] = $category->pivot->name;
            }
        }
        return implode(', ', $data);
    }
    /* public function getMetaDescription
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getMetaDescriptionAttribute()
    {
        if($this->pivotBody->count())
        {
            $desc = str_limit($this->pivotBody[0]->pivot->body, $limit = 300, $end = '...');
        }
        else
        {
            $desc = '';
        }
        return $desc;
    }
    /* public function scopeCountStatus
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function countStatus($status)
    {
        $count = \Advert::where('status', $status)->count();
        
        return $count;
    }
    /* public function scopeGetAdminAdvert
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function scopeGetAdminAdverts($query, $status)
    {
        $query->orderBy('created_at', 'DESC');
        $query->where('status', $status);
        
        if(\Request::has('type'))
        {
            $query->where('type', \Request::get('type'));
        }
        
        if(\Request::has('name'))
        {
            $query->where('name', 'like', '%'.\Request::get('name').'%');
        }
        
        if(\Request::has('avtor'))
        {
            $users = \User::where('name', 'like', '%'.\Request::get('avtor').'%')->get();
            foreach($users as $user)
            {
                $items[$user->id] = $user->id;
            }
            $query->whereIn('user_id', $items);
        }
        $adverts = $query->paginate(20);
        return $adverts;
    }
    
    /* public function getAreaSotAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getAreaSotAttribute()
    {
        $data = array();
        $data['value']     = ($this->pivotAreaSot->count()) ? $this->pivotAreaSot[0]->pivot->area_sot : null;
        return (object) $data;
    }
    /* public function getVidObjectAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getVidObjectAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotVidObject->count()) ? $this->pivotVidObject[0]->pivot->id : null;
        $data['name']   = ($this->pivotVidObject->count()) ? $this->pivotVidObject[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getVidBuildAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getVidBuildAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotVidBuild->count()) ? $this->pivotVidBuild[0]->pivot->id : null;
        $data['name']   = ($this->pivotVidBuild->count()) ? $this->pivotVidBuild[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getMaterialWallsAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getMaterialWallsAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotMaterialWalls->count()) ? $this->pivotMaterialWalls[0]->pivot->id : null;
        $data['name']   = ($this->pivotMaterialWalls->count()) ? $this->pivotMaterialWalls[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getMaterialWallsAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCarPlacesAttribute()
    {
        $data = array();
        $data['id']     = ($this->pivotCarPlaces->count()) ? $this->pivotCarPlaces[0]->pivot->id : null;
        $data['name']   = ($this->pivotCarPlaces->count()) ? $this->pivotCarPlaces[0]->pivot->name : null;
        return (object) $data;
    }
    /* public function getAreaSotAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCountryAttribute()
    {
        $data = array();
        $data['value']     = ($this->pivotCountry->count()) ? $this->pivotCountry[0]->pivot->country : null;
        return (object) $data;
    }
    
}