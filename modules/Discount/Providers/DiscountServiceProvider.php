<?php namespace Modules\Discount\Providers;

use Illuminate\Support\ServiceProvider;

class DiscountServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Boot the application events.
	 * 
	 * @return void
	 */
	public function boot()
	{
		$this->registerTranslations();
		$this->registerConfig();
		$this->registerViews();
		
		\Event::listen('user.login', function  ($user) {
			if($user->location->region_id == 0)
			{
				\Session::flash('info', 'Для получения скидки на платные услуги, заполните профиль. И обязательно укажите свой город.');
			}
			//else
			//{
			//	$discounts = \Discount::getByRegion($user->location->region_id);
			//	if($user->discount_first->count() == 0)
			//	{
			//		foreach($discounts as $discount)
			//		{
			//			
			//		}
			//	}
			//}
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{		
		//
	}

	/**
	 * Register config.
	 * 
	 * @return void
	 */
	protected function registerConfig()
	{
		$this->publishes([
		    __DIR__.'/../Config/config.php' => config_path('discount.php'),
		]);
		$this->mergeConfigFrom(
		    __DIR__.'/../Config/config.php', 'discount'
		);
	}

	/**
	 * Register views.
	 * 
	 * @return void
	 */
	public function registerViews()
	{
		$viewPath = null; // base_path('resources/views/modules/discount');

		$sourcePath = __DIR__.'/../Resources/views';

		//$this->publishes([
		//	$sourcePath => $viewPath
		//]);

		$this->loadViewsFrom(array_merge(array_map(function ($path) {
			return $path . '/modules/discount';
		}, \Config::get('view.paths')), [$sourcePath]), 'discount');
	}

	/**
	 * Register translations.
	 * 
	 * @return void
	 */
	public function registerTranslations()
	{
		$langPath = null; //base_path('resources/lang/modules/discount');

		if (is_dir($langPath)) {
			$this->loadTranslationsFrom($langPath, 'discount');
		} else {
			$this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'discount');
		}
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
