<?php namespace Modules\Discount\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Discount extends Model {

    protected $table = 'discount';

    protected $fillable = [];
    
   // protected $dates = ['dayend'];
    
    protected $guarded = ['_token', 'category_id', 'region_id'];
    /* public function getStatussAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getStatussAttribute() {
        
        switch($this->status)
        {
            case 0:
                return 'Не активная';
            break;
        
            case 1:
                return 'Активная';
            break;
        }
        
    }
    /* public function getDateAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getDateAttribute() {
        return date('d.m.Y', strtotime($this->updated_at));
    }
    public function getDateEndAttribute() {
        return !empty($this->dayend) ? date('d.m.Y', strtotime($this->dayend)) : '';
    }
    /*
     *
     */
    public function category()
    {
       return $this->belongsToMany('Discount', 'discount_category')
       ->withPivot('category_id')
       ->leftJoin('taxonomy_term', 'discount_category.category_id', '=', 'taxonomy_term.id')
       ->select(
            "taxonomy_term.id as pivot_id",
            "taxonomy_term.name as pivot_name"
        );
    }
    /*
     *
     */
    public function location()
    {
        return $this->belongsToMany('Discount', 'discount_location')
        ->withPivot('country_id', 'region_id', 'city_id')
        ->leftJoin('geo_region', 'discount_location.region_id', '=', 'geo_region.region_id')
        ->select(
            "geo_region.name_ru as pivot_region"
        );
    }
    /* public function getRegionsAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getRegionsAttribute()
    {
        $data = array();
        $regions = array();
        $regionsN = array();
        if($this->location->count())
        {
            foreach($this->location as $location)
            {
                $regions[$location->pivot->region_id] = $location->pivot->region_id;
                $regionsN[$location->pivot->region_id] = $location->pivot->region;
            }
        }
        $data['default'] = $regions;
        $data['names'] = implode(', ', $regionsN);
        
        return (object) $data;
    }
    /* public function getCategoriesAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getCategoriesAttribute($id)
    {
        $data = array();
        $data['id'] = $this->category->first()->pivot->id or null;
        return (object) $data;
    }
    /* public function scopeGetByRegion
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function scopeGetByRegion($query, $region_id, $category_id)
    {
        $query->where('discount.status', 1);
        $query->leftJoin('discount_location', 'discount.id', '=', 'discount_location.discount_id');
        $query->leftJoin('discount_category', 'discount.id', '=', 'discount_category.discount_id');
        $query->where('discount_location.region_id', $region_id);
        $query->where('discount_category.category_id', $category_id);
        $discount = $query->with('category', 'location')->first();
        return $discount;
    }

}