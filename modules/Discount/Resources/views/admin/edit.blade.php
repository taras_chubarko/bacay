@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Редактировать страницу') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Редактировать страницу</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            
        </div>
        
        <div class="box-body">
            {!! Form::open(array('route' => ['admin.discount.update', $discount->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'form')) !!}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Название *</label>
                    {!! Form::text('name', $discount->name, array('class' => 'form-control')) !!}
                </div>
                    
                <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                    <label for="category_id">Категрия скидки *</label>
                    {!! Form::select('category_id', TaxonomyTerm::itemsArray2(10), $discount->categories->id, array('class' => 'form-control selectpicker', 'title' => '- Выбрать -')) !!}
                </div>
                    
                <div class="form-group{{ $errors->has('region_id') ? ' has-error' : '' }}">
                    <label for="city">Регион *</label>
                    {!! Form::select('region_id[]', Region::getArray(), $discount->regions->default, array('class' => 'form-control selectpicker', 'multiple' => 'multiple', 'data-live-search' => true, 'data-actions-box' => true, 'title' => '- Выбрать -')) !!}
                </div>
                    
                <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
                    <label for="discount">% скидки *</label>
                    {!! Form::text('discount', $discount->discount, array('class' => 'form-control')) !!}
                    <p class="help-block">Например: 10% = 0.1</p>
                </div>
                    
                <div class="form-group{{ $errors->has('day') ? ' has-error' : '' }}">
                    <label for="day">Сколько дней действует скидка? </label>
                    {!! Form::text('day', $discount->day, array('class' => 'form-control')) !!}
                    <p class="help-block">Обязательно для времменой скидки. Если не установлен параметр, скидка будет действовать постоянно</p>
                </div>
                    
                <div class="form-group">
                    {!! Form::hidden('status', 0) !!}
                    <label for="status" class="icheck">
                        {!! Form::checkbox('status', 1, $discount->status) !!}
                        Активная
                    </label>
                </div>
                    
                <div class="form-group">
                   {!! Form::submit('Сохранить', array('class' => 'btn btn-primary btn-flat')) !!}
                </div>
            {!! Form::close() !!}
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>

@stop