@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Скидка') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Скидка</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('discount.create'))
		<a class="btn btn-success btn-flat" href="{{ route('admin.discount.create') }}">Создать</a>
	    @endif
        </div>
            
        <div class="box-body">
            @if($discounts->count())
                <table class="table table-bordered b-c-222D32">
                    <thead>
                        <tr>
                            <th width="10">#</th>
                            <th>Название</th>
                            <th>Скидка</th>
                            <th>Дата</th>
                            <th>Дата окончания</th>
                            <th>Статус</th>
                            <th width="120">Действие</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($discounts as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->discount }}</td>
                            <td>{{ $item->date }}</td>
                            <td>{{ $item->dateEnd }}</td>
                            <td>{{ $item->statuss }}</td>
                            <td>
                            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('discount.edit'))
                                <a href="{{ route('admin.discount.edit', $item->id) }}" class="btn btn-sm bg-navy btn-flat"><i class="fa fa-fw fa-edit"></i></a>
                            @endif
                            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('discount.delete'))
                                @include('cms::admin.delete', ['route' => 'admin.discount.destroy', 'id' => $item->id])
                            @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                Нет скидок.
            @endif
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop