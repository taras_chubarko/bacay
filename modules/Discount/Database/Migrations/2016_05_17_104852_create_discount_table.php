<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discount', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->integer('discount');
            $table->integer('status');
            $table->date('date_end');
            $table->timestamps();
        });
        
        Schema::create('discount_category', function(Blueprint $table)
        {
            $table->integer('discount_id')->unsigned()->nullable();
            $table->foreign('discount_id')->references('id')->on('discount')->onDelete('cascade');
            $table->integer('category_id');
        });
        
        Schema::create('discount_region', function(Blueprint $table)
        {
            $table->integer('discount_id')->unsigned()->nullable();
            $table->foreign('discount_id')->references('id')->on('discount')->onDelete('cascade');
            $table->integer('region_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('discount');
    }

}
