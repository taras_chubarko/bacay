<?php namespace Modules\Discount\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DiscountReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' 		=> 'required',
			'category_id' 	=> 'required',
			'region_id' 	=> 'required',
			'discount' 	=> 'required|numeric',
		];
	}
    protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		$validator->setAttributeNames([
			'name' 		=> '"Название"',
			'category_id' 	=> '"Категрия скидки"',
			'region_id' 	=> '"Регион"',
			'discount' 	=> '"% скидки"',
		]);
		return $validator;
	}

}
