<?php namespace Modules\Discount\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class DiscountController extends Controller {
	/* public function index
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function index()
	{
		$discounts = \Discount::orderBy('created_at', 'DESC')->paginate(20);
		//dd($discounts[0]->dayend->format('d.m.Y'));
		//$dt = \Carbon::instance($discounts[0]->dayend);
		//dd($dt);
		return view('discount::admin.index', compact('discounts'));
	}
	/* public function create
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function create()
	{
		return view('discount::admin.create');
	}
	/* public function store
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function store(\Modules\Discount\Http\Requests\DiscountReq $request)
	{
		$data = $request->all();
		
		if($request->day)
		{
			$data['dayend'] = \Carbon::now()->addDays($request->day);
		}
		
		$dicsount = \Discount::create($data);
		
		$dicsount->category()->attach($dicsount->id, [
			'category_id' => $request->category_id,
		]);
		
		if($request->region_id)
		{
			foreach($request->region_id as $region)
			{
				$dicsount->location()->attach($dicsount->id, [
					'region_id' => $region,
				]);
				//$this->setUserDiscount($region, $dicsount);
			}
		}
		
		return redirect()->route('admin.discount.index')->with('success', 'Скидка создана.');
	}
	/* public function edit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function edit($id)
	{
		$discount = \Discount::findOrFail($id);
		return view('discount::admin.edit', compact('discount'));
	}
	/* public function update
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function update(\Modules\Discount\Http\Requests\DiscountReq $request, $id)
	{
		$data = $request->all();
		$discount = \Discount::findOrFail($id);
		
		if($discount->day > 0)
		{
			$discount->dayend = \Carbon::instance($discount->created_at)->addDays($discount->day);
		}
			
		$discount->update($data);
		
		$discount->category()->detach($discount->id);
		$discount->category()->attach($discount->id, [
			'category_id' => $request->category_id,
		]);
		
		$discount->location()->detach($discount->id);
		if($request->region_id)
		{
			foreach($request->region_id as $region)
			{
				$discount->location()->attach($discount->id, [
					'region_id' => $region,
				]);
				//$this->setUserDiscount($region, $discount);
			}
		}
		return redirect()->route('admin.discount.index')->with('success', 'Скидка обновлена.');
	}
	/* public function destroy
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function destroy($id)
	{
		$discount = \Discount::findOrFail($id);
		$discount->category()->detach($discount->id);
		$discount->location()->detach($discount->id);
		\DB::table('users_discount')->where('discount_id', $discount->id)->delete();
		\DB::table('users_discount_first')->where('discount_id', $discount->id)->delete();
		$discount->delete();
		return redirect()->route('admin.discount.index')->with('success', 'Скидка удалена.');
	}
	/* public function setUserDiscount
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function setUserDiscount($region_id, $discount)
	{
		$users = \User::getByRegion($region_id);
		/*
		 * Одноразовая скидка
		 */
		if($discount->category->first()->pivot->id == 373)
		{
			foreach($users as $user)
			{
				$user->discount_first()->detach($user->id);
				if($discount->status == 1)
				{
					$user->discount_first()->attach($user->id, [
						'discount_id' 	=> $discount->id,
						'discount' 	=> $discount->discount,
					]);
				}
			}
		}
		/*
		 * Пользовательсяка скидка на время
		 */
		if($discount->category->first()->pivot->id == 374)
		{
			foreach($users as $user)
			{
				$user->discount()->detach($user->id);
				if($discount->status == 1)
				{
					$now = date('Y-m-d');
					$start_date = strtotime($now);
					$end_date = strtotime('+'.$discount->day.' day', $start_date);
					$user->discount()->attach($user->id, [
						'discount_id' 	=> $discount->id,
						'discount' 	=> $discount->discount,
						'date_end' 	=> date('Y-m-d H:i:s', $end_date),
					]);
				}
			}
		}
	}
}