<?php

Route::group(['middleware' => 'web', 'prefix' => 'discount', 'namespace' => 'Modules\Discount\Http\Controllers'], function()
{
	Route::get('/', 'DiscountController@index');
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\Discount\Http\Controllers'], function()
{
	Route::resource('discount', 'DiscountController');
});