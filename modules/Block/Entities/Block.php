<?php namespace Modules\Block\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Block extends Model {

    protected $table = 'blocks';

    protected $fillable = [];
    
    protected $guarded = ['_token'];
    
    /* public function scopeGetBlocks
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function scopeBlocks($query, $region)
    {
        $blocks = Block::where('status', 1)->where('region', $region)->orderBy('sort', 'ASC')->get();
        return $blocks;
    }
    /* public function getStatussAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getStatussAttribute() {
        
        switch($this->status)
        {
            case 0:
                return 'Не опубликовано';
            break;
            
            case 1:
                return 'Опубликовано';
            break;
        }
        
    }
    /* public function getDateAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getDateAttribute() {
        return date('d.m.Y', strtotime($this->created_at));
    }
    /* public function block
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function view($id, $data = null)
    {
        $app = app();
        
        if(is_numeric($id))
        {
            return $app->make('Modules\Block\Http\Controllers\BlockController')->show($id);
        }
        else
        {
            switch($id)
            {
                case 'category':
                    return $app->make('Modules\Block\Http\Controllers\BlockController')->blockCategory();
                break;
                
                case 'search':
                    return $app->make('Modules\Block\Http\Controllers\BlockController')->blockSearch();
                break;
                
                case 'similar':
                    return $app->make('Modules\Advert\Http\Controllers\AdvertController')->similar($data);
                break;
                
                case 'vip':
                    return $app->make('Modules\Advert\Http\Controllers\AdvertController')->vip();
                break; 
                
            }
        }
        
    }
    /*
     * function scopeRegion
     * @param $arg
     */
    public static function region($region)
    {
        $app = app();
        return $app->make('Modules\Block\Http\Controllers\BlockController')->region($region);
    }
    

}