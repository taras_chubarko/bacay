<?php

Route::group(['middleware' => 'web', 'prefix' => 'block', 'namespace' => 'Modules\Block\Http\Controllers'], function()
{
	Route::get('/', 'BlockController@index');
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\Block\Http\Controllers'], function()
{
	Route::resource('blocks', 'BlockController');
});