<?php namespace Modules\Block\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateBlockReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' 		=> 'required',
			'body' 		=> 'required',
			'format' 	=> 'required',
		];
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		$validator->setAttributeNames([
			'name' 		=> '"Название блока"',
			'body' 		=> '"Содержимое блока"',
			'format' 	=> '"Формат"',
		]);
	 
		return $validator;
	}

}
