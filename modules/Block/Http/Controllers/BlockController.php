<?php namespace Modules\Block\Http\Controllers;

use Pingpong\Modules\Routing\Controller;
use Illuminate\Http\Request;

class BlockController extends Controller {
	
	public function index()
	{
		$blocks = \Block::all();
		
		return view('block::admin.index', compact('blocks'));
	}
	/* public function create
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function create()
	{
	    return view('block::admin.create');
	}
	/* public function store
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function store(\Modules\Block\Http\Requests\CreateBlockReq $request)
	{
		$block = \Block::create($request->all());
		return redirect()->route('admin.blocks.index')->with('success', 'Блок создан.');
	}
	/* public function edit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function edit($id)
	{
		$block = \Block::findOrFail($id);
		return view('block::admin.edit', compact('block'));
	}
	/* public function update
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function update(\Modules\Block\Http\Requests\CreateBlockReq $request, $id)
	{
		$block = \Block::findOrFail($id);
		$block->update($request->all());
		return redirect()->route('admin.blocks.index')->with('success', 'Блок обновлен.');
	}
	/* public function destroy
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function destroy($id)
	{
		$block = \Block::findOrFail($id);
		$block->delete();
		return redirect()->route('admin.blocks.index')->with('success', 'Блок удален.');
	}
	/* public function show
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function show($id)
	{
		$block = \Block::findOrFail($id);
		ob_start();
		print eval('?>' . $block->body);
		$body = ob_get_contents();
		ob_end_clean();
		    
		if($block->tpl == 1)
		{
			$view = view('block::site.block-tpl', compact('body'));
		}
		else
		{
			$view = view('block::site.block', compact('block', 'body'));
		}
		return $view;
	}
	/* public function region
	 * @param $region
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function region($region) {
		
		//$blocks = \Block::where('status', 1)->where('region', $region)->orderBy('sort', 'ASC')->get();
		$blocks = \Block::blocks($region);
		//if( !\Cache::has('blockRegions') )
		//{
		//    $blocks = \Block::blocks($region);
		//    \Cache::put('blockRegions', $blocks, 60);
		//}
		//$blocks =  \Cache::get('blockRegions');
		
		$out = '';
		
		foreach($blocks as $block)
		{
			ob_start();
			print eval('?>' . $block->body);
			$body = ob_get_contents();
			ob_end_clean();
			
			if($block->tpl == 1)
			{
				$view = view('block::site.block-tpl', compact('body'));
			}
			else
			{
				$view = view('block::site.block', compact('block', 'body'));
			}
			
			switch($block->actions)
			{
				case 1:
					$out .= $view;
				break;
				
				case 2:
					$alias = preg_split('/\r\n|\r|\n/', $block->alias);
					//if(in_array($path, $alias))
					//{
					//    $out .= $view;
					//}
					foreach($alias as $ali)
					{
						if($ali == \Request::path())
						{
							$out .= $view;
						}
						
						//if(Request::is($ali))
						//{
						//	$out .= $view;
						//}
					}
				break;
				
				case 3:
					//$alias = explode("\n\r", $block->alias);
					$alias = preg_split('/\r\n|\r|\n/', $block->alias);
					if(in_array(\Request::path(), $alias))
					{
						$out .= '';
					}
					else
					{
						//print '<pre>' . htmlspecialchars(print_r($alias, true)) . '</pre>';
						$out .= $view;
					}
				break;	
			}
			
		}
		
		return $out;
		
	}
	/* public function blockCategory
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function blockCategory()
	{
		if( !\Cache::has('blockCategory') )
		{
		    $categories = \TaxonomyTerm::where('taxonomy_id', 1)->where('parent_id', 0)->get();
		    \Cache::put('blockCategory', $categories, 60);
		}
		$categories =  \Cache::get('blockCategory');
		return view('block::site.block-category', compact('categories'));
	}
	/* public function blockSearch
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function blockSearch()
	{
		$uri = \Request::path();
		
		if ($uri == '/') {
			return view('block::site.search-small');
		}
		else
		{
			$disable = array(
				'user',
				'favorites',
				'wallet'
			);
			if(!in_array($uri, $disable))
			{
				return view('block::site.search-big');	
			}
			
		}
	}
	
	
}