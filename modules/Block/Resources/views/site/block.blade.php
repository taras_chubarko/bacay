<div id="block-{{ ($block->style_id) ? $block->style_id : $block->id }}" class="block blosk-{{ ($block->style_class) ? $block->style_class : $block->id }}">
    @if($block->title)
        <h2>{{ $block->title }}</h2>
    @endif
    <div class="cont">
        {!! $body !!}
    </div>
</div>