@if($categories->count())
<div class="main-tiles-list">
    <ul>
        @foreach($categories as $category)
        <li>
            <a href="{{ route('adverts.category', $category->slug) }}">
                <span class="{{ $category->ico }}"></span>
                {!! $category->name !!}
            </a>
        </li>
        @endforeach
    </ul>
</div>
@endif