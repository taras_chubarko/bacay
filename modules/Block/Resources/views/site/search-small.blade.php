{!! Form::open(array('route' => 'adverts.search', 'role' => 'form', 'class' => 'form-poisk')) !!}
<div class="search-head-panel search-head-middle">
<button class="button-search" type="submit">Найти</button>
<div class="dinamic-structur-search">
<div class="search-choise-category">
{!! Form::select('category', TaxonomyTerm::get(1,0), null, array('class' => 'selectpicker', 'title' => 'Выбрать категорию', 'data-width' => '180px')) !!}
</div>
<div class="search-input">
{!! Form::text('search', Request::get('search'), array('class' => 'form-control', 'placeholder' => 'Поиск по сайту')) !!}
</div>
</div>
</div>
{!! Form::close() !!}