{!! Form::open(array('route' => 'adverts.search', 'role' => 'form', 'class' => 'form-poisk')) !!}
<div class="search-head-panel">
  <button class="button-search" type="submit">Найти</button>
  <div class="dinamic-structur-search">
    <div class="search-choise-category">
            {!! Form::select('category', TaxonomyTerm::get(1,0), Advert::getDefaultCategory(), array('class' => 'selectpicker', 'title' => 'Выбрать категорию')) !!}
    </div>
    <div class="search-input">
      {!! Form::text('search', Request::get('search'), array('class' => 'form-control', 'placeholder' => 'Поиск по сайту')) !!}
    </div>
    <div class="search-choise-city">
            {!! Form::select('city', City::getFavorite(session('location.city.city_id')), session('location.city.city_id'), array('class' => 'selectpicker geocity', 'title' => 'По всей России')) !!}
    </div>
  </div>
</div>
<div class="search-footer-panel">
  @yield('filterparam')
</div>
{!! Form::close() !!}