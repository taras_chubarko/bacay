@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Блоки') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Блоки</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('block.create'))
                <a href="{{ route('admin.blocks.create') }}" class="btn btn-success btn-flat">Добавить</a>
            @endif
        </div>
        
        <div class="box-body">
            @foreach(config('block.regions') as $key => $region)
                <div class="panel panel-default">
                  <div class="panel-heading">{{ $region }}</div>
                  <div class="panel-body">
                    
                    @if(Block::blocks($key)->count())
                    <table class="table table-bordered b-c-222D32">
                    <thead>
                      <tr>
                          <th width="20">#</th>
                          <th>Название блока</th>
                          <th width="150">Статус</th>
                          <th width="50">Позиция</th>
                          <th width="150">Дата</th>
                          <th width="100">Действие</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach(Block::blocks($key) as $block)
                        <tr>
                          <td>{{ $block->id }}</td>
                          <td>{{ $block->name }}</td>
                          <td>{{ $block->statuss }}</td>
                          <td class="text-center">{{ $block->sort }}</td>
                          <td>{{ $block->date }}</td>
                          <td>
                            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('block.edit'))
                                <a href="{{ route('admin.blocks.edit', $block->id) }}" class="btn btn-sm bg-navy btn-flat"><i class="fa fa-fw fa-edit"></i></a>
                            @endif
                            @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('block.delete'))
                                @include('cms::admin.delete', ['route' => 'admin.blocks.destroy', 'id' => $block->id])
                            @endif
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                  @else
                    Нет блоков.
                  @endif
                  </div>
                </div>
            @endforeach
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop