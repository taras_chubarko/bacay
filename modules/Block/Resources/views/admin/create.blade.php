@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Создать блок') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Создать блок</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            
        </div>
        
        <div class="box-body">
            {!! Form::open(array('route' => 'admin.blocks.store', 'role' => 'form', 'class' => 'form', 'id' => 'blocks-form')) !!}
                <div class="form-group">
                    <label for="name">Название блока</label>
                    {!! Form::text('name', null, array('class' => 'form-control')) !!}
                </div>
                  
                <div class="form-group">
                    <label for="title">Заголовок</label>
                    {!! Form::text('title', null, array('class' => 'form-control')) !!}
                </div>
                  
                <div class="form-group">
                    <label for="body">Содержимое блока</label>
                    {!! Form::textarea('body', null, array('class' => 'form-control', 'rows' => 5, 'id' => 'filem')) !!}
                </div>
                  
                <div class="form-group formats">
                    <label for="format">Формат</label>
                    {!! Form::radios('format', config('block.format'), 'html') !!}
                </div>
                  
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="region">Регион</label>
                        {!! Form::select('region', config('block.regions'), null, array('class' => 'form-control selectpicker')) !!}
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="sort">Позиция</label>
                        {!! Form::selectRange('sort', -50, 50, 0, array('class' => 'form-control selectpicker')) !!}
                    </div>
                  </div>
                </div>
                
                <div class="form-group">
                    <label for="alias">Видимость на страницах</label>
                    {!! Form::textarea('alias', null, array('class' => 'form-control', 'rows' => 3)) !!}
                    {!! Form::radios('actions', config('block.actions'), 1) !!}
                </div>
                  
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="style_id">id блока</label>
                        {!! Form::text('style_id', null, array('class' => 'form-control')) !!}
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="style_class">class блока</label>
                        {!! Form::text('style_class', null, array('class' => 'form-control')) !!}
                    </div>
                  </div>
                </div>
                  
                <div class="form-group">
                  {!! Form::hidden('tpl', 0) !!}
                  <label for="status" class="icheck">
                    {!! Form::checkbox('tpl', 1) !!}
                    Своя тема шаблона
                  </label>
                </div>
                  
                <div class="form-group">
                  {!! Form::hidden('status', 0) !!}
                  <label for="status" class="icheck">
                    {!! Form::checkbox('status', 1, 1) !!}
                    Опубликовать
                  </label>
                </div>
                {!! Form::submit('Сохранить', array('class' => 'btn btn-primary')) !!}
            {!! Form::close() !!}
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop