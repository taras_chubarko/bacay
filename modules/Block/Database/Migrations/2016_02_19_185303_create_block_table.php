<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlockTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blocks', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
            $table->string('title');
            $table->text('body');
            $table->string('region');
            $table->integer('sort');
            $table->string('format');
            $table->string('alias');
            $table->integer('status');
            $table->string('style_id');
            $table->string('style_class');
            $table->integer('tpl');
            $table->integer('actions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blocks');
    }

}
