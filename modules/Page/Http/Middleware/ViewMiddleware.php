<?php namespace Modules\Page\Http\Middleware; 

use Closure;

class ViewMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if(\Sentinel::check())
	{
	    $user = \Sentinel::getUser();
	    
	    if($user->inRole('admin'))
	    {
		return $next($request);
	    }
	    
	    if($user->hasAccess(['pages.view']))
	    {
		return $next($request);
	    }
	    else
	    {
		abort(403, 'У вас нет доступа для просмотра страниц.');
	    }
	}
	
	if (\Sentinel::guest())
        {
            $role = \Sentinel::findRoleBySlug('anonim');
	    
	    if($role->hasAccess(['pages.view']))
	    {
		return $next($request);
	    }
	    else
	    {
		abort(403, 'У вас нет доступа для просмотра страниц.');
	    }
	    
        }
	
	return $next($request);
    }
    
}
