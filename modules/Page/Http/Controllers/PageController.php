<?php namespace Modules\Page\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class PageController extends Controller {
	
	public function index()
	{
		$pages = \Page::orderBy('created_at', 'DESC')->paginate(20);
		
		return view('page::admin.index', compact('pages'));
	}
	
	/* public function create
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function create() {
		return view('page::admin.create');
	}
	
	public function store(\Modules\Page\Http\Requests\CreatePageReq $request)
	{
	    
	    $user = \Sentinel::getUser();
	    $data = $request->all();
	    $data['user_id'] = $user->id;
	    
	    $page = \Page::create($data);
	    
	    \Meta::set($page->id, $request->meta);
	    
	    return redirect()->route('pages.slug', $page->slug)->with('success', 'Страница создана.');
	}
	
	/**
	* Display the specified resource.
	*
	* @param  int  $id
	* @return Response
	*/
	public function show($slug)
	{
		$page = \Page::where('slug', $slug)->with('meta')->first();
		
		if (!$page || $page->status == 0) {
		    abort(404);
		}
		
		\MetaTag::set('title',  (empty($page->meta) || $page->meta->title == '') ? $page->name : $page->meta->title);
		\MetaTag::set('robots', !empty($page->meta) ? $page->meta->robots : '');
		\MetaTag::set('description', !empty($page->meta) ? $page->meta->description : '');
		\MetaTag::set('keywords', !empty($page->meta) ? $page->meta->keywords : '');
	   
		ob_start();
		print eval('?>' . $page->body);
		$body = ob_get_contents();
		ob_end_clean();
		
		return view('page::site.page-show', compact('page', 'body'));
        }
   
       /**
	* Show the form for editing the specified resource.
	*
	* @param  int  $id
	* @return Response
	*/
       public function edit($id)
       {
	   $page = \Page::findOrFail($id);
		
	   return view('page::admin.edit', compact('page'));
       }
   
       /**
	* Update the specified resource in storage.
	*
	* @param  int  $id
	* @return Response
	*/
       public function update(\Modules\Page\Http\Requests\CreatePageReq $request, $id)
       {       
	   $page = \Page::findOrFail($id);
	   $page->update($request->all());
	   \Meta::updateMeta($request->meta);
		
	   return redirect()->route('pages.slug', $page->slug)->with('success', 'Страница обновлена.');
       }
   
       /**
	* Remove the specified resource from storage.
	*
	* @param  int  $id
	* @return Response
	*/
       public function destroy($id)
       {
	   $page = \Page::findOrFail($id);
	   \Meta::deleteMeta('pages', $id);
	   $page->delete();
       
	   return redirect()->route('admin.pages.index')->with('success', 'Страница удалена.');
       }
	
}