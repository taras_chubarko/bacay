<?php
Route::group(['middleware' => ['web', 'pages']], function () {
	
	Route::get('{slug}', [
		'as' 	=> 'pages.slug',
		'uses' 	=> 'Modules\Page\Http\Controllers\PageController@show'
	])->where('slug', Page::allSlug());
	
});
/*
 * Админка
 */


Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\Page\Http\Controllers'], function()
{
	Route::resource('pages', 'PageController');
});