@extends('admin::layouts.master')

@section('content')

<section class="content-header">
	<h1>{{ MetaTag::set('title', 'Страницы') }}</h1>
	<ol class="breadcrumb">
		<li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
		<li class="active">Страницы</li>
	</ol>
</section>
  
<section class="content">

	<div class="box">
		<div class="box-header with-border">
			@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('pages.create'))
				<a class="btn btn-success btn-flat" href="{{ route('admin.pages.create') }}">Создать</a>
			@endif
		</div>
		
		<div class="box-body">
			@if($pages->count())
			<table class="table table-bordered b-c-222D32">
				<thead>
					<tr>
						<th width="10">#</th>
						<th>Название</th>
						<th width="150">Дата</th>
						<th width="150">Статус</th>
						<th width="100">Действие</th>
					</tr>
				</thead>
				<tbody>
					@foreach($pages as $item)
					<tr>
						<td>{{ $item->id }}</td>
						<td><a href="{{ route('pages.slug', $item->slug) }}">{{ $item->name }}</a></td>
						<td>{{ $item->date }}</td>
						<td>{{ $item->statuss }}</td>
						<td>
							@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('pages.edit'))
								<a href="{{ route('admin.pages.edit', $item->id) }}" class="btn btn-sm bg-navy btn-flat"><i class="fa fa-fw fa-edit"></i></a>
							@endif
							
							@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('pages.delete'))
								@include('cms::admin.delete', ['route' => 'admin.pages.destroy', 'id' => $item->id])
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{!! $pages->render() !!}
			@else
				<p>Нет страниц.</p>
			@endif
		  
		</div><!-- /.box-body -->
		
		<div class="box-footer">
		 
		</div><!-- /.box-footer-->
	</div>

</section>

@stop