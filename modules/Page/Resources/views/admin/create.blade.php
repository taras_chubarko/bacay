@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Создать страницу') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Создать страницу</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            
        </div>
        
        <div class="box-body">
            {!! Form::open(array('route' => 'admin.pages.store', 'role' => 'form', 'class' => 'form')) !!}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name">Название *</label>
                    {!! Form::text('name', null, array('class' => 'form-control')) !!}
                </div>
                    
                <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                    <label for="body">Содержимое *</label>
                    {!! Form::textarea('body', null, array('class' => 'form-control wredaktor', 'rows' => 10)) !!}
                </div>
                    
                @include('meta::admin.form-meta', ['meta' => null, 'entity' => 'pages', 'entity_id' => null])
                    
                <div class="form-group">
                    {!! Form::hidden('status', 0) !!}
                    <label for="status" class="icheck">
                        {!! Form::checkbox('status', 1, 1) !!}
                        Опубликовать
                    </label>
                </div>
                    
                <div class="form-group">
                   {!! Form::submit('Сохранить', array('class' => 'btn btn-primary btn-flat')) !!}
                </div>
            {!! Form::close() !!}
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>

@stop