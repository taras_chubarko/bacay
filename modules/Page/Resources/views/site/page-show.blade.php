@extends('site::layouts.master')

@section('content')
<div class="rule-page">
	<h1>{{ $page->name }}</h1>
	@include('cms::admin.edit', ['route' => 'admin.pages.edit', 'param' => $page->id])
        <div class="rule-wrapper">
		<div class="scrollbar">
			<div class="rule-block">
				{!! $body !!}
				@if(Request::path() == 'kontakty')
					@include('cms::site.form-fedback')
				@endif
			</div>
		</div>
        </div>
</div>
@stop