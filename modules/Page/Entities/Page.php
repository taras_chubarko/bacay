<?php namespace Modules\Page\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Page extends Model implements SluggableInterface {

    use SluggableTrait;
    
    protected $table = 'pages';

    protected $fillable = [];
    
    protected $guarded = ['_token', 'meta'];
    
    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
        'on_update'  => false,
    ];
    
    /* public function allSlug
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public static function allSlug() {
        
        $slugs = Page::all();
        
        $items = [];
        
        
        if($slugs->count())
        {
            foreach($slugs as $slug)
            {
                $items[] = $slug->slug;
            }
            
            $route = implode('|', $items);
            
        }
        else
        {
            $route = '[a-z]+';
        }
        
        return $route;
        
    }
    
    /* public function meta
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function meta() {
        return $this->hasOne('Meta', 'entity_id', 'id')->where('entity', 'pages');
    }
    
    /* public function getStatussAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function getStatussAttribute() {
        
        switch($this->status)
        {
            case 0:
                return 'Не опубликовано';
            break;
        
            case 1:
                return 'Опубликовано';
            break;
        }
        
    }
    
    /* public function getDateAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    
    public function getDateAttribute() {
        return date('d.m.Y', strtotime($this->created_at));
    }
    

}