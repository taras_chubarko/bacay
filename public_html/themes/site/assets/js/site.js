$(document).ready(function() {
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        },
	beforeSend:function(xhr){
            $('body').append('<div id="ajax-load"></div>');
        },
        complete:function(){
            $('#ajax-load').remove();
        }
    });
    //$('.ct').load('http://bacay.progim.net/adverts/load/nedvizhimost');
    
    $('body').find('#admin-menu').parent().css('margin-top', 30);
    
    //$('.selectpicker, .files, .geocity').styler('destroy');
    $('.styler').styler();
    
    $('.selectpicker').selectpicker();
    
    $('[data-toggle="tooltip"]').tooltip();
    
    
    //
    $('input.phone').mask('+7(999) 999-99-99');
    
    $('.icheck-flat-blue input').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue',
    });
    
    $('.info').responsiveEqualHeightGrid();
    
//    $('.form').on('submit', function(){
//	//return false;
//    });
    
    if (typeof  mat != 'undefined') {
	$('textarea, input[type="text"]').keyup(function(){  
	    var a = $(this).val().toLowerCase();
	    var b = mat.split(',');
	    for(i=0; i<b.length; ++i)
	    {
		if(a.search(b[i])!= -1)
		{
		    $(this).val(a.replace(b[i],'***')); 
		    alert('Внимание! Вы пытаетесь использовать запрещеные слова.'); 
		    return false;
		}
	    }
	});
    }
    
    function selectCategory() {
	var sc = $('#select-category').val();
	
	if (sc == 16 || sc == 17) {
	    $('#form-16-17').removeClass('hidden');
	    $('#form-16-17').find('input').attr('disabled', false);
	    $('#form-16-17').find('select').attr('disabled', false);
	}
	else
	{
	    $('#form-16-17').addClass('hidden');
	    $('#form-16-17').find('input').attr('disabled', true);
	    $('#form-16-17').find('select').attr('disabled', true);
	}
	//-------------------------------------------
	if (sc == 18) {
	    $('#form-18').removeClass('hidden');
	    $('#form-18').find('input').attr('disabled', false);
	    $('#form-18').find('select').attr('disabled', false);
	}
	else
	{
	    $('#form-18').addClass('hidden');
	    $('#form-18').find('input').attr('disabled', true);
	    $('#form-18').find('select').attr('disabled', true);
	}
	//-------------------------------------------
	if (sc == 19) {
	    $('#form-19').removeClass('hidden');
	    $('#form-19').find('input').attr('disabled', false);
	    $('#form-19').find('select').attr('disabled', false);
	}
	else
	{
	    $('#form-19').addClass('hidden');
	    $('#form-19').find('input').attr('disabled', true);
	    $('#form-19').find('select').attr('disabled', true);
	}
	//-------------------------------------------
	if (sc == 20) {
	    $('#form-20').removeClass('hidden');
	    $('#form-20').find('input').attr('disabled', false);
	    $('#form-20').find('select').attr('disabled', false);
	}
	else
	{
	    $('#form-20').addClass('hidden');
	    $('#form-20').find('input').attr('disabled', true);
	    $('#form-20').find('select').attr('disabled', true);
	}
	//-------------------------------------------
	if (sc == 21 || sc == 22 || sc == 23) {
	    $('#form-21-22-23').removeClass('hidden');
	    $('#form-21-22-23').find('input').attr('disabled', false);
	    $('#form-21-22-23').find('select').attr('disabled', false);
	}
	else
	{
	    $('#form-21-22-23').addClass('hidden');
	    $('#form-21-22-23').find('input').attr('disabled', true);
	    $('#form-21-22-23').find('select').attr('disabled', true);
	}
	//-------------------------------------------
	if (sc == 24) {
	    $('#form-24').removeClass('hidden');
	    $('#form-24').find('input').attr('disabled', false);
	    $('#form-24').find('select').attr('disabled', false);
	}
	else
	{
	    $('#form-24').addClass('hidden');
	    $('#form-24').find('input').attr('disabled', true);
	    $('#form-24').find('select').attr('disabled', true);
	}
	//-------------------------------------------
	if (sc == 98) {
	    $('#form-98').removeClass('hidden');
	    $('#form-98').find('input').attr('disabled', false);
	    $('#form-98').find('select').attr('disabled', false);
	}
	else
	{
	    $('#form-98').addClass('hidden');
	    $('#form-98').find('input').attr('disabled', true);
	    $('#form-98').find('select').attr('disabled', true);
	}
	//-------------------------------------------
	if (sc == 99) {
	    $('#form-98').removeClass('hidden');
	    $('#form-98').find('input').attr('disabled', false);
	    $('#form-98').find('select').attr('disabled', false);
	    $('#form-99').removeClass('hidden');
	    $('#form-99').find('input').attr('disabled', false);
	    $('#form-99').find('select').attr('disabled', false);
	}
	else
	{
	    //$('#form-98').addClass('hidden');
	    //$('#form-98').find('input').attr('disabled', true);
	    //$('#form-98').find('select').attr('disabled', true);
	    $('#form-99').addClass('hidden');
	    $('#form-99').find('input').attr('disabled', true);
	    $('#form-99').find('select').attr('disabled', true);
	}
	//-------------------------------------------
	if (sc == 376) {
	    $('#form-376').removeClass('hidden');
	    $('#form-376').find('input').attr('disabled', false);
	    $('#form-376').find('select').attr('disabled', false);
	}
	else
	{
	    $('#form-376').addClass('hidden');
	    $('#form-376').find('input').attr('disabled', true);
	    $('#form-376').find('select').attr('disabled', true);
	}
	
    } selectCategory();
    
    $('#select-category').on('change', function(){
	selectCategory();
    });
   

    uploadImages('nedvizhimost');
    uploadImages('transport');
    uploadImages('uslugi');
    uploadImages('biznes');
    uploadImages('elektronika');
    uploadImages('dom');
    uploadImages('zhivotnye');
    uploadImages('odezhda');
    uploadImages('krasota');
    uploadImages('detskiy');
    uploadImages('sport-otdykh-khobbi');
    uploadImages('otdam-darom');
    uploadImages('obmen-i-barakholka');
    uploadImages('drugoe');
    uploadImages('rabota-i-obrazovanie');
    
    //Filter.init();
    /*------------------------
     * 06.06.2016
     * Фильтрация аякс (по колхозному нормально чегото глючит)
     *------------------------
     */
    function filter() {
	/*
	 * Выбор категории
	 */
	$('.form-filter input.category').change(function() {
	    filter_submit();
	    if ($('input.category:checked').length > 0) {
		$('.typadverts').removeClass('hidden');
		//$('.more').removeClass('hidden');
	    }
	    else
	    {
		$('.typadverts').addClass('hidden');
		$('.more').addClass('hidden');
	    }
	    filter_clear_subfilter();
	});
	/*
	 * Выбор типа объявления
	 */
	$('.typadvert a').on('click', function(){
	    var typadvert = $(this).data('typadvert');
	    $('.typadvert').find('input').val(typadvert);
	    var cat = $('input.category:checked').val();
	    filter_nedvizhimost(cat);
	    filter_submit();
	});
	/*
	 * Выбор селестов
	 */
	function filter_select() {
	    $('.form-filter select').change(function() {
		filter_submit();
	    });
	} filter_select();
	/*
	 * Выбор цены
	 */
	function filter_price() {
	    $('.filter-price .dropdown-menu').click(function(ev) {
		return false;
	    });
	    $('.price-apply').click(function(ev) {
		ev.preventDefault()
		$('.filter-price').removeClass('open');
		filter_submit();
	    });
	} filter_price();
	/*
	 * Выбор площади
	 */
	function filter_area() {
	    $('.filter-area .dropdown-menu').click(function(ev) {
		return false;
	    });
	    $('.area-apply').click(function(ev) {
		ev.preventDefault()
		$('.filter-area').removeClass('open');
		filter_submit();
	    });
	} filter_area();
	/*
	 * Фильтр в недвижимости
	 */
	function filter_nedvizhimost(cat) {
	    if (cat == 16 || cat == 17) {
		$('#filter-16-17').removeClass('hidden');
		$('#filter-16-17').find('input').attr('disabled', false);
		$('#filter-16-17').find('select').attr('disabled', false);
	    }
	    else
	    {
		$('#filter-16-17').addClass('hidden');
		$('#filter-16-17').find('input').attr('disabled', true);
		$('#filter-16-17').find('select').attr('disabled', true);
	    }
	    //----------------------------------------------
	    if (cat == 18) {
		$('#filter-18').removeClass('hidden');
		$('#filter-18').find('input').attr('disabled', false);
		$('#filter-18').find('select').attr('disabled', false);
	    }
	    else
	    {
		$('#filter-18').addClass('hidden');
		$('#filter-18').find('input').attr('disabled', true);
		$('#filter-18').find('select').attr('disabled', true);
	    }
	    //----------------------------------------------
	    if (cat == 19) {
		$('#filter-19').removeClass('hidden');
		$('#filter-19').find('input').attr('disabled', false);
		$('#filter-19').find('select').attr('disabled', false);
	    }
	    else
	    {
		$('#filter-19').addClass('hidden');
		$('#filter-19').find('input').attr('disabled', true);
		$('#filter-19').find('select').attr('disabled', true);
	    }
	    //----------------------------------------------
	    if (cat == 20) {
		$('#filter-20').removeClass('hidden');
		$('#filter-20').find('input').attr('disabled', false);
		$('#filter-20').find('select').attr('disabled', false);
	    }
	    else
	    {
		$('#filter-20').addClass('hidden');
		$('#filter-20').find('input').attr('disabled', true);
		$('#filter-20').find('select').attr('disabled', true);
	    }
	    //----------------------------------------------
	    if (cat == 21 || cat == 22 || cat == 23) {
		$('#filter-21-22-23').removeClass('hidden');
		$('#filter-21-22-32').find('input').attr('disabled', false);
		$('#filter-21-22-23').find('select').attr('disabled', false);
	    }
	    else
	    {
		$('#filter-21-22-23').addClass('hidden');
		$('#filter-21-22-32').find('input').attr('disabled', true);
		$('#filter-21-22-23').find('select').attr('disabled', true);
	    }
	    //----------------------------------------------
	    if (cat == 24) {
		$('#filter-24').removeClass('hidden');
		$('#filter-24').find('input').attr('disabled', false);
		$('#filter-24').find('select').attr('disabled', false);
	    }
	    else
	    {
		$('#filter-24').addClass('hidden');
		$('#filter-24').find('input').attr('disabled', true);
		$('#filter-24').find('select').attr('disabled', true);
	    }
	}
	/*
	 * Фильтр в транспорте
	 */
	$('a.any-param').on('click', function(){
	    $('div.any-param').slideToggle(500);
	});
	$('div.any-param input').change(function() {
	    filter_submit();
	});
	/*
	 * Транспорт марка модель
	 */
	function filter_transport() {
	    $('.filter-type input').change(function() {
		$('.more').removeClass('hidden');
		filter_submit();
	    });
	}
	function filter_brand() {
	    $('#brand-filter').change(function() {
		filter_submit();
	    });
	}
	function filter_model() {
	    $('#model-filter').change(function() {
		filter_submit();
	    });
	}
	/*
	 * Другие фильтры
	 */
	function filter_any() {
	    $('.selectpicker').selectpicker();
	    filter_select();
	    filter_price();
	    filter_area();
	}
	/*
	 * Очистка подфильтра
	 */
	function filter_clear_subfilter() {
	    $('.subfilter').addClass('hidden');
	    $('.subfilter').find('input').val('');
	    $('.subfilter').find('.selectpicker').selectpicker('deselectAll');
	}
	/*
	 * Отправка формы
	 */
	function filter_submit() {
	    var data = $('.form-filter').serializeArray();
	    $.ajax({
		url: window.location.href,
		type: 'GET',
		data: data,
		beforeSend:function(data){
		    $('.catalog-wrap').css('opacity', 0.2);
		    $('.srch').remove();
		    $('.form-filter').append('<p class="srch">Ищем... <i class="fa fa-spinner fa-pulse"></i></p>');
		},
		success: function(data){
		    $('.catalog-wrap').css('opacity', 1);
		    $('.srch').html('Нашли '+data.total+' объявлений');
		    $('.catalog-wrap').html(data.result);
		    $('.filter-type').html(data.filterData.type);
		    $('.styler').styler();
		    $('.brand-filter').html(data.filterData.brand);
		    $('.model-filter').html(data.filterData.model);
		    $('.selectpicker').selectpicker('refresh');
		    filter_transport();
		    filter_brand();
		    filter_model();
		    if(data.filterData.append)
		    {
			$('.append').html(data.filterData.append);
			filter_any();
		    }
		},
		error: function(data){
		    
		}
	    });
	}
	
    } filter();
    
    
    
});


function ulogins(arg) {
    $.ajax({
        url: '/user/ulogin',
        type: 'POST',
        data: {arg:arg},
        beforeSend:function(data){
             
        },
        success: function(data){
            window.location.href='/user';
        },
        error: function(data){
            $('.wrapper-registration-form').prepend('<div class="alert alert-danger alert-dismissable">'
                +'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
                +data.responseJSON.msg
            +' </div>');
        }
    });
}

function uploadImages(arg) {
    var self = $('#input-image-'+arg);
    var uri = self.data('uri');
    
    self.fileinput({
        allowedFileExtensions : ['png', 'jpg', 'jpeg'],
        uploadUrl: '/filem/upload/images/advert',
        uploadAsync: true,
        showUpload: false,
        dropZoneEnabled: false,
        showPreview: false,
        language: 'ru',
        showCaption: false,
        showRemove: false,
        showCancel: false, 
        browseClass: 'btn btn-link',
        browseLabel: '',
        browseIcon: '<i class=\"fa fa-camera\"></i> ',
        uploadExtraData:{uri:uri},
        layoutTemplates:{
            progress: '',
        },
        slugCallback: function (filename){
            return filename;
        }
    }).on('filebatchselected', function(event, files) {
	$('.btn-file i').removeClass('fa-camera').addClass('fa-spinner fa-spin');
        self.fileinput("upload"); 
    }).on('filebatchuploadsuccess', function(event, data, previewId, index) {
        var wrap = $('#image-widget ul');
        wrap.append(data.response.result);
        wrap.find('li').each(function(k, v){
            $(this).find('.sort').val(k);
            $(this).find('.fid').attr('name', 'image['+k+'][fid]');
            $(this).find('.sort').attr('name', 'image['+k+'][sort]');
        });
        $('.btn-file i').removeClass('fa-spinner fa-spin').addClass('fa-camera');
        deleteImages();
        //sortImage();
    });
}

function deleteImages() {
    $('.delim').on('click', function(){
         
        var image_id = $(this).data('image-id');
         
        $(this).parent().remove();
         
        $.post('/filem/delete/images', {image_id:image_id});
    });
    
} deleteImages();

function geoCity() {
    $('.geocity')
    .selectpicker({
        liveSearch: true
    })
    .ajaxSelectPicker({
        ajax: {
            url: '/geo/city',
        },
        locale: {
            emptyTitle: 'Поиск города...'
        },
        preprocessData: function(data){
            var city = [];
            var len = data.length;
            for(var i = 0; i < len; i++){
                var curr = data[i];
                city.push({
                    value: curr.city_id,
                    text: curr.city,
                    data: {
                        subtext: curr.region
                    },
                });
            }
            return city;
        },
        preserveSelected: false
    });
    
    $('.geocity').on('change', function(){
	var val = $(this).val();
	if (val == 4400 || val == 4962) {
	    getMetro(val);
	}
	else
	{
	    $('.geometro').attr('disabled', true).selectpicker('deselectAll').selectpicker('refresh');
	}
	
    });
    
} geoCity();
//setSessionCity();
/*
 * function getMetro
 * @param $arg
 */

function getMetro(city_id)
{
    $.ajax({
	url: '/geo/get/metro',
	type: 'POST',
	data: {city_id:city_id},
	beforeSend:function(data){
	    $('.metro .selectpicker').selectpicker('deselectAll');
	    $('.metro').find('.filter-option').html('Загрузка... <i class="fa fa-spinner fa-pulse"></i>');
	},
	success: function(data){
	    $('.metro').html(data.result); 
	    $('.selectpicker').selectpicker('refresh');
	},
	error: function(data){
	   
	}
    });
}


//var key = 0;
//function process(key) {
//    
//    $.get('/cms/test/'+key, {}, function(data){
//        key += 1;
//        if (key != data.count) {
//          process(key);
//        }
//    });
//}
//process(key);

/*
 * function getBrands
 * @param $arg
 */
function getType()
{
    $('select.category').on('change', function(){
	var category = $(this).val();
	$.ajax({
	    url: '/transport/get/type',
	    type: 'POST',
	    data: {category:category},
	    beforeSend:function(data){
		$('#brand .selectpicker').selectpicker('deselectAll');
		$('#model .selectpicker').selectpicker('deselectAll');
		$('#type').find('.filter-option').html('Загрузка... <i class="fa fa-spinner fa-pulse"></i>');
	    },
	    success: function(data){
		$('#type').html(data.result);
		$('.selectpicker').selectpicker('refresh');
		getBrands();
	    },
	    error: function(data){
	       
	    }
	});
    });
} getType();

/*
 * function getBrands
 * @param $arg
 */
function getBrands()
{
    $('select.type').on('change', function(){
	var type = $(this).val();
	$.ajax({
	    url: '/transport/get/brands',
	    type: 'POST',
	    data: {type:type},
	    beforeSend:function(data){
		$('#model .selectpicker').selectpicker('deselectAll');
		$('#brand').find('.filter-option').html('Загрузка... <i class="fa fa-spinner fa-pulse"></i>');
	    },
	    success: function(data){
		$('#brand').html(data.result);
		$('.selectpicker').selectpicker('refresh');
		getModels();
	    },
	    error: function(data){
	       
	    }
	});
    });
} getBrands();

/*
 * function getBrands
 * @param $arg
 */
function getModels()
{
    $('select[name=brand]').on('change', function(){
	var brand = $(this).val();
	$.ajax({
	    url: '/transport/get/models',
	    type: 'POST',
	    data: {brand:brand},
	    beforeSend:function(data){
		$('#model').find('.filter-option').html('Загрузка... <i class="fa fa-spinner fa-pulse"></i>');
	    },
	    success: function(data){
		$('#model').html(data.result);
		$('.selectpicker').selectpicker('refresh');
	    },
	    error: function(data){
	       
	    }
	});
    });
} getModels();

/*
 * function getResume
 * @param $arg
 */
function getResume()
{
    
    $('select.catrobota').on('change', function(){
	var category = $(this).val();
	
	if (category == 99)
	{
	    $('.resume').css('display', 'block');
	    $('select[name=education], select[name=gender]').selectpicker('deselectAll');
	    $('input[name=age]').val('');
	}
	else
	{
	    $('.resume').css('display', 'none');
	}
    });
    
    $('select.catrobota option:selected').each(function() {
	var category = $(this).val();
	if (category == 99)
	{
	    $('.resume').css('display', 'block');
	}
	else
	{
	    $('.resume').css('display', 'none');
	}
    }); 
} //getResume();

/*
 * function slidekit
 * @param $arg
 */
function sliderkits()
{
    $(".photosgallery-vertical").sliderkit({
	circular:true,
	mousewheel:true,
//	shownavitems:6,
	verticalnav:true,
	navclipcenter:true,
	auto:false
    });
} sliderkits();

/*
 * function name
 * @param $arg
 */
function typeaheadCity()
{
    $('.typeahead-city').typeahead({
	display: 'cityD',
	val: 'city',
	itemSelected: function(item, val, text) {
	    $('.typeahead-city').val(val);
	},
	ajax: {
	    url: '/geo/city',
	    preDispatch: function (query) {
		return {q: query}
	    },
	    preProcess: function (data) {
		var mylist = [];
		$.each(data, function(k, v){
		    mylist.push({city_id:v.city_id, cityD: v.city+', '+v.region, city:v.city });
		});
		return mylist;
	    }
	},
    });
} typeaheadCity();

/*
 * function avatarUpload
 * @param $arg
 */
function avatarUpload()
{
    var self = $('#input-image-avatar');
    var uri = self.data('uri');
    
    self.fileinput({
        allowedFileExtensions : ['png', 'jpg', 'jpeg'],
        uploadUrl: '/filem/upload/images/avatar',
        uploadAsync: true,
        showUpload: false,
        dropZoneEnabled: false,
        showPreview: false,
        language: 'ru',
        showCaption: false,
        showRemove: false,
        showCancel: false, 
        browseClass: 'btn btn-link',
        browseLabel: '',
        browseIcon: '<i class=\"fa fa-camera\"></i> ',
        uploadExtraData:{uri:uri},
        layoutTemplates:{
            progress: '',
        },
        slugCallback: function (filename){
            return filename;
        }
    }).on('filebatchselected', function(event, files) {
	$('.btn-file i').removeClass('fa-camera').addClass('fa-spinner fa-spin');
        self.fileinput("upload"); 
    }).on('filebatchuploadsuccess', function(event, data, previewId, index) {
        $('.btn-file i').removeClass('fa-spinner fa-spin').addClass('fa-camera');
	$('.user-img').html(data.response.result);
	$('input[name="photo_big"]').val(data.response.item);
    });
} avatarUpload();

/*
 * function getPhone
 * @param $arg
 */
function getPhone()
{
    $('.btn-ph').on('click', function(){
	var self = $(this);
	var advert = self.data('advert');
	$.ajax({
	    url: '/adverts/get/phone',
	    type: 'POST',
	    data: {advert:advert},
	    beforeSend:function(data){
		self.text('Ищем телефон...');
	    },
	    success: function(data){
		self.text(data.result);
	    },
	    error: function(data){
	       
	    }
	});
    });
} getPhone();

/*
 * function sendMessage
 * @param $arg
 */
function sendMessage()
{
    $('.btn-mess').on('click', function(){
	var self = $(this);
	var advert = self.data('advert');
	$.ajax({
	    url: '/adverts/sendmessage',
	    type: 'GET',
	    data: {advert:advert},
	    beforeSend:function(data){
		$('#sendMessage').remove();
		self.html('Написать сообщение <i class="fa fa-spinner fa-pulse"></i>');
	    },
	    success: function(data){
		self.html('Написать сообщение');
		$('body').append(data.result);
		$('#sendMessage').modal('show');
		send();
	    },
	    error: function(data){
	       
	    }
	});
    });
    
    function send() {
	$('#sendMessage form').on('submit', function(){
	    var self = $(this);
	    var action = self.attr('action');
	    var data = self.serializeArray();
	    $.ajax({
		url: action,
		type: 'POST',
		data: data,
		beforeSend:function(data){
		    self.find('.status').html('<i class="fa fa-spinner fa-pulse"></i>');
		},
		success: function(data){
		    self.find('.status').html('<i class="fa fa-check text-success"></i>');
		    self.find('.form-group').removeClass('has-error');
		    self.find('p').remove();
		    setTimeout(function(){
			$('#sendMessage').modal('hide');
		    }, 1000);
		},
		error: function(data){
		    self.find('.modal-body').prepend('<p class="text-danger">Все поля обязательны для заполнения.</p>')
		    $.each(data.responseJSON, function(k,v){
			$('.'+k).addClass('has-error');
			self.find('.status').html('<i class="fa fa-exclamation-triangle text-danger"></i>');
		    });
		}
	    });
	    return false;    
	});
    }
    
} sendMessage();

/*
 * function complain
 * @param $arg
 */
function complain()
{
    $('.complain').on('click', function(){
	var self = $(this);
	var advert = self.data('advert');
	$.ajax({
	    url: '/adverts/complain',
	    type: 'GET',
	    data: {advert:advert},
	    beforeSend:function(data){
		$('#sendComplain').remove();
		self.html('Пожаловаться <i class="fa fa-spinner fa-pulse pa m-t-5"></i>');
	    },
	    success: function(data){
		self.html('Пожаловаться');
		$('body').append(data.result);
		$('#sendComplain').modal('show');
		$('.selectpicker').selectpicker('refresh');
		send();
	    },
	    error: function(data){
	       
	    }
	});
    });
    
    function send() {
	$('#sendComplain form').on('submit', function(){
	    var self = $(this);
	    var action = self.attr('action');
	    var data = self.serializeArray();
	    $.ajax({
		url: action,
		type: 'POST',
		data: data,
		beforeSend:function(data){
		    self.find('.status').html('<i class="fa fa-spinner fa-pulse"></i>');
		},
		success: function(data){
		    self.find('.status').html('<i class="fa fa-check text-success"></i>');
		    self.find('.form-group').removeClass('has-error');
		    self.find('p').remove();
		    setTimeout(function(){
			$('#sendComplain').modal('hide');
		    }, 1000);
		},
		error: function(data){
		    self.find('.modal-body').prepend('<p class="text-danger">Все поля обязательны для заполнения.</p>')
		    $.each(data.responseJSON, function(k,v){
			$('.'+k).addClass('has-error');
			self.find('.status').html('<i class="fa fa-exclamation-triangle text-danger"></i>');
		    });
		}
	    });
	    return false;    
	});
    }
    
} complain();

/*
 * function social
 * @param $arg
 */
function social()
{
    $('.ic-l-p2').on('click', function(){
	$('#popover-social').slideToggle();
    });
} social();

/*
 * function setSessionCity
 * @param $arg
 */
function setSessionCity()
{
    $('.head-choise-city select').on('change', function(){
	//var self = $(this);
	//var city = self.val();
	var city = $(this).find("option:selected").val();
	
	if (city.length > 0) {
	    $.ajax({
	        url: '/cms/set/session/city',
	        type: 'POST',
	        data: {city:city},
	        beforeSend:function(data){
	    	$('.head-choise-city .filter-option').append('<i class="fa fa-spinner fa-pulse pa m-t-10"></i>');
	        },
	        success: function(data){
	    	window.location.reload();
	        },
	        error: function(data){
	           
	        }
	    });
	}
    });
} setSessionCity();

/*
 * function poisk
 * @param $arg
 */

function poisk()
{
    $('.form-poisk').on('submit', function(){
	var data = $(this).serializeArray();
	
	$.ajax({
	    url: $(this).attr('action'),
	    type: 'POST',
	    data: data,
	    beforeSend:function(data){
		
	    },
	    success: function(data){
		window.location.href = data.link;
	    },
	    error: function(data){
	       //console.log(data);
	       var tpl = '<div class="alert alert-danger m-t-15 alert-dismissible" role="alert">'
			    +'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
			    +'</div>';
		$('.form-poisk').append(tpl);
		$.each(data.responseJSON, function(k,v){
		    $('.alert-danger').append('<p>'+v[0]+'</p>');
		});
	    }
	});
	
	return false;
    });
}  poisk();

/*
 * function favoritesAdd
 * @param $arg
 */
function favoritesAdd()
{
    $('.fav-add').on('click', function(){
	var self = $(this);
	var id = self.data('id');
	
	$.ajax({
	    url: '/favorites/add',
	    type: 'POST',
	    data: {id:id},
	    beforeSend:function(data){
		
	    },
	    success: function(data){
		self.attr('title', 'Добавлено в избранное.');
		self.find('.fa').removeClass('fa-star-o').addClass('fa-star').css('color', '#C80730');
		self.tooltip('show');
	    },
	    error: function(data){
		
	    }
	});
    });
} favoritesAdd();

/*
 * function deleteFavorites
 * @param $arg
 */
function deleteFavorites()
{
    var form = $('.form-favorites');
    var chk = $('.form-favorites .del:checked').length;
    (chk > 0) ? form.find('.btn').attr('disabled', false) : form.find('.btn').attr('disabled', true);

    $('.form-favorites .del').on('ifChecked', function(event){
	var chk = $('.form-favorites .del:checked').length;
	(chk > 0) ? form.find('.btn').attr('disabled', false) : form.find('.btn').attr('disabled', true);
    }).on('ifUnchecked', function(event){
	var chk = $('.form-favorites .del:checked').length;
	(chk > 0) ? form.find('.btn').attr('disabled', false) : form.find('.btn').attr('disabled', true);
    });
    
    $('.form-favorites .checkall').on('ifChecked', function(event){
	$('.form-favorites .del').iCheck('check');
    }).on('ifUnchecked', function(event){
	$('.form-favorites .del').iCheck('uncheck');
    });
    
} deleteFavorites();

/*
 * function ajax-services
 * @param $arg
 */
function ajaxServices()
{
    $('.ajax-services').on('click', function(){
	var self = $(this);
	var link = self.attr('href');
	$.ajax({
	    url: link,
	    type: 'GET',
	    beforeSend:function(data){
		$('#modal-services').remove();
		self.append('<i class="fa fa-spinner fa-pulse pa m-t-5"></i>');
	    },
	    success: function(data){
		$('body').append(data.result);
		$('.btn-group-100').removeClass('open');
		$('#modal-services').modal('show');
		$('.fa-spinner').remove();
		aclick();
		ajaxBay();
	    },
	    error: function(data){
		
	    }
	});
    });
} ajaxServices();

/*
 * function ajaxBay
 * @param $arg
 */
function ajaxBay()
{
    $('.ajax-bay').on('click', function(){
	var self = $(this);
	var link = self.attr('href');
	$.ajax({
	    url: link,
	    type: 'POST',
	    beforeSend:function(data){
		self.html('<i class="fa fa-spinner fa-pulse"></i>');
	    },
	    success: function(data){
		$('#modal-services').modal('hide');
	    },
	    error: function(data){
		
	    }
	});
    });
} ajaxBay();
/*
 *
 */
function aclick() {
    $('a.ajax').on('click', function(){
	return false;	
    });
} aclick();

/*
 * function ajaxAdvertAdd
 * @param $arg
 */
function ajaxAdvertAdd()
{
    $('.ajax-advert-add').on('click', function(){
	var self = $(this);
	var link = self.attr('href');
	$.ajax({
	    url: link,
	    type: 'GET',
	    beforeSend:function(data){
		self.html('Загрузка... <i class="fa fa-spinner fa-pulse"></i>');
	    },
	    success: function(data){
		self.html('Добавить объявление');
		$('body').append(data.result);
		$('#modal-advert-add').modal('show');
	    },
	    error: function(data){
		self.html('Добавить объявление');
		errorMsg('У вас нет прав для создания объявлений.');
		//console.log(data);
	    }
	});
    });
} ajaxAdvertAdd();

/*
 * function checkMail
 * @param $arg
 */
function checkMail()
{
    $('.checkMail').mouseleave(function() {
	var self = $(this);
	$.ajax({
	    url: '/adverts/check/mail',
	    type: 'POST',
	    data: {email:self.val()},
	    beforeSend:function(data){
		
	    },
	    success: function(data){
		
	    },
	    error: function(data){
		alert(data.responseJSON[0]);
	    }
	});
    });
} checkMail();

/*
 * function ajaxForm
 * @param $arg
 */
function ajaxForm()
{
    $('.ajax-form').on('click', function(){
	var self 	= $(this);
	var method 	= self.data('method');
	var href 	= self.attr('href');
	var token 	= $('meta[name="_token"]').attr('content');
	
	var $form 		= $('<form/>', {action: href, method: 'post'});
        var $inputMethod 	= $('<input/>', {type: 'hidden', name: '_method', value: method});
        var $inputToken 	= $('<input/>', {type: 'hidden', name: '_token', value: token});
        $form.append($inputMethod, $inputToken).hide().appendTo('body').submit();
    });
    
} ajaxForm();

Filter = {
    init: function() {
	this.checkbox();
	this.select();
	this.price();
	this.area();
	this.anyparam();
	this.experience();
	this.age();
    },
    hash: function() {
	var data = $('.form-filter').serializeArray();
	
	Hash.clear();
	$.each(data, function(k,v){
	    switch (v.name) {
		case 'qtyroom[]':
		    Hash.add('qtyroom['+v.value+']', v.value);
		break;
		    
		case 'floor[]':
		    Hash.add('floor['+v.value+']', v.value);
		break;
		    
		case 'qtyfloor[]':
		    Hash.add('qtyfloor['+v.value+']', v.value);
		break;
		
		case 'year[]':
		    Hash.add('year['+v.value+']', v.value);
		break;
		    
		case 'service[]':
		    Hash.add('service['+v.value+']', v.value);
		break;
		    
		case 'schedule[]':
		    Hash.add('schedule['+v.value+']', v.value);
		break;
		    
		default: Hash.add(v.name, v.value);
	    }
	});
	Filter.submit();
    },
    checkbox: function() {
	$('input.category').change(function() {
	    if ($('input.category:checked').length > 0) {
		$('.typadverts').removeClass('hidden');
	    }
	    else
	    {
		$('.typadverts').addClass('hidden');
		$('.more').addClass('hidden');
	    }
	    Filter.hash();
	});
	
	$('.typadvert a').on('click', function(){
	    var typadvert = $(this).data('typadvert');
	    $('.typadvert').find('input').val(typadvert);
	    Filter.hash();
	    
	    var cat = $('input.category:checked').val();
	    Filter.nedvizhimost(cat);
	    
	    var hash = Hash.get();
	    if(hash.typadvert) {
		$('.more').removeClass('hidden');
	    }
	    else
	    {
		$('.more').addClass('hidden');
	    }
	});
	
	$('div.any-param input').change(function() {
	    Filter.hash();
	});
	$('.chbx').change(function() {
	    Filter.hash();
	});
	
    },
    select: function(){
	$('.form-filter select').change(function() {
	    Filter.hash();
	});
    },
    type: function(){
	$('input.type').change(function() {
	    Filter.hash();
	    $('.more').removeClass('hidden');
	});
	
    },
    price: function(){
	$('.filter-price .dropdown-menu').click(function(ev) {
            return false;
	});
	
	$('.price-apply').click(function(ev) {
	    ev.preventDefault()
            Filter.hash();
	    $('.filter-price').removeClass('open');
	});
	
    },
    area: function(){
	if ($('#area').length > 0) {
	    $('#area').slider({}).on('slide', function(slideEvt) {
		var min = slideEvt.value[0];
		var max = slideEvt.value[1];
		$('input[name="area[min]"]').val(min);
		$('input[name="area[max]"]').val(max);
		$('.filter-area').find('.btn').html(min+'-'+max+' м² <span class="caret"></span>');
	    }).on('slideStop', function(){
		Filter.hash();
	    });
	}
    },
    experience: function(){
	$('.filter-experience .dropdown-menu').click(function(ev) {
            return false;
	});
	
	$('.experience-apply').click(function(ev) {
	    ev.preventDefault()
            Filter.hash();
	    $('.filter-experience').removeClass('open');
	});
    },
    age: function(){
	$('.filter-age .dropdown-menu').click(function(ev) {
            return false;
	});
	
	$('.age-apply').click(function(ev) {
	    ev.preventDefault()
            Filter.hash();
	    $('.filter-age').removeClass('open');
	});
    },
    bland: function(){
	$('#brand-filter').change(function() {
	    Filter.hash();
	});
    },
    model: function(){
	$('#model-filter').change(function() {
	    Filter.hash();
	});
    },
    anyparam: function(){
	$('a.any-param').on('click', function(){
	    $('div.any-param').slideToggle(500);
	});
    },
    submit: function()
    {
	$.ajax({
	    url: window.location.href,
	    type: 'GET',
	    beforeSend:function(data){
		$('.catalog-wrap').css('opacity', 0.2);
		$('.srch').remove();
		$('.form-filter').append('<p class="srch">Ищем... <i class="fa fa-spinner fa-pulse"></i></p>');
	    },
	    success: function(data){
		$('.catalog-wrap').css('opacity', 1);
		$('.srch').html('Нашли '+data.total+' объявлений');
		$('.catalog-wrap').html(data.result);
		$('.filter-type').html(data.filterData.type);
		$('.styler').styler();
		Filter.type();
		$('.brand-filter').html(data.filterData.brand);
		$('.model-filter').html(data.filterData.model);
		Filter.bland();
		Filter.model();
		if(data.filterData.append)
		{
		    $('.append').html(data.filterData.append);
		    Filter.select();
		    Filter.price();
		    Filter.experience();
		    Filter.age();
		}
		
		$('.selectpicker').selectpicker('refresh');
	    },
	    error: function(data){
		
	    }
	});
    },
    nedvizhimost: function(cat)
    {
	console.log(cat);
	if (cat == 16 || cat == 17) {
	    $('#filter-16-17').removeClass('hidden');
	}
	else
	{
	    $('#filter-16-17').addClass('hidden');
	}
    }
    
    
};
/*
 *
 */
Hash = {
	// Получаем данные из адреса
	get: function() {
		var vars = {}, hash, splitter, hashes;
		if (!this.oldbrowser()) {
			var pos = window.location.href.indexOf('?');
			hashes = (pos != -1) ? decodeURIComponent(window.location.href.substr(pos + 1)) : '';
			splitter = '&';
		}
		else {
			hashes = decodeURIComponent(window.location.hash.substr(1));
			splitter = '/';
		}

		if (hashes.length == 0) {return vars;}
		else {hashes = hashes.split(splitter);}

		for (var i in hashes) {
			if (hashes.hasOwnProperty(i)) {
				hash = hashes[i].split('=');
				if (typeof hash[1] == 'undefined') {
					vars['anchor'] = hash[0];
				}
				else {
					vars[hash[0]] = hash[1];
				}
			}
		}
		return vars;
	},
	// Заменяем данные в адресе на полученный массив
	set: function(vars) {
		var hash = '';
		for (var i in vars) {
			if (vars.hasOwnProperty(i)) {
				hash += '&' + i + '=' + vars[i];
			}
		}

		if (!this.oldbrowser()) {
			if (hash.length != 0) {
				hash = '?' + hash.substr(1);
			}
			window.history.pushState(hash, '', document.location.pathname + hash);
		}
		else {
			window.location.hash = hash.substr(1);
		}
	},
	// Добавляем одно значение в адрес
	add: function(key, val) {
		var hash = this.get();
		hash[key] = val;
		this.set(hash);
	},
	// Удаляем одно значение из адреса
	remove: function(key) {
		var hash = this.get();
		delete hash[key];
		this.set(hash);
	},
	// Очищаем все значения в адресе
	clear: function() {
		this.set({});
	},
	// Проверка на поддержку history api браузером
	oldbrowser: function() {
		return !(window.history && history.pushState);
	},
};

/*
 * function errorMsg
 * @param $arg
 */
function errorMsg(msg)
{
    bootbox.alert({ 
	message: '<div class="alert-error"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i><br><p>'+msg+'</p></div>', 
    });
}
/*
 * function banerClicks
 * @param $arg
 */

function banerClicks()
{
    $('.baner-click').on('click', function(){
	var self = $(this);
	var id = self.data('id');
	var link = self.attr('href');
	$.post('/baners/click', {id:id}, function(){
	    window.location.href=link;
	});
	return false;
    });
} banerClicks();

