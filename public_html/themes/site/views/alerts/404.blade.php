@extends('site::site.main')

@section('content')
    <div id="p404">
        <h1>{{ MetaTag::set('title', '404') }}</h1>
        <p>Такой страници не существует.</p>
    </div>
@stop