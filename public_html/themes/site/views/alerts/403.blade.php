@extends('site::site.main')

@section('title')
    <title>403 | {{ Config::get('admin.name') }}</title>
@stop

@section('content')
    <div class="container">
        <h1 class="text-center">403</h1>
        <div class="mms text-center">{!! $msg->getMessage() !!}</div>
    </div>
@stop