<!DOCTYPE html>
<html lang="ru">
	<head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>{{ config('cms.name') }}</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>
	<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
	    <tr>
                <td style="height: 50px; background: #F9F9F9; padding: 10px 20px;">
                    <a class="pull-left" href="{{ config('cms.siteUrl') }}" id="logo">
                        <img alt="" src="{{ config('cms.siteUrl') }}/themes/site/assets/img/logo.png">
                    </a>
                </td>
            </tr>
            <tr>
                <td style="height: 50px; background: #4D99CB; color: #fff; padding: 0 20px;">
                    <h1 style="text-align: center;">
                        @yield('title')
                    </h1>
                </td>
            </tr>
            <tr>
                <td style="min-height: 200px; background: #fff; padding: 50px; vertical-align: top;">
                   @yield('content')
                </td>
            </tr>
            <tr>
                <td style="height: 50px; background: #ccc; text-align: center;  padding: 0 20px; color: #000;">
                    © {{ date('Y') }}, {{ config('cms.name') }}
                </td>
            </tr>
	</table>	    
	</body>
</html>