<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" />
    
    <title>{{ MetaTag::get('title') }} | {{ config('cms.name') }}</title>
        
	@if(MetaTag::get('robots'))
	    <meta content="{{ MetaTag::get('robots') }}" name="robots">
	@endif
	
	@if(MetaTag::get('description'))
	    <meta content="{{ MetaTag::get('description') }}" name="description">
	@endif
	
	@if(MetaTag::get('keywords'))
	    <meta content="{{ MetaTag::get('keywords') }}" name="keywords">
	@endif
        
	{!! MetaTag::openGraph() !!}
	{!! MetaTag::twitterCard() !!}
    
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700&subset=latin,cyrillic,cyrillic-ext' rel='stylesheet' type='text/css'>
    <link href="{{ url('/') }}/themes/site/assets/css/all.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/site/assets/lib/icheck/skins/all.css" rel="stylesheet">
    <link href="{{ url('/') }}/themes/site/assets/css/site.css" rel="stylesheet">
</head>

<body>
{!! Menu::admin('top') !!}
<div id="wrapper">
  <header id="header" class="header">
    <div class="container-wrap">
      <div class="row">
        <div class="col-xs-7 col-sm-6">
          <div class="head-choise-city">
            {!! Form::select('city', City::getFavorite(session('location.city.city_id')), session('location.city.city_id'), array('class' => 'selectpicker geocity', 'title' => 'Вся Россия', 'data-width' => '150px')) !!}
          </div>
          <a href="{{ route('adverts.favorites') }}" class="head-fav">Избранное</a>
        </div>
        <div class="col-xs-5 col-sm-6">
          @include('user::site.block-user')
        </div>
      </div>
    </div>
  </header><!--header-->

  <section id="middle">
    {!! Baners::view('p1') !!}
    {!! Baners::view('p2') !!}
    <div id="container" class="group">

      <div class="container-fluid">
        <div class="head-panel-main head-pan-m-not-border">
          <div class="row">
            <div class="col-xs-6">
              <div class="logo"><a href="{{ url('/') }}"><img src="/themes/site/assets/img/logo.png" alt=""></a></div>
            </div>
            <div class="col-xs-6">
              <div class="text-right">
                <a href="{{ route('adverts.add') }}" class="btn btn-lg btn-add-ad ajax ajax-advert-add">Добавить объявление</a>
              </div>
            </div>
          </div>
	  {!! Block::view('search') !!}
          @yield('breadcrumbs')
	  
        </div>

        <div class="row">
          <div class="col-xs-12">
	    @include('site::alerts.alert')
	    @yield('content')
          </div>
        </div>
      </div><!-- .centered-bl-->

    </div><!-- #container-->
  </section><!-- #middle-->

  <div class="page-buffer"></div>
</div><!-- #wrapper -->

<footer id="footer" class="footer">
  <div class="footer-wrap-nav">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-8">
          {!! Menu::menuBottom() !!}
        </div>
        <div class="col-sm-4 hidden-xs">
          <div class="soc">
            <a href="https://vk.com/public120307742" class="soc-icon-1"></a>
            <a href="https://www.facebook.com/BACAY.ru/timeline" class="soc-icon-2"></a>
            <a href="https://twitter.com/BACAY_ru" class="soc-icon-3"></a>
            <span>— Присоединяйтесь к нам</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="info-footer">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-xs-4">
          <div class="copy">
            <b>© BACAY 2015</b>
	    Создание и разработка сайта - <a href="http://realsoft.pro">Realsoft.pro</a>
            Все права защищены.
          </div>
        </div>
        <div class="col-sm-8 col-xs-7">
          <p>ООО “BACAY” — это доска бесплатных объявлений городов России. Объявления публикуются бесплатно и без регистрации. </p>
          <div class="soc">
            <a href="https://vk.com/public120307742" class="soc-icon-1"></a>
            <a href="https://www.facebook.com/BACAY.ru/timeline" class="soc-icon-2"></a>
            <a href="https://twitter.com/BACAY_ru" class="soc-icon-3"></a>
            <span>— Присоединяйтесь к нам</span>
          </div>
        </div>
        <div class="col-sm-1 col-xs-1">
          <span class="age-limit"></span>
        </div>
      </div>
    </div>
  </div>
</footer><!-- #footer -->

<script src="{{ url('/') }}/themes/site/assets/js/jquery-2.1.4.min.js"></script>
<script src="{{ url('/') }}/themes/site/assets/js/bootstrap.min.js"></script>
<script src="{{ url('/') }}/themes/site/assets/js/all.js"></script>
<script src="{{ url('/') }}/themes/site/assets/lib/icheck/icheck.min.js"></script>
  <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56e2c9b59f31c3b9"></script>
@yield('script')
<script src="{{ url('/') }}/themes/site/assets/js/site.js"></script>

</body>
</html>