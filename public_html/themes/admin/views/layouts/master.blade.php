<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="_token" content="{{ csrf_token() }}" />
		<link rel="shortcut icon" href="{{ url('/') }}/uploads/favicon.ico" type="image/x-icon">
		<title>{{ MetaTag::get('title') }} | Админка</title>
		<link href="/themes/admin/assets/css/all.css" rel="stylesheet">
        @yield('style')
	</head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="skin-blue sidebar-mini">
    {!! Menu::admin('top') !!}
    <div class="wrapper">
	
      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">BAC</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">{{ config('cms.name') }}</span>
        </a>

        <!-- Header Navbar -->
	<nav role="navigation" class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a role="button" data-toggle="offcanvas" class="sidebar-toggle" href="#">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          
        </nav>
          
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
	
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image">
				<img src="{{ Sentinel::getUser()->avatar->image }}" style="height: 45px; width: 45px;" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p>{{ Sentinel::getUser()->username }}</p>
				<!-- Status -->
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- Sidebar user panel (optional) -->
		{!! Menu::admin('left') !!}
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
	@include('site::alerts.alert')
        @yield('content')
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
          
        </div>
        <!-- Default to the left -->
        <strong>Авторское право &copy; {{ date('Y') }} <!--<a href="#">Progim.net</a>.</strong> -->Все права защищены.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane active" id="control-sidebar-home-tab">
            

          </div><!-- /.tab-pane -->
          <!-- Stats tab content -->
          <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
          <!-- Settings tab content -->
          <div class="tab-pane" id="control-sidebar-settings-tab">
            
          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

	<!-- REQUIRED JS SCRIPTS -->
	<!-- jQuery 2.1.4 -->
	<script src="/themes/admin/assets/js/jQuery-2.1.4.min.js"></script>
	<script src="/themes/admin/assets/js/bootstrap.min.js"></script>
	<script src="/themes/admin/assets/js/jquery.nestable.js"></script>
    @yield('script')
	<script src="/themes/admin/assets/js/all.js"></script>
	<script src="/themes/admin/assets/js/admin.js"></script>
  </body>
</html>
