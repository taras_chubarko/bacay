@extends('admin::layouts.master')

@section('content') 
<section class="content-header">
    <h1>Панель администратора</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Панель администратора</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
        
        </div>
        
        <div class="box-body">
          
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{ $data->advertCount }}</h3>
                        <p>Объявлений на сайте</p>
                        <small>Подано сегодня: {{ $data->advertCountDay }}</small><br>
                        <small>Подано за неделю: {{ $data->advertCountWeek }}</small><br>
                        <small>Подано за месяц: {{ $data->advertCountMonth }}</small>    
                    </div>
                    <div class="icon">
                        <i class="fa fa-file-text" aria-hidden="true"></i>
                    </div>
                    <a class="small-box-footer" href="/admin/adverts">Список <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{ -$data->walletCount }} <sup style="font-size: 20px">руб.</sup></h3>
                        <p>Заработано</p>
                        <small>За сегодня: {{ -$data->walletCountDay }} <sub>руб.</sub></small><br>
                        <small>За неделю: {{ -$data->walletCountWeek }} <sub>руб.</sub></small><br>
                        <small>За месяц: {{ -$data->walletCountMonth }} <sub>руб.</sub></small>  
                    </div>
                    <div class="icon">
                        <i class="fa fa-rub" aria-hidden="true"></i>
                    </div>
                    <span class="small-box-footer" style="height: 25px"></span>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{ $data->visitCount }}</h3>
                        <p>Всего посещений</p>
                        <small>За сегодня: {{ $data->visitCountDay }}</small><br>
                        <small>За неделю: {{ $data->visitCountWeek }}</small><br>
                        <small>За месяц: {{ $data->visitCountMonth }}</small>    
                    </div>
                    <div class="icon">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </div>
                    <span class="small-box-footer" style="height: 25px"></span>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{ $data->usersCount }}</h3>
                        <p>Зарегистрированых пользователей</p>
                        <small>За сегодня: {{ $data->usersCountDay }}</small><br>
                        <small>За неделю: {{ $data->usersCountWeek }}</small><br>
                        <small>За месяц: {{ $data->usersCountMonth }}</small>    
                    </div>
                    <div class="icon">
                        <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a class="small-box-footer" href="/admin/users">Список <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
          </div>
          
          
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>
    
</section>
@stop