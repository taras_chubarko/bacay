$(document).ready(function() {
    
    //var $city2 = $('.geocity2');
    //
    //if($city2.length > 0)
    //{
    //    $.ajax({
    //        url: '/geo/get/cities2',
    //        type: 'POST',
    //        //data: {id: $id},
    //        beforeSend:function(jqXHR){
    //            
    //        },
    //        success: function(data){
    //           $city2.html(data).selectpicker('refresh');
    //        },
    //        error: function(data){
    //           
    //        }
    //    });
    //}
    $('.geocity2').selectpicker();
    
    
    $('#field-cities').on('click', function(){
        var $self = $(this);
        var $id = $('input[name=id]').val();
        $.ajax({
            url: '/geo/get/cities',
            type: 'POST',
            data: {id: $id},
            beforeSend:function(jqXHR){
                $self.val('Загрузка...');
            },
            success: function(data){
                $self.val('');
                $('body').append(data);
                $('#modal-cities').modal('show');
                checkAll();
                apply();
            },
            error: function(data){
               
            }
        });
    });
    
    function checkAll()
    {
        //$( "label:contains('Мос')" ).css( "text-decoration", "underline" );
        $('input[name=all]').on('change', function(){
            var $self = $(this);
            $self.parent().parent().find('input:checkbox').prop('checked', $self.prop('checked'));
            var $count = $self.parent().parent().find('.row input:checkbox:checked').length;
            var $total = $self.parent().parent().parent().parent().find('h4 a small');
            $total.remove();
            if($count !== 0)
            {
                $self.parent().parent().parent().parent().find('h4 a').append('<small class="text-success">Выбрано '+ $count +'</small>');
            }
        });
        
        $('.row input:checkbox').on('change', function(){
            var $self = $(this);
            var $count = $self.parent().parent().parent().parent().find('.row input:checkbox:checked').length;
            var $total = $self.parent().parent().parent().parent().parent().parent().find('h4 a small');
            $total.remove();
            if($count !== 0)
            {
                $self.parent().parent().parent().parent().parent().parent().find('h4 a').append('<small class="text-success">Выбрано '+ $count +'</small>');
            }
        });
    }
    
    function apply()
    {
        $('.apply').on('click', function(){
            var $count = $('.row input:checkbox:checked').length;
            if($count === 0)
            {
                alert('Нужно выбрать хоть один город.');
            }
            var $data = [];
            $('.row input:checkbox:checked').each(function(){
                var $self = $(this);
                $data.push('<input type="hidden" name="region[id]['+$self.val()+']" value="'+$self.val()+'">');
            });
            $('#field-cities-id').html($data.join(''));
            $('#field-cities').val('Выбрано ' + $count);
            $('#modal-cities').modal('hide');
            
        });
    }
    
});