$(document).ready(function() {
    $('[data-toggle="popover-hover"]').popover({
	trigger: 'hover',
        html: true,
        content: function() {
            return $(this).parent().find('.popover-content').html();
        }
    });
    /*
     *
     */
    function typeaheadCity()
    {
	$('.typeahead-city').typeahead({
	    display: 'cityD',
	    val: 'city',
	    itemSelected: function(item, val, text) {
		$('.typeahead-city').val(val);
	    },
	    ajax: {
		url: '/geo/city',
		preDispatch: function (query) {
		    return {q: query}
		},
		preProcess: function (data) {
		    var mylist = [];
		    $.each(data, function(k, v){
			mylist.push({city_id:v.city_id, cityD: v.city+', '+v.region, city:v.city });
		    });
		    return mylist;
		}
	    },
	});
    } typeaheadCity();
});
