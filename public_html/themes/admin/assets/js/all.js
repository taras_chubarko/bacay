/*! AdminLTE app.js
 * ================
 * Main JS application file for AdminLTE v2. This file
 * should be included in all pages. It controls some layout
 * options and implements exclusive AdminLTE plugins.
 *
 * @Author  Almsaeed Studio
 * @Support <http://www.almsaeedstudio.com>
 * @Email   <support@almsaeedstudio.com>
 * @version 2.1.2
 * @license MIT <http://opensource.org/licenses/MIT>
 */
"use strict";function _init(){$.AdminLTE.layout={activate:function(){var a=this;a.fix(),a.fixSidebar(),$(window,".wrapper").resize(function(){a.fix(),a.fixSidebar()})},fix:function(){var a=$(".main-header").outerHeight()+$(".main-footer").outerHeight(),b=$(window).height(),c=$(".sidebar").height();if($("body").hasClass("fixed"))$(".content-wrapper, .right-side").css("min-height",b-$(".main-footer").outerHeight());else{var d;b>=c?($(".content-wrapper, .right-side").css("min-height",b-a),d=b-a):($(".content-wrapper, .right-side").css("min-height",c),d=c);var e=$($.AdminLTE.options.controlSidebarOptions.selector);"undefined"!=typeof e&&e.height()>d&&$(".content-wrapper, .right-side").css("min-height",e.height())}},fixSidebar:function(){return $("body").hasClass("fixed")?("undefined"==typeof $.fn.slimScroll&&console&&console.error("Error: the fixed layout requires the slimscroll plugin!"),void($.AdminLTE.options.sidebarSlimScroll&&"undefined"!=typeof $.fn.slimScroll&&($(".sidebar").slimScroll({destroy:!0}).height("auto"),$(".sidebar").slimscroll({height:$(window).height()-$(".main-header").height()+"px",color:"rgba(0,0,0,0.2)",size:"3px"})))):void("undefined"!=typeof $.fn.slimScroll&&$(".sidebar").slimScroll({destroy:!0}).height("auto"))}},$.AdminLTE.pushMenu={activate:function(a){var b=$.AdminLTE.options.screenSizes;$(a).on("click",function(a){a.preventDefault(),$(window).width()>b.sm-1?$("body").hasClass("sidebar-collapse")?$("body").removeClass("sidebar-collapse").trigger("expanded.pushMenu"):$("body").addClass("sidebar-collapse").trigger("collapsed.pushMenu"):$("body").hasClass("sidebar-open")?$("body").removeClass("sidebar-open").removeClass("sidebar-collapse").trigger("collapsed.pushMenu"):$("body").addClass("sidebar-open").trigger("expanded.pushMenu")}),$(".content-wrapper").click(function(){$(window).width()<=b.sm-1&&$("body").hasClass("sidebar-open")&&$("body").removeClass("sidebar-open")}),($.AdminLTE.options.sidebarExpandOnHover||$("body").hasClass("fixed")&&$("body").hasClass("sidebar-mini"))&&this.expandOnHover()},expandOnHover:function(){var a=this,b=$.AdminLTE.options.screenSizes.sm-1;$(".main-sidebar").hover(function(){$("body").hasClass("sidebar-mini")&&$("body").hasClass("sidebar-collapse")&&$(window).width()>b&&a.expand()},function(){$("body").hasClass("sidebar-mini")&&$("body").hasClass("sidebar-expanded-on-hover")&&$(window).width()>b&&a.collapse()})},expand:function(){$("body").removeClass("sidebar-collapse").addClass("sidebar-expanded-on-hover")},collapse:function(){$("body").hasClass("sidebar-expanded-on-hover")&&$("body").removeClass("sidebar-expanded-on-hover").addClass("sidebar-collapse")}},$.AdminLTE.tree=function(a){var b=this,c=$.AdminLTE.options.animationSpeed;$("li a",$(a)).on("click",function(a){var d=$(this),e=d.next();if(e.is(".treeview-menu")&&e.is(":visible"))e.slideUp(c,function(){e.removeClass("menu-open")}),e.parent("li").removeClass("active");else if(e.is(".treeview-menu")&&!e.is(":visible")){var f=d.parents("ul").first(),g=f.find("ul:visible").slideUp(c);g.removeClass("menu-open");var h=d.parent("li");e.slideDown(c,function(){e.addClass("menu-open"),f.find("li.active").removeClass("active"),h.addClass("active"),b.layout.fix()})}e.is(".treeview-menu")&&a.preventDefault()})},$.AdminLTE.controlSidebar={activate:function(){var a=this,b=$.AdminLTE.options.controlSidebarOptions,c=$(b.selector),d=$(b.toggleBtnSelector);d.on("click",function(d){d.preventDefault(),c.hasClass("control-sidebar-open")||$("body").hasClass("control-sidebar-open")?a.close(c,b.slide):a.open(c,b.slide)});var e=$(".control-sidebar-bg");a._fix(e),$("body").hasClass("fixed")?a._fixForFixed(c):$(".content-wrapper, .right-side").height()<c.height()&&a._fixForContent(c)},open:function(a,b){b?a.addClass("control-sidebar-open"):$("body").addClass("control-sidebar-open")},close:function(a,b){b?a.removeClass("control-sidebar-open"):$("body").removeClass("control-sidebar-open")},_fix:function(a){var b=this;$("body").hasClass("layout-boxed")?(a.css("position","absolute"),a.height($(".wrapper").height()),$(window).resize(function(){b._fix(a)})):a.css({position:"fixed",height:"auto"})},_fixForFixed:function(a){a.css({position:"fixed","max-height":"100%",overflow:"auto","padding-bottom":"50px"})},_fixForContent:function(a){$(".content-wrapper, .right-side").css("min-height",a.height())}},$.AdminLTE.boxWidget={selectors:$.AdminLTE.options.boxWidgetOptions.boxWidgetSelectors,icons:$.AdminLTE.options.boxWidgetOptions.boxWidgetIcons,animationSpeed:$.AdminLTE.options.animationSpeed,activate:function(a){var b=this;a||(a=document),$(a).find(b.selectors.collapse).on("click",function(a){a.preventDefault(),b.collapse($(this))}),$(a).find(b.selectors.remove).on("click",function(a){a.preventDefault(),b.remove($(this))})},collapse:function(a){var b=this,c=a.parents(".box").first(),d=c.find("> .box-body, > .box-footer, > form  >.box-body, > form > .box-footer");c.hasClass("collapsed-box")?(a.children(":first").removeClass(b.icons.open).addClass(b.icons.collapse),d.slideDown(b.animationSpeed,function(){c.removeClass("collapsed-box")})):(a.children(":first").removeClass(b.icons.collapse).addClass(b.icons.open),d.slideUp(b.animationSpeed,function(){c.addClass("collapsed-box")}))},remove:function(a){var b=a.parents(".box").first();b.slideUp(this.animationSpeed)}}}if("undefined"==typeof jQuery)throw new Error("AdminLTE requires jQuery");$.AdminLTE={},$.AdminLTE.options={navbarMenuSlimscroll:!0,navbarMenuSlimscrollWidth:"3px",navbarMenuHeight:"200px",animationSpeed:500,sidebarToggleSelector:"[data-toggle='offcanvas']",sidebarPushMenu:!0,sidebarSlimScroll:!0,sidebarExpandOnHover:!1,enableBoxRefresh:!0,enableBSToppltip:!0,BSTooltipSelector:"[data-toggle='tooltip']",enableFastclick:!0,enableControlSidebar:!0,controlSidebarOptions:{toggleBtnSelector:"[data-toggle='control-sidebar']",selector:".control-sidebar",slide:!0},enableBoxWidget:!0,boxWidgetOptions:{boxWidgetIcons:{collapse:"fa-minus",open:"fa-plus",remove:"fa-times"},boxWidgetSelectors:{remove:'[data-widget="remove"]',collapse:'[data-widget="collapse"]'}},directChat:{enable:!0,contactToggleSelector:'[data-widget="chat-pane-toggle"]'},colors:{lightBlue:"#3c8dbc",red:"#f56954",green:"#00a65a",aqua:"#00c0ef",yellow:"#f39c12",blue:"#0073b7",navy:"#001F3F",teal:"#39CCCC",olive:"#3D9970",lime:"#01FF70",orange:"#FF851B",fuchsia:"#F012BE",purple:"#8E24AA",maroon:"#D81B60",black:"#222222",gray:"#d2d6de"},screenSizes:{xs:480,sm:768,md:992,lg:1200}},$(function(){"undefined"!=typeof AdminLTEOptions&&$.extend(!0,$.AdminLTE.options,AdminLTEOptions);var a=$.AdminLTE.options;_init(),$.AdminLTE.layout.activate(),$.AdminLTE.tree(".sidebar"),a.enableControlSidebar&&$.AdminLTE.controlSidebar.activate(),a.navbarMenuSlimscroll&&"undefined"!=typeof $.fn.slimscroll&&$(".navbar .menu").slimscroll({height:a.navbarMenuHeight,alwaysVisible:!1,size:a.navbarMenuSlimscrollWidth}).css("width","100%"),a.sidebarPushMenu&&$.AdminLTE.pushMenu.activate(a.sidebarToggleSelector),a.enableBSToppltip&&$("body").tooltip({selector:a.BSTooltipSelector}),a.enableBoxWidget&&$.AdminLTE.boxWidget.activate(),a.enableFastclick&&"undefined"!=typeof FastClick&&FastClick.attach(document.body),a.directChat.enable&&$(a.directChat.contactToggleSelector).on("click",function(){var a=$(this).parents(".direct-chat").first();a.toggleClass("direct-chat-contacts-open")}),$('.btn-group[data-toggle="btn-toggle"]').each(function(){var a=$(this);$(this).find(".btn").on("click",function(b){a.find(".btn.active").removeClass("active"),$(this).addClass("active"),b.preventDefault()})})}),function(a){a.fn.boxRefresh=function(b){function c(a){a.append(f),e.onLoadStart.call(a)}function d(a){a.find(f).remove(),e.onLoadDone.call(a)}var e=a.extend({trigger:".refresh-btn",source:"",onLoadStart:function(a){},onLoadDone:function(a){}},b),f=a('<div class="overlay"><div class="fa fa-refresh fa-spin"></div></div>');return this.each(function(){if(""===e.source)return void(console&&console.log("Please specify a source first - boxRefresh()"));var b=a(this),f=b.find(e.trigger).first();f.on("click",function(a){a.preventDefault(),c(b),b.find(".box-body").load(e.source,function(){d(b)})})})}}(jQuery),function(a){a.fn.activateBox=function(){a.AdminLTE.boxWidget.activate(this)}}(jQuery),function(a){a.fn.todolist=function(b){var c=a.extend({onCheck:function(a){},onUncheck:function(a){}},b);return this.each(function(){"undefined"!=typeof a.fn.iCheck?(a("input",this).on("ifChecked",function(b){var d=a(this).parents("li").first();d.toggleClass("done"),c.onCheck.call(d)}),a("input",this).on("ifUnchecked",function(b){var d=a(this).parents("li").first();d.toggleClass("done"),c.onUncheck.call(d)})):a("input",this).on("change",function(b){var d=a(this).parents("li").first();d.toggleClass("done"),c.onCheck.call(d)})})}}(jQuery);
/**
 * bootbox.js v4.4.0
 *
 * http://bootboxjs.com/license.txt
 */
!function(a,b){"use strict";"function"==typeof define&&define.amd?define(["jquery"],b):"object"==typeof exports?module.exports=b(require("jquery")):a.bootbox=b(a.jQuery)}(this,function a(b,c){"use strict";function d(a){var b=q[o.locale];return b?b[a]:q.en[a]}function e(a,c,d){a.stopPropagation(),a.preventDefault();var e=b.isFunction(d)&&d.call(c,a)===!1;e||c.modal("hide")}function f(a){var b,c=0;for(b in a)c++;return c}function g(a,c){var d=0;b.each(a,function(a,b){c(a,b,d++)})}function h(a){var c,d;if("object"!=typeof a)throw new Error("Please supply an object of options");if(!a.message)throw new Error("Please specify a message");return a=b.extend({},o,a),a.buttons||(a.buttons={}),c=a.buttons,d=f(c),g(c,function(a,e,f){if(b.isFunction(e)&&(e=c[a]={callback:e}),"object"!==b.type(e))throw new Error("button with key "+a+" must be an object");e.label||(e.label=a),e.className||(e.className=2>=d&&f===d-1?"btn-primary":"btn-default")}),a}function i(a,b){var c=a.length,d={};if(1>c||c>2)throw new Error("Invalid argument length");return 2===c||"string"==typeof a[0]?(d[b[0]]=a[0],d[b[1]]=a[1]):d=a[0],d}function j(a,c,d){return b.extend(!0,{},a,i(c,d))}function k(a,b,c,d){var e={className:"bootbox-"+a,buttons:l.apply(null,b)};return m(j(e,d,c),b)}function l(){for(var a={},b=0,c=arguments.length;c>b;b++){var e=arguments[b],f=e.toLowerCase(),g=e.toUpperCase();a[f]={label:d(g)}}return a}function m(a,b){var d={};return g(b,function(a,b){d[b]=!0}),g(a.buttons,function(a){if(d[a]===c)throw new Error("button key "+a+" is not allowed (options are "+b.join("\n")+")")}),a}var n={dialog:"<div class='bootbox modal' tabindex='-1' role='dialog'><div class='modal-dialog'><div class='modal-content'><div class='modal-body'><div class='bootbox-body'></div></div></div></div></div>",header:"<div class='modal-header'><h4 class='modal-title'></h4></div>",footer:"<div class='modal-footer'></div>",closeButton:"<button type='button' class='bootbox-close-button close' data-dismiss='modal' aria-hidden='true'>&times;</button>",form:"<form class='bootbox-form'></form>",inputs:{text:"<input class='bootbox-input bootbox-input-text form-control' autocomplete=off type=text />",textarea:"<textarea class='bootbox-input bootbox-input-textarea form-control'></textarea>",email:"<input class='bootbox-input bootbox-input-email form-control' autocomplete='off' type='email' />",select:"<select class='bootbox-input bootbox-input-select form-control'></select>",checkbox:"<div class='checkbox'><label><input class='bootbox-input bootbox-input-checkbox' type='checkbox' /></label></div>",date:"<input class='bootbox-input bootbox-input-date form-control' autocomplete=off type='date' />",time:"<input class='bootbox-input bootbox-input-time form-control' autocomplete=off type='time' />",number:"<input class='bootbox-input bootbox-input-number form-control' autocomplete=off type='number' />",password:"<input class='bootbox-input bootbox-input-password form-control' autocomplete='off' type='password' />"}},o={locale:"en",backdrop:"static",animate:!0,className:null,closeButton:!0,show:!0,container:"body"},p={};p.alert=function(){var a;if(a=k("alert",["ok"],["message","callback"],arguments),a.callback&&!b.isFunction(a.callback))throw new Error("alert requires callback property to be a function when provided");return a.buttons.ok.callback=a.onEscape=function(){return b.isFunction(a.callback)?a.callback.call(this):!0},p.dialog(a)},p.confirm=function(){var a;if(a=k("confirm",["cancel","confirm"],["message","callback"],arguments),a.buttons.cancel.callback=a.onEscape=function(){return a.callback.call(this,!1)},a.buttons.confirm.callback=function(){return a.callback.call(this,!0)},!b.isFunction(a.callback))throw new Error("confirm requires a callback");return p.dialog(a)},p.prompt=function(){var a,d,e,f,h,i,k;if(f=b(n.form),d={className:"bootbox-prompt",buttons:l("cancel","confirm"),value:"",inputType:"text"},a=m(j(d,arguments,["title","callback"]),["cancel","confirm"]),i=a.show===c?!0:a.show,a.message=f,a.buttons.cancel.callback=a.onEscape=function(){return a.callback.call(this,null)},a.buttons.confirm.callback=function(){var c;switch(a.inputType){case"text":case"textarea":case"email":case"select":case"date":case"time":case"number":case"password":c=h.val();break;case"checkbox":var d=h.find("input:checked");c=[],g(d,function(a,d){c.push(b(d).val())})}return a.callback.call(this,c)},a.show=!1,!a.title)throw new Error("prompt requires a title");if(!b.isFunction(a.callback))throw new Error("prompt requires a callback");if(!n.inputs[a.inputType])throw new Error("invalid prompt type");switch(h=b(n.inputs[a.inputType]),a.inputType){case"text":case"textarea":case"email":case"date":case"time":case"number":case"password":h.val(a.value);break;case"select":var o={};if(k=a.inputOptions||[],!b.isArray(k))throw new Error("Please pass an array of input options");if(!k.length)throw new Error("prompt with select requires options");g(k,function(a,d){var e=h;if(d.value===c||d.text===c)throw new Error("given options in wrong format");d.group&&(o[d.group]||(o[d.group]=b("<optgroup/>").attr("label",d.group)),e=o[d.group]),e.append("<option value='"+d.value+"'>"+d.text+"</option>")}),g(o,function(a,b){h.append(b)}),h.val(a.value);break;case"checkbox":var q=b.isArray(a.value)?a.value:[a.value];if(k=a.inputOptions||[],!k.length)throw new Error("prompt with checkbox requires options");if(!k[0].value||!k[0].text)throw new Error("given options in wrong format");h=b("<div/>"),g(k,function(c,d){var e=b(n.inputs[a.inputType]);e.find("input").attr("value",d.value),e.find("label").append(d.text),g(q,function(a,b){b===d.value&&e.find("input").prop("checked",!0)}),h.append(e)})}return a.placeholder&&h.attr("placeholder",a.placeholder),a.pattern&&h.attr("pattern",a.pattern),a.maxlength&&h.attr("maxlength",a.maxlength),f.append(h),f.on("submit",function(a){a.preventDefault(),a.stopPropagation(),e.find(".btn-primary").click()}),e=p.dialog(a),e.off("shown.bs.modal"),e.on("shown.bs.modal",function(){h.focus()}),i===!0&&e.modal("show"),e},p.dialog=function(a){a=h(a);var d=b(n.dialog),f=d.find(".modal-dialog"),i=d.find(".modal-body"),j=a.buttons,k="",l={onEscape:a.onEscape};if(b.fn.modal===c)throw new Error("$.fn.modal is not defined; please double check you have included the Bootstrap JavaScript library. See http://getbootstrap.com/javascript/ for more details.");if(g(j,function(a,b){k+="<button data-bb-handler='"+a+"' type='button' class='btn "+b.className+"'>"+b.label+"</button>",l[a]=b.callback}),i.find(".bootbox-body").html(a.message),a.animate===!0&&d.addClass("fade"),a.className&&d.addClass(a.className),"large"===a.size?f.addClass("modal-lg"):"small"===a.size&&f.addClass("modal-sm"),a.title&&i.before(n.header),a.closeButton){var m=b(n.closeButton);a.title?d.find(".modal-header").prepend(m):m.css("margin-top","-10px").prependTo(i)}return a.title&&d.find(".modal-title").html(a.title),k.length&&(i.after(n.footer),d.find(".modal-footer").html(k)),d.on("hidden.bs.modal",function(a){a.target===this&&d.remove()}),d.on("shown.bs.modal",function(){d.find(".btn-primary:first").focus()}),"static"!==a.backdrop&&d.on("click.dismiss.bs.modal",function(a){d.children(".modal-backdrop").length&&(a.currentTarget=d.children(".modal-backdrop").get(0)),a.target===a.currentTarget&&d.trigger("escape.close.bb")}),d.on("escape.close.bb",function(a){l.onEscape&&e(a,d,l.onEscape)}),d.on("click",".modal-footer button",function(a){var c=b(this).data("bb-handler");e(a,d,l[c])}),d.on("click",".bootbox-close-button",function(a){e(a,d,l.onEscape)}),d.on("keyup",function(a){27===a.which&&d.trigger("escape.close.bb")}),b(a.container).append(d),d.modal({backdrop:a.backdrop?"static":!1,keyboard:!1,show:!1}),a.show&&d.modal("show"),d},p.setDefaults=function(){var a={};2===arguments.length?a[arguments[0]]=arguments[1]:a=arguments[0],b.extend(o,a)},p.hideAll=function(){return b(".bootbox").modal("hide"),p};var q={bg_BG:{OK:"Ок",CANCEL:"Отказ",CONFIRM:"Потвърждавам"},br:{OK:"OK",CANCEL:"Cancelar",CONFIRM:"Sim"},cs:{OK:"OK",CANCEL:"Zrušit",CONFIRM:"Potvrdit"},da:{OK:"OK",CANCEL:"Annuller",CONFIRM:"Accepter"},de:{OK:"OK",CANCEL:"Abbrechen",CONFIRM:"Akzeptieren"},el:{OK:"Εντάξει",CANCEL:"Ακύρωση",CONFIRM:"Επιβεβαίωση"},en:{OK:"OK",CANCEL:"Cancel",CONFIRM:"OK"},es:{OK:"OK",CANCEL:"Cancelar",CONFIRM:"Aceptar"},et:{OK:"OK",CANCEL:"Katkesta",CONFIRM:"OK"},fa:{OK:"قبول",CANCEL:"لغو",CONFIRM:"تایید"},fi:{OK:"OK",CANCEL:"Peruuta",CONFIRM:"OK"},fr:{OK:"OK",CANCEL:"Annuler",CONFIRM:"D'accord"},he:{OK:"אישור",CANCEL:"ביטול",CONFIRM:"אישור"},hu:{OK:"OK",CANCEL:"Mégsem",CONFIRM:"Megerősít"},hr:{OK:"OK",CANCEL:"Odustani",CONFIRM:"Potvrdi"},id:{OK:"OK",CANCEL:"Batal",CONFIRM:"OK"},it:{OK:"OK",CANCEL:"Annulla",CONFIRM:"Conferma"},ja:{OK:"OK",CANCEL:"キャンセル",CONFIRM:"確認"},lt:{OK:"Gerai",CANCEL:"Atšaukti",CONFIRM:"Patvirtinti"},lv:{OK:"Labi",CANCEL:"Atcelt",CONFIRM:"Apstiprināt"},nl:{OK:"OK",CANCEL:"Annuleren",CONFIRM:"Accepteren"},no:{OK:"OK",CANCEL:"Avbryt",CONFIRM:"OK"},pl:{OK:"OK",CANCEL:"Anuluj",CONFIRM:"Potwierdź"},pt:{OK:"OK",CANCEL:"Cancelar",CONFIRM:"Confirmar"},ru:{OK:"OK",CANCEL:"Отмена",CONFIRM:"Применить"},sq:{OK:"OK",CANCEL:"Anulo",CONFIRM:"Prano"},sv:{OK:"OK",CANCEL:"Avbryt",CONFIRM:"OK"},th:{OK:"ตกลง",CANCEL:"ยกเลิก",CONFIRM:"ยืนยัน"},tr:{OK:"Tamam",CANCEL:"İptal",CONFIRM:"Onayla"},zh_CN:{OK:"OK",CANCEL:"取消",CONFIRM:"确认"},zh_TW:{OK:"OK",CANCEL:"取消",CONFIRM:"確認"}};return p.addLocale=function(a,c){return b.each(["OK","CANCEL","CONFIRM"],function(a,b){if(!c[b])throw new Error("Please supply a translation for '"+b+"'")}),q[a]={OK:c.OK,CANCEL:c.CANCEL,CONFIRM:c.CONFIRM},p},p.removeLocale=function(a){return delete q[a],p},p.setLocale=function(a){return p.setDefaults("locale",a)},p.init=function(c){return a(c||b)},p});
/*!
 * Bootstrap-select v1.7.2 (http://silviomoreto.github.io/bootstrap-select)
 *
 * Copyright 2013-2015 bootstrap-select
 * Licensed under MIT (https://github.com/silviomoreto/bootstrap-select/blob/master/LICENSE)
 */
!function(a,b){"function"==typeof define&&define.amd?define(["jquery"],function(a){return b(a)}):"object"==typeof exports?module.exports=b(require("jquery")):b(jQuery)}(this,function(){!function(a){"use strict";function b(b){var c=[{re:/[\xC0-\xC6]/g,ch:"A"},{re:/[\xE0-\xE6]/g,ch:"a"},{re:/[\xC8-\xCB]/g,ch:"E"},{re:/[\xE8-\xEB]/g,ch:"e"},{re:/[\xCC-\xCF]/g,ch:"I"},{re:/[\xEC-\xEF]/g,ch:"i"},{re:/[\xD2-\xD6]/g,ch:"O"},{re:/[\xF2-\xF6]/g,ch:"o"},{re:/[\xD9-\xDC]/g,ch:"U"},{re:/[\xF9-\xFC]/g,ch:"u"},{re:/[\xC7-\xE7]/g,ch:"c"},{re:/[\xD1]/g,ch:"N"},{re:/[\xF1]/g,ch:"n"}];return a.each(c,function(){b=b.replace(this.re,this.ch)}),b}function c(a){var b={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;"},c="(?:"+Object.keys(b).join("|")+")",d=new RegExp(c),e=new RegExp(c,"g"),f=null==a?"":""+a;return d.test(f)?f.replace(e,function(a){return b[a]}):f}function d(b,c){var d=arguments,f=b,g=c;[].shift.apply(d);var h,i=this.each(function(){var b=a(this);if(b.is("select")){var c=b.data("selectpicker"),i="object"==typeof f&&f;if(c){if(i)for(var j in i)i.hasOwnProperty(j)&&(c.options[j]=i[j])}else{var k=a.extend({},e.DEFAULTS,a.fn.selectpicker.defaults||{},b.data(),i);b.data("selectpicker",c=new e(this,k,g))}"string"==typeof f&&(h=c[f]instanceof Function?c[f].apply(c,d):c.options[f])}});return"undefined"!=typeof h?h:i}String.prototype.includes||!function(){var a={}.toString,b=function(){try{var a={},b=Object.defineProperty,c=b(a,a,a)&&b}catch(d){}return c}(),c="".indexOf,d=function(b){if(null==this)throw TypeError();var d=String(this);if(b&&"[object RegExp]"==a.call(b))throw TypeError();var e=d.length,f=String(b),g=f.length,h=arguments.length>1?arguments[1]:void 0,i=h?Number(h):0;i!=i&&(i=0);var j=Math.min(Math.max(i,0),e);return g+j>e?!1:-1!=c.call(d,f,i)};b?b(String.prototype,"includes",{value:d,configurable:!0,writable:!0}):String.prototype.includes=d}(),String.prototype.startsWith||!function(){var a=function(){try{var a={},b=Object.defineProperty,c=b(a,a,a)&&b}catch(d){}return c}(),b={}.toString,c=function(a){if(null==this)throw TypeError();var c=String(this);if(a&&"[object RegExp]"==b.call(a))throw TypeError();var d=c.length,e=String(a),f=e.length,g=arguments.length>1?arguments[1]:void 0,h=g?Number(g):0;h!=h&&(h=0);var i=Math.min(Math.max(h,0),d);if(f+i>d)return!1;for(var j=-1;++j<f;)if(c.charCodeAt(i+j)!=e.charCodeAt(j))return!1;return!0};a?a(String.prototype,"startsWith",{value:c,configurable:!0,writable:!0}):String.prototype.startsWith=c}(),Object.keys||(Object.keys=function(a,b,c){c=[];for(b in a)c.hasOwnProperty.call(a,b)&&c.push(b);return c}),a.expr[":"].icontains=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.text()).toUpperCase();return f.includes(d[3].toUpperCase())},a.expr[":"].ibegins=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.text()).toUpperCase();return f.startsWith(d[3].toUpperCase())},a.expr[":"].aicontains=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.data("normalizedText")||e.text()).toUpperCase();return f.includes(d[3].toUpperCase())},a.expr[":"].aibegins=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.data("normalizedText")||e.text()).toUpperCase();return f.startsWith(d[3].toUpperCase())};var e=function(b,c,d){d&&(d.stopPropagation(),d.preventDefault()),this.$element=a(b),this.$newElement=null,this.$button=null,this.$menu=null,this.$lis=null,this.options=c,null===this.options.title&&(this.options.title=this.$element.attr("title")),this.val=e.prototype.val,this.render=e.prototype.render,this.refresh=e.prototype.refresh,this.setStyle=e.prototype.setStyle,this.selectAll=e.prototype.selectAll,this.deselectAll=e.prototype.deselectAll,this.destroy=e.prototype.remove,this.remove=e.prototype.remove,this.show=e.prototype.show,this.hide=e.prototype.hide,this.init()};e.VERSION="1.7.2",e.DEFAULTS={noneSelectedText:"Nothing selected",noneResultsText:"No results matched {0}",countSelectedText:function(a,b){return 1==a?"{0} item selected":"{0} items selected"},maxOptionsText:function(a,b){return[1==a?"Limit reached ({n} item max)":"Limit reached ({n} items max)",1==b?"Group limit reached ({n} item max)":"Group limit reached ({n} items max)"]},selectAllText:"Select All",deselectAllText:"Deselect All",doneButton:!1,doneButtonText:"Close",multipleSeparator:", ",styleBase:"btn",style:"btn-default",size:"auto",title:null,selectedTextFormat:"values",width:!1,container:!1,hideDisabled:!1,showSubtext:!1,showIcon:!0,showContent:!0,dropupAuto:!0,header:!1,liveSearch:!1,liveSearchPlaceholder:null,liveSearchNormalize:!1,liveSearchStyle:"contains",actionsBox:!1,iconBase:"glyphicon",tickIcon:"glyphicon-ok",maxOptions:!1,mobile:!1,selectOnTab:!1,dropdownAlignRight:!1},e.prototype={constructor:e,init:function(){var b=this,c=this.$element.attr("id");this.$element.addClass("bs-select-hidden"),this.liObj={},this.multiple=this.$element.prop("multiple"),this.autofocus=this.$element.prop("autofocus"),this.$newElement=this.createView(),this.$element.after(this.$newElement),this.$button=this.$newElement.children("button"),this.$menu=this.$newElement.children(".dropdown-menu"),this.$menuInner=this.$menu.children(".inner"),this.$searchbox=this.$menu.find("input"),this.options.dropdownAlignRight&&this.$menu.addClass("dropdown-menu-right"),"undefined"!=typeof c&&(this.$button.attr("data-id",c),a('label[for="'+c+'"]').click(function(a){a.preventDefault(),b.$button.focus()})),this.checkDisabled(),this.clickListener(),this.options.liveSearch&&this.liveSearchListener(),this.render(),this.setStyle(),this.setWidth(),this.options.container&&this.selectPosition(),this.$menu.data("this",this),this.$newElement.data("this",this),this.options.mobile&&this.mobile(),this.$newElement.on("hide.bs.dropdown",function(a){b.$element.trigger("hide.bs.select",a)}),this.$newElement.on("hidden.bs.dropdown",function(a){b.$element.trigger("hidden.bs.select",a)}),this.$newElement.on("show.bs.dropdown",function(a){b.$element.trigger("show.bs.select",a)}),this.$newElement.on("shown.bs.dropdown",function(a){b.$element.trigger("shown.bs.select",a)}),setTimeout(function(){b.$element.trigger("loaded.bs.select")})},createDropdown:function(){var b=this.multiple?" show-tick":"",d=this.$element.parent().hasClass("input-group")?" input-group-btn":"",e=this.autofocus?" autofocus":"",f=this.options.header?'<div class="popover-title"><button type="button" class="close" aria-hidden="true">&times;</button>'+this.options.header+"</div>":"",g=this.options.liveSearch?'<div class="bs-searchbox"><input type="text" class="form-control" autocomplete="off"'+(null===this.options.liveSearchPlaceholder?"":' placeholder="'+c(this.options.liveSearchPlaceholder)+'"')+"></div>":"",h=this.multiple&&this.options.actionsBox?'<div class="bs-actionsbox"><div class="btn-group btn-group-sm btn-block"><button type="button" class="actions-btn bs-select-all btn btn-default">'+this.options.selectAllText+'</button><button type="button" class="actions-btn bs-deselect-all btn btn-default">'+this.options.deselectAllText+"</button></div></div>":"",i=this.multiple&&this.options.doneButton?'<div class="bs-donebutton"><div class="btn-group btn-block"><button type="button" class="btn btn-sm btn-default">'+this.options.doneButtonText+"</button></div></div>":"",j='<div class="btn-group bootstrap-select'+b+d+'"><button type="button" class="'+this.options.styleBase+' dropdown-toggle" data-toggle="dropdown"'+e+'><span class="filter-option pull-left"></span>&nbsp;<span class="caret"></span></button><div class="dropdown-menu open">'+f+g+h+'<ul class="dropdown-menu inner" role="menu"></ul>'+i+"</div></div>";return a(j)},createView:function(){var a=this.createDropdown(),b=this.createLi();return a.find("ul")[0].innerHTML=b,a},reloadLi:function(){this.destroyLi();var a=this.createLi();this.$menuInner[0].innerHTML=a},destroyLi:function(){this.$menu.find("li").remove()},createLi:function(){var d=this,e=[],f=0,g=document.createElement("option"),h=-1,i=function(a,b,c,d){return"<li"+("undefined"!=typeof c&""!==c?' class="'+c+'"':"")+("undefined"!=typeof b&null!==b?' data-original-index="'+b+'"':"")+("undefined"!=typeof d&null!==d?'data-optgroup="'+d+'"':"")+">"+a+"</li>"},j=function(a,e,f,g){return'<a tabindex="0"'+("undefined"!=typeof e?' class="'+e+'"':"")+("undefined"!=typeof f?' style="'+f+'"':"")+(d.options.liveSearchNormalize?' data-normalized-text="'+b(c(a))+'"':"")+("undefined"!=typeof g||null!==g?' data-tokens="'+g+'"':"")+">"+a+'<span class="'+d.options.iconBase+" "+d.options.tickIcon+' check-mark"></span></a>'};if(this.options.title&&!this.multiple&&(h--,!this.$element.find(".bs-title-option").length)){var k=this.$element[0];g.className="bs-title-option",g.appendChild(document.createTextNode(this.options.title)),g.value="",k.insertBefore(g,k.firstChild),null===k.options[k.selectedIndex].getAttribute("selected")&&(g.selected=!0)}return this.$element.find("option").each(function(b){var c=a(this);if(h++,!c.hasClass("bs-title-option")){var g=this.className||"",k=this.style.cssText,l=c.data("content")?c.data("content"):c.html(),m=c.data("tokens")?c.data("tokens"):null,n="undefined"!=typeof c.data("subtext")?'<small class="text-muted">'+c.data("subtext")+"</small>":"",o="undefined"!=typeof c.data("icon")?'<span class="'+d.options.iconBase+" "+c.data("icon")+'"></span> ':"",p=this.disabled||"OPTGROUP"===this.parentElement.tagName&&this.parentElement.disabled;if(""!==o&&p&&(o="<span>"+o+"</span>"),d.options.hideDisabled&&p)return void h--;if(c.data("content")||(l=o+'<span class="text">'+l+n+"</span>"),"OPTGROUP"===this.parentElement.tagName&&c.data("divider")!==!0){if(0===c.index()){f+=1;var q=this.parentElement.label,r="undefined"!=typeof c.parent().data("subtext")?'<small class="text-muted">'+c.parent().data("subtext")+"</small>":"",s=c.parent().data("icon")?'<span class="'+d.options.iconBase+" "+c.parent().data("icon")+'"></span> ':"",t=" "+this.parentElement.className||"";q=s+'<span class="text">'+q+r+"</span>",0!==b&&e.length>0&&(h++,e.push(i("",null,"divider",f+"div"))),h++,e.push(i(q,null,"dropdown-header"+t,f))}e.push(i(j(l,"opt "+g+t,k,m),b,"",f))}else c.data("divider")===!0?e.push(i("",b,"divider")):c.data("hidden")===!0?e.push(i(j(l,g,k,m),b,"hidden is-hidden")):(this.previousElementSibling&&"OPTGROUP"===this.previousElementSibling.tagName&&(h++,e.push(i("",null,"divider",f+"div"))),e.push(i(j(l,g,k,m),b)));d.liObj[b]=h}}),this.multiple||0!==this.$element.find("option:selected").length||this.options.title||this.$element.find("option").eq(0).prop("selected",!0).attr("selected","selected"),e.join("")},findLis:function(){return null==this.$lis&&(this.$lis=this.$menu.find("li")),this.$lis},render:function(b){var c,d=this;b!==!1&&this.$element.find("option").each(function(a){var b=d.findLis().eq(d.liObj[a]);d.setDisabled(a,this.disabled||"OPTGROUP"===this.parentElement.tagName&&this.parentElement.disabled,b),d.setSelected(a,this.selected,b)}),this.tabIndex();var e=this.$element.find("option").map(function(){if(this.selected){if(d.options.hideDisabled&&(this.disabled||"OPTGROUP"===this.parentElement.tagName&&this.parentElement.disabled))return!1;var b,c=a(this),e=c.data("icon")&&d.options.showIcon?'<i class="'+d.options.iconBase+" "+c.data("icon")+'"></i> ':"";return b=d.options.showSubtext&&c.data("subtext")&&!d.multiple?' <small class="text-muted">'+c.data("subtext")+"</small>":"","undefined"!=typeof c.attr("title")?c.attr("title"):c.data("content")&&d.options.showContent?c.data("content"):e+c.html()+b}}).toArray(),f=this.multiple?e.join(this.options.multipleSeparator):e[0];if(this.multiple&&this.options.selectedTextFormat.indexOf("count")>-1){var g=this.options.selectedTextFormat.split(">");if(g.length>1&&e.length>g[1]||1==g.length&&e.length>=2){c=this.options.hideDisabled?", [disabled]":"";var h=this.$element.find("option").not('[data-divider="true"], [data-hidden="true"]'+c).length,i="function"==typeof this.options.countSelectedText?this.options.countSelectedText(e.length,h):this.options.countSelectedText;f=i.replace("{0}",e.length.toString()).replace("{1}",h.toString())}}void 0==this.options.title&&(this.options.title=this.$element.attr("title")),"static"==this.options.selectedTextFormat&&(f=this.options.title),f||(f="undefined"!=typeof this.options.title?this.options.title:this.options.noneSelectedText),this.$button.attr("title",a.trim(f.replace(/<[^>]*>?/g,""))),this.$button.children(".filter-option").html(f),this.$element.trigger("rendered.bs.select")},setStyle:function(a,b){this.$element.attr("class")&&this.$newElement.addClass(this.$element.attr("class").replace(/selectpicker|mobile-device|bs-select-hidden|validate\[.*\]/gi,""));var c=a?a:this.options.style;"add"==b?this.$button.addClass(c):"remove"==b?this.$button.removeClass(c):(this.$button.removeClass(this.options.style),this.$button.addClass(c))},liHeight:function(b){if(b||this.options.size!==!1&&!this.sizeInfo){var c=document.createElement("div"),d=document.createElement("div"),e=document.createElement("ul"),f=document.createElement("li"),g=document.createElement("li"),h=document.createElement("a"),i=document.createElement("span"),j=this.options.header?this.$menu.find(".popover-title")[0].cloneNode(!0):null,k=this.options.liveSearch?document.createElement("div"):null,l=this.options.actionsBox&&this.multiple?this.$menu.find(".bs-actionsbox")[0].cloneNode(!0):null,m=this.options.doneButton&&this.multiple?this.$menu.find(".bs-donebutton")[0].cloneNode(!0):null;if(i.className="text",c.className=this.$menu[0].parentNode.className+" open",d.className="dropdown-menu open",e.className="dropdown-menu inner",f.className="divider",i.appendChild(document.createTextNode("Inner text")),h.appendChild(i),g.appendChild(h),e.appendChild(g),e.appendChild(f),j&&d.appendChild(j),k){var n=document.createElement("span");k.className="bs-searchbox",n.className="form-control",k.appendChild(n),d.appendChild(k)}l&&d.appendChild(l),d.appendChild(e),m&&d.appendChild(m),c.appendChild(d),document.body.appendChild(c);var o=h.offsetHeight,p=j?j.offsetHeight:0,q=k?k.offsetHeight:0,r=l?l.offsetHeight:0,s=m?m.offsetHeight:0,t=a(f).outerHeight(!0),u=getComputedStyle?getComputedStyle(d):!1,v=u?a(d):null,w=parseInt(u?u.paddingTop:v.css("paddingTop"))+parseInt(u?u.paddingBottom:v.css("paddingBottom"))+parseInt(u?u.borderTopWidth:v.css("borderTopWidth"))+parseInt(u?u.borderBottomWidth:v.css("borderBottomWidth")),x=w+parseInt(u?u.marginTop:v.css("marginTop"))+parseInt(u?u.marginBottom:v.css("marginBottom"))+2;document.body.removeChild(c),this.sizeInfo={liHeight:o,headerHeight:p,searchHeight:q,actionsHeight:r,doneButtonHeight:s,dividerHeight:t,menuPadding:w,menuExtras:x}}},setSize:function(){this.findLis(),this.liHeight();var b,c,d,e,f=this,g=this.$menu,h=this.$menuInner,i=a(window),j=this.$newElement[0].offsetHeight,k=this.sizeInfo.liHeight,l=this.sizeInfo.headerHeight,m=this.sizeInfo.searchHeight,n=this.sizeInfo.actionsHeight,o=this.sizeInfo.doneButtonHeight,p=this.sizeInfo.dividerHeight,q=this.sizeInfo.menuPadding,r=this.sizeInfo.menuExtras,s=this.options.hideDisabled?".disabled":"",t=function(){d=f.$newElement.offset().top-i.scrollTop(),e=i.height()-d-j};if(t(),this.options.header&&g.css("padding-top",0),"auto"===this.options.size){var u=function(){var i,j=function(b,c){return function(d){return c?d.classList?d.classList.contains(b):a(d).hasClass(b):!(d.classList?d.classList.contains(b):a(d).hasClass(b))}},p=f.$menuInner[0].getElementsByTagName("li"),s=Array.prototype.filter?Array.prototype.filter.call(p,j("hidden",!1)):f.$lis.not(".hidden"),u=Array.prototype.filter?Array.prototype.filter.call(s,j("dropdown-header",!0)):s.filter(".dropdown-header");t(),b=e-r,f.options.container?(g.data("height")||g.data("height",g.height()),c=g.data("height")):c=g.height(),f.options.dropupAuto&&f.$newElement.toggleClass("dropup",d>e&&c>b-r),f.$newElement.hasClass("dropup")&&(b=d-r),i=s.length+u.length>3?3*k+r-2:0,g.css({"max-height":b+"px",overflow:"hidden","min-height":i+l+m+n+o+"px"}),h.css({"max-height":b-l-m-n-o-q+"px","overflow-y":"auto","min-height":Math.max(i-q,0)+"px"})};u(),this.$searchbox.off("input.getSize propertychange.getSize").on("input.getSize propertychange.getSize",u),i.off("resize.getSize scroll.getSize").on("resize.getSize scroll.getSize",u)}else if(this.options.size&&"auto"!=this.options.size&&this.$lis.not(s).length>this.options.size){var v=this.$lis.not(".divider").not(s).children().slice(0,this.options.size).last().parent().index(),w=this.$lis.slice(0,v+1).filter(".divider").length;b=k*this.options.size+w*p+q,f.options.container?(g.data("height")||g.data("height",g.height()),c=g.data("height")):c=g.height(),f.options.dropupAuto&&this.$newElement.toggleClass("dropup",d>e&&c>b-r),g.css({"max-height":b+l+m+n+o+"px",overflow:"hidden","min-height":""}),h.css({"max-height":b-q+"px","overflow-y":"auto","min-height":""})}},setWidth:function(){if("auto"===this.options.width){this.$menu.css("min-width","0");var a=this.$menu.parent().clone().appendTo("body"),b=this.options.container?this.$newElement.clone().appendTo("body"):a,c=a.children(".dropdown-menu").outerWidth(),d=b.css("width","auto").children("button").outerWidth();a.remove(),b.remove(),this.$newElement.css("width",Math.max(c,d)+"px")}else"fit"===this.options.width?(this.$menu.css("min-width",""),this.$newElement.css("width","").addClass("fit-width")):this.options.width?(this.$menu.css("min-width",""),this.$newElement.css("width",this.options.width)):(this.$menu.css("min-width",""),this.$newElement.css("width",""));this.$newElement.hasClass("fit-width")&&"fit"!==this.options.width&&this.$newElement.removeClass("fit-width")},selectPosition:function(){var b,c,d=this,e="<div />",f=a(e),g=function(a){f.addClass(a.attr("class").replace(/form-control|fit-width/gi,"")).toggleClass("dropup",a.hasClass("dropup")),b=a.offset(),c=a.hasClass("dropup")?0:a[0].offsetHeight,f.css({top:b.top+c,left:b.left,width:a[0].offsetWidth,position:"absolute"})};this.$newElement.on("click",function(){d.isDisabled()||(g(a(this)),f.appendTo(d.options.container),f.toggleClass("open",!a(this).hasClass("open")),f.append(d.$menu))}),a(window).on("resize scroll",function(){g(d.$newElement)}),this.$element.on("hide.bs.select",function(){d.$menu.data("height",d.$menu.height()),f.detach()})},setSelected:function(a,b,c){if(!c)var c=this.findLis().eq(this.liObj[a]);c.toggleClass("selected",b)},setDisabled:function(a,b,c){if(!c)var c=this.findLis().eq(this.liObj[a]);b?c.addClass("disabled").children("a").attr("href","#").attr("tabindex",-1):c.removeClass("disabled").children("a").removeAttr("href").attr("tabindex",0)},isDisabled:function(){return this.$element[0].disabled},checkDisabled:function(){var a=this;this.isDisabled()?(this.$newElement.addClass("disabled"),this.$button.addClass("disabled").attr("tabindex",-1)):(this.$button.hasClass("disabled")&&(this.$newElement.removeClass("disabled"),this.$button.removeClass("disabled")),-1!=this.$button.attr("tabindex")||this.$element.data("tabindex")||this.$button.removeAttr("tabindex")),this.$button.click(function(){return!a.isDisabled()})},tabIndex:function(){this.$element.is("[tabindex]")&&(this.$element.data("tabindex",this.$element.attr("tabindex")),this.$button.attr("tabindex",this.$element.data("tabindex")))},clickListener:function(){var b=this,c=a(document);this.$newElement.on("touchstart.dropdown",".dropdown-menu",function(a){a.stopPropagation()}),c.data("spaceSelect",!1),this.$button.on("keyup",function(a){/(32)/.test(a.keyCode.toString(10))&&c.data("spaceSelect")&&(a.preventDefault(),c.data("spaceSelect",!1))}),this.$newElement.on("click",function(){b.setSize(),b.$element.on("shown.bs.select",function(){if(b.options.liveSearch||b.multiple){if(!b.multiple){var a=b.liObj[b.$element[0].selectedIndex];if("number"!=typeof a)return;var c=b.$lis.eq(a)[0].offsetTop-b.$menuInner[0].offsetTop;c=c-b.$menuInner[0].offsetHeight/2+b.sizeInfo.liHeight/2,b.$menuInner[0].scrollTop=c}}else b.$menu.find(".selected a").focus()})}),this.$menu.on("click","li a",function(c){var d=a(this),e=d.parent().data("originalIndex"),f=b.$element.val(),g=b.$element.prop("selectedIndex");if(b.multiple&&c.stopPropagation(),c.preventDefault(),!b.isDisabled()&&!d.parent().hasClass("disabled")){var h=b.$element.find("option"),i=h.eq(e),j=i.prop("selected"),k=i.parent("optgroup"),l=b.options.maxOptions,m=k.data("maxOptions")||!1;if(b.multiple){if(i.prop("selected",!j),b.setSelected(e,!j),d.blur(),l!==!1||m!==!1){var n=l<h.filter(":selected").length,o=m<k.find("option:selected").length;if(l&&n||m&&o)if(l&&1==l)h.prop("selected",!1),i.prop("selected",!0),b.$menu.find(".selected").removeClass("selected"),b.setSelected(e,!0);else if(m&&1==m){k.find("option:selected").prop("selected",!1),i.prop("selected",!0);var p=d.parent().data("optgroup");b.$menu.find('[data-optgroup="'+p+'"]').removeClass("selected"),b.setSelected(e,!0)}else{var q="function"==typeof b.options.maxOptionsText?b.options.maxOptionsText(l,m):b.options.maxOptionsText,r=q[0].replace("{n}",l),s=q[1].replace("{n}",m),t=a('<div class="notify"></div>');q[2]&&(r=r.replace("{var}",q[2][l>1?0:1]),s=s.replace("{var}",q[2][m>1?0:1])),i.prop("selected",!1),b.$menu.append(t),l&&n&&(t.append(a("<div>"+r+"</div>")),b.$element.trigger("maxReached.bs.select")),m&&o&&(t.append(a("<div>"+s+"</div>")),b.$element.trigger("maxReachedGrp.bs.select")),setTimeout(function(){b.setSelected(e,!1)},10),t.delay(750).fadeOut(300,function(){a(this).remove()})}}}else h.prop("selected",!1),i.prop("selected",!0),b.$menu.find(".selected").removeClass("selected"),b.setSelected(e,!0);b.multiple?b.options.liveSearch&&b.$searchbox.focus():b.$button.focus(),(f!=b.$element.val()&&b.multiple||g!=b.$element.prop("selectedIndex")&&!b.multiple)&&(b.$element.change(),b.$element.trigger("changed.bs.select",[e,i.prop("selected"),j]))}}),this.$menu.on("click","li.disabled a, .popover-title, .popover-title :not(.close)",function(c){c.currentTarget==this&&(c.preventDefault(),c.stopPropagation(),b.options.liveSearch&&!a(c.target).hasClass("close")?b.$searchbox.focus():b.$button.focus())}),this.$menu.on("click","li.divider, li.dropdown-header",function(a){a.preventDefault(),a.stopPropagation(),b.options.liveSearch?b.$searchbox.focus():b.$button.focus()}),this.$menu.on("click",".popover-title .close",function(){b.$button.click()}),this.$searchbox.on("click",function(a){a.stopPropagation()}),this.$menu.on("click",".actions-btn",function(c){b.options.liveSearch?b.$searchbox.focus():b.$button.focus(),c.preventDefault(),c.stopPropagation(),a(this).hasClass("bs-select-all")?b.selectAll():b.deselectAll(),b.$element.change()}),this.$element.change(function(){b.render(!1)})},liveSearchListener:function(){var d=this,e=a('<li class="no-results"></li>');this.$newElement.on("click.dropdown.data-api touchstart.dropdown.data-api",function(){d.$menuInner.find(".active").removeClass("active"),d.$searchbox.val()&&(d.$searchbox.val(""),d.$lis.not(".is-hidden").removeClass("hidden"),e.parent().length&&e.remove()),d.multiple||d.$menuInner.find(".selected").addClass("active"),setTimeout(function(){d.$searchbox.focus()},10)}),this.$searchbox.on("click.dropdown.data-api focus.dropdown.data-api touchend.dropdown.data-api",function(a){a.stopPropagation()}),this.$searchbox.on("input propertychange",function(){if(d.$searchbox.val()){var f=d.$lis.not(".is-hidden").removeClass("hidden").children("a");f=d.options.liveSearchNormalize?f.not(":a"+d._searchStyle()+"("+b(d.$searchbox.val())+")"):f.not(":"+d._searchStyle()+"("+d.$searchbox.val()+")"),f.parent().addClass("hidden"),d.$lis.filter(".dropdown-header").each(function(){var b=a(this),c=b.data("optgroup");0===d.$lis.filter("[data-optgroup="+c+"]").not(b).not(".hidden").length&&(b.addClass("hidden"),d.$lis.filter("[data-optgroup="+c+"div]").addClass("hidden"))});var g=d.$lis.not(".hidden");g.each(function(b){var c=a(this);c.hasClass("divider")&&(c.index()===g.eq(0).index()||c.index()===g.last().index()||g.eq(b+1).hasClass("divider"))&&c.addClass("hidden")}),d.$lis.not(".hidden, .no-results").length?e.parent().length&&e.remove():(e.parent().length&&e.remove(),e.html(d.options.noneResultsText.replace("{0}",'"'+c(d.$searchbox.val())+'"')).show(),d.$menuInner.append(e))}else d.$lis.not(".is-hidden").removeClass("hidden"),e.parent().length&&e.remove();d.$lis.filter(".active").removeClass("active"),d.$lis.not(".hidden, .divider, .dropdown-header").eq(0).addClass("active").children("a").focus(),a(this).focus()})},_searchStyle:function(){var a="icontains";switch(this.options.liveSearchStyle){case"begins":case"startsWith":a="ibegins";break;case"contains":}return a},val:function(a){return"undefined"!=typeof a?(this.$element.val(a),this.render(),this.$element):this.$element.val()},selectAll:function(){this.findLis(),this.$element.find("option:enabled").not("[data-divider], [data-hidden]").prop("selected",!0),this.$lis.not(".divider, .dropdown-header, .disabled, .hidden").addClass("selected"),this.render(!1)},deselectAll:function(){this.findLis(),this.$element.find("option:enabled").not("[data-divider], [data-hidden]").prop("selected",!1),this.$lis.not(".divider, .dropdown-header, .disabled, .hidden").removeClass("selected"),this.render(!1)},keydown:function(c){var d,e,f,g,h,i,j,k,l,m=a(this),n=m.is("input")?m.parent().parent():m.parent(),o=n.data("this"),p=":not(.disabled, .hidden, .dropdown-header, .divider)",q={32:" ",48:"0",49:"1",50:"2",51:"3",52:"4",53:"5",54:"6",55:"7",56:"8",57:"9",59:";",65:"a",66:"b",67:"c",68:"d",69:"e",70:"f",71:"g",72:"h",73:"i",74:"j",75:"k",76:"l",77:"m",78:"n",79:"o",80:"p",81:"q",82:"r",83:"s",84:"t",85:"u",86:"v",87:"w",88:"x",89:"y",90:"z",96:"0",97:"1",98:"2",99:"3",100:"4",101:"5",102:"6",103:"7",104:"8",105:"9"};if(o.options.liveSearch&&(n=m.parent().parent()),o.options.container&&(n=o.$menu),d=a("[role=menu] li a",n),l=o.$menu.parent().hasClass("open"),!l&&(c.keyCode>=48&&c.keyCode<=57||event.keyCode>=65&&event.keyCode<=90)&&(o.options.container?o.$newElement.trigger("click"):(o.setSize(),o.$menu.parent().addClass("open"),l=!0),o.$searchbox.focus()),o.options.liveSearch&&(/(^9$|27)/.test(c.keyCode.toString(10))&&l&&0===o.$menu.find(".active").length&&(c.preventDefault(),o.$menu.parent().removeClass("open"),o.options.container&&o.$newElement.removeClass("open"),o.$button.focus()),d=a("[role=menu] li:not(.disabled, .hidden, .dropdown-header, .divider)",n),m.val()||/(38|40)/.test(c.keyCode.toString(10))||0===d.filter(".active").length&&(d=o.$newElement.find("li"),d=o.options.liveSearchNormalize?d.filter(":a"+o._searchStyle()+"("+b(q[c.keyCode])+")"):d.filter(":"+o._searchStyle()+"("+q[c.keyCode]+")"))),d.length){if(/(38|40)/.test(c.keyCode.toString(10)))e=d.index(d.filter(":focus")),g=d.parent(p).first().data("originalIndex"),h=d.parent(p).last().data("originalIndex"),f=d.eq(e).parent().nextAll(p).eq(0).data("originalIndex"),i=d.eq(e).parent().prevAll(p).eq(0).data("originalIndex"),j=d.eq(f).parent().prevAll(p).eq(0).data("originalIndex"),o.options.liveSearch&&(d.each(function(b){a(this).hasClass("disabled")||a(this).data("index",b)}),e=d.index(d.filter(".active")),g=d.first().data("index"),h=d.last().data("index"),f=d.eq(e).nextAll().eq(0).data("index"),i=d.eq(e).prevAll().eq(0).data("index"),j=d.eq(f).prevAll().eq(0).data("index")),k=m.data("prevIndex"),38==c.keyCode?(o.options.liveSearch&&(e-=1),e!=j&&e>i&&(e=i),g>e&&(e=g),e==k&&(e=h)):40==c.keyCode&&(o.options.liveSearch&&(e+=1),-1==e&&(e=0),e!=j&&f>e&&(e=f),e>h&&(e=h),e==k&&(e=g)),m.data("prevIndex",e),o.options.liveSearch?(c.preventDefault(),m.hasClass("dropdown-toggle")||(d.removeClass("active").eq(e).addClass("active").children("a").focus(),m.focus())):d.eq(e).focus();else if(!m.is("input")){var r,s,t=[];d.each(function(){a(this).parent().hasClass("disabled")||a.trim(a(this).text().toLowerCase()).substring(0,1)==q[c.keyCode]&&t.push(a(this).parent().index())}),r=a(document).data("keycount"),r++,a(document).data("keycount",r),s=a.trim(a(":focus").text().toLowerCase()).substring(0,1),s!=q[c.keyCode]?(r=1,a(document).data("keycount",r)):r>=t.length&&(a(document).data("keycount",0),r>t.length&&(r=1)),d.eq(t[r-1]).focus()}if((/(13|32)/.test(c.keyCode.toString(10))||/(^9$)/.test(c.keyCode.toString(10))&&o.options.selectOnTab)&&l){if(/(32)/.test(c.keyCode.toString(10))||c.preventDefault(),o.options.liveSearch)/(32)/.test(c.keyCode.toString(10))||(o.$menu.find(".active a").click(),m.focus());else{var u=a(":focus");u.click(),u.focus(),c.preventDefault(),a(document).data("spaceSelect",!0)}a(document).data("keycount",0)}(/(^9$|27)/.test(c.keyCode.toString(10))&&l&&(o.multiple||o.options.liveSearch)||/(27)/.test(c.keyCode.toString(10))&&!l)&&(o.$menu.parent().removeClass("open"),o.options.container&&o.$newElement.removeClass("open"),o.$button.focus())}},mobile:function(){this.$element.addClass("mobile-device").appendTo(this.$newElement),this.options.container&&this.$menu.hide()},refresh:function(){this.$lis=null,this.reloadLi(),this.render(),this.checkDisabled(),this.liHeight(!0),this.setStyle(),this.setWidth(),this.$lis&&this.$searchbox.trigger("propertychange"),this.$element.trigger("refreshed.bs.select")},hide:function(){this.$newElement.hide()},show:function(){this.$newElement.show()},remove:function(){this.$newElement.remove(),this.$element.remove()}};var f=a.fn.selectpicker;a.fn.selectpicker=d,a.fn.selectpicker.Constructor=e,a.fn.selectpicker.noConflict=function(){return a.fn.selectpicker=f,this},a(document).data("keycount",0).on("keydown",'.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="menu"], .bs-searchbox input',e.prototype.keydown).on("focusin.modal",'.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="menu"], .bs-searchbox input',function(a){a.stopPropagation()}),a(window).on("load.bs.select.data-api",function(){a(".selectpicker").each(function(){var b=a(this);d.call(b,b.data())})})}(jQuery)});
//# sourceMappingURL=bootstrap-select.js.map
/*!
 * Ajax Bootstrap Select
 *
 * Extends existing [Bootstrap Select] implementations by adding the ability to search via AJAX requests as you type. Originally for CROSCON.
 *
 * @version 1.3.4
 * @author Adam Heim - https://github.com/truckingsim
 * @link https://github.com/truckingsim/Ajax-Bootstrap-Select
 * @copyright 2015 Adam Heim
 * @license Released under the MIT license.
 *
 * Contributors:
 *   Mark Carver - https://github.com/markcarver
 *
 * Last build: 2015-10-12 3:07:08 PM EDT
 */
!function(a,b){var c=function(c,d){var e,f,g=this;d=d||{},this.$element=a(c),this.options=a.extend(!0,{},a.fn.ajaxSelectPicker.defaults,d),this.LOG_ERROR=1,this.LOG_WARNING=2,this.LOG_INFO=3,this.LOG_DEBUG=4,this.lastRequest=!1,this.previousQuery="",this.query="",this.request=!1;var h=[{from:"ajaxResultsPreHook",to:"preprocessData"},{from:"ajaxSearchUrl",to:{ajax:{url:"{{{value}}}"}}},{from:"ajaxOptions",to:"ajax"},{from:"debug",to:function(b){var c={};c.log=Boolean(g.options[b.from])?g.LOG_DEBUG:0,g.options=a.extend(!0,{},g.options,c),delete g.options[b.from],g.log(g.LOG_WARNING,'Deprecated option "'+b.from+'". Update code to use:',c)}},{from:"mixWithCurrents",to:"preserveSelected"},{from:"placeHolderOption",to:{locale:{emptyTitle:"{{{value}}}"}}}];h.length&&a.map(h,function(b){if(g.options[b.from])if(a.isPlainObject(b.to))g.replaceValue(b.to,"{{{value}}}",g.options[b.from]),g.options=a.extend(!0,{},g.options,b.to),g.log(g.LOG_WARNING,'Deprecated option "'+b.from+'". Update code to use:',b.to),delete g.options[b.from];else if(a.isFunction(b.to))b.to.apply(g,[b]);else{var c={};c[b.to]=g.options[b.from],g.options=a.extend(!0,{},g.options,c),g.log(g.LOG_WARNING,'Deprecated option "'+b.from+'". Update code to use:',c),delete g.options[b.from]}});var i=this.$element.data();i.searchUrl&&(g.log(g.LOG_WARNING,'Deprecated attribute name: "data-search-url". Update markup to use: \' data-abs-ajax-url="'+i.searchUrl+"\" '"),this.options.ajax.url=i.searchUrl);var j=function(a,b){return b.toLowerCase()},k=function(a,b,c){var d=[].concat(a),e=d.length,f=c||{};if(e){var g=d.shift();f[g]=k(d,b,f[g])}return e?f:b},l=Object.keys(i).filter(/./.test.bind(new RegExp("^abs[A-Z]")));if(l.length){var m={},n=["locale"];for(e=0,f=l.length;f>e;e++){var o=l[e].replace(/^abs([A-Z])/,j).replace(/([A-Z])/g,"-$1").toLowerCase(),p=o.split("-");if(p[0]&&p.length>1&&-1!==n.indexOf(p[0])){for(var q=[p.shift()],r="",s=0;s<p.length;s++)r+=0===s?p[s]:p[s].charAt(0).toUpperCase()+p[s].slice(1);q.push(r),p=q}this.log(this.LOG_DEBUG,'Processing data attribute "data-abs-'+o+'":',i[l[e]]),k(p,i[l[e]],m)}this.options=a.extend(!0,{},this.options,m),this.log(this.LOG_DEBUG,"Merged in the data attribute options: ",m,this.options)}if(this.selectpicker=i.selectpicker,!this.selectpicker)return this.log(this.LOG_ERROR,"Cannot instantiate an AjaxBootstrapSelect instance without selectpicker first being initialized!"),null;if(!this.options.ajax.url)return this.log(this.LOG_ERROR,'Option "ajax.url" must be set! Options:',this.options),null;if(this.locale=a.extend(!0,{},a.fn.ajaxSelectPicker.locale),this.options.langCode=this.options.langCode||b.navigator.userLanguage||b.navigator.language||"en",!this.locale[this.options.langCode]){var t=this.options.langCode;this.options.langCode="en";var u=t.split("-");for(e=0,f=u.length;f>e;e++){var v=u.join("-");if(v.length&&this.locale[v]){this.options.langCode=v;break}u.pop()}this.log(this.LOG_WARNING,'Unknown langCode option: "'+t+'". Using the following langCode instead: "'+this.options.langCode+'".')}this.locale[this.options.langCode]=a.extend(!0,{},this.locale[this.options.langCode],this.options.locale),this.list=new b.AjaxBootstrapSelectList(this),this.list.refresh(),setTimeout(function(){g.init()},500)};c.prototype.init=function(){var c,d=this;this.options.preserveSelected&&this.selectpicker.$menu.off("click",".actions-btn").on("click",".actions-btn",function(b){d.selectpicker.options.liveSearch?d.selectpicker.$searchbox.focus():d.selectpicker.$button.focus(),b.preventDefault(),b.stopPropagation(),a(this).is(".bs-select-all")?(null===d.selectpicker.$lis&&(d.selectpicker.$lis=d.selectpicker.$menu.find("li")),d.$element.find("option:enabled").prop("selected",!0),a(d.selectpicker.$lis).not(".disabled").addClass("selected"),d.selectpicker.render()):(null===d.selectpicker.$lis&&(d.selectpicker.$lis=d.selectpicker.$menu.find("li")),d.$element.find("option:enabled").prop("selected",!1),a(d.selectpicker.$lis).not(".disabled").removeClass("selected"),d.selectpicker.render()),d.selectpicker.$element.change()}),this.selectpicker.$searchbox.attr("placeholder",this.t("searchPlaceholder")).off("input propertychange"),this.selectpicker.$searchbox.on(this.options.bindEvent,function(e){var f=d.selectpicker.$searchbox.val();if(d.log(d.LOG_DEBUG,'Bind event fired: "'+d.options.bindEvent+'", keyCode:',e.keyCode,e),d.options.cache||(d.options.ignoredKeys[13]="enter"),d.options.ignoredKeys[e.keyCode])return void d.log(d.LOG_DEBUG,"Key ignored.");if(clearTimeout(c),f.length||(d.options.clearOnEmpty&&d.list.destroy(),d.options.emptyRequest)){if(d.previousQuery=d.query,d.query=f,d.options.cache&&13!==e.keyCode){var g=d.list.cacheGet(d.query);if(g)return d.list.setStatus(g.length?"":d.t("statusNoResults")),d.list.replaceOptions(g),void d.log(d.LOG_INFO,"Rebuilt options from cached data.")}c=setTimeout(function(){d.lastRequest&&d.lastRequest.jqXHR&&a.isFunction(d.lastRequest.jqXHR.abort)&&d.lastRequest.jqXHR.abort(),d.request=new b.AjaxBootstrapSelectRequest(d),d.request.jqXHR.always(function(){d.lastRequest=d.request,d.request=!1})},d.options.requestDelay||300)}})},c.prototype.log=function(a,c){if(b.console&&this.options.log){if("number"!=typeof this.options.log)switch("string"==typeof this.options.log&&(this.options.log=this.options.log.toLowerCase()),this.options.log){case!0:case"debug":this.options.log=this.LOG_DEBUG;break;case"info":this.options.log=this.LOG_INFO;break;case"warn":case"warning":this.options.log=this.LOG_WARNING;break;default:case!1:case"error":this.options.log=this.LOG_ERROR}if(a<=this.options.log){var d=[].slice.apply(arguments,[2]);switch(a){case this.LOG_DEBUG:a="debug";break;case this.LOG_INFO:a="info";break;case this.LOG_WARNING:a="warn";break;default:case this.LOG_ERROR:a="error"}var e="["+a.toUpperCase()+"] AjaxBootstrapSelect:";"string"==typeof c?d.unshift(e+" "+c):(d.unshift(c),d.unshift(e)),b.console[a].apply(b.console,d)}}},c.prototype.replaceValue=function(b,c,d,e){var f=this;e=a.extend({recursive:!0,depth:!1,limit:!1},e),a.each(b,function(g,h){return e.limit!==!1&&"number"==typeof e.limit&&e.limit<=0?!1:void(a.isArray(b[g])||a.isPlainObject(b[g])?(e.recursive&&e.depth===!1||e.recursive&&"number"==typeof e.depth&&e.depth>0)&&f.replaceValue(b[g],c,d,e):h===c&&(e.limit!==!1&&"number"==typeof e.limit&&e.limit--,b[g]=d))})},c.prototype.t=function(a,b){return b=b||this.options.langCode,this.locale[b]&&this.locale[b].hasOwnProperty(a)?this.locale[b][a]:(this.log(this.LOG_WARNING,"Unknown translation key:",a),a)},b.AjaxBootstrapSelect=b.AjaxBootstrapSelect||c;var d=function(b){var c=this;this.$status=a(b.options.templates.status).hide().appendTo(b.selectpicker.$menu);var d=b.t("statusInitialized");d&&d.length&&this.setStatus(d),this.cache={},this.plugin=b,this.selected=[],this.title=null,this.selectedTextFormat=b.selectpicker.options.selectedTextFormat;var e=[];b.$element.find("option").each(function(){var b=a(this),c=b.attr("value");e.push({value:c,text:b.text(),"class":b.attr("class")||"",data:b.data()||{},preserved:!1,selected:!1})}),this.cacheSet("",e),b.options.preserveSelected&&b.$element.on("change.abs.preserveSelected",function(){var d=b.$element.find(":selected");c.selected=[],b.selectpicker.multiple||(d=d.last()),d.each(function(){var b=a(this),d=b.attr("value");c.selected.push({value:d,text:b.text(),"class":b.attr("class")||"",data:b.data()||{},preserved:!0,selected:!0})}),c.replaceOptions(c.cacheGet(c.plugin.query))})};d.prototype.build=function(b){var c,d,e=b.length,f=a("<select/>"),g=a("<optgroup/>").attr("label",this.plugin.t("currentlySelected"));for(this.plugin.log(this.plugin.LOG_DEBUG,"Building the select list options from data:",b),d=0;e>d;d++){var h=b[d],i=a("<option/>").appendTo(h.preserved?g:f);if(h.hasOwnProperty("divider"))i.attr("data-divider","true");else{i.val(h.value).text(h.text),h["class"].length&&i.attr("class",h["class"]),h.disabled&&i.attr("disabled",!0),h.selected&&!this.plugin.selectpicker.multiple&&f.find(":selected").prop("selected",!1),h.selected&&i.attr("selected",!0);for(c in h.data)h.data.hasOwnProperty(c)&&i.attr("data-"+c,h.data[c])}}g.find("option").length&&g["before"===this.plugin.options.preserveSelectedPosition?"prependTo":"appendTo"](f);var j=f.html();return this.plugin.log(this.plugin.LOG_DEBUG,j),j},d.prototype.cacheGet=function(a,b){var c=this.cache[a]||b;return this.plugin.log(this.LOG_DEBUG,"Retrieving cache:",a,c),c},d.prototype.cacheSet=function(a,b){this.cache[a]=b,this.plugin.log(this.LOG_DEBUG,"Saving to cache:",a,b)},d.prototype.destroy=function(){this.replaceOptions(),this.plugin.list.setStatus(),this.plugin.log(this.plugin.LOG_DEBUG,"Destroyed select list.")},d.prototype.refresh=function(a){this.plugin.selectpicker.$menu.css("minHeight",0),this.plugin.selectpicker.$menu.find("> .inner").css("minHeight",0);var b=this.plugin.t("emptyTitle");!this.plugin.$element.find("option").length&&b&&b.length?this.setTitle(b):this.title&&this.restoreTitle(),this.plugin.selectpicker.refresh(),this.plugin.selectpicker.findLis(),a&&(this.plugin.log(this.plugin.LOG_DEBUG,"Triggering Change"),this.plugin.$element.trigger("change.$")),this.plugin.log(this.plugin.LOG_DEBUG,"Refreshed select list.")},d.prototype.replaceOptions=function(a){var b,c,d,e="",f=[],g=[],h=[];if(a=a||[],this.selected&&this.selected.length){for(this.plugin.log(this.plugin.LOG_INFO,"Processing preserved selections:",this.selected),g=[].concat(this.selected,a),c=g.length,b=0;c>b;b++)d=g[b],d.hasOwnProperty("value")&&-1===h.indexOf(d.value+"")?(h.push(d.value+""),f.push(d)):this.plugin.log(this.plugin.LOG_DEBUG,"Duplicate item found, ignoring.");a=f}a.length&&(e=this.plugin.list.build(a)),this.plugin.$element.html(e),this.refresh(),this.plugin.log(this.plugin.LOG_DEBUG,"Replaced options with data:",a)},d.prototype.restore=function(){var a=this.plugin.list.cacheGet(this.plugin.previousQuery);return a&&this.plugin.list.replaceOptions(a)&&this.plugin.log(this.plugin.LOG_DEBUG,"Restored select list to the previous query: ",this.plugin.previousQuery),this.plugin.log(this.plugin.LOG_DEBUG,"Unable to restore select list to the previous query:",this.plugin.previousQuery),!1},d.prototype.restoreTitle=function(){this.plugin.request||(this.plugin.selectpicker.options.selectedTextFormat=this.selectedTextFormat,this.title?this.plugin.$element.attr("title",this.title):this.plugin.$element.removeAttr("title"),this.title=null)},d.prototype.setTitle=function(a){this.plugin.request||(this.title=this.plugin.$element.attr("title"),this.plugin.selectpicker.options.selectedTextFormat="static",this.plugin.$element.attr("title",a))},d.prototype.setStatus=function(a){a=a||"",a.length?this.$status.html(a).show():this.$status.html("").hide()},b.AjaxBootstrapSelectList=b.AjaxBootstrapSelectList||d;var e=function(b){var c,d=this,e=function(a){return function(){d.plugin.log(d.plugin.LOG_INFO,"Invoking AjaxBootstrapSelectRequest."+a+" callback:",arguments),d[a].apply(d,arguments),d.callbacks[a]&&(d.plugin.log(d.plugin.LOG_INFO,"Invoking ajax."+a+" callback:",arguments),d.callbacks[a].apply(d,arguments))}},f=["beforeSend","success","error","complete"],g=f.length;for(this.plugin=b,this.options=a.extend(!0,{},b.options.ajax),this.callbacks={},c=0;g>c;c++){var h=f[c];this.options[h]&&a.isFunction(this.options[h])&&(this.callbacks[h]=this.options[h]),this.options[h]=e(h)}this.options.data&&a.isFunction(this.options.data)&&(this.options.data=this.options.data.apply(this)||{q:"{{{q}}}"}),this.plugin.replaceValue(this.options.data,"{{{q}}}",this.plugin.query),this.jqXHR=a.ajax(this.options)};e.prototype.beforeSend=function(){this.plugin.list.destroy(),this.plugin.list.setStatus(this.plugin.t("statusSearching"))},e.prototype.complete=function(a,b){if("abort"!==b){var c=this.plugin.list.cacheGet(this.plugin.query);if(c){if(!c.length)return this.plugin.list.destroy(),this.plugin.list.setStatus(this.plugin.t("statusNoResults")),void this.plugin.log(this.plugin.LOG_INFO,"No results were returned.");this.plugin.list.setStatus()}this.plugin.list.refresh(!0)}},e.prototype.error=function(a,b){"abort"!==b&&(this.plugin.list.cacheSet(this.plugin.query),this.plugin.options.clearOnError&&this.plugin.list.destroy(),this.plugin.list.setStatus(this.plugin.t("errorText")),this.plugin.options.restoreOnError&&(this.plugin.list.restore(),this.plugin.list.setStatus()))},e.prototype.process=function(b){var c,d,e,f,g,h,i=[],j=[];if(this.plugin.log(this.plugin.LOG_INFO,"Processing raw data for:",this.plugin.query,b),g=b,a.isFunction(this.plugin.options.preprocessData)&&(this.plugin.log(this.plugin.LOG_DEBUG,"Invoking preprocessData callback:",this.plugin.options.processData),e=this.plugin.options.preprocessData.apply(this,[g]),"undefined"!=typeof e&&null!==e&&e!==!1&&(g=e)),!a.isArray(g))return this.plugin.log(this.plugin.LOG_ERROR,'The data returned is not an Array. Use the "preprocessData" callback option to parse the results and construct a proper array for this plugin.',g),!1;for(d=g.length,c=0;d>c;c++)f=g[c],this.plugin.log(this.plugin.LOG_DEBUG,"Processing item:",f),a.isPlainObject(f)&&(f.hasOwnProperty("divider")||f.hasOwnProperty("data")&&a.isPlainObject(f.data)&&f.data.divider?(this.plugin.log(this.plugin.LOG_DEBUG,"Item is a divider, ignoring provided data."),i.push({divider:!0})):f.hasOwnProperty("value")?-1===j.indexOf(f.value+"")?(j.push(f.value+""),f=a.extend({text:f.value,"class":"",data:{},disabled:!1,selected:!1},f),i.push(f)):this.plugin.log(this.plugin.LOG_DEBUG,"Duplicate item found, ignoring."):this.plugin.log(this.plugin.LOG_DEBUG,'Data item must have a "value" property, skipping.'));if(h=[].concat(i),a.isFunction(this.plugin.options.processData)&&(this.plugin.log(this.plugin.LOG_DEBUG,"Invoking processData callback:",this.plugin.options.processData),e=this.plugin.options.processData.apply(this,[h]),"undefined"!=typeof e&&null!==e&&e!==!1)){if(!a.isArray(e))return this.plugin.log(this.plugin.LOG_ERROR,"The processData callback did not return an array.",e),!1;h=e}return this.plugin.list.cacheSet(this.plugin.query,h),this.plugin.log(this.plugin.LOG_INFO,"Processed data:",h),h},e.prototype.success=function(b){if(!a.isArray(b)&&!a.isPlainObject(b))return this.plugin.log(this.plugin.LOG_ERROR,"Request did not return a JSON Array or Object.",b),void this.plugin.list.destroy();var c=this.process(b);this.plugin.list.replaceOptions(c)},b.AjaxBootstrapSelectRequest=b.AjaxBootstrapSelectRequest||e,a.fn.ajaxSelectPicker=function(c){return this.each(function(){a(this).data("AjaxBootstrapSelect")||a(this).data("AjaxBootstrapSelect",new b.AjaxBootstrapSelect(this,c))})},a.fn.ajaxSelectPicker.locale={},a.fn.ajaxSelectPicker.defaults={ajax:{url:null,type:"POST",dataType:"json",data:{q:"{{{q}}}"}},bindEvent:"keyup",cache:!0,clearOnEmpty:!0,clearOnError:!0,emptyRequest:!1,ignoredKeys:{9:"tab",16:"shift",17:"ctrl",18:"alt",27:"esc",37:"left",39:"right",38:"up",40:"down",91:"meta",229:"unknown"},langCode:null,locale:null,log:"error",preprocessData:function(){},preserveSelected:!0,preserveSelectedPosition:"after",processData:function(){},requestDelay:300,restoreOnError:!1,templates:{status:'<div class="status"></div>'}},/*!
 * English translation for the "en-US" and "en" language codes.
 * Mark Carver <mark.carver@me.com>
 */
a.fn.ajaxSelectPicker.locale["en-US"]={currentlySelected:"Currently Selected",emptyTitle:"Select and begin typing",errorText:"Unable to retrieve results",searchPlaceholder:"Search...",statusInitialized:"Start typing a search query",statusNoResults:"No Results",statusSearching:"Searching..."},a.fn.ajaxSelectPicker.locale.en=a.fn.ajaxSelectPicker.locale["en-US"]}(jQuery,window);
/*!
 * Ajax Bootstrap Select
 *
 * Extends existing [Bootstrap Select] implementations by adding the ability to search via AJAX requests as you type. Originally for CROSCON.
 *
 * @version 1.3.4
 * @author Adam Heim - https://github.com/truckingsim
 * @link https://github.com/truckingsim/Ajax-Bootstrap-Select
 * @copyright 2015 Adam Heim
 * @license Released under the MIT license.
 *
 * Contributors:
 *   Mark Carver - https://github.com/markcarver
 *
 * Last build: 2015-10-12 3:07:08 PM EDT
 */
!(function ($) {
/*!
 * English translation for the "en-US" and "en" language codes.
 * Mark Carver <mark.carver@me.com>
 */
$.fn.ajaxSelectPicker.locale["en-US"]={currentlySelected:"Выбранный в данный момент",emptyTitle:"Выберите и начните вводить",errorText:"Не удалось получить результаты",searchPlaceholder:"Поиск...",statusInitialized:"Начните вводить поисковый запрос",statusNoResults:"Нет результатов",statusSearching:"Идет поиск ..."},$.fn.ajaxSelectPicker.locale.en=$.fn.ajaxSelectPicker.locale["en-US"];})(jQuery);

/*! Sortable 1.4.2 - MIT | git://github.com/rubaxa/Sortable.git */
!function(a){"use strict";"function"==typeof define&&define.amd?define(a):"undefined"!=typeof module&&"undefined"!=typeof module.exports?module.exports=a():"undefined"!=typeof Package?Sortable=a():window.Sortable=a()}(function(){"use strict";function a(a,b){if(!a||!a.nodeType||1!==a.nodeType)throw"Sortable: `el` must be HTMLElement, and not "+{}.toString.call(a);this.el=a,this.options=b=r({},b),a[L]=this;var c={group:Math.random(),sort:!0,disabled:!1,store:null,handle:null,scroll:!0,scrollSensitivity:30,scrollSpeed:10,draggable:/[uo]l/i.test(a.nodeName)?"li":">*",ghostClass:"sortable-ghost",chosenClass:"sortable-chosen",ignore:"a, img",filter:null,animation:0,setData:function(a,b){a.setData("Text",b.textContent)},dropBubble:!1,dragoverBubble:!1,dataIdAttr:"data-id",delay:0,forceFallback:!1,fallbackClass:"sortable-fallback",fallbackOnBody:!1};for(var d in c)!(d in b)&&(b[d]=c[d]);V(b);for(var f in this)"_"===f.charAt(0)&&(this[f]=this[f].bind(this));this.nativeDraggable=b.forceFallback?!1:P,e(a,"mousedown",this._onTapStart),e(a,"touchstart",this._onTapStart),this.nativeDraggable&&(e(a,"dragover",this),e(a,"dragenter",this)),T.push(this._onDragOver),b.store&&this.sort(b.store.get(this))}function b(a){v&&v.state!==a&&(h(v,"display",a?"none":""),!a&&v.state&&w.insertBefore(v,s),v.state=a)}function c(a,b,c){if(a){c=c||N,b=b.split(".");var d=b.shift().toUpperCase(),e=new RegExp("\\s("+b.join("|")+")(?=\\s)","g");do if(">*"===d&&a.parentNode===c||(""===d||a.nodeName.toUpperCase()==d)&&(!b.length||((" "+a.className+" ").match(e)||[]).length==b.length))return a;while(a!==c&&(a=a.parentNode))}return null}function d(a){a.dataTransfer&&(a.dataTransfer.dropEffect="move"),a.preventDefault()}function e(a,b,c){a.addEventListener(b,c,!1)}function f(a,b,c){a.removeEventListener(b,c,!1)}function g(a,b,c){if(a)if(a.classList)a.classList[c?"add":"remove"](b);else{var d=(" "+a.className+" ").replace(K," ").replace(" "+b+" "," ");a.className=(d+(c?" "+b:"")).replace(K," ")}}function h(a,b,c){var d=a&&a.style;if(d){if(void 0===c)return N.defaultView&&N.defaultView.getComputedStyle?c=N.defaultView.getComputedStyle(a,""):a.currentStyle&&(c=a.currentStyle),void 0===b?c:c[b];b in d||(b="-webkit-"+b),d[b]=c+("string"==typeof c?"":"px")}}function i(a,b,c){if(a){var d=a.getElementsByTagName(b),e=0,f=d.length;if(c)for(;f>e;e++)c(d[e],e);return d}return[]}function j(a,b,c,d,e,f,g){var h=N.createEvent("Event"),i=(a||b[L]).options,j="on"+c.charAt(0).toUpperCase()+c.substr(1);h.initEvent(c,!0,!0),h.to=b,h.from=e||b,h.item=d||b,h.clone=v,h.oldIndex=f,h.newIndex=g,b.dispatchEvent(h),i[j]&&i[j].call(a,h)}function k(a,b,c,d,e,f){var g,h,i=a[L],j=i.options.onMove;return g=N.createEvent("Event"),g.initEvent("move",!0,!0),g.to=b,g.from=a,g.dragged=c,g.draggedRect=d,g.related=e||b,g.relatedRect=f||b.getBoundingClientRect(),a.dispatchEvent(g),j&&(h=j.call(i,g)),h}function l(a){a.draggable=!1}function m(){R=!1}function n(a,b){var c=a.lastElementChild,d=c.getBoundingClientRect();return(b.clientY-(d.top+d.height)>5||b.clientX-(d.right+d.width)>5)&&c}function o(a){for(var b=a.tagName+a.className+a.src+a.href+a.textContent,c=b.length,d=0;c--;)d+=b.charCodeAt(c);return d.toString(36)}function p(a){var b=0;if(!a||!a.parentNode)return-1;for(;a&&(a=a.previousElementSibling);)"TEMPLATE"!==a.nodeName.toUpperCase()&&b++;return b}function q(a,b){var c,d;return function(){void 0===c&&(c=arguments,d=this,setTimeout(function(){1===c.length?a.call(d,c[0]):a.apply(d,c),c=void 0},b))}}function r(a,b){if(a&&b)for(var c in b)b.hasOwnProperty(c)&&(a[c]=b[c]);return a}var s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J={},K=/\s+/g,L="Sortable"+(new Date).getTime(),M=window,N=M.document,O=M.parseInt,P=!!("draggable"in N.createElement("div")),Q=function(a){return a=N.createElement("x"),a.style.cssText="pointer-events:auto","auto"===a.style.pointerEvents}(),R=!1,S=Math.abs,T=([].slice,[]),U=q(function(a,b,c){if(c&&b.scroll){var d,e,f,g,h=b.scrollSensitivity,i=b.scrollSpeed,j=a.clientX,k=a.clientY,l=window.innerWidth,m=window.innerHeight;if(z!==c&&(y=b.scroll,z=c,y===!0)){y=c;do if(y.offsetWidth<y.scrollWidth||y.offsetHeight<y.scrollHeight)break;while(y=y.parentNode)}y&&(d=y,e=y.getBoundingClientRect(),f=(S(e.right-j)<=h)-(S(e.left-j)<=h),g=(S(e.bottom-k)<=h)-(S(e.top-k)<=h)),f||g||(f=(h>=l-j)-(h>=j),g=(h>=m-k)-(h>=k),(f||g)&&(d=M)),(J.vx!==f||J.vy!==g||J.el!==d)&&(J.el=d,J.vx=f,J.vy=g,clearInterval(J.pid),d&&(J.pid=setInterval(function(){d===M?M.scrollTo(M.pageXOffset+f*i,M.pageYOffset+g*i):(g&&(d.scrollTop+=g*i),f&&(d.scrollLeft+=f*i))},24)))}},30),V=function(a){var b=a.group;b&&"object"==typeof b||(b=a.group={name:b}),["pull","put"].forEach(function(a){a in b||(b[a]=!0)}),a.groups=" "+b.name+(b.put.join?" "+b.put.join(" "):"")+" "};return a.prototype={constructor:a,_onTapStart:function(a){var b=this,d=this.el,e=this.options,f=a.type,g=a.touches&&a.touches[0],h=(g||a).target,i=h,k=e.filter;if(!("mousedown"===f&&0!==a.button||e.disabled)&&(h=c(h,e.draggable,d))){if(D=p(h),"function"==typeof k){if(k.call(this,a,h,this))return j(b,i,"filter",h,d,D),void a.preventDefault()}else if(k&&(k=k.split(",").some(function(a){return a=c(i,a.trim(),d),a?(j(b,a,"filter",h,d,D),!0):void 0})))return void a.preventDefault();(!e.handle||c(i,e.handle,d))&&this._prepareDragStart(a,g,h)}},_prepareDragStart:function(a,b,c){var d,f=this,h=f.el,j=f.options,k=h.ownerDocument;c&&!s&&c.parentNode===h&&(G=a,w=h,s=c,t=s.parentNode,x=s.nextSibling,F=j.group,d=function(){f._disableDelayedDrag(),s.draggable=!0,g(s,f.options.chosenClass,!0),f._triggerDragStart(b)},j.ignore.split(",").forEach(function(a){i(s,a.trim(),l)}),e(k,"mouseup",f._onDrop),e(k,"touchend",f._onDrop),e(k,"touchcancel",f._onDrop),j.delay?(e(k,"mouseup",f._disableDelayedDrag),e(k,"touchend",f._disableDelayedDrag),e(k,"touchcancel",f._disableDelayedDrag),e(k,"mousemove",f._disableDelayedDrag),e(k,"touchmove",f._disableDelayedDrag),f._dragStartTimer=setTimeout(d,j.delay)):d())},_disableDelayedDrag:function(){var a=this.el.ownerDocument;clearTimeout(this._dragStartTimer),f(a,"mouseup",this._disableDelayedDrag),f(a,"touchend",this._disableDelayedDrag),f(a,"touchcancel",this._disableDelayedDrag),f(a,"mousemove",this._disableDelayedDrag),f(a,"touchmove",this._disableDelayedDrag)},_triggerDragStart:function(a){a?(G={target:s,clientX:a.clientX,clientY:a.clientY},this._onDragStart(G,"touch")):this.nativeDraggable?(e(s,"dragend",this),e(w,"dragstart",this._onDragStart)):this._onDragStart(G,!0);try{N.selection?N.selection.empty():window.getSelection().removeAllRanges()}catch(b){}},_dragStarted:function(){w&&s&&(g(s,this.options.ghostClass,!0),a.active=this,j(this,w,"start",s,w,D))},_emulateDragOver:function(){if(H){if(this._lastX===H.clientX&&this._lastY===H.clientY)return;this._lastX=H.clientX,this._lastY=H.clientY,Q||h(u,"display","none");var a=N.elementFromPoint(H.clientX,H.clientY),b=a,c=" "+this.options.group.name,d=T.length;if(b)do{if(b[L]&&b[L].options.groups.indexOf(c)>-1){for(;d--;)T[d]({clientX:H.clientX,clientY:H.clientY,target:a,rootEl:b});break}a=b}while(b=b.parentNode);Q||h(u,"display","")}},_onTouchMove:function(b){if(G){a.active||this._dragStarted(),this._appendGhost();var c=b.touches?b.touches[0]:b,d=c.clientX-G.clientX,e=c.clientY-G.clientY,f=b.touches?"translate3d("+d+"px,"+e+"px,0)":"translate("+d+"px,"+e+"px)";I=!0,H=c,h(u,"webkitTransform",f),h(u,"mozTransform",f),h(u,"msTransform",f),h(u,"transform",f),b.preventDefault()}},_appendGhost:function(){if(!u){var a,b=s.getBoundingClientRect(),c=h(s),d=this.options;u=s.cloneNode(!0),g(u,d.ghostClass,!1),g(u,d.fallbackClass,!0),h(u,"top",b.top-O(c.marginTop,10)),h(u,"left",b.left-O(c.marginLeft,10)),h(u,"width",b.width),h(u,"height",b.height),h(u,"opacity","0.8"),h(u,"position","fixed"),h(u,"zIndex","100000"),h(u,"pointerEvents","none"),d.fallbackOnBody&&N.body.appendChild(u)||w.appendChild(u),a=u.getBoundingClientRect(),h(u,"width",2*b.width-a.width),h(u,"height",2*b.height-a.height)}},_onDragStart:function(a,b){var c=a.dataTransfer,d=this.options;this._offUpEvents(),"clone"==F.pull&&(v=s.cloneNode(!0),h(v,"display","none"),w.insertBefore(v,s)),b?("touch"===b?(e(N,"touchmove",this._onTouchMove),e(N,"touchend",this._onDrop),e(N,"touchcancel",this._onDrop)):(e(N,"mousemove",this._onTouchMove),e(N,"mouseup",this._onDrop)),this._loopId=setInterval(this._emulateDragOver,50)):(c&&(c.effectAllowed="move",d.setData&&d.setData.call(this,c,s)),e(N,"drop",this),setTimeout(this._dragStarted,0))},_onDragOver:function(a){var d,e,f,g=this.el,i=this.options,j=i.group,l=j.put,o=F===j,p=i.sort;if(void 0!==a.preventDefault&&(a.preventDefault(),!i.dragoverBubble&&a.stopPropagation()),I=!0,F&&!i.disabled&&(o?p||(f=!w.contains(s)):F.pull&&l&&(F.name===j.name||l.indexOf&&~l.indexOf(F.name)))&&(void 0===a.rootEl||a.rootEl===this.el)){if(U(a,i,this.el),R)return;if(d=c(a.target,i.draggable,g),e=s.getBoundingClientRect(),f)return b(!0),void(v||x?w.insertBefore(s,v||x):p||w.appendChild(s));if(0===g.children.length||g.children[0]===u||g===a.target&&(d=n(g,a))){if(d){if(d.animated)return;r=d.getBoundingClientRect()}b(o),k(w,g,s,e,d,r)!==!1&&(s.contains(g)||(g.appendChild(s),t=g),this._animate(e,s),d&&this._animate(r,d))}else if(d&&!d.animated&&d!==s&&void 0!==d.parentNode[L]){A!==d&&(A=d,B=h(d),C=h(d.parentNode));var q,r=d.getBoundingClientRect(),y=r.right-r.left,z=r.bottom-r.top,D=/left|right|inline/.test(B.cssFloat+B.display)||"flex"==C.display&&0===C["flex-direction"].indexOf("row"),E=d.offsetWidth>s.offsetWidth,G=d.offsetHeight>s.offsetHeight,H=(D?(a.clientX-r.left)/y:(a.clientY-r.top)/z)>.5,J=d.nextElementSibling,K=k(w,g,s,e,d,r);if(K!==!1){if(R=!0,setTimeout(m,30),b(o),1===K||-1===K)q=1===K;else if(D){var M=s.offsetTop,N=d.offsetTop;q=M===N?d.previousElementSibling===s&&!E||H&&E:N>M}else q=J!==s&&!G||H&&G;s.contains(g)||(q&&!J?g.appendChild(s):d.parentNode.insertBefore(s,q?J:d)),t=s.parentNode,this._animate(e,s),this._animate(r,d)}}}},_animate:function(a,b){var c=this.options.animation;if(c){var d=b.getBoundingClientRect();h(b,"transition","none"),h(b,"transform","translate3d("+(a.left-d.left)+"px,"+(a.top-d.top)+"px,0)"),b.offsetWidth,h(b,"transition","all "+c+"ms"),h(b,"transform","translate3d(0,0,0)"),clearTimeout(b.animated),b.animated=setTimeout(function(){h(b,"transition",""),h(b,"transform",""),b.animated=!1},c)}},_offUpEvents:function(){var a=this.el.ownerDocument;f(N,"touchmove",this._onTouchMove),f(a,"mouseup",this._onDrop),f(a,"touchend",this._onDrop),f(a,"touchcancel",this._onDrop)},_onDrop:function(b){var c=this.el,d=this.options;clearInterval(this._loopId),clearInterval(J.pid),clearTimeout(this._dragStartTimer),f(N,"mousemove",this._onTouchMove),this.nativeDraggable&&(f(N,"drop",this),f(c,"dragstart",this._onDragStart)),this._offUpEvents(),b&&(I&&(b.preventDefault(),!d.dropBubble&&b.stopPropagation()),u&&u.parentNode.removeChild(u),s&&(this.nativeDraggable&&f(s,"dragend",this),l(s),g(s,this.options.ghostClass,!1),g(s,this.options.chosenClass,!1),w!==t?(E=p(s),E>=0&&(j(null,t,"sort",s,w,D,E),j(this,w,"sort",s,w,D,E),j(null,t,"add",s,w,D,E),j(this,w,"remove",s,w,D,E))):(v&&v.parentNode.removeChild(v),s.nextSibling!==x&&(E=p(s),E>=0&&(j(this,w,"update",s,w,D,E),j(this,w,"sort",s,w,D,E)))),a.active&&((null===E||-1===E)&&(E=D),j(this,w,"end",s,w,D,E),this.save())),w=s=t=u=x=v=y=z=G=H=I=E=A=B=F=a.active=null)},handleEvent:function(a){var b=a.type;"dragover"===b||"dragenter"===b?s&&(this._onDragOver(a),d(a)):("drop"===b||"dragend"===b)&&this._onDrop(a)},toArray:function(){for(var a,b=[],d=this.el.children,e=0,f=d.length,g=this.options;f>e;e++)a=d[e],c(a,g.draggable,this.el)&&b.push(a.getAttribute(g.dataIdAttr)||o(a));return b},sort:function(a){var b={},d=this.el;this.toArray().forEach(function(a,e){var f=d.children[e];c(f,this.options.draggable,d)&&(b[a]=f)},this),a.forEach(function(a){b[a]&&(d.removeChild(b[a]),d.appendChild(b[a]))})},save:function(){var a=this.options.store;a&&a.set(this)},closest:function(a,b){return c(a,b||this.options.draggable,this.el)},option:function(a,b){var c=this.options;return void 0===b?c[a]:(c[a]=b,void("group"===a&&V(c)))},destroy:function(){var a=this.el;a[L]=null,f(a,"mousedown",this._onTapStart),f(a,"touchstart",this._onTapStart),this.nativeDraggable&&(f(a,"dragover",this),f(a,"dragenter",this)),Array.prototype.forEach.call(a.querySelectorAll("[draggable]"),function(a){a.removeAttribute("draggable")}),T.splice(T.indexOf(this._onDragOver),1),this._onDrop(),this.el=a=null}},a.utils={on:e,off:f,css:h,find:i,is:function(a,b){return!!c(a,b,a)},extend:r,throttle:q,closest:c,toggleClass:g,index:p},a.create=function(b,c){return new a(b,c)},a.version="1.4.2",a});
/*!
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2014 - 2015
 * @version 4.2.1
 *
 * File input styled for Bootstrap 3.0 that utilizes HTML5 File Input's advanced 
 * features including the FileReader API. 
 * 
 * The plugin drastically enhances the HTML file input to preview multiple files on the client before
 * upload. In addition it provides the ability to preview content of images, text, videos, audio, html, 
 * flash and other objects. It also offers the ability to upload and delete files using AJAX, and add 
 * files in batches (i.e. preview, append, or remove before upload).
 * 
 * Author: Kartik Visweswaran
 * Copyright: 2015, Kartik Visweswaran, Krajee.com
 * For more JQuery plugins visit http://plugins.krajee.com
 * For more Yii related demos visit http://demos.krajee.com
 */!function(e){"use strict";e.fn.fileinputLocales={},String.prototype.repl=function(e,i){return this.split(e).join(i)};var i=function(e){var i,t=document.createElement("div");return t.innerHTML="<!--[if IE "+e+"]><i></i><![endif]-->",i=1===t.getElementsByTagName("i").length,document.body.appendChild(t),t.parentNode.removeChild(t),i},t={data:{},init:function(e){var i=e.initialPreview,a=e.id;i.length>0&&!z(i)&&(i=i.split(e.initialPreviewDelimiter)),t.data[a]={content:i,config:e.initialPreviewConfig,tags:e.initialPreviewThumbTags,delimiter:e.initialPreviewDelimiter,template:e.previewGenericTemplate,msg:function(i){return e.getMsgSelected(i)},initId:e.previewInitId,footer:e.getLayoutTemplate("footer"),isDelete:e.initialPreviewShowDelete,caption:e.initialCaption,actions:function(i,t,a,n,r){return e.renderFileActions(i,t,a,n,r)}}},fetch:function(e){return t.data[e].content.filter(function(e){return null!==e})},count:function(e,i){return t.data[e]&&t.data[e].content?i?t.data[e].content.length:t.fetch(e).length:0},get:function(e,i,a){var n,r="init_"+i,l=t.data[e],o=l.initId+"-"+r;return a=void 0===a?!0:a,null===l.content[i]?"":(n=l.template.repl("{previewId}",o).repl("{frameClass}"," file-preview-initial").repl("{fileindex}",r).repl("{content}",l.content[i]).repl("{footer}",t.footer(e,i,a)),l.tags.length&&l.tags[i]&&(n=_(n,l.tags[i])),n)},add:function(i,a,n,r,l){var o,s=e.extend(!0,{},t.data[i]);return z(a)||(a=a.split(s.delimiter)),l?(o=s.content.push(a)-1,s.config[o]=n,s.tags[o]=r):(o=a.length,s.content=a,s.config=n,s.tags=r),t.data[i]=s,o},set:function(i,a,n,r,l){var o,s=e.extend(!0,{},t.data[i]);if(z(a)||(a=a.split(s.delimiter)),l){for(o=0;o<a.length;o++)s.content.push(a[o]);for(o=0;o<n.length;o++)s.config.push(n[o]);for(o=0;o<r.length;o++)s.tags.push(r[o])}else s.content=a,s.config=n,s.tags=r;t.data[i]=s},unset:function(e,i){var a=t.count(e);if(a){if(1===a)return t.data[e].content=[],void(t.data[e].config=[]);t.data[e].content[i]=null,t.data[e].config[i]=null}},out:function(e){var i,a="",n=t.data[e],r=t.count(e,!0);if(0===r)return{content:"",caption:""};for(var l=0;r>l;l++)a+=t.get(e,l);return i=n.msg(t.count(e)),{content:a,caption:i}},footer:function(e,i,a){var n=t.data[e];if(a=void 0===a?!0:a,0===n.config.length||R(n.config[i]))return"";var r=n.config[i],l=M("caption",r)?r.caption:"",o=M("width",r)?r.width:"auto",s=M("url",r)?r.url:!1,d=M("key",r)?r.key:null,p=s===!1&&a,c=n.isDelete?n.actions(!1,!0,p,s,d):"",u=n.footer.repl("{actions}",c);return u.repl("{caption}",l).repl("{width}",o).repl("{indicator}","").repl("{indicatorTitle}","")}},a=function(e,i){return i=i||0,"number"==typeof e?e:("string"==typeof e&&(e=parseFloat(e)),isNaN(e)?i:e)},n=function(){return window.File&&window.FileReader},r=function(){var e=document.createElement("div");return!i(9)&&(void 0!==e.draggable||void 0!==e.ondragstart&&void 0!==e.ondrop)},l=function(){return n&&window.FormData},o=function(e,i){e.removeClass(i).addClass(i)},s='style="width:{width};height:{height};"',d='      <param name="controller" value="true" />\n      <param name="allowFullScreen" value="true" />\n      <param name="allowScriptAccess" value="always" />\n      <param name="autoPlay" value="false" />\n      <param name="autoStart" value="false" />\n      <param name="quality" value="high" />\n',p='<div class="file-preview-other">\n       {previewFileIcon}\n   </div>',c={removeIcon:'<i class="glyphicon glyphicon-trash text-danger"></i>',removeClass:"btn btn-xs btn-default",removeTitle:"Remove file",uploadIcon:'<i class="glyphicon glyphicon-upload text-info"></i>',uploadClass:"btn btn-xs btn-default",uploadTitle:"Upload file",indicatorNew:'<i class="glyphicon glyphicon-hand-down text-warning"></i>',indicatorSuccess:'<i class="glyphicon glyphicon-ok-sign file-icon-large text-success"></i>',indicatorError:'<i class="glyphicon glyphicon-exclamation-sign text-danger"></i>',indicatorLoading:'<i class="glyphicon glyphicon-hand-up text-muted"></i>',indicatorNewTitle:"Not uploaded yet",indicatorSuccessTitle:"Uploaded",indicatorErrorTitle:"Upload Error",indicatorLoadingTitle:"Uploading ..."},u='{preview}\n<div class="kv-upload-progress hide"></div>\n<div class="input-group {class}">\n   {caption}\n   <div class="input-group-btn">\n       {remove}\n       {cancel}\n       {upload}\n       {browse}\n   </div>\n</div>',f='{preview}\n<div class="kv-upload-progress hide"></div>\n{remove}\n{cancel}\n{upload}\n{browse}\n',v='<div class="file-preview {class}">\n    <div class="close fileinput-remove">&times;</div>\n    <div class="{dropClass}">\n    <div class="file-preview-thumbnails">\n    </div>\n    <div class="clearfix"></div>    <div class="file-preview-status text-center text-success"></div>\n    <div class="kv-fileinput-error"></div>\n    </div>\n</div>',h='<span class="glyphicon glyphicon-file kv-caption-icon"></span>',m='<div tabindex="-1" class="form-control file-caption {class}">\n   <span class="file-caption-ellipsis">&hellip;</span>\n   <div class="file-caption-name"></div>\n</div>',g='<div id="{id}" class="modal fade">\n  <div class="modal-dialog modal-lg">\n    <div class="modal-content">\n      <div class="modal-header">\n        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\n        <h3 class="modal-title">Detailed Preview <small>{title}</small></h3>\n      </div>\n      <div class="modal-body">\n        <textarea class="form-control" style="font-family:Monaco,Consolas,monospace; height: {height}px;" readonly>{body}</textarea>\n      </div>\n    </div>\n  </div>\n</div>',w='<div class="progress">\n    <div class="{class}" role="progressbar" aria-valuenow="{percent}" aria-valuemin="0" aria-valuemax="100" style="width:{percent}%;">\n        {percent}%\n     </div>\n</div>',b='<div class="file-thumbnail-footer">\n    <div class="file-caption-name">{caption}</div>\n    {actions}\n</div>',C='<div class="file-actions">\n    <div class="file-footer-buttons">\n        {upload}{delete}{other}    </div>\n    <div class="file-upload-indicator" tabindex="-1" title="{indicatorTitle}">{indicator}</div>\n    <div class="clearfix"></div>\n</div>',x='<button type="button" class="kv-file-remove {removeClass}" title="{removeTitle}"{dataUrl}{dataKey}>{removeIcon}</button>\n',y='<button type="button" class="kv-file-upload {uploadClass}" title="{uploadTitle}">   {uploadIcon}\n</button>\n',T='<div class="file-preview-frame{frameClass}" id="{previewId}" data-fileindex="{fileindex}">\n   {content}\n   {footer}\n</div>\n',E='<div class="file-preview-frame{frameClass}" id="{previewId}" data-fileindex="{fileindex}">\n    <object data="{data}" type="{type}" width="{width}" height="{height}">\n       '+p+"\n    </object>\n   {footer}\n</div>",k='<div class="file-preview-frame{frameClass}" id="{previewId}" data-fileindex="{fileindex}">\n   <img src="{data}" class="file-preview-image" title="{caption}" alt="{caption}" '+s+">\n   {footer}\n</div>\n",F='<div class="file-preview-frame{frameClass}" id="{previewId}" data-fileindex="{fileindex}">\n   <div class="file-preview-text" title="{caption}" '+s+">\n       {data}\n   </div>\n   {footer}\n</div>",$='<div class="file-preview-frame{frameClass}" id="{previewId}" data-fileindex="{fileindex}" title="{caption}" '+s+'>\n   <video width="{width}" height="{height}" controls>\n       <source src="{data}" type="{type}">\n       '+p+"\n   </video>\n   {footer}\n</div>\n",D='<div class="file-preview-frame{frameClass}" id="{previewId}" data-fileindex="{fileindex}" title="{caption}" '+s+'>\n   <audio controls>\n       <source src="{data}" type="{type}">\n       '+p+"\n   </audio>\n   {footer}\n</div>",I='<div class="file-preview-frame{frameClass}" id="{previewId}" data-fileindex="{fileindex}" title="{caption}" '+s+'>\n   <object type="application/x-shockwave-flash" width="{width}" height="{height}" data="{data}">\n'+d+"       "+p+"\n   </object>\n   {footer}\n</div>\n",P='<div class="file-preview-frame{frameClass}" id="{previewId}" data-fileindex="{fileindex}" title="{caption}" '+s+'>\n   <object data="{data}" type="{type}" width="{width}" height="{height}">\n       <param name="movie" value="{caption}" />\n'+d+"         "+p+"\n   </object>\n   {footer}\n</div>",S='<div class="file-preview-frame{frameClass}" id="{previewId}" data-fileindex="{fileindex}" title="{caption}" '+s+">\n   "+p+"\n   {footer}\n</div>",U={main1:u,main2:f,preview:v,icon:h,caption:m,modal:g,progress:w,footer:b,actions:C,actionDelete:x,actionUpload:y},j={generic:T,html:E,image:k,text:F,video:$,audio:D,flash:I,object:P,other:S},A=["image","html","text","video","audio","flash","object"],L={image:{width:"auto",height:"160px"},html:{width:"213px",height:"160px"},text:{width:"160px",height:"160px"},video:{width:"213px",height:"160px"},audio:{width:"213px",height:"80px"},flash:{width:"213px",height:"160px"},object:{width:"160px",height:"160px"},other:{width:"160px",height:"160px"}},O={image:function(e,i){return void 0!==e?e.match("image.*"):i.match(/\.(gif|png|jpe?g)$/i)},html:function(e,i){return void 0!==e?"text/html"===e:i.match(/\.(htm|html)$/i)},text:function(e,i){return void 0!==e&&e.match("text.*")||i.match(/\.(txt|md|csv|nfo|php|ini)$/i)},video:function(e,i){return void 0!==e&&e.match(/\.video\/(ogg|mp4|webm)$/i)||i.match(/\.(og?|mp4|webm)$/i)},audio:function(e,i){return void 0!==e&&e.match(/\.audio\/(ogg|mp3|wav)$/i)||i.match(/\.(ogg|mp3|wav)$/i)},flash:function(e,i){return void 0!==e&&"application/x-shockwave-flash"===e||i.match(/\.(swf)$/i)},object:function(){return!0},other:function(){return!0}},R=function(i,t){return null===i||void 0===i||0===i.length||t&&""===e.trim(i)},z=function(e){return Array.isArray(e)||"[object Array]"===Object.prototype.toString.call(e)},M=function(e,i){return"object"==typeof i&&e in i},N=function(i,t,a){return R(i)||R(i[t])?a:e(i[t])},B=function(){return Math.round((new Date).getTime()+100*Math.random())},Z=function(e){return String(e).repl("&","&amp;").repl('"',"&quot;").repl("'","&#39;").repl("<","&lt;").repl(">","&gt;")},_=function(i,t){var a=i;return t=t||{},e.each(t,function(e,i){"function"==typeof i&&(i=i()),a=a.repl(e,i)}),a},q=window.URL||window.webkitURL,H=function(t,a){var r=this;r.$element=e(t),r.validate()&&(n()||i(9)?(r.init(a),r.listen()):r.$element.removeClass("file-loading"))};H.prototype={constructor:H,validate:function(){var e,i=this;return"file"===i.$element.attr("type")?!0:(e='<div class="help-block alert alert-warning"><h4>Invalid Input Type</h4>You must set an input <code>type = file</code> for <b>bootstrap-fileinput</b> plugin to initialize.</div>',i.$element.after(e),!1)},init:function(n){var s,d=this,p=d.$element;e.each(n,function(e,i){d[e]="maxFileCount"===e||"maxFileSize"===e?a(i):i}),d.fileInputCleared=!1,d.fileBatchCompleted=!0,R(d.allowedPreviewTypes)&&(d.allowedPreviewTypes=A),d.uploadFileAttr=R(p.attr("name"))?"file_data":p.attr("name"),d.reader=null,d.formdata={},d.isIE9=i(9),d.isIE10=i(10),d.filestack=[],d.ajaxRequests=[],d.isError=!1,d.ajaxAborted=!1,d.dropZoneEnabled=r()&&d.dropZoneEnabled,d.isDisabled=d.$element.attr("disabled")||d.$element.attr("readonly"),d.isUploadable=l&&!R(d.uploadUrl),d.slug="function"==typeof n.slugCallback?n.slugCallback:d.slugDefault,d.mainTemplate=d.getLayoutTemplate(d.showCaption?"main1":"main2"),d.captionTemplate=d.getLayoutTemplate("caption"),d.previewGenericTemplate=d.getPreviewTemplate("generic"),R(d.$element.attr("id"))&&d.$element.attr("id",B()),void 0===d.$container?d.$container=d.createContainer():d.refreshContainer(),d.$progress=d.$container.find(".kv-upload-progress"),d.$btnUpload=d.$container.find(".kv-fileinput-upload"),d.$captionContainer=N(n,"elCaptionContainer",d.$container.find(".file-caption")),d.$caption=N(n,"elCaptionText",d.$container.find(".file-caption-name")),d.$previewContainer=N(n,"elPreviewContainer",d.$container.find(".file-preview")),d.$preview=N(n,"elPreviewImage",d.$container.find(".file-preview-thumbnails")),d.$previewStatus=N(n,"elPreviewStatus",d.$container.find(".file-preview-status")),d.$errorContainer=N(n,"elErrorContainer",d.$previewContainer.find(".kv-fileinput-error")),R(d.msgErrorClass)||o(d.$errorContainer,d.msgErrorClass),d.$errorContainer.hide(),d.fileActionSettings=e.extend(c,n.fileActionSettings),d.previewInitId="preview-"+B(),d.id=d.$element.attr("id"),t.init(d),d.initPreview(!0),d.initPreviewDeletes(),d.options=n,d.setFileDropZoneTitle(),d.uploadCount=0,d.uploadPercent=0,d.$element.removeClass("file-loading"),s=d.getLayoutTemplate("progress"),d.progressTemplate=s.replace("{class}",d.progressClass),d.progressCompleteTemplate=s.replace("{class}",d.progressCompleteClass),d.setEllipsis()},parseError:function(i,t,a){var n=this,r=e.trim(t+""),l="."===r.slice(-1)?"":".",o=void 0!==i.responseJSON&&void 0!==i.responseJSON.error?i.responseJSON.error:i.responseText;return n.showAjaxErrorDetails?(o=e.trim(o.replace(/\n\s*\n/g,"\n")),o=o.length>0?"<pre>"+o+"</pre>":"",r+=l+o):r+=l,a?"<b>"+a+": </b>"+i:r},raise:function(i,t){var a=this,n=e.Event(i);if(void 0!==t?a.$element.trigger(n,t):a.$element.trigger(n),!n.result)return!1;switch(i){case"filebatchuploadcomplete":case"filebatchuploadsuccess":case"fileuploaded":case"fileclear":case"filecleared":case"filereset":case"fileerror":case"filefoldererror":case"fileuploaderror":case"filebatchuploaderror":case"filedeleteerror":case"filecustomerror":case"filesuccessremove":break;default:a.ajaxAborted=!0}return!0},getLayoutTemplate:function(e){var i=this,t=M(e,i.layoutTemplates)?i.layoutTemplates[e]:U[e];return R(i.customLayoutTags)?t:_(t,i.customLayoutTags)},getPreviewTemplate:function(e){var i=this,t=M(e,i.previewTemplates)?i.previewTemplates[e]:j[e];return t=t.repl("{previewFileIcon}",i.previewFileIcon),R(i.customPreviewTags)?t:_(t,i.customPreviewTags)},getOutData:function(e,i,t){var a=this;return e=e||{},i=i||{},t=t||a.filestack.slice(0)||{},{form:a.formdata,files:t,extra:a.getExtraData(),response:i,reader:a.reader,jqXHR:e}},setEllipsis:function(){var e=this,i=e.$captionContainer,t=e.$caption,a=t.clone().css("height","auto").hide();i.parent().before(a),i.removeClass("kv-has-ellipsis"),a.outerWidth()>t.outerWidth()&&i.addClass("kv-has-ellipsis"),a.remove()},listen:function(){var i=this,t=i.$element,a=i.$captionContainer,n=i.$btnFile,r=t.closest("form");t.on("change",e.proxy(i.change,i)),e(window).on("resize",function(){i.setEllipsis()}),n.off("click").on("click",function(){i.raise("filebrowse"),i.isError&&!i.isUploadable&&i.clear(),a.focus()}),r.off("reset").on("reset",e.proxy(i.reset,i)),i.$container.off("click").on("click",".fileinput-remove:not([disabled])",e.proxy(i.clear,i)).on("click",".fileinput-cancel",e.proxy(i.cancel,i)),i.isUploadable&&i.dropZoneEnabled&&i.showPreview&&i.initDragDrop(),i.isUploadable||r.on("submit",e.proxy(i.submitForm,i)),i.$container.find(".kv-fileinput-upload").off("click").on("click",function(t){i.isUploadable&&(t.preventDefault(),!e(this).hasClass("disabled")&&R(e(this).attr("disabled"))&&i.upload())})},submitForm:function(){var e=this,i=e.$element,t=i.get(0).files;return t&&t.length<e.minFileCount&&e.minFileCount>0?(e.noFilesError({}),!1):!e.abort({})},abort:function(i){var t,a=this;return a.ajaxAborted&&"object"==typeof a.ajaxAborted&&void 0!==a.ajaxAborted.message?(t=void 0!==a.ajaxAborted.data?a.getOutData({},a.ajaxAborted.data):a.getOutData(),t=e.extend(t,i),a.showUploadError(a.ajaxAborted.message,t,"filecustomerror"),!0):!1},noFilesError:function(e){var i=this,t=i.minFileCount>1?i.filePlural:i.fileSingle,a=i.msgFilesTooLess.repl("{n}",i.minFileCount).repl("{files}",t),n=i.$errorContainer;n.html(a),i.isError=!0,i.updateFileDetails(0),n.fadeIn(800),i.raise("fileerror",[e]),i.clearFileInput(),o(i.$container,"has-error")},setProgress:function(e){var i=this,t=Math.min(e,100),a=100>t?i.progressTemplate:i.progressCompleteTemplate;i.$progress.html(a.repl("{percent}",t))},upload:function(){var i,t,a,n=this,r=n.getFileStack().length,l={},o=!e.isEmptyObject(n.getExtraData());if(r<n.minFileCount&&n.minFileCount>0)return void n.noFilesError(l);if(n.isUploadable&&!n.isDisabled&&(0!==r||o)){if(n.resetUpload(),n.$progress.removeClass("hide"),n.uploadCount=0,n.uploadPercent=0,n.lock(),n.setProgress(0),0===r&&o)return void n.uploadExtraOnly();if(a=n.filestack.length,n.hasInitData=!1,n.uploadAsync&&n.showPreview)for(t=n.getOutData(),n.raise("filebatchpreupload",[t]),n.fileBatchCompleted=!1,n.uploadCache={content:[],config:[],tags:[],append:!0},i=0;a>i;i+=1)void 0!==n.filestack[i]&&n.uploadSingle(i,n.filestack,!0);else n.uploadBatch()}},lock:function(){var e=this;e.resetErrors(),e.disable(),e.showRemove&&o(e.$container.find(".fileinput-remove"),"hide"),e.showCancel&&e.$container.find(".fileinput-cancel").removeClass("hide"),e.raise("filelock",[e.filestack,e.getExtraData()])},unlock:function(e){var i=this;void 0===e&&(e=!0),i.enable(),i.showCancel&&o(i.$container.find(".fileinput-cancel"),"hide"),i.showRemove&&i.$container.find(".fileinput-remove").removeClass("hide"),e&&i.resetFileStack(),i.raise("fileunlock",[i.filestack,i.getExtraData()])},resetFileStack:function(){var i=this,t=0,a=[];i.getThumbs().each(function(){var n=e(this),r=n.attr("data-fileindex"),l=i.filestack[r];-1!==r&&(void 0!==l?(a[t]=l,n.attr({id:i.previewInitId+"-"+t,"data-fileindex":t}),t+=1):n.attr({id:"uploaded-"+B(),"data-fileindex":"-1"}))}),i.filestack=a},refresh:function(i){var t,a=this,n=a.$element,r=arguments.length?e.extend(a.options,i):a.options;n.off(),a.init(r),t=a.$container.find(".file-drop-zone"),t.off("dragenter dragover drop"),e(document).off("dragenter dragover drop"),a.listen(),a.setFileDropZoneTitle()},initDragDrop:function(){var i=this,t=i.$container.find(".file-drop-zone");t.off("dragenter dragover drop"),e(document).off("dragenter dragover drop"),t.on("dragenter dragover",function(t){t.stopPropagation(),t.preventDefault(),i.isDisabled||o(e(this),"highlighted")}),t.on("dragleave",function(t){t.stopPropagation(),t.preventDefault(),i.isDisabled||e(this).removeClass("highlighted")}),t.on("drop",function(t){t.preventDefault(),i.isDisabled||(i.change(t,"dragdrop"),e(this).removeClass("highlighted"))}),e(document).on("dragenter dragover drop",function(e){e.stopPropagation(),e.preventDefault()})},setFileDropZoneTitle:function(){var e=this,i=e.$container.find(".file-drop-zone");i.find("."+e.dropZoneTitleClass).remove(),e.isUploadable&&e.showPreview&&0!==i.length&&!(e.getFileStack().length>0)&&e.dropZoneEnabled&&(0===i.find(".file-preview-frame").length&&i.prepend('<div class="'+e.dropZoneTitleClass+'">'+e.dropZoneTitle+"</div>"),e.$container.removeClass("file-input-new"),o(e.$container,"file-input-ajax-new"))},initFileActions:function(){var i=this;i.$preview.find(".kv-file-remove").each(function(){var a,n,r=e(this),l=r.closest(".file-preview-frame"),o=l.attr("data-fileindex");r.off("click").on("click",function(){l.fadeOut("slow",function(){i.filestack[o]=void 0,i.clearObjects(l),l.remove();var e=i.getFileStack(),r=e.length,s=t.count(i.id);i.clearFileInput(),0===r&&0===s?i.reset():(a=s+r,n=a>1?i.getMsgSelected(a):e[0].name,i.setCaption(n))})})}),i.$preview.find(".kv-file-upload").each(function(){var t=e(this);t.off("click").on("click",function(){var e=t.closest(".file-preview-frame"),a=e.attr("data-fileindex");i.uploadSingle(a,i.filestack,!1)})})},getMsgSelected:function(e){var i=this,t=1===e?i.fileSingle:i.filePlural;return i.msgSelected.repl("{n}",e).repl("{files}",t)},renderFileFooter:function(e,i){var t,a,n=this,r=n.fileActionSettings,l=n.getLayoutTemplate("footer");return n.isUploadable?(t=l.repl("{actions}",n.renderFileActions(!0,!0,!1,!1,!1)),a=t.repl("{caption}",e).repl("{width}",i).repl("{indicator}",r.indicatorNew).repl("{indicatorTitle}",r.indicatorNewTitle)):a=l.repl("{actions}","").repl("{caption}",e).repl("{width}",i).repl("{indicator}","").repl("{indicatorTitle}",""),a=_(a,n.previewThumbTags)},renderFileActions:function(e,i,t,a,n){if(!e&&!i)return"";var r=this,l=a===!1?"":' data-url="'+a+'"',o=n===!1?"":' data-key="'+n+'"',s=r.getLayoutTemplate("actionDelete"),d="",p=r.getLayoutTemplate("actions"),c=r.otherActionButtons.repl("{dataKey}",o),u=r.fileActionSettings,f=t?u.removeClass+" disabled":u.removeClass;return s=s.repl("{removeClass}",f).repl("{removeIcon}",u.removeIcon).repl("{removeTitle}",u.removeTitle).repl("{dataUrl}",l).repl("{dataKey}",o),e&&(d=r.getLayoutTemplate("actionUpload").repl("{uploadClass}",u.uploadClass).repl("{uploadIcon}",u.uploadIcon).repl("{uploadTitle}",u.uploadTitle)),p.repl("{delete}",s).repl("{upload}",d).repl("{other}",c)},setThumbStatus:function(e,i){var t=this,a="indicator"+i,n=a+"Title",r="file-preview-"+i.toLowerCase(),l=e.find(".file-upload-indicator"),o=t.fileActionSettings;e.removeClass("file-preview-success file-preview-error file-preview-loading"),l.html(o[a]),l.attr("title",o[n]),e.addClass(r)},clearPreview:function(){var e=this,i=e.$preview.find(e.showUploadedThumbs?".file-preview-frame:not(.file-preview-success)":".file-preview-frame");i.remove()},initPreview:function(e){var i,a=this,n=a.initialCaption||"";return t.count(a.id)?(i=t.out(a.id),n=e&&a.initialCaption?a.initialCaption:i.caption,a.$preview.html(i.content),a.setCaption(n),void(R(i.content)||a.$container.removeClass("file-input-new"))):(a.clearPreview(),void(e?a.setCaption(n):a.initCaption()))},initPreviewDeletes:function(){var i=this,a=i.deleteExtraData||{},n=function(){0===i.$preview.find(".kv-file-remove").length&&(i.reset(),i.initialCaption="")};i.$preview.find(".kv-file-remove").each(function(){var r=e(this),l=r.data("url")||i.deleteUrl,s=r.data("key");if(!R(l)&&void 0!==s){var d,p,c,u,f=r.closest(".file-preview-frame"),v=t.data[i.id],h=f.data("fileindex");h=parseInt(h.replace("init_","")),c=R(v.config)&&R(v.config[h])?null:v.config[h],u=R(c)||R(c.extra)?a:c.extra,"function"==typeof u&&(u=u()),p={id:r.attr("id"),key:s,extra:u},d=e.extend({url:l,type:"DELETE",dataType:"json",data:e.extend({key:s},u),beforeSend:function(e){i.ajaxAborted=!1,i.raise("filepredelete",[s,e,u]),i.ajaxAborted?e.abort():(o(f,"file-uploading"),o(r,"disabled"))},success:function(e,a,l){var o,d;return void 0!==e&&void 0!==e.error?(p.jqXHR=l,p.response=e,i.showError(e.error,p,"filedeleteerror"),f.removeClass("file-uploading"),r.removeClass("disabled"),void n()):(t.unset(i.id,h),o=t.count(i.id),d=o>0?i.getMsgSelected(o):"",i.raise("filedeleted",[s,l,u]),i.setCaption(d),f.removeClass("file-uploading").addClass("file-deleted"),void f.fadeOut("slow",function(){i.clearObjects(f),f.remove(),n(),o||0!==i.getFileStack().length||(i.setCaption(""),i.reset())}))},error:function(e,t,a){var r=i.parseError(e,a);p.jqXHR=e,p.response={},i.showError(r,p,"filedeleteerror"),f.removeClass("file-uploading"),n()}},i.ajaxDeleteSettings),r.off("click").on("click",function(){e.ajax(d)})}})},clearObjects:function(i){i.find("video audio").each(function(){this.pause(),e(this).remove()}),i.find("img object div").each(function(){e(this).remove()})},clearFileInput:function(){var i,t,a,n=this,r=n.$element;R(r.val())||(n.isIE9||n.isIE10?(i=r.closest("form"),t=e(document.createElement("form")),a=e(document.createElement("div")),r.before(a),i.length?i.after(t):a.after(t),t.append(r).trigger("reset"),a.before(r).remove(),t.remove()):r.val(""),n.fileInputCleared=!0)},resetUpload:function(){var e=this;e.uploadCache={content:[],config:[],tags:[],append:!0},e.uploadCount=0,e.uploadPercent=0,e.$btnUpload.removeAttr("disabled"),e.setProgress(0),o(e.$progress,"hide"),e.resetErrors(!1),e.ajaxAborted=!1,e.ajaxRequests=[]},cancel:function(){var i,t=this,a=t.ajaxRequests,n=a.length;if(n>0)for(i=0;n>i;i+=1)a[i].abort();t.getThumbs().each(function(){var i=e(this),a=i.attr("data-fileindex");i.removeClass("file-uploading"),void 0!==t.filestack[a]&&(i.find(".kv-file-upload").removeClass("disabled").removeAttr("disabled"),i.find(".kv-file-remove").removeClass("disabled").removeAttr("disabled")),t.unlock()})},clear:function(){var i,a=this;a.$btnUpload.removeAttr("disabled"),a.resetUpload(),a.filestack=[],a.clearFileInput(),a.resetErrors(!0),a.raise("fileclear"),!a.overwriteInitial&&t.count(a.id)?(a.showFileIcon(),a.resetPreview(),a.setEllipsis(),a.initPreviewDeletes(),a.$container.removeClass("file-input-new")):(a.getThumbs().each(function(){a.clearObjects(e(this))}),a.$preview.html(""),i=!a.overwriteInitial&&a.initialCaption.length>0?a.initialCaption:"",a.setCaption(i),a.setEllipsis(),a.$caption.attr("title",""),o(a.$container,"file-input-new")),0===a.$container.find(".file-preview-frame").length&&(a.initCaption()||a.$captionContainer.find(".kv-caption-icon").hide(),a.setEllipsis()),a.hideFileIcon(),a.raise("filecleared"),a.$captionContainer.focus(),a.setFileDropZoneTitle()},resetPreview:function(){var e,i=this;t.count(i.id)?(e=t.out(i.id),i.$preview.html(e.content),i.setCaption(e.caption)):(i.$preview.html(""),i.initCaption())},reset:function(){var e=this;e.clear(),e.resetPreview(),e.setEllipsis(),e.$container.find(".fileinput-filename").text(""),e.raise("filereset"),e.initialPreview.length>0&&e.$container.removeClass("file-input-new"),e.setFileDropZoneTitle(),e.filestack=[],e.formdata={}},disable:function(){var e=this;e.isDisabled=!0,e.raise("filedisabled"),e.$element.attr("disabled","disabled"),e.$container.find(".kv-fileinput-caption").addClass("file-caption-disabled"),e.$container.find(".btn-file, .fileinput-remove, .kv-fileinput-upload").attr("disabled",!0),e.initDragDrop()},enable:function(){var e=this;e.isDisabled=!1,e.raise("fileenabled"),e.$element.removeAttr("disabled"),e.$container.find(".kv-fileinput-caption").removeClass("file-caption-disabled"),e.$container.find(".btn-file, .fileinput-remove, .kv-fileinput-upload").removeAttr("disabled"),e.initDragDrop()},getThumbs:function(e){return e=e||"",this.$preview.find(".file-preview-frame:not(.file-preview-initial)"+e)},getExtraData:function(){var e=this,i=e.uploadExtraData;return"function"==typeof e.uploadExtraData&&(i=e.uploadExtraData()),i},uploadExtra:function(){var i=this,t=i.getExtraData();0!==t.length&&e.each(t,function(e,t){i.formdata.append(e,t)})},initXhr:function(e,i){var t=this;return e.upload&&e.upload.addEventListener("progress",function(e){var a=0,n=e.loaded||e.position,r=e.total;e.lengthComputable&&(a=Math.ceil(n/r*i)),t.uploadPercent=Math.max(a,t.uploadPercent),t.setProgress(t.uploadPercent)},!1),e},ajaxSubmit:function(i,t,a,n){var r,l=this;l.uploadExtra(),r=e.extend({xhr:function(){var i=e.ajaxSettings.xhr();return l.initXhr(i,98)},url:l.uploadUrl,type:"POST",dataType:"json",data:l.formdata,cache:!1,processData:!1,contentType:!1,beforeSend:i,success:t,complete:a,error:n},l.ajaxSettings),l.ajaxRequests.push(e.ajax(r))},initUploadSuccess:function(i,a,n){var r,l,o,s,d,p,c,u=this;"object"!=typeof i||e.isEmptyObject(i)||void 0!==i.initialPreview&&i.initialPreview.length>0&&(u.hasInitData=!0,d=i.initialPreview||[],p=i.initialPreviewConfig||[],c=i.initialPreviewThumbTags||[],r=void 0===i.append||i.append?!0:!1,u.overwriteInitial=!1,void 0===a||n?n?(u.uploadCache.content.push(d[0]),u.uploadCache.config.push(p[0]),u.uploadCache.tags.push(c[0]),u.uploadCache.append=r):(t.set(u.id,d,p,c,r),u.initPreview(),u.initPreviewDeletes()):(o=t.add(u.id,d,p[0],c[0],r),l=t.get(u.id,o,!1),s=e(l).hide(),a.after(s).fadeOut("slow",function(){s.fadeIn("slow").css("display:inline-block"),u.initPreviewDeletes(),u.clearFileInput()})))},initThumbDelete:function(e){var i=this;e.find(".kv-file-remove").removeAttr("disabled").off("click").on("click",function(){var t=i.raise("filesuccessremove",[e.attr("id"),e.data("fileindex")]);t!==!1&&e.fadeOut("slow",function(){e.remove(),i.$preview.find(".file-preview-frame").length||i.reset()})})},uploadSingle:function(i,a,n){var r,l,s,d,p,c,u,f,v,h=this,m=h.getFileStack().length,g=new FormData,w=h.previewInitId+"-"+i,b=e("#"+w+":not(.file-preview-initial)"),C=b.find(".kv-file-upload"),x=b.find(".kv-file-remove"),y=h.filestack.length>0||!e.isEmptyObject(h.uploadExtraData),T={id:w,index:i};h.formdata=g,0===m||!y||C.hasClass("disabled")||h.abort(T)||(s=function(){var e=h.getThumbs(".file-uploading");e.length>0||h.fileBatchCompleted||(h.fileBatchCompleted=!0,setTimeout(function(){t.set(h.id,h.uploadCache.content,h.uploadCache.config,h.uploadCache.tags,h.uploadCache.append),h.hasInitData&&(h.initPreview(),h.initPreviewDeletes()),h.setProgress(100),h.unlock(),h.clearFileInput(),h.raise("filebatchuploadcomplete",[h.filestack,h.getExtraData()])},100))},d=function(){!n||0===m||h.uploadPercent>=100||(h.uploadCount+=1,l=80+Math.ceil(20*h.uploadCount/m),h.uploadPercent=Math.max(l,h.uploadPercent),h.setProgress(h.uploadPercent),h.initPreviewDeletes())},p=function(){C.removeAttr("disabled"),x.removeAttr("disabled"),b.removeClass("file-uploading")},c=function(t){r=h.getOutData(t),b.hasClass("file-preview-success")||(h.setThumbStatus(b,"Loading"),o(b,"file-uploading")),C.attr("disabled",!0),x.attr("disabled",!0),n||h.lock(),h.raise("filepreupload",[r,w,i]),T=e.extend(T,r),h.abort(T)&&(t.abort(),h.setProgress(100))},u=function(t,a,l){r=h.getOutData(l,t),T=e.extend(T,r),setTimeout(function(){void 0===t.error?(h.setThumbStatus(b,"Success"),C.hide(),h.initThumbDelete(b),h.filestack[i]=void 0,h.raise("fileuploaded",[r,w,i]),h.initUploadSuccess(t,b,n),n||h.resetFileStack()):(h.setThumbStatus(b,"Error"),h.showUploadError(t.error,T))},100)},f=function(){setTimeout(function(){d(),p(),n?s():h.unlock(!1)},100)},v=function(t,r,l){var o=h.parseError(t,l,n?a[i].name:null);h.setThumbStatus(b,"Error"),T=e.extend(T,h.getOutData(t)),h.showUploadError(o,T)},g.append(h.uploadFileAttr,a[i]),g.append("file_id",i),h.ajaxSubmit(c,u,f,v))},uploadBatch:function(){var i,t,a,n,r,l=this,s=l.filestack,d=s.length,p=l.filestack.length>0||!e.isEmptyObject(l.uploadExtraData),c={};l.formdata=new FormData,0!==d&&p&&!l.abort(c)&&(i=function(){e.each(s,function(e){l.filestack[e]=void 0}),l.clearFileInput()},t=function(i){l.lock();var t=l.getOutData(i);l.showPreview&&l.getThumbs().each(function(){var i=e(this),t=i.find(".kv-file-upload"),a=i.find(".kv-file-remove");i.hasClass("file-preview-success")||(l.setThumbStatus(i,"Loading"),o(i,"file-uploading")),t.attr("disabled",!0),a.attr("disabled",!0)}),l.raise("filebatchpreupload",[t]),l.abort(t)&&i.abort()},a=function(t,a,n){var r=l.getOutData(n,t),o=l.getThumbs(),s=R(t.errorkeys)?[]:t.errorkeys;void 0===t.error||R(t.error)?(l.raise("filebatchuploadsuccess",[r]),i(),l.showPreview?(o.each(function(){var i=e(this),t=i.find(".kv-file-upload");i.find(".kv-file-upload").hide(),l.setThumbStatus(i,"Success"),i.removeClass("file-uploading"),t.removeAttr("disabled"),l.initThumbDelete(i)}),l.initUploadSuccess(t)):l.reset()):(l.showPreview&&(o.each(function(){var i=e(this),t=i.find(".kv-file-remove"),a=i.find(".kv-file-upload");return i.removeClass("file-uploading"),a.removeAttr("disabled"),t.removeAttr("disabled"),0===s.length?void l.setThumbStatus(i,"Error"):void(-1!==e.inArray(key,s)?l.setThumbStatus(i,"Error"):(i.find(".kv-file-upload").hide(),l.setThumbStatus(i,"Success"),l.initThumbDelete(i),l.filestack[key]=void 0))}),l.initUploadSuccess(t)),l.showUploadError(t.error,r,"filebatchuploaderror"))},n=function(){l.setProgress(100),l.unlock(),l.raise("filebatchuploadcomplete",[l.filestack,l.getExtraData()]),l.clearFileInput()},r=function(i,t,a){var n=l.getOutData(i),r=l.parseError(i,a);l.showUploadError(r,n,"filebatchuploaderror"),l.uploadFileCount=d-1,l.showPreview&&(l.getThumbs().each(function(){var i=e(this),t=i.attr("data-fileindex");i.removeClass("file-uploading"),void 0!==l.filestack[t]&&l.setThumbStatus(i,"Error")}),l.getThumbs().removeClass("file-uploading"),l.getThumbs(" .kv-file-upload").removeAttr("disabled"),l.getThumbs(" .kv-file-delete").removeAttr("disabled"))},e.each(s,function(e,i){R(s[e])||l.formdata.append(l.uploadFileAttr,i)}),l.ajaxSubmit(t,a,n,r))},uploadExtraOnly:function(){var e,i,t,a,n=this,r={};n.formdata=new FormData,n.abort(r)||(e=function(e){n.lock();var i=n.getOutData(e);n.raise("filebatchpreupload",[i]),n.setProgress(50),r.data=i,r.xhr=e,n.abort(r)&&(e.abort(),n.setProgress(100))},i=function(e,i,t){var a=n.getOutData(t,e);

void 0===e.error||R(e.error)?(n.raise("filebatchuploadsuccess",[a]),n.clearFileInput(),n.initUploadSuccess(e)):n.showUploadError(e.error,a,"filebatchuploaderror")},t=function(){n.setProgress(100),n.unlock(),n.raise("filebatchuploadcomplete",[n.filestack,n.getExtraData()]),n.clearFileInput()},a=function(e,i,t){var a=n.getOutData(e),l=n.parseError(e,t);r.data=a,n.showUploadError(l,a,"filebatchuploaderror")},n.ajaxSubmit(e,i,t,a))},hideFileIcon:function(){this.overwriteInitial&&this.$captionContainer.find(".kv-caption-icon").hide()},showFileIcon:function(){this.$captionContainer.find(".kv-caption-icon").show()},resetErrors:function(e){var i=this,t=i.$errorContainer;i.isError=!1,i.$container.removeClass("has-error"),t.html(""),e?t.fadeOut("slow"):t.hide()},showFolderError:function(e){var i=this,t=i.$errorContainer;e&&(t.html(i.msgFoldersNotAllowed.repl("{n}",e)),t.fadeIn(800),o(i.$container,"has-error"),i.raise("filefoldererror",[e]))},showUploadError:function(e,i,t){var a=this,n=a.$errorContainer,r=t||"fileuploaderror";return 0===n.find("ul").length?n.html("<ul><li>"+e+"</li></ul>"):n.find("ul").append("<li>"+e+"</li>"),n.fadeIn(800),a.raise(r,[i]),o(a.$container,"has-error"),!0},showError:function(e,i,t){var a=this,n=a.$errorContainer,r=t||"fileerror";return i=i||{},i.reader=a.reader,n.html(e),n.fadeIn(800),a.raise(r,[i]),a.isUploadable||a.clearFileInput(),o(a.$container,"has-error"),a.$btnUpload.attr("disabled",!0),!0},errorHandler:function(e,i){var t=this,a=e.target.error;switch(a.code){case a.NOT_FOUND_ERR:t.showError(t.msgFileNotFound.repl("{name}",i));break;case a.SECURITY_ERR:t.showError(t.msgFileSecured.repl("{name}",i));break;case a.NOT_READABLE_ERR:t.showError(t.msgFileNotReadable.repl("{name}",i));break;case a.ABORT_ERR:t.showError(t.msgFilePreviewAborted.repl("{name}",i));break;default:t.showError(t.msgFilePreviewError.repl("{name}",i))}},parseFileType:function(e){var i,t,a,n,r=this;for(n=0;n<A.length;n+=1)if(a=A[n],i=M(a,r.fileTypeSettings)?r.fileTypeSettings[a]:O[a],t=i(e.type,e.name)?a:"",!R(t))return t;return"other"},previewDefault:function(i,t,a){if(this.showPreview){var n=this,r=q.createObjectURL(i),l=e("#"+t),o=n.previewSettings.other,s=n.renderFileFooter(i.name,o.width),d=n.getPreviewTemplate("other"),p=t.slice(t.lastIndexOf("-")+1),c="";a===!0&&(c=" btn disabled",s+='<div class="file-other-error text-danger"><i class="glyphicon glyphicon-exclamation-sign"></i></div>'),n.$preview.append("\n"+d.repl("{previewId}",t).repl("{frameClass}",c).repl("{fileindex}",p).repl("{caption}",n.slug(i.name)).repl("{width}",o.width).repl("{height}",o.height).repl("{type}",i.type).repl("{data}",r).repl("{footer}",s)),l.on("load",function(){q.revokeObjectURL(l.attr("data"))})}},previewFile:function(e,i,t,a){if(this.showPreview){var n,r,l,o,s=this,d=s.parseFileType(e),p=s.slug(e.name),c=s.allowedPreviewTypes,u=s.allowedPreviewMimeTypes,f=s.getPreviewTemplate(d),v=M(d,s.previewSettings)?s.previewSettings[d]:L[d],h=parseInt(s.wrapTextLength,10),m=s.wrapIndicator,g=c.indexOf(d)>=0,w=R(u)||!R(u)&&-1!==u.indexOf(e.type),b=s.renderFileFooter(p,v.width),C="",x=t.slice(t.lastIndexOf("-")+1);g&&w?("text"===d?(r=Z(i.target.result),q.revokeObjectURL(a),r.length>h&&(l="text-"+B(),o=.75*window.innerHeight,C=s.getLayoutTemplate("modal").repl("{id}",l).repl("{title}",p).repl("{height}",o).repl("{body}",r),m=m.repl("{title}",p).repl("{dialog}","$('#"+l+"').modal('show')"),r=r.substring(0,h-1)+m),n=f.repl("{previewId}",t).repl("{caption}",p).repl("{frameClass}","").repl("{type}",e.type).repl("{width}",v.width).repl("{height}",v.height).repl("{data}",r).repl("{footer}",b).repl("{fileindex}",x)+C):n=f.repl("{previewId}",t).repl("{caption}",p).repl("{frameClass}","").repl("{type}",e.type).repl("{data}",a).repl("{width}",v.width).repl("{height}",v.height).repl("{footer}",b).repl("{fileindex}",x),s.$preview.append("\n"+n),s.autoSizeImage(t)):s.previewDefault(e,t)}},slugDefault:function(e){return R(e)?"":e.split(/(\\|\/)/g).pop().replace(/[^\w\u00C0-\u017F\-.\\\/ ]+/g,"")},getFileStack:function(){var e=this;return e.filestack.filter(function(e){return void 0!==e})},readFiles:function(i){function t(e){if(R(n.attr("multiple"))&&(u=1),e>=u)return a.isUploadable&&a.filestack.length>0?a.raise("filebatchselected",[a.getFileStack()]):a.raise("filebatchselected",[i]),o.removeClass("loading"),void s.html("");var m,g,w,b,C,x,y=v+e,T=c+"-"+y,E=i[e],k=a.slug(E.name),F=(E.size||0)/1e3,$="",D=q.createObjectURL(E),I=0,P=a.allowedFileTypes,S=R(P)?"":P.join(", "),U=a.allowedFileExtensions,j=R(U)?"":U.join(", ");if(R(U)||($=new RegExp("\\.("+U.join("|")+")$","i")),F=F.toFixed(2),a.maxFileSize>0&&F>a.maxFileSize)return b=a.msgSizeTooLarge.repl("{name}",k).repl("{size}",F).repl("{maxSize}",a.maxFileSize),void(a.isError=h(b,E,T,e));if(!R(P)&&z(P)){for(w=0;w<P.length;w+=1)C=P[w],g=f[C],x=void 0!==g&&g(E.type,k),I+=R(x)?0:x.length;if(0===I)return b=a.msgInvalidFileType.repl("{name}",k).repl("{types}",S),void(a.isError=h(b,E,T,e))}return 0!==I||R(U)||!z(U)||R($)||(x=k.match($),I+=R(x)?0:x.length,0!==I)?a.showPreview?(r.length>0&&void 0!==FileReader?(s.html(d.repl("{index}",e+1).repl("{files}",u)),o.addClass("loading"),l.onerror=function(e){a.errorHandler(e,k)},l.onload=function(e){a.previewFile(E,e,T,D),a.initFileActions()},l.onloadend=function(){b=p.repl("{index}",e+1).repl("{files}",u).repl("{percent}",50).repl("{name}",k),setTimeout(function(){s.html(b),q.revokeObjectURL(D)},100),setTimeout(function(){t(e+1),a.updateFileDetails(u)},100),a.raise("fileloaded",[E,T,e,l])},l.onprogress=function(i){if(i.lengthComputable){var t=i.loaded/i.total*100,a=Math.ceil(t);b=p.repl("{index}",e+1).repl("{files}",u).repl("{percent}",a).repl("{name}",k),setTimeout(function(){s.html(b)},100)}},m=M("text",f)?f.text:O.text,m(E.type,k)?l.readAsText(E,a.textEncoding):l.readAsArrayBuffer(E)):(a.previewDefault(E,T),setTimeout(function(){t(e+1),a.updateFileDetails(u)},100),a.raise("fileloaded",[E,T,e,l])),void a.filestack.push(E)):(a.filestack.push(E),setTimeout(t(e+1),100),void a.raise("fileloaded",[E,T,e,l])):(b=a.msgInvalidFileExtension.repl("{name}",k).repl("{extensions}",j),void(a.isError=h(b,E,T,e)))}this.reader=new FileReader;var a=this,n=a.$element,r=a.$preview,l=a.reader,o=a.$previewContainer,s=a.$previewStatus,d=a.msgLoading,p=a.msgProgress,c=a.previewInitId,u=i.length,f=a.fileTypeSettings,v=a.filestack.length,h=function(t,n,r,l){var o=e.extend(a.getOutData({},{},i),{id:r,index:l}),s={id:r,index:l,file:n,files:i};return a.previewDefault(n,r,!0),a.isUploadable?a.showUploadError(t,o):a.showError(t,s)};t(0),a.updateFileDetails(u,!1)},updateFileDetails:function(e){var i=this,a=i.$element,n=i.getFileStack(),r=a.val()||n.length&&n[0].name||"",l=i.slug(r),o=i.isUploadable?n.length:e,s=t.count(i.id)+o,d=o>1?i.getMsgSelected(s):l;i.isError?(i.$previewContainer.removeClass("loading"),i.$previewStatus.html(""),i.$captionContainer.find(".kv-caption-icon").hide()):i.showFileIcon(),i.setCaption(d,i.isError),i.$container.removeClass("file-input-new file-input-ajax-new"),1===arguments.length&&i.raise("fileselect",[e,l]),t.count(i.id)&&i.initPreviewDeletes()},change:function(i){var a=this,n=a.$element;if(!a.isUploadable&&R(n.val())&&a.fileInputCleared)return void(a.fileInputCleared=!1);a.fileInputCleared=!1;var r,l,o,s,d=a.$preview,p=arguments.length>1,c=p?i.originalEvent.dataTransfer.files:n.get(0).files,u=R(n.attr("multiple")),f=0,v=0,h=a.filestack.length,m=a.isUploadable,g=function(i,t,n,r){var l=e.extend(a.getOutData({},{},c),{id:n,index:r}),o={id:n,index:r,file:t,files:c};return a.isUploadable?a.showUploadError(i,l):a.showError(i,o)};if(a.reader=null,a.resetUpload(),a.hideFileIcon(),a.isUploadable&&a.$container.find(".file-drop-zone ."+a.dropZoneTitleClass).remove(),p)for(r=[];c[f];)s=c[f],s.type||s.size%4096!==0?r.push(s):v++,f++;else r=void 0===i.target.files?i.target&&i.target.value?[{name:i.target.value.replace(/^.+\\/,"")}]:[]:i.target.files;if(R(r)||0===r.length)return m||a.clear(),a.showFolderError(v),void a.raise("fileselectnone");if(a.resetErrors(),o=a.isUploadable?a.getFileStack().length+r.length:r.length,a.maxFileCount>0&&o>a.maxFileCount)return l=a.msgFilesTooMany.repl("{m}",a.maxFileCount).repl("{n}",o),a.isError=g(l,null,null,null),a.$captionContainer.find(".kv-caption-icon").hide(),a.$caption.html(a.msgValidationError),a.setEllipsis(),void a.$container.removeClass("file-input-new file-input-ajax-new");if(!m||u&&h>0){if(!a.overwriteInitial&&t.count(a.id)){var w=t.out(a.id);d.html(w.content),a.setCaption(w.caption),a.initPreviewDeletes()}else a.clearPreview();u&&h>0&&(a.filestack=[])}else m&&a.overwriteInitial&&(a.clearPreview(),a.filestack=[]);a.isIE9?a.updateFileDetails(1):a.readFiles(r),a.showFolderError(v)},autoSizeImage:function(e){var i,t,a,n=this,r=n.$preview,l=r.find("#"+e),o=l.find("img");o.length&&o.on("load",function(){i=l.width(),t=r.width(),i>t&&(o.css("width","100%"),l.css("width","97%")),a=o.closest(".file-preview-frame").find(".file-caption-name"),a.length&&(a.width(o.width()),a.attr("title",a.text())),n.raise("fileimageloaded",e)})},initCaption:function(){var e=this,i=e.initialCaption||"";return e.overwriteInitial||R(i)?(e.$caption.html(""),!1):(e.setCaption(i),!0)},setCaption:function(i,t){var a,n,r=this,l=t||!1;if(l)a=e("<div>"+r.msgValidationError+"</div>").text(),n='<span class="'+r.msgValidationErrorClass+'">'+r.msgValidationErrorIcon+a+"</span>";else{if(R(i)||0===r.$caption.length)return;a=e("<div>"+i+"</div>").text(),n=r.getLayoutTemplate("icon")+a}r.$caption.html(n),r.$caption.attr("title",a),r.$captionContainer.find(".file-caption-ellipsis").attr("title",a),r.setEllipsis()},initBrowse:function(e){var i=this;i.$btnFile=e.find(".btn-file"),i.$btnFile.append(i.$element)},createContainer:function(){var i=this,t=e(document.createElement("span")).attr({"class":"file-input file-input-new"}).html(i.renderMain());return i.$element.before(t),i.initBrowse(t),t},refreshContainer:function(){var e=this,i=e.$container;i.before(e.$element),i.html(e.renderMain()),e.initBrowse(i)},renderMain:function(){var e=this,i=e.isUploadable&&e.dropZoneEnabled?" file-drop-zone":"",t=e.showPreview?e.getLayoutTemplate("preview").repl("{class}",e.previewClass).repl("{dropClass}",i):"",a=e.isDisabled?e.captionClass+" file-caption-disabled":e.captionClass,n=e.captionTemplate.repl("{class}",a+" kv-fileinput-caption");return e.mainTemplate.repl("{class}",e.mainClass).repl("{preview}",t).repl("{caption}",n).repl("{upload}",e.renderUpload()).repl("{remove}",e.renderRemove()).repl("{cancel}",e.renderCancel()).repl("{browse}",e.renderBrowse())},renderBrowse:function(){var e=this,i=e.browseClass+" btn-file",t="";return e.isDisabled&&(t=" disabled "),'<div class="'+i+'"'+t+"> "+e.browseIcon+e.browseLabel+" </div>"},renderRemove:function(){var e=this,i=e.removeClass+" fileinput-remove fileinput-remove-button",t="";return e.showRemove?(e.isDisabled&&(t=" disabled "),'<button type="button" title="'+e.removeTitle+'" class="'+i+'"'+t+">"+e.removeIcon+e.removeLabel+"</button>"):""},renderCancel:function(){var e=this,i=e.cancelClass+" fileinput-cancel fileinput-cancel-button";return e.showCancel?'<button type="button" title="'+e.cancelTitle+'" class="hide '+i+'">'+e.cancelIcon+e.cancelLabel+"</button>":""},renderUpload:function(){var e=this,i=e.uploadClass+" kv-fileinput-upload fileinput-upload-button",t="",a="";return e.showUpload?(e.isDisabled&&(a=" disabled "),t=!e.isUploadable||e.isDisabled?'<button type="submit" title="'+e.uploadTitle+'"class="'+i+'"'+a+">"+e.uploadIcon+e.uploadLabel+"</button>":'<a href="'+e.uploadUrl+'" title="'+e.uploadTitle+'" class="'+i+'"'+a+">"+e.uploadIcon+e.uploadLabel+"</a>"):""}},e.fn.fileinput=function(t){if(n()||i(9)){var a=Array.apply(null,arguments);return a.shift(),this.each(function(){var i,n=e(this),r=n.data("fileinput"),l="object"==typeof t&&t,o=l.language||n.data("language")||"en";r||(i=e.extend({},e.fn.fileinput.defaults),"en"===o||R(e.fn.fileinputLocales[o])||(i=e.extend(i,e.fn.fileinputLocales[o])),r=new H(this,e.extend(i,l,n.data())),n.data("fileinput",r)),"string"==typeof t&&r[t].apply(r,a)})}},e.fn.fileinput.defaults={language:"en",showCaption:!0,showPreview:!0,showRemove:!0,showUpload:!0,showCancel:!0,showUploadedThumbs:!0,mainClass:"",previewClass:"",captionClass:"",mainTemplate:null,initialCaption:"",initialPreview:[],initialPreviewDelimiter:"*$$*",initialPreviewConfig:[],initialPreviewThumbTags:[],previewThumbTags:{},initialPreviewShowDelete:!0,deleteUrl:"",deleteExtraData:{},overwriteInitial:!0,layoutTemplates:U,previewTemplates:j,allowedPreviewTypes:A,allowedPreviewMimeTypes:null,allowedFileTypes:null,allowedFileExtensions:null,customLayoutTags:{},customPreviewTags:{},previewSettings:L,fileTypeSettings:O,previewFileIcon:'<i class="glyphicon glyphicon-file"></i>',browseIcon:'<i class="glyphicon glyphicon-folder-open"></i> &nbsp;',browseClass:"btn btn-primary",removeIcon:'<i class="glyphicon glyphicon-trash"></i> ',removeClass:"btn btn-default",cancelIcon:'<i class="glyphicon glyphicon-ban-circle"></i> ',cancelClass:"btn btn-default",uploadIcon:'<i class="glyphicon glyphicon-upload"></i> ',uploadClass:"btn btn-default",uploadUrl:null,uploadAsync:!0,uploadExtraData:{},maxFileSize:0,minFileCount:0,maxFileCount:0,msgValidationErrorClass:"text-danger",msgValidationErrorIcon:'<i class="glyphicon glyphicon-exclamation-sign"></i> ',msgErrorClass:"file-error-message",progressClass:"progress-bar progress-bar-success progress-bar-striped active",progressCompleteClass:"progress-bar progress-bar-success",previewFileType:"image",wrapTextLength:250,wrapIndicator:' <span class="wrap-indicator" title="{title}" onclick="{dialog}">[&hellip;]</span>',elCaptionContainer:null,elCaptionText:null,elPreviewContainer:null,elPreviewImage:null,elPreviewStatus:null,elErrorContainer:null,slugCallback:null,dropZoneEnabled:!0,dropZoneTitleClass:"file-drop-zone-title",fileActionSettings:{},otherActionButtons:"",textEncoding:"UTF-8",ajaxSettings:{},ajaxDeleteSettings:{},showAjaxErrorDetails:!0},e.fn.fileinputLocales.en={fileSingle:"file",filePlural:"files",browseLabel:"Browse &hellip;",removeLabel:"Remove",removeTitle:"Clear selected files",cancelLabel:"Cancel",cancelTitle:"Abort ongoing upload",uploadLabel:"Upload",uploadTitle:"Upload selected files",msgSizeTooLarge:'File "{name}" (<b>{size} KB</b>) exceeds maximum allowed upload size of <b>{maxSize} KB</b>. Please retry your upload!',msgFilesTooLess:"You must select at least <b>{n}</b> {files} to upload. Please retry your upload!",msgFilesTooMany:"Number of files selected for upload <b>({n})</b> exceeds maximum allowed limit of <b>{m}</b>. Please retry your upload!",msgFileNotFound:'File "{name}" not found!',msgFileSecured:'Security restrictions prevent reading the file "{name}".',msgFileNotReadable:'File "{name}" is not readable.',msgFilePreviewAborted:'File preview aborted for "{name}".',msgFilePreviewError:'An error occurred while reading the file "{name}".',msgInvalidFileType:'Invalid type for file "{name}". Only "{types}" files are supported.',msgInvalidFileExtension:'Invalid extension for file "{name}". Only "{extensions}" files are supported.',msgValidationError:"File Upload Error",msgLoading:"Loading file {index} of {files} &hellip;",msgProgress:"Loading file {index} of {files} - {name} - {percent}% completed.",msgSelected:"{n} {files} selected",msgFoldersNotAllowed:"Drag & drop files only! {n} folder(s) dropped were skipped.",dropZoneTitle:"Drag & drop files here &hellip;"},e.extend(e.fn.fileinput.defaults,e.fn.fileinputLocales.en),e.fn.fileinput.Constructor=H,e(document).ready(function(){var i=e("input.file[type=file]");i.length&&i.fileinput()})}(window.jQuery);
/*!
 * FileInput Russian Translations
 *
 * This file must be loaded after 'fileinput.js'. Patterns in braces '{}', or
 * any HTML markup tags in the messages must not be converted or translated.
 *
 * @see http://github.com/kartik-v/bootstrap-fileinput
 * @author CyanoFresh <cyanofresh@gmail.com>
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
(function ($) {
    "use strict";

    $.fn.fileinputLocales['ru'] = {
        fileSingle: 'файл',
        filePlural: 'файлы',
        browseLabel: 'Выбрать &hellip;',
        removeLabel: 'Удалить',
        removeTitle: 'Очистить выбранные файлы',
        cancelLabel: 'Отмена',
        cancelTitle: 'Отменить текущую загрузку',
        uploadLabel: 'Загрузить',
        uploadTitle: 'Загрузить выбранные файлы',
        msgSizeTooLarge: 'Файл "{name}" (<b>{size} KB</b>) превышает максимальный размер <b>{maxSize} KB</b>',
        msgFilesTooLess: 'Вы должны выбрать как минимум <b>{n}</b> {files} для загрузки',
        msgFilesTooMany: 'Количество выбранных файлов <b>({n})</b> превышает максимально допустимое количество <b>{m}</b>',
        msgFileNotFound: 'Файл "{name}" не найден!',
        msgFileSecured: 'Ограничения безопасности запрещают читать файл "{name}".',
        msgFileNotReadable: 'Файл "{name}" невозможно прочитать.',
        msgFilePreviewAborted: 'Предпросмотр отменен для файла "{name}".',
        msgFilePreviewError: 'Произошла ошибка при чтении файла "{name}".',
        msgInvalidFileType: 'Запрещенный тип файла для "{name}". Только "{types}" разрешены.',
        msgInvalidFileExtension: 'Запрещенное расширение для файла "{name}". Только "{extensions}" разрешены.',
        msgValidationError: 'Ошибка при загрузке файла',
        msgLoading: 'Загрузка файла {index} из {files} &hellip;',
        msgProgress: 'Загрузка файла {index} из {files} - {name} - {percent}% завершено.',
        msgSelected: '{n} {files} выбрано',
        msgFoldersNotAllowed: 'Разрешено только перетаскивание файлов! Пропущено {n} папок.',
        dropZoneTitle: 'Перетащите файлы сюда &hellip;'
    };
})(window.jQuery);

/*! iCheck v1.0.1 by Damir Sultanov, http://git.io/arlzeA, MIT Licensed */
(function(h){function F(a,b,d){var c=a[0],e=/er/.test(d)?m:/bl/.test(d)?s:l,f=d==H?{checked:c[l],disabled:c[s],indeterminate:"true"==a.attr(m)||"false"==a.attr(w)}:c[e];if(/^(ch|di|in)/.test(d)&&!f)D(a,e);else if(/^(un|en|de)/.test(d)&&f)t(a,e);else if(d==H)for(e in f)f[e]?D(a,e,!0):t(a,e,!0);else if(!b||"toggle"==d){if(!b)a[p]("ifClicked");f?c[n]!==u&&t(a,e):D(a,e)}}function D(a,b,d){var c=a[0],e=a.parent(),f=b==l,A=b==m,B=b==s,K=A?w:f?E:"enabled",p=k(a,K+x(c[n])),N=k(a,b+x(c[n]));if(!0!==c[b]){if(!d&&
b==l&&c[n]==u&&c.name){var C=a.closest("form"),r='input[name="'+c.name+'"]',r=C.length?C.find(r):h(r);r.each(function(){this!==c&&h(this).data(q)&&t(h(this),b)})}A?(c[b]=!0,c[l]&&t(a,l,"force")):(d||(c[b]=!0),f&&c[m]&&t(a,m,!1));L(a,f,b,d)}c[s]&&k(a,y,!0)&&e.find("."+I).css(y,"default");e[v](N||k(a,b)||"");B?e.attr("aria-disabled","true"):e.attr("aria-checked",A?"mixed":"true");e[z](p||k(a,K)||"")}function t(a,b,d){var c=a[0],e=a.parent(),f=b==l,h=b==m,q=b==s,p=h?w:f?E:"enabled",t=k(a,p+x(c[n])),
u=k(a,b+x(c[n]));if(!1!==c[b]){if(h||!d||"force"==d)c[b]=!1;L(a,f,p,d)}!c[s]&&k(a,y,!0)&&e.find("."+I).css(y,"pointer");e[z](u||k(a,b)||"");q?e.attr("aria-disabled","false"):e.attr("aria-checked","false");e[v](t||k(a,p)||"")}function M(a,b){if(a.data(q)){a.parent().html(a.attr("style",a.data(q).s||""));if(b)a[p](b);a.off(".i").unwrap();h(G+'[for="'+a[0].id+'"]').add(a.closest(G)).off(".i")}}function k(a,b,d){if(a.data(q))return a.data(q).o[b+(d?"":"Class")]}function x(a){return a.charAt(0).toUpperCase()+
a.slice(1)}function L(a,b,d,c){if(!c){if(b)a[p]("ifToggled");a[p]("ifChanged")[p]("if"+x(d))}}var q="iCheck",I=q+"-helper",u="radio",l="checked",E="un"+l,s="disabled",w="determinate",m="in"+w,H="update",n="type",v="addClass",z="removeClass",p="trigger",G="label",y="cursor",J=/ipad|iphone|ipod|android|blackberry|windows phone|opera mini|silk/i.test(navigator.userAgent);h.fn[q]=function(a,b){var d='input[type="checkbox"], input[type="'+u+'"]',c=h(),e=function(a){a.each(function(){var a=h(this);c=a.is(d)?
c.add(a):c.add(a.find(d))})};if(/^(check|uncheck|toggle|indeterminate|determinate|disable|enable|update|destroy)$/i.test(a))return a=a.toLowerCase(),e(this),c.each(function(){var c=h(this);"destroy"==a?M(c,"ifDestroyed"):F(c,!0,a);h.isFunction(b)&&b()});if("object"!=typeof a&&a)return this;var f=h.extend({checkedClass:l,disabledClass:s,indeterminateClass:m,labelHover:!0,aria:!1},a),k=f.handle,B=f.hoverClass||"hover",x=f.focusClass||"focus",w=f.activeClass||"active",y=!!f.labelHover,C=f.labelHoverClass||
"hover",r=(""+f.increaseArea).replace("%","")|0;if("checkbox"==k||k==u)d='input[type="'+k+'"]';-50>r&&(r=-50);e(this);return c.each(function(){var a=h(this);M(a);var c=this,b=c.id,e=-r+"%",d=100+2*r+"%",d={position:"absolute",top:e,left:e,display:"block",width:d,height:d,margin:0,padding:0,background:"#fff",border:0,opacity:0},e=J?{position:"absolute",visibility:"hidden"}:r?d:{position:"absolute",opacity:0},k="checkbox"==c[n]?f.checkboxClass||"icheckbox":f.radioClass||"i"+u,m=h(G+'[for="'+b+'"]').add(a.closest(G)),
A=!!f.aria,E=q+"-"+Math.random().toString(36).replace("0.",""),g='<div class="'+k+'" '+(A?'role="'+c[n]+'" ':"");m.length&&A&&m.each(function(){g+='aria-labelledby="';this.id?g+=this.id:(this.id=E,g+=E);g+='"'});g=a.wrap(g+"/>")[p]("ifCreated").parent().append(f.insert);d=h('<ins class="'+I+'"/>').css(d).appendTo(g);a.data(q,{o:f,s:a.attr("style")}).css(e);f.inheritClass&&g[v](c.className||"");f.inheritID&&b&&g.attr("id",q+"-"+b);"static"==g.css("position")&&g.css("position","relative");F(a,!0,H);
if(m.length)m.on("click.i mouseover.i mouseout.i touchbegin.i touchend.i",function(b){var d=b[n],e=h(this);if(!c[s]){if("click"==d){if(h(b.target).is("a"))return;F(a,!1,!0)}else y&&(/ut|nd/.test(d)?(g[z](B),e[z](C)):(g[v](B),e[v](C)));if(J)b.stopPropagation();else return!1}});a.on("click.i focus.i blur.i keyup.i keydown.i keypress.i",function(b){var d=b[n];b=b.keyCode;if("click"==d)return!1;if("keydown"==d&&32==b)return c[n]==u&&c[l]||(c[l]?t(a,l):D(a,l)),!1;if("keyup"==d&&c[n]==u)!c[l]&&D(a,l);else if(/us|ur/.test(d))g["blur"==
d?z:v](x)});d.on("click mousedown mouseup mouseover mouseout touchbegin.i touchend.i",function(b){var d=b[n],e=/wn|up/.test(d)?w:B;if(!c[s]){if("click"==d)F(a,!1,!0);else{if(/wn|er|in/.test(d))g[v](e);else g[z](e+" "+w);if(m.length&&y&&e==B)m[/ut|nd/.test(d)?z:v](C)}if(J)b.stopPropagation();else return!1}})})}})(window.jQuery||window.Zepto);

/*! Summernote v0.8.1 | (c) 2013-2015 Alan Hong and other contributors | MIT license */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof module&&module.exports?module.exports=a(require("jquery")):a(window.jQuery)}(function(a){"use strict";var b,c=function(){var b=function(a){return function(b){return a===b}},c=function(a,b){return a===b},d=function(a){return function(b,c){return b[a]===c[a]}},e=function(){return!0},f=function(){return!1},g=function(a){return function(){return!a.apply(a,arguments)}},h=function(a,b){return function(c){return a(c)&&b(c)}},i=function(a){return a},j=function(a,b){return function(){return a[b].apply(a,arguments)}},k=0,l=function(a){var b=++k+"";return a?a+b:b},m=function(b){var c=a(document);return{top:b.top+c.scrollTop(),left:b.left+c.scrollLeft(),width:b.right-b.left,height:b.bottom-b.top}},n=function(a){var b={};for(var c in a)a.hasOwnProperty(c)&&(b[a[c]]=c);return b},o=function(a,b){return b=b||"",b+a.split(".").map(function(a){return a.substring(0,1).toUpperCase()+a.substring(1)}).join("")};return{eq:b,eq2:c,peq2:d,ok:e,fail:f,self:i,not:g,and:h,invoke:j,uniqueId:l,rect2bnd:m,invertObject:n,namespaceToCamel:o}}(),d=function(){var b=function(a){return a[0]},d=function(a){return a[a.length-1]},e=function(a){return a.slice(0,a.length-1)},f=function(a){return a.slice(1)},g=function(a,b){for(var c=0,d=a.length;d>c;c++){var e=a[c];if(b(e))return e}},h=function(a,b){for(var c=0,d=a.length;d>c;c++)if(!b(a[c]))return!1;return!0},i=function(b,c){return a.inArray(c,b)},j=function(a,b){return-1!==i(a,b)},k=function(a,b){return b=b||c.self,a.reduce(function(a,c){return a+b(c)},0)},l=function(a){for(var b=[],c=-1,d=a.length;++c<d;)b[c]=a[c];return b},m=function(a){return!a||!a.length},n=function(a,c){if(!a.length)return[];var e=f(a);return e.reduce(function(a,b){var e=d(a);return c(d(e),b)?e[e.length]=b:a[a.length]=[b],a},[[b(a)]])},o=function(a){for(var b=[],c=0,d=a.length;d>c;c++)a[c]&&b.push(a[c]);return b},p=function(a){for(var b=[],c=0,d=a.length;d>c;c++)j(b,a[c])||b.push(a[c]);return b},q=function(a,b){var c=i(a,b);return-1===c?null:a[c+1]},r=function(a,b){var c=i(a,b);return-1===c?null:a[c-1]};return{head:b,last:d,initial:e,tail:f,prev:r,next:q,find:g,contains:j,all:h,sum:k,from:l,isEmpty:m,clusterBy:n,compact:o,unique:p}}(),e="function"==typeof define&&define.amd,f=function(b){var c="Comic Sans MS"===b?"Courier New":"Comic Sans MS",d=a("<div>").css({position:"absolute",left:"-9999px",top:"-9999px",fontSize:"200px"}).text("mmmmmmmmmwwwwwww").appendTo(document.body),e=d.css("fontFamily",c).width(),f=d.css("fontFamily",b+","+c).width();return d.remove(),e!==f},g=navigator.userAgent,h=/MSIE|Trident/i.test(g);if(h){var i=/MSIE (\d+[.]\d+)/.exec(g);i&&(b=parseFloat(i[1])),i=/Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.exec(g),i&&(b=parseFloat(i[1]))}var j=/Edge\/\d+/.test(g),k=!!window.CodeMirror;if(!k&&e&&require)if(require.hasOwnProperty("resolve"))try{require.resolve("codemirror"),k=!0}catch(l){k=!1}else require.hasOwnProperty("specified")&&(k=require.specified("codemirror"));var m={isMac:navigator.appVersion.indexOf("Mac")>-1,isMSIE:h,isEdge:j,isFF:!j&&/firefox/i.test(g),isPhantom:/PhantomJS/i.test(g),isWebkit:!j&&/webkit/i.test(g),isChrome:!j&&/chrome/i.test(g),isSafari:!j&&/safari/i.test(g),browserVersion:b,jqueryVersion:parseFloat(a.fn.jquery),isSupportAmd:e,hasCodeMirror:k,isFontInstalled:f,isW3CRangeSupport:!!document.createRange},n=String.fromCharCode(160),o="\ufeff",p=function(){var b=function(b){return b&&a(b).hasClass("note-editable")},e=function(b){return b&&a(b).hasClass("note-control-sizing")},f=function(a){return a=a.toUpperCase(),function(b){return b&&b.nodeName.toUpperCase()===a}},g=function(a){return a&&3===a.nodeType},h=function(a){return a&&1===a.nodeType},i=function(a){return a&&/^BR|^IMG|^HR|^IFRAME|^BUTTON/.test(a.nodeName.toUpperCase())},j=function(a){return b(a)?!1:a&&/^DIV|^P|^LI|^H[1-7]/.test(a.nodeName.toUpperCase())},k=function(a){return a&&/^H[1-7]/.test(a.nodeName.toUpperCase())},l=f("PRE"),q=f("LI"),r=function(a){return j(a)&&!q(a)},s=f("TABLE"),t=function(a){return!(y(a)||u(a)||v(a)||j(a)||s(a)||x(a))},u=function(a){return a&&/^UL|^OL/.test(a.nodeName.toUpperCase())},v=f("HR"),w=function(a){return a&&/^TD|^TH/.test(a.nodeName.toUpperCase())},x=f("BLOCKQUOTE"),y=function(a){return w(a)||x(a)||b(a)},z=f("A"),A=function(a){return t(a)&&!!J(a,j)},B=function(a){return t(a)&&!J(a,j)},C=f("BODY"),D=function(a,b){return a.nextSibling===b||a.previousSibling===b},E=function(a,b){b=b||c.ok;var d=[];return a.previousSibling&&b(a.previousSibling)&&d.push(a.previousSibling),d.push(a),a.nextSibling&&b(a.nextSibling)&&d.push(a.nextSibling),d},F=m.isMSIE&&m.browserVersion<11?"&nbsp;":"<br>",G=function(a){return g(a)?a.nodeValue.length:a.childNodes.length},H=function(a){var b=G(a);return 0===b?!0:g(a)||1!==b||a.innerHTML!==F?d.all(a.childNodes,g)&&""===a.innerHTML?!0:!1:!0},I=function(a){i(a)||G(a)||(a.innerHTML=F)},J=function(a,c){for(;a;){if(c(a))return a;if(b(a))break;a=a.parentNode}return null},K=function(a,c){for(a=a.parentNode;a&&1===G(a);){if(c(a))return a;if(b(a))break;a=a.parentNode}return null},L=function(a,d){d=d||c.fail;var e=[];return J(a,function(a){return b(a)||e.push(a),d(a)}),e},M=function(a,b){var c=L(a);return d.last(c.filter(b))},N=function(b,c){for(var d=L(b),e=c;e;e=e.parentNode)if(a.inArray(e,d)>-1)return e;return null},O=function(a,b){b=b||c.fail;for(var d=[];a&&!b(a);)d.push(a),a=a.previousSibling;return d},P=function(a,b){b=b||c.fail;for(var d=[];a&&!b(a);)d.push(a),a=a.nextSibling;return d},Q=function(a,b){var d=[];return b=b||c.ok,function e(c){a!==c&&b(c)&&d.push(c);for(var f=0,g=c.childNodes.length;g>f;f++)e(c.childNodes[f])}(a),d},R=function(b,c){var d=b.parentNode,e=a("<"+c+">")[0];return d.insertBefore(e,b),e.appendChild(b),e},S=function(a,b){var c=b.nextSibling,d=b.parentNode;return c?d.insertBefore(a,c):d.appendChild(a),a},T=function(b,c){return a.each(c,function(a,c){b.appendChild(c)}),b},U=function(a){return 0===a.offset},V=function(a){return a.offset===G(a.node)},W=function(a){return U(a)||V(a)},X=function(a,b){for(;a&&a!==b;){if(0!==_(a))return!1;a=a.parentNode}return!0},Y=function(a,b){for(;a&&a!==b;){if(_(a)!==G(a.parentNode)-1)return!1;a=a.parentNode}return!0},Z=function(a,b){return U(a)&&X(a.node,b)},$=function(a,b){return V(a)&&Y(a.node,b)},_=function(a){for(var b=0;a=a.previousSibling;)b+=1;return b},aa=function(a){return!!(a&&a.childNodes&&a.childNodes.length)},ba=function(a,c){var d,e;if(0===a.offset){if(b(a.node))return null;d=a.node.parentNode,e=_(a.node)}else aa(a.node)?(d=a.node.childNodes[a.offset-1],e=G(d)):(d=a.node,e=c?0:a.offset-1);return{node:d,offset:e}},ca=function(a,c){var d,e;if(G(a.node)===a.offset){if(b(a.node))return null;d=a.node.parentNode,e=_(a.node)+1}else aa(a.node)?(d=a.node.childNodes[a.offset],e=0):(d=a.node,e=c?G(a.node):a.offset+1);return{node:d,offset:e}},da=function(a,b){return a.node===b.node&&a.offset===b.offset},ea=function(a){if(g(a.node)||!aa(a.node)||H(a.node))return!0;var b=a.node.childNodes[a.offset-1],c=a.node.childNodes[a.offset];return b&&!i(b)||c&&!i(c)?!1:!0},fa=function(a,b){for(;a;){if(b(a))return a;a=ba(a)}return null},ga=function(a,b){for(;a;){if(b(a))return a;a=ca(a)}return null},ha=function(a){if(!g(a.node))return!1;var b=a.node.nodeValue.charAt(a.offset-1);return b&&" "!==b&&b!==n},ia=function(a,b,c,d){for(var e=a;e&&(c(e),!da(e,b));){var f=d&&a.node!==e.node&&b.node!==e.node;e=ca(e,f)}},ja=function(a,b){var d=L(b,c.eq(a));return d.map(_).reverse()},ka=function(a,b){for(var c=a,d=0,e=b.length;e>d;d++)c=c.childNodes.length<=b[d]?c.childNodes[c.childNodes.length-1]:c.childNodes[b[d]];return c},la=function(a,b){var c=b&&b.isSkipPaddingBlankHTML,d=b&&b.isNotSplitEdgePoint;if(W(a)&&(g(a.node)||d)){if(U(a))return a.node;if(V(a))return a.node.nextSibling}if(g(a.node))return a.node.splitText(a.offset);var e=a.node.childNodes[a.offset],f=S(a.node.cloneNode(!1),a.node);return T(f,P(e)),c||(I(a.node),I(f)),f},ma=function(a,b,d){var e=L(b.node,c.eq(a));return e.length?1===e.length?la(b,d):e.reduce(function(a,c){return a===b.node&&(a=la(b,d)),la({node:c,offset:a?p.position(a):G(c)},d)}):null},na=function(a,b){var c,e,f=b?j:y,g=L(a.node,f),h=d.last(g)||a.node;f(h)?(c=g[g.length-2],e=h):(c=h,e=c.parentNode);var i=c&&ma(c,a,{isSkipPaddingBlankHTML:b,isNotSplitEdgePoint:b});return i||e!==a.node||(i=a.node.childNodes[a.offset]),{rightNode:i,container:e}},oa=function(a){return document.createElement(a)},pa=function(a){return document.createTextNode(a)},qa=function(a,b){if(a&&a.parentNode){if(a.removeNode)return a.removeNode(b);var c=a.parentNode;if(!b){var d,e,f=[];for(d=0,e=a.childNodes.length;e>d;d++)f.push(a.childNodes[d]);for(d=0,e=f.length;e>d;d++)c.insertBefore(f[d],a)}c.removeChild(a)}},ra=function(a,c){for(;a&&!b(a)&&c(a);){var d=a.parentNode;qa(a),a=d}},sa=function(a,b){if(a.nodeName.toUpperCase()===b.toUpperCase())return a;var c=oa(b);return a.style.cssText&&(c.style.cssText=a.style.cssText),T(c,d.from(a.childNodes)),S(c,a),qa(a),c},ta=f("TEXTAREA"),ua=function(a,b){var c=ta(a[0])?a.val():a.html();return b?c.replace(/[\n\r]/g,""):c},va=function(b,c){var d=ua(b);if(c){var e=/<(\/?)(\b(?!!)[^>\s]*)(.*?)(\s*\/?>)/g;d=d.replace(e,function(a,b,c){c=c.toUpperCase();var d=/^DIV|^TD|^TH|^P|^LI|^H[1-7]/.test(c)&&!!b,e=/^BLOCKQUOTE|^TABLE|^TBODY|^TR|^HR|^UL|^OL/.test(c);return a+(d||e?"\n":"")}),d=a.trim(d)}return d},wa=function(b){var c=a(b),d=c.offset(),e=c.outerHeight(!0);return{left:d.left,top:d.top+e}},xa=function(a,b){Object.keys(b).forEach(function(c){a.on(c,b[c])})},ya=function(a,b){Object.keys(b).forEach(function(c){a.off(c,b[c])})};return{NBSP_CHAR:n,ZERO_WIDTH_NBSP_CHAR:o,blank:F,emptyPara:"<p>"+F+"</p>",makePredByNodeName:f,isEditable:b,isControlSizing:e,isText:g,isElement:h,isVoid:i,isPara:j,isPurePara:r,isHeading:k,isInline:t,isBlock:c.not(t),isBodyInline:B,isBody:C,isParaInline:A,isPre:l,isList:u,isTable:s,isCell:w,isBlockquote:x,isBodyContainer:y,isAnchor:z,isDiv:f("DIV"),isLi:q,isBR:f("BR"),isSpan:f("SPAN"),isB:f("B"),isU:f("U"),isS:f("S"),isI:f("I"),isImg:f("IMG"),isTextarea:ta,isEmpty:H,isEmptyAnchor:c.and(z,H),isClosestSibling:D,withClosestSiblings:E,nodeLength:G,isLeftEdgePoint:U,isRightEdgePoint:V,isEdgePoint:W,isLeftEdgeOf:X,isRightEdgeOf:Y,isLeftEdgePointOf:Z,isRightEdgePointOf:$,prevPoint:ba,nextPoint:ca,isSamePoint:da,isVisiblePoint:ea,prevPointUntil:fa,nextPointUntil:ga,isCharPoint:ha,walkPoint:ia,ancestor:J,singleChildAncestor:K,listAncestor:L,lastAncestor:M,listNext:P,listPrev:O,listDescendant:Q,commonAncestor:N,wrap:R,insertAfter:S,appendChildNodes:T,position:_,hasChildren:aa,makeOffsetPath:ja,fromOffsetPath:ka,splitTree:ma,splitPoint:na,create:oa,createText:pa,remove:qa,removeWhile:ra,replace:sa,html:va,value:ua,posFromPlaceholder:wa,attachEvents:xa,detachEvents:ya}}(),q=function(b,e){var f=this,g=a.summernote.ui;return this.memos={},this.modules={},this.layoutInfo={},this.options=e,this.initialize=function(){return this.layoutInfo=g.createLayout(b,e),this._initialize(),b.hide(),this},this.destroy=function(){this._destroy(),b.removeData("summernote"),g.removeLayout(b,this.layoutInfo)},this.reset=function(){var a=f.isDisabled();this.code(p.emptyPara),this._destroy(),this._initialize(),a&&f.disable()},this._initialize=function(){var b=a.extend({},this.options.buttons);Object.keys(b).forEach(function(a){f.memo("button."+a,b[a])});var c=a.extend({},this.options.modules,a.summernote.plugins||{});Object.keys(c).forEach(function(a){f.module(a,c[a],!0)}),Object.keys(this.modules).forEach(function(a){f.initializeModule(a)})},this._destroy=function(){Object.keys(this.modules).reverse().forEach(function(a){f.removeModule(a)}),Object.keys(this.memos).forEach(function(a){f.removeMemo(a)})},this.code=function(a){var c=this.invoke("codeview.isActivated");return void 0===a?(this.invoke("codeview.sync"),c?this.layoutInfo.codable.val():this.layoutInfo.editable.html()):(c?this.layoutInfo.codable.val(a):this.layoutInfo.editable.html(a),b.val(a),this.triggerEvent("change",a),void 0)},this.isDisabled=function(){return"false"===this.layoutInfo.editable.attr("contenteditable")},this.enable=function(){this.layoutInfo.editable.attr("contenteditable",!0),this.invoke("toolbar.activate",!0)},this.disable=function(){this.invoke("codeview.isActivated")&&this.invoke("codeview.deactivate"),this.layoutInfo.editable.attr("contenteditable",!1),this.invoke("toolbar.deactivate",!0)},this.triggerEvent=function(){var a=d.head(arguments),e=d.tail(d.from(arguments)),f=this.options.callbacks[c.namespaceToCamel(a,"on")];f&&f.apply(b[0],e),b.trigger("summernote."+a,e)},this.initializeModule=function(a){var d=this.modules[a];d.shouldInitialize=d.shouldInitialize||c.ok,d.shouldInitialize()&&(d.initialize&&d.initialize(),d.events&&p.attachEvents(b,d.events))},this.module=function(a,b,c){return 1===arguments.length?this.modules[a]:(this.modules[a]=new b(this),void(c||this.initializeModule(a)))},this.removeModule=function(a){var c=this.modules[a];c.shouldInitialize()&&(c.events&&p.detachEvents(b,c.events),c.destroy&&c.destroy()),delete this.modules[a]},this.memo=function(a,b){return 1===arguments.length?this.memos[a]:void(this.memos[a]=b)},this.removeMemo=function(a){this.memos[a]&&this.memos[a].destroy&&this.memos[a].destroy(),delete this.memos[a]},this.createInvokeHandler=function(b,c){return function(d){d.preventDefault(),f.invoke(b,c||a(d.target).closest("[data-value]").data("value"))}},this.invoke=function(){var a=d.head(arguments),b=d.tail(d.from(arguments)),c=a.split("."),e=c.length>1,f=e&&d.head(c),g=e?d.last(c):d.head(c),h=this.modules[f||"editor"];return!f&&this[g]?this[g].apply(this,b):h&&h[g]&&h.shouldInitialize()?h[g].apply(h,b):void 0},this.initialize()};a.fn.extend({summernote:function(){var b=a.type(d.head(arguments)),c="string"===b,e="object"===b,f=e?d.head(arguments):{};f=a.extend({},a.summernote.options,f),f.langInfo=a.extend(!0,{},a.summernote.lang["en-US"],a.summernote.lang[f.lang]),this.each(function(b,c){var d=a(c);if(!d.data("summernote")){var e=new q(d,f);d.data("summernote",e),d.data("summernote").triggerEvent("init",e.layoutInfo)}});var g=this.first();if(g.length){var h=g.data("summernote");if(c)return h.invoke.apply(h,d.from(arguments));f.focus&&h.invoke("editor.focus")}return this}});var r=function(b,c,d,e){this.render=function(f){var g=a(b);if(d&&d.contents&&g.html(d.contents),d&&d.className&&g.addClass(d.className),d&&d.data&&a.each(d.data,function(a,b){g.attr("data-"+a,b)}),d&&d.click&&g.on("click",d.click),c){var h=g.find(".note-children-container");c.forEach(function(a){a.render(h.length?h:g)})}return e&&e(g,d),d&&d.callback&&d.callback(g),f&&f.append(g),g}},s={create:function(b,c){return function(){var d=a.isArray(arguments[0])?arguments[0]:[],e="object"==typeof arguments[1]?arguments[1]:arguments[0];return e&&e.children&&(d=e.children),new r(b,d,e,c)}}},t=s.create('<div class="note-editor note-frame panel panel-default"/>'),u=s.create('<div class="note-toolbar panel-heading"/>'),v=s.create('<div class="note-editing-area"/>'),w=s.create('<textarea class="note-codable"/>'),x=s.create('<div class="note-editable panel-body" contentEditable="true"/>'),y=s.create(['<div class="note-statusbar">','  <div class="note-resizebar">','    <div class="note-icon-bar"/>','    <div class="note-icon-bar"/>','    <div class="note-icon-bar"/>',"  </div>","</div>"].join("")),z=s.create('<div class="note-editor"/>'),A=s.create('<div class="note-editable" contentEditable="true"/>'),B=s.create('<div class="note-btn-group btn-group">'),C=s.create('<button type="button" class="note-btn btn btn-default btn-sm">',function(a,b){b&&b.tooltip&&a.attr({title:b.tooltip}).tooltip({container:"body",trigger:"hover",placement:"bottom"})}),D=s.create('<div class="dropdown-menu">',function(b,c){var d=a.isArray(c.items)?c.items.map(function(a){var b="string"==typeof a?a:a.value||"",d=c.template?c.template(a):a;return'<li><a href="#" data-value="'+b+'">'+d+"</a></li>"}).join(""):c.items;b.html(d)}),E=s.create('<div class="dropdown-menu note-check">',function(b,c){var d=a.isArray(c.items)?c.items.map(function(a){var b="string"==typeof a?a:a.value||"",d=c.template?c.template(a):a;return'<li><a href="#" data-value="'+b+'">'+I(c.checkClassName)+" "+d+"</a></li>"}).join(""):c.items;b.html(d)}),F=s.create('<div class="note-color-palette"/>',function(a,b){for(var c=[],d=0,e=b.colors.length;e>d;d++){for(var f=b.eventName,g=b.colors[d],h=[],i=0,j=g.length;j>i;i++){var k=g[i];h.push(['<button type="button" class="note-color-btn"','style="background-color:',k,'" ','data-event="',f,'" ','data-value="',k,'" ','title="',k,'" ','data-toggle="button" tabindex="-1"></button>'].join(""))}c.push('<div class="note-color-row">'+h.join("")+"</div>")}a.html(c.join("")),a.find(".note-color-btn").tooltip({container:"body",trigger:"hover",placement:"bottom"})}),G=s.create('<div class="modal" aria-hidden="false" tabindex="-1"/>',function(a,b){b.fade&&a.addClass("fade"),a.html(['<div class="modal-dialog">','  <div class="modal-content">',b.title?'    <div class="modal-header">      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>      <h4 class="modal-title">'+b.title+"</h4>    </div>":"",'    <div class="modal-body">'+b.body+"</div>",b.footer?'    <div class="modal-footer">'+b.footer+"</div>":"","  </div>","</div>"].join(""))}),H=s.create(['<div class="note-popover popover in">','  <div class="arrow"/>','  <div class="popover-content note-children-container"/>',"</div>"].join(""),function(a,b){var c="undefined"!=typeof b.direction?b.direction:"bottom";a.addClass(c),b.hideArrow&&a.find(".arrow").hide()}),I=function(a,b){return b=b||"i","<"+b+' class="'+a+'"/>'},J={editor:t,toolbar:u,editingArea:v,codable:w,editable:x,statusbar:y,airEditor:z,airEditable:A,buttonGroup:B,button:C,dropdown:D,dropdownCheck:E,palette:F,dialog:G,popover:H,icon:I,toggleBtn:function(a,b){a.toggleClass("disabled",!b),a.attr("disabled",!b)},toggleBtnActive:function(a,b){a.toggleClass("active",b)},onDialogShown:function(a,b){a.one("shown.bs.modal",b)},onDialogHidden:function(a,b){a.one("hidden.bs.modal",b)},showDialog:function(a){a.modal("show")},hideDialog:function(a){a.modal("hide")},createLayout:function(a,b){var c=(b.airMode?J.airEditor([J.editingArea([J.airEditable()])]):J.editor([J.toolbar(),J.editingArea([J.codable(),J.editable()]),J.statusbar()])).render();return c.insertAfter(a),{note:a,editor:c,toolbar:c.find(".note-toolbar"),editingArea:c.find(".note-editing-area"),editable:c.find(".note-editable"),codable:c.find(".note-codable"),statusbar:c.find(".note-statusbar")}},removeLayout:function(a,b){a.html(b.editable.html()),b.editor.remove(),a.show()}};a.summernote=a.summernote||{lang:{}},a.extend(a.summernote.lang,{"en-US":{font:{bold:"Bold",italic:"Italic",underline:"Underline",clear:"Remove Font Style",height:"Line Height",name:"Font Family",strikethrough:"Strikethrough",subscript:"Subscript",superscript:"Superscript",size:"Font Size"},image:{image:"Picture",insert:"Insert Image",resizeFull:"Resize Full",resizeHalf:"Resize Half",resizeQuarter:"Resize Quarter",floatLeft:"Float Left",floatRight:"Float Right",floatNone:"Float None",shapeRounded:"Shape: Rounded",shapeCircle:"Shape: Circle",shapeThumbnail:"Shape: Thumbnail",shapeNone:"Shape: None",dragImageHere:"Drag image or text here",dropImage:"Drop image or Text",selectFromFiles:"Select from files",maximumFileSize:"Maximum file size",maximumFileSizeError:"Maximum file size exceeded.",url:"Image URL",remove:"Remove Image"},video:{video:"Video",videoLink:"Video Link",insert:"Insert Video",url:"Video URL?",providers:"(YouTube, Vimeo, Vine, Instagram, DailyMotion or Youku)"},link:{link:"Link",insert:"Insert Link",unlink:"Unlink",edit:"Edit",textToDisplay:"Text to display",url:"To what URL should this link go?",openInNewWindow:"Open in new window"},table:{table:"Table"},hr:{insert:"Insert Horizontal Rule"},style:{style:"Style",normal:"Normal",blockquote:"Quote",pre:"Code",h1:"Header 1",h2:"Header 2",h3:"Header 3",h4:"Header 4",h5:"Header 5",h6:"Header 6"},lists:{unordered:"Unordered list",ordered:"Ordered list"},options:{help:"Help",fullscreen:"Full Screen",codeview:"Code View"},paragraph:{paragraph:"Paragraph",outdent:"Outdent",indent:"Indent",left:"Align left",center:"Align center",right:"Align right",justify:"Justify full"},color:{recent:"Recent Color",more:"More Color",background:"Background Color",foreground:"Foreground Color",transparent:"Transparent",setTransparent:"Set transparent",reset:"Reset",resetToDefault:"Reset to default"},shortcut:{shortcuts:"Keyboard shortcuts",close:"Close",textFormatting:"Text formatting",action:"Action",paragraphFormatting:"Paragraph formatting",documentStyle:"Document Style",extraKeys:"Extra keys"},help:{insertParagraph:"Insert Paragraph",undo:"Undoes the last command",redo:"Redoes the last command",tab:"Tab",untab:"Untab",bold:"Set a bold style",italic:"Set a italic style",underline:"Set a underline style",strikethrough:"Set a strikethrough style",removeFormat:"Clean a style",justifyLeft:"Set left align",justifyCenter:"Set center align",justifyRight:"Set right align",justifyFull:"Set full align",insertUnorderedList:"Toggle unordered list",insertOrderedList:"Toggle ordered list",outdent:"Outdent on current paragraph",indent:"Indent on current paragraph",formatPara:"Change current block's format as a paragraph(P tag)",formatH1:"Change current block's format as H1",formatH2:"Change current block's format as H2",formatH3:"Change current block's format as H3",formatH4:"Change current block's format as H4",formatH5:"Change current block's format as H5",formatH6:"Change current block's format as H6",insertHorizontalRule:"Insert horizontal rule","linkDialog.show":"Show Link Dialog"},history:{undo:"Undo",redo:"Redo"},specialChar:{specialChar:"SPECIAL CHARACTERS",select:"Select Special characters"}}});var K,L=function(){var a={BACKSPACE:8,TAB:9,ENTER:13,SPACE:32,LEFT:37,UP:38,RIGHT:39,DOWN:40,NUM0:48,NUM1:49,NUM2:50,NUM3:51,NUM4:52,NUM5:53,NUM6:54,NUM7:55,NUM8:56,B:66,E:69,I:73,J:74,K:75,L:76,R:82,S:83,U:85,V:86,Y:89,Z:90,SLASH:191,LEFTBRACKET:219,BACKSLASH:220,RIGHTBRACKET:221};return{isEdit:function(b){return d.contains([a.BACKSPACE,a.TAB,a.ENTER,a.SPACE],b)},isMove:function(b){return d.contains([a.LEFT,a.UP,a.RIGHT,a.DOWN],b)},nameFromCode:c.invertObject(a),code:a}}(),M=function(){var b=function(a,b){var c,e,f=a.parentElement(),g=document.body.createTextRange(),h=d.from(f.childNodes);for(c=0;c<h.length;c++)if(!p.isText(h[c])){if(g.moveToElementText(h[c]),g.compareEndPoints("StartToStart",a)>=0)break;e=h[c]}if(0!==c&&p.isText(h[c-1])){var i=document.body.createTextRange(),j=null;i.moveToElementText(e||f),i.collapse(!e),j=e?e.nextSibling:f.firstChild;var k=a.duplicate();k.setEndPoint("StartToStart",i);for(var l=k.text.replace(/[\r\n]/g,"").length;l>j.nodeValue.length&&j.nextSibling;)l-=j.nodeValue.length,j=j.nextSibling;j.nodeValue;b&&j.nextSibling&&p.isText(j.nextSibling)&&l===j.nodeValue.length&&(l-=j.nodeValue.length,j=j.nextSibling),f=j,c=l}return{cont:f,offset:c}},e=function(a){var b=function(a,e){var f,g;if(p.isText(a)){var h=p.listPrev(a,c.not(p.isText)),i=d.last(h).previousSibling;f=i||a.parentNode,e+=d.sum(d.tail(h),p.nodeLength),g=!i}else{if(f=a.childNodes[e]||a,p.isText(f))return b(f,0);e=0,g=!1}return{node:f,collapseToStart:g,offset:e}},e=document.body.createTextRange(),f=b(a.node,a.offset);return e.moveToElementText(f.node),e.collapse(f.collapseToStart),e.moveStart("character",f.offset),e},f=function(b,g,h,i){this.sc=b,this.so=g,this.ec=h,this.eo=i;var j=function(){if(m.isW3CRangeSupport){var a=document.createRange();return a.setStart(b,g),a.setEnd(h,i),a}var c=e({node:b,offset:g});return c.setEndPoint("EndToEnd",e({node:h,offset:i})),c};this.getPoints=function(){return{sc:b,so:g,ec:h,eo:i}},this.getStartPoint=function(){return{node:b,offset:g}},this.getEndPoint=function(){return{node:h,offset:i}},this.select=function(){var a=j();if(m.isW3CRangeSupport){var b=document.getSelection();b.rangeCount>0&&b.removeAllRanges(),b.addRange(a)}else a.select();return this},this.scrollIntoView=function(b){var c=a(b).height();return b.scrollTop+c<this.sc.offsetTop&&(b.scrollTop+=Math.abs(b.scrollTop+c-this.sc.offsetTop)),this},this.normalize=function(){var a=function(a,b){if(p.isVisiblePoint(a)&&!p.isEdgePoint(a)||p.isVisiblePoint(a)&&p.isRightEdgePoint(a)&&!b||p.isVisiblePoint(a)&&p.isLeftEdgePoint(a)&&b||p.isVisiblePoint(a)&&p.isBlock(a.node)&&p.isEmpty(a.node))return a;var c=p.ancestor(a.node,p.isBlock);if((p.isLeftEdgePointOf(a,c)||p.isVoid(p.prevPoint(a).node))&&!b||(p.isRightEdgePointOf(a,c)||p.isVoid(p.nextPoint(a).node))&&b){if(p.isVisiblePoint(a))return a;b=!b}var d=b?p.nextPointUntil(p.nextPoint(a),p.isVisiblePoint):p.prevPointUntil(p.prevPoint(a),p.isVisiblePoint);return d||a},b=a(this.getEndPoint(),!1),c=this.isCollapsed()?b:a(this.getStartPoint(),!0);return new f(c.node,c.offset,b.node,b.offset)},this.nodes=function(a,b){a=a||c.ok;var e=b&&b.includeAncestor,f=b&&b.fullyContains,g=this.getStartPoint(),h=this.getEndPoint(),i=[],j=[];return p.walkPoint(g,h,function(b){if(!p.isEditable(b.node)){var c;f?(p.isLeftEdgePoint(b)&&j.push(b.node),p.isRightEdgePoint(b)&&d.contains(j,b.node)&&(c=b.node)):c=e?p.ancestor(b.node,a):b.node,c&&a(c)&&i.push(c)}},!0),d.unique(i)},this.commonAncestor=function(){return p.commonAncestor(b,h)},this.expand=function(a){var c=p.ancestor(b,a),d=p.ancestor(h,a);if(!c&&!d)return new f(b,g,h,i);var e=this.getPoints();return c&&(e.sc=c,e.so=0),d&&(e.ec=d,e.eo=p.nodeLength(d)),new f(e.sc,e.so,e.ec,e.eo)},this.collapse=function(a){return a?new f(b,g,b,g):new f(h,i,h,i)},this.splitText=function(){var a=b===h,c=this.getPoints();return p.isText(h)&&!p.isEdgePoint(this.getEndPoint())&&h.splitText(i),p.isText(b)&&!p.isEdgePoint(this.getStartPoint())&&(c.sc=b.splitText(g),c.so=0,a&&(c.ec=c.sc,c.eo=i-g)),new f(c.sc,c.so,c.ec,c.eo)},this.deleteContents=function(){if(this.isCollapsed())return this;var b=this.splitText(),c=b.nodes(null,{fullyContains:!0}),e=p.prevPointUntil(b.getStartPoint(),function(a){return!d.contains(c,a.node)}),g=[];return a.each(c,function(a,b){var c=b.parentNode;e.node!==c&&1===p.nodeLength(c)&&g.push(c),p.remove(b,!1)}),a.each(g,function(a,b){p.remove(b,!1)}),new f(e.node,e.offset,e.node,e.offset).normalize()};var k=function(a){return function(){var c=p.ancestor(b,a);return!!c&&c===p.ancestor(h,a)}};this.isOnEditable=k(p.isEditable),this.isOnList=k(p.isList),this.isOnAnchor=k(p.isAnchor),this.isOnCell=k(p.isCell),this.isLeftEdgeOf=function(a){if(!p.isLeftEdgePoint(this.getStartPoint()))return!1;var b=p.ancestor(this.sc,a);return b&&p.isLeftEdgeOf(this.sc,b)},this.isCollapsed=function(){return b===h&&g===i},this.wrapBodyInlineWithPara=function(){if(p.isBodyContainer(b)&&p.isEmpty(b))return b.innerHTML=p.emptyPara,new f(b.firstChild,0,b.firstChild,0);var a=this.normalize();if(p.isParaInline(b)||p.isPara(b))return a;var e;if(p.isInline(a.sc)){var g=p.listAncestor(a.sc,c.not(p.isInline));e=d.last(g),p.isInline(e)||(e=g[g.length-2]||a.sc.childNodes[a.so])}else e=a.sc.childNodes[a.so>0?a.so-1:0];var h=p.listPrev(e,p.isParaInline).reverse();if(h=h.concat(p.listNext(e.nextSibling,p.isParaInline)),h.length){var i=p.wrap(d.head(h),"p");p.appendChildNodes(i,d.tail(h))}return this.normalize()},this.insertNode=function(a){var b=this.wrapBodyInlineWithPara().deleteContents(),c=p.splitPoint(b.getStartPoint(),p.isInline(a));return c.rightNode?c.rightNode.parentNode.insertBefore(a,c.rightNode):c.container.appendChild(a),a},this.pasteHTML=function(b){var c=a("<div></div>").html(b)[0],e=d.from(c.childNodes),f=this.wrapBodyInlineWithPara().deleteContents();return e.reverse().map(function(a){return f.insertNode(a)}).reverse()},this.toString=function(){var a=j();return m.isW3CRangeSupport?a.toString():a.text},this.getWordRange=function(a){var b=this.getEndPoint();if(!p.isCharPoint(b))return this;var c=p.prevPointUntil(b,function(a){return!p.isCharPoint(a)});return a&&(b=p.nextPointUntil(b,function(a){return!p.isCharPoint(a)})),new f(c.node,c.offset,b.node,b.offset)},this.bookmark=function(a){return{s:{path:p.makeOffsetPath(a,b),offset:g},e:{path:p.makeOffsetPath(a,h),offset:i}}},this.paraBookmark=function(a){return{s:{path:d.tail(p.makeOffsetPath(d.head(a),b)),offset:g},e:{path:d.tail(p.makeOffsetPath(d.last(a),h)),offset:i}}},this.getClientRects=function(){var a=j();return a.getClientRects()}};return{create:function(a,b,c,d){if(4===arguments.length)return new f(a,b,c,d);if(2===arguments.length)return c=a,d=b,new f(a,b,c,d);var e=this.createFromSelection();return e||1!==arguments.length?e:(e=this.createFromNode(arguments[0]),e.collapse(p.emptyPara===arguments[0].innerHTML))},createFromSelection:function(){var a,c,d,e;if(m.isW3CRangeSupport){var g=document.getSelection();if(!g||0===g.rangeCount)return null;if(p.isBody(g.anchorNode))return null;var h=g.getRangeAt(0);a=h.startContainer,c=h.startOffset,d=h.endContainer,e=h.endOffset}else{var i=document.selection.createRange(),j=i.duplicate();j.collapse(!1);var k=i;k.collapse(!0);var l=b(k,!0),n=b(j,!1);p.isText(l.node)&&p.isLeftEdgePoint(l)&&p.isTextNode(n.node)&&p.isRightEdgePoint(n)&&n.node.nextSibling===l.node&&(l=n),a=l.cont,c=l.offset,d=n.cont,e=n.offset}return new f(a,c,d,e)},createFromNode:function(a){var b=a,c=0,d=a,e=p.nodeLength(d);return p.isVoid(b)&&(c=p.listPrev(b).length-1,b=b.parentNode),p.isBR(d)?(e=p.listPrev(d).length-1,d=d.parentNode):p.isVoid(d)&&(e=p.listPrev(d).length,d=d.parentNode),this.create(b,c,d,e)},createFromNodeBefore:function(a){return this.createFromNode(a).collapse(!0)},createFromNodeAfter:function(a){return this.createFromNode(a).collapse()},createFromBookmark:function(a,b){var c=p.fromOffsetPath(a,b.s.path),d=b.s.offset,e=p.fromOffsetPath(a,b.e.path),g=b.e.offset;return new f(c,d,e,g)},createFromParaBookmark:function(a,b){var c=a.s.offset,e=a.e.offset,g=p.fromOffsetPath(d.head(b),a.s.path),h=p.fromOffsetPath(d.last(b),a.e.path);return new f(g,c,h,e)}}}(),N=function(){var b=function(b){return a.Deferred(function(c){a.extend(new FileReader,{onload:function(a){var b=a.target.result;c.resolve(b)},onerror:function(){c.reject(this)}}).readAsDataURL(b)}).promise()},c=function(b){return a.Deferred(function(c){var d=a("<img>");d.one("load",function(){d.off("error abort"),c.resolve(d)}).one("error abort",function(){d.off("load").detach(),c.reject(d)}).css({display:"none"}).appendTo(document.body).attr("src",b)}).promise()};return{readFileAsDataURL:b,createImage:c}}(),O=function(a){var b=[],c=-1,d=a[0],e=function(){var b=M.create(d),c={s:{path:[],offset:0},e:{path:[],offset:0}};return{contents:a.html(),bookmark:b?b.bookmark(d):c}},f=function(b){null!==b.contents&&a.html(b.contents),null!==b.bookmark&&M.createFromBookmark(d,b.bookmark).select()};this.rewind=function(){a.html()!==b[c].contents&&this.recordUndo(),c=0,f(b[c])},this.reset=function(){b=[],c=-1,a.html(""),this.recordUndo()},this.undo=function(){a.html()!==b[c].contents&&this.recordUndo(),c>0&&(c--,f(b[c]))},this.redo=function(){b.length-1>c&&(c++,f(b[c]))},this.recordUndo=function(){c++,b.length>c&&(b=b.slice(0,c)),b.push(e())}},P=function(){var b=function(b,c){if(m.jqueryVersion<1.9){var d={};return a.each(c,function(a,c){d[c]=b.css(c)}),d}return b.css.call(b,c)};this.fromNode=function(a){var c=["font-family","font-size","text-align","list-style-type","line-height"],d=b(a,c)||{};return d["font-size"]=parseInt(d["font-size"],10),d},this.stylePara=function(b,c){a.each(b.nodes(p.isPara,{includeAncestor:!0}),function(b,d){a(d).css(c)})},this.styleNodes=function(b,e){b=b.splitText();var f=e&&e.nodeName||"SPAN",g=!(!e||!e.expandClosestSibling),h=!(!e||!e.onlyPartialContains);if(b.isCollapsed())return[b.insertNode(p.create(f))];var i=p.makePredByNodeName(f),j=b.nodes(p.isText,{fullyContains:!0}).map(function(a){return p.singleChildAncestor(a,i)||p.wrap(a,f)});if(g){if(h){var k=b.nodes();i=c.and(i,function(a){return d.contains(k,a)})}return j.map(function(b){var c=p.withClosestSiblings(b,i),e=d.head(c),f=d.tail(c);
return a.each(f,function(a,b){p.appendChildNodes(e,b.childNodes),p.remove(b)}),d.head(c)})}return j},this.current=function(b){var c=a(p.isElement(b.sc)?b.sc:b.sc.parentNode),d=this.fromNode(c);try{d=a.extend(d,{"font-bold":document.queryCommandState("bold")?"bold":"normal","font-italic":document.queryCommandState("italic")?"italic":"normal","font-underline":document.queryCommandState("underline")?"underline":"normal","font-subscript":document.queryCommandState("subscript")?"subscript":"normal","font-superscript":document.queryCommandState("superscript")?"superscript":"normal","font-strikethrough":document.queryCommandState("strikethrough")?"strikethrough":"normal"})}catch(e){}if(b.isOnList()){var f=["circle","disc","disc-leading-zero","square"],g=a.inArray(d["list-style-type"],f)>-1;d["list-style"]=g?"unordered":"ordered"}else d["list-style"]="none";var h=p.ancestor(b.sc,p.isPara);if(h&&h.style["line-height"])d["line-height"]=h.style.lineHeight;else{var i=parseInt(d["line-height"],10)/parseInt(d["font-size"],10);d["line-height"]=i.toFixed(1)}return d.anchor=b.isOnAnchor()&&p.ancestor(b.sc,p.isAnchor),d.ancestors=p.listAncestor(b.sc,p.isEditable),d.range=b,d}},Q=function(){var b=this;this.insertOrderedList=function(a){this.toggleList("OL",a)},this.insertUnorderedList=function(a){this.toggleList("UL",a)},this.indent=function(b){var e=this,f=M.create(b).wrapBodyInlineWithPara(),g=f.nodes(p.isPara,{includeAncestor:!0}),h=d.clusterBy(g,c.peq2("parentNode"));a.each(h,function(b,c){var f=d.head(c);p.isLi(f)?e.wrapList(c,f.parentNode.nodeName):a.each(c,function(b,c){a(c).css("marginLeft",function(a,b){return(parseInt(b,10)||0)+25})})}),f.select()},this.outdent=function(b){var e=this,f=M.create(b).wrapBodyInlineWithPara(),g=f.nodes(p.isPara,{includeAncestor:!0}),h=d.clusterBy(g,c.peq2("parentNode"));a.each(h,function(b,c){var f=d.head(c);p.isLi(f)?e.releaseList([c]):a.each(c,function(b,c){a(c).css("marginLeft",function(a,b){return b=parseInt(b,10)||0,b>25?b-25:""})})}),f.select()},this.toggleList=function(e,f){var g=M.create(f).wrapBodyInlineWithPara(),h=g.nodes(p.isPara,{includeAncestor:!0}),i=g.paraBookmark(h),j=d.clusterBy(h,c.peq2("parentNode"));if(d.find(h,p.isPurePara)){var k=[];a.each(j,function(a,c){k=k.concat(b.wrapList(c,e))}),h=k}else{var l=g.nodes(p.isList,{includeAncestor:!0}).filter(function(b){return!a.nodeName(b,e)});l.length?a.each(l,function(a,b){p.replace(b,e)}):h=this.releaseList(j,!0)}M.createFromParaBookmark(i,h).select()},this.wrapList=function(a,b){var c=d.head(a),e=d.last(a),f=p.isList(c.previousSibling)&&c.previousSibling,g=p.isList(e.nextSibling)&&e.nextSibling,h=f||p.insertAfter(p.create(b||"UL"),e);return a=a.map(function(a){return p.isPurePara(a)?p.replace(a,"LI"):a}),p.appendChildNodes(h,a),g&&(p.appendChildNodes(h,d.from(g.childNodes)),p.remove(g)),a},this.releaseList=function(b,c){var e=[];return a.each(b,function(b,f){var g=d.head(f),h=d.last(f),i=c?p.lastAncestor(g,p.isList):g.parentNode,j=i.childNodes.length>1?p.splitTree(i,{node:h.parentNode,offset:p.position(h)+1},{isSkipPaddingBlankHTML:!0}):null,k=p.splitTree(i,{node:g.parentNode,offset:p.position(g)},{isSkipPaddingBlankHTML:!0});f=c?p.listDescendant(k,p.isLi):d.from(k.childNodes).filter(p.isLi),(c||!p.isList(i.parentNode))&&(f=f.map(function(a){return p.replace(a,"P")})),a.each(d.from(f).reverse(),function(a,b){p.insertAfter(b,i)});var l=d.compact([i,k,j]);a.each(l,function(b,c){var d=[c].concat(p.listDescendant(c,p.isList));a.each(d.reverse(),function(a,b){p.nodeLength(b)||p.remove(b,!0)})}),e=e.concat(f)}),e}},R=function(){var b=new Q;this.insertTab=function(a,b){var c=p.createText(new Array(b+1).join(p.NBSP_CHAR));a=a.deleteContents(),a.insertNode(c,!0),a=M.create(c,b),a.select()},this.insertParagraph=function(c){var d=M.create(c);d=d.deleteContents(),d=d.wrapBodyInlineWithPara();var e,f=p.ancestor(d.sc,p.isPara);if(f){if(p.isEmpty(f)&&p.isLi(f))return void b.toggleList(f.parentNode.nodeName);if(p.isEmpty(f)&&p.isPara(f)&&p.isBlockquote(f.parentNode))p.insertAfter(f,f.parentNode),e=f;else{e=p.splitTree(f,d.getStartPoint());var g=p.listDescendant(f,p.isEmptyAnchor);g=g.concat(p.listDescendant(e,p.isEmptyAnchor)),a.each(g,function(a,b){p.remove(b)}),(p.isHeading(e)||p.isPre(e))&&p.isEmpty(e)&&(e=p.replace(e,"p"))}}else{var h=d.sc.childNodes[d.so];e=a(p.emptyPara)[0],h?d.sc.insertBefore(e,h):d.sc.appendChild(e)}M.create(e,0).normalize().select().scrollIntoView(c)}},S=function(){this.tab=function(a,b){var c=p.ancestor(a.commonAncestor(),p.isCell),e=p.ancestor(c,p.isTable),f=p.listDescendant(e,p.isCell),g=d[b?"prev":"next"](f,c);g&&M.create(g,0).select()},this.createTable=function(b,c,d){for(var e,f=[],g=0;b>g;g++)f.push("<td>"+p.blank+"</td>");e=f.join("");for(var h,i=[],j=0;c>j;j++)i.push("<tr>"+e+"</tr>");h=i.join("");var k=a("<table>"+h+"</table>");return d&&d.tableClassName&&k.addClass(d.tableClassName),k[0]}},T="bogus",U=function(b){var c=this,e=b.layoutInfo.note,f=b.layoutInfo.editor,g=b.layoutInfo.editable,h=b.options,i=h.langInfo,j=g[0],k=null,l=new P,n=new S,o=new R,q=new Q,r=new O(g);this.initialize=function(){g.on("keydown",function(a){a.keyCode===L.code.ENTER&&b.triggerEvent("enter",a),b.triggerEvent("keydown",a),h.shortcuts&&!a.isDefaultPrevented()&&c.handleKeyMap(a)}).on("keyup",function(a){b.triggerEvent("keyup",a)}).on("focus",function(a){b.triggerEvent("focus",a)}).on("blur",function(a){b.triggerEvent("blur",a)}).on("mousedown",function(a){b.triggerEvent("mousedown",a)}).on("mouseup",function(a){b.triggerEvent("mouseup",a)}).on("scroll",function(a){b.triggerEvent("scroll",a)}).on("paste",function(a){b.triggerEvent("paste",a)}),g.html(p.html(e)||p.emptyPara);var a=m.isMSIE?"DOMCharacterDataModified DOMSubtreeModified DOMNodeInserted":"input";g.on(a,function(){b.triggerEvent("change",g.html())}),f.on("focusin",function(a){b.triggerEvent("focusin",a)}).on("focusout",function(a){b.triggerEvent("focusout",a)}),!h.airMode&&h.height&&this.setHeight(h.height),!h.airMode&&h.maxHeight&&g.css("max-height",h.maxHeight),!h.airMode&&h.minHeight&&g.css("min-height",h.minHeight),r.recordUndo()},this.destroy=function(){g.off()},this.handleKeyMap=function(a){var c=h.keyMap[m.isMac?"mac":"pc"],d=[];a.metaKey&&d.push("CMD"),a.ctrlKey&&!a.altKey&&d.push("CTRL"),a.shiftKey&&d.push("SHIFT");var e=L.nameFromCode[a.keyCode];e&&d.push(e);var f=c[d.join("+")];f?(a.preventDefault(),b.invoke(f)):L.isEdit(a.keyCode)&&this.afterCommand()},this.createRange=function(){return this.focus(),M.create(j)},this.saveRange=function(a){k=this.createRange(),a&&k.collapse().select()},this.restoreRange=function(){k&&(k.select(),this.focus())},this.saveTarget=function(a){g.data("target",a)},this.clearTarget=function(){g.removeData("target")},this.restoreTarget=function(){return g.data("target")},this.currentStyle=function(){var a=M.create();return a&&(a=a.normalize()),a?l.current(a):l.fromNode(g)},this.styleFromNode=function(a){return l.fromNode(a)},this.undo=function(){b.triggerEvent("before.command",g.html()),r.undo(),b.triggerEvent("change",g.html())},b.memo("help.undo",i.help.undo),this.redo=function(){b.triggerEvent("before.command",g.html()),r.redo(),b.triggerEvent("change",g.html())},b.memo("help.redo",i.help.redo);for(var s=this.beforeCommand=function(){b.triggerEvent("before.command",g.html()),c.focus()},t=this.afterCommand=function(a){r.recordUndo(),a||b.triggerEvent("change",g.html())},u=["bold","italic","underline","strikethrough","superscript","subscript","justifyLeft","justifyCenter","justifyRight","justifyFull","formatBlock","removeFormat","backColor","foreColor","fontName"],v=0,w=u.length;w>v;v++)this[u[v]]=function(a){return function(b){s(),document.execCommand(a,!1,b),t(!0)}}(u[v]),b.memo("help."+u[v],i.help[u[v]]);this.tab=function(){var a=this.createRange();a.isCollapsed()&&a.isOnCell()?n.tab(a):(s(),o.insertTab(a,h.tabSize),t())},b.memo("help.tab",i.help.tab),this.untab=function(){var a=this.createRange();a.isCollapsed()&&a.isOnCell()&&n.tab(a,!0)},b.memo("help.untab",i.help.untab),this.wrapCommand=function(a){return function(){s(),a.apply(c,arguments),t()}},this.insertParagraph=this.wrapCommand(function(){o.insertParagraph(j)}),b.memo("help.insertParagraph",i.help.insertParagraph),this.insertOrderedList=this.wrapCommand(function(){q.insertOrderedList(j)}),b.memo("help.insertOrderedList",i.help.insertOrderedList),this.insertUnorderedList=this.wrapCommand(function(){q.insertUnorderedList(j)}),b.memo("help.insertUnorderedList",i.help.insertUnorderedList),this.indent=this.wrapCommand(function(){q.indent(j)}),b.memo("help.indent",i.help.indent),this.outdent=this.wrapCommand(function(){q.outdent(j)}),b.memo("help.outdent",i.help.outdent),this.insertImage=function(a,c){return N.createImage(a,c).then(function(a){s(),"function"==typeof c?c(a):("string"==typeof c&&a.attr("data-filename",c),a.css("width",Math.min(g.width(),a.width()))),a.show(),M.create(j).insertNode(a[0]),M.createFromNodeAfter(a[0]).select(),t()}).fail(function(a){b.triggerEvent("image.upload.error",a)})},this.insertImages=function(d){a.each(d,function(a,d){var e=d.name;h.maximumImageFileSize&&h.maximumImageFileSize<d.size?b.triggerEvent("image.upload.error",i.image.maximumFileSizeError):N.readFileAsDataURL(d).then(function(a){return c.insertImage(a,e)}).fail(function(){b.triggerEvent("image.upload.error")})})},this.insertImagesOrCallback=function(a){var c=h.callbacks;c.onImageUpload?b.triggerEvent("image.upload",a):this.insertImages(a)},this.insertNode=this.wrapCommand(function(a){var b=this.createRange();b.insertNode(a),M.createFromNodeAfter(a).select()}),this.insertText=this.wrapCommand(function(a){var b=this.createRange(),c=b.insertNode(p.createText(a));M.create(c,p.nodeLength(c)).select()}),this.getSelectedText=function(){var a=this.createRange();return a.isOnAnchor()&&(a=M.createFromNode(p.ancestor(a.sc,p.isAnchor))),a.toString()},this.pasteHTML=this.wrapCommand(function(a){var b=this.createRange().pasteHTML(a);M.createFromNodeAfter(d.last(b)).select()}),this.formatBlock=this.wrapCommand(function(a){a=m.isMSIE?"<"+a+">":a,document.execCommand("FormatBlock",!1,a)}),this.formatPara=function(){this.formatBlock("P")},b.memo("help.formatPara",i.help.formatPara);for(var v=1;6>=v;v++)this["formatH"+v]=function(a){return function(){this.formatBlock("H"+a)}}(v),b.memo("help.formatH"+v,i.help["formatH"+v]);this.fontSize=function(b){var c=this.createRange();if(c&&c.isCollapsed()){var e=l.styleNodes(c),f=d.head(e);a(e).css({"font-size":b+"px"}),f&&!p.nodeLength(f)&&(f.innerHTML=p.ZERO_WIDTH_NBSP_CHAR,M.createFromNodeAfter(f.firstChild).select(),g.data(T,f))}else s(),a(l.styleNodes(c)).css({"font-size":b+"px"}),t()},this.insertHorizontalRule=this.wrapCommand(function(){var a=this.createRange().insertNode(p.create("HR"));a.nextSibling&&M.create(a.nextSibling,0).normalize().select()}),b.memo("help.insertHorizontalRule",i.help.insertHorizontalRule),this.removeBogus=function(){var a=g.data(T);if(a){var b=d.find(d.from(a.childNodes),p.isText),c=b.nodeValue.indexOf(p.ZERO_WIDTH_NBSP_CHAR);-1!==c&&b.deleteData(c,1),p.isEmpty(a)&&p.remove(a),g.removeData(T)}},this.lineHeight=this.wrapCommand(function(a){l.stylePara(this.createRange(),{lineHeight:a})}),this.unlink=function(){var a=this.createRange();if(a.isOnAnchor()){var b=p.ancestor(a.sc,p.isAnchor);a=M.createFromNode(b),a.select(),s(),document.execCommand("unlink"),t()}},this.createLink=this.wrapCommand(function(b){var c=b.url,e=b.text,f=b.isNewWindow,g=b.range||this.createRange(),i=g.toString()!==e;h.onCreateLink&&(c=h.onCreateLink(c));var j=[];if(i){g=g.deleteContents();var k=g.insertNode(a("<A>"+e+"</A>")[0]);j.push(k)}else j=l.styleNodes(g,{nodeName:"A",expandClosestSibling:!0,onlyPartialContains:!0});a.each(j,function(b,d){a(d).attr("href",c),f?a(d).attr("target","_blank"):a(d).removeAttr("target")});var m=M.createFromNodeBefore(d.head(j)),n=m.getStartPoint(),o=M.createFromNodeAfter(d.last(j)),p=o.getEndPoint();M.create(n.node,n.offset,p.node,p.offset).select()}),this.getLinkInfo=function(){var b=this.createRange().expand(p.isAnchor),c=a(d.head(b.nodes(p.isAnchor)));return{range:b,text:b.toString(),isNewWindow:c.length?"_blank"===c.attr("target"):!1,url:c.length?c.attr("href"):""}},this.color=this.wrapCommand(function(a){var b=a.foreColor,c=a.backColor;b&&document.execCommand("foreColor",!1,b),c&&document.execCommand("backColor",!1,c)}),this.insertTable=this.wrapCommand(function(a){var b=a.split("x"),c=this.createRange().deleteContents();c.insertNode(n.createTable(b[0],b[1],h))}),this.floatMe=this.wrapCommand(function(b){var c=a(this.restoreTarget());c.css("float",b)}),this.resize=this.wrapCommand(function(b){var c=a(this.restoreTarget());c.css({width:100*b+"%",height:""})}),this.resizeTo=function(a,b,c){var d;if(c){var e=a.y/a.x,f=b.data("ratio");d={width:f>e?a.x:a.y/f,height:f>e?a.x*f:a.y}}else d={width:a.x,height:a.y};b.css(d)},this.removeMedia=this.wrapCommand(function(){var c=a(this.restoreTarget()).detach();b.triggerEvent("media.delete",c,g)}),this.hasFocus=function(){return g.is(":focus")},this.focus=function(){this.hasFocus()||g.focus()},this.isEmpty=function(){return p.isEmpty(g[0])||p.emptyPara===g.html()},this.empty=function(){b.invoke("code",p.emptyPara)},this.setHeight=function(a){g.outerHeight(a)}},V=function(b){var c=this,e=b.layoutInfo.editable;this.events={"summernote.keydown":function(a,d){c.needKeydownHook()&&(d.ctrlKey||d.metaKey)&&d.keyCode===L.code.V&&(b.invoke("editor.saveRange"),c.$paste.focus(),setTimeout(function(){c.pasteByHook()},0))}},this.needKeydownHook=function(){return m.isMSIE&&m.browserVersion>10||m.isFF},this.initialize=function(){this.needKeydownHook()?(this.$paste=a("<div />").attr("contenteditable",!0).css({position:"absolute",left:-1e5,opacity:0}),e.before(this.$paste),this.$paste.on("paste",function(a){b.triggerEvent("paste",a)})):e.on("paste",this.pasteByEvent)},this.destroy=function(){this.needKeydownHook()&&(this.$paste.remove(),this.$paste=null)},this.pasteByHook=function(){var c=this.$paste[0].firstChild;if(p.isImg(c)){for(var d=c.src,e=atob(d.split(",")[1]),f=new Uint8Array(e.length),g=0;g<e.length;g++)f[g]=e.charCodeAt(g);var h=new Blob([f],{type:"image/png"});h.name="clipboard.png",b.invoke("editor.restoreRange"),b.invoke("editor.focus"),b.invoke("editor.insertImagesOrCallback",[h])}else{var i=a("<div />").html(this.$paste.html()).html();b.invoke("editor.restoreRange"),b.invoke("editor.focus"),i&&b.invoke("editor.pasteHTML",i)}this.$paste.empty()},this.pasteByEvent=function(a){var c=a.originalEvent.clipboardData;if(c&&c.items&&c.items.length){var e=d.head(c.items);"file"===e.kind&&-1!==e.type.indexOf("image/")&&b.invoke("editor.insertImagesOrCallback",[e.getAsFile()]),b.invoke("editor.afterCommand")}}},W=function(b){var c=a(document),d=b.layoutInfo.editor,e=b.layoutInfo.editable,f=b.options,g=f.langInfo,h=a(['<div class="note-dropzone">','  <div class="note-dropzone-message"/>',"</div>"].join("")).prependTo(d);this.initialize=function(){f.disableDragAndDrop?c.on("drop",function(a){a.preventDefault()}):this.attachDragAndDropEvent()},this.attachDragAndDropEvent=function(){var f=a(),i=h.find(".note-dropzone-message");c.on("dragenter",function(a){var c=b.invoke("codeview.isActivated"),e=d.width()>0&&d.height()>0;c||f.length||!e||(d.addClass("dragover"),h.width(d.width()),h.height(d.height()),i.text(g.image.dragImageHere)),f=f.add(a.target)}).on("dragleave",function(a){f=f.not(a.target),f.length||d.removeClass("dragover")}).on("drop",function(){f=a(),d.removeClass("dragover")}),h.on("dragenter",function(){h.addClass("hover"),i.text(g.image.dropImage)}).on("dragleave",function(){h.removeClass("hover"),i.text(g.image.dragImageHere)}),h.on("drop",function(c){var d=c.originalEvent.dataTransfer;d&&d.files&&d.files.length?(c.preventDefault(),e.focus(),b.invoke("editor.insertImagesOrCallback",d.files)):a.each(d.types,function(c,e){var f=d.getData(e);e.toLowerCase().indexOf("text")>-1?b.invoke("editor.pasteHTML",f):a(f).each(function(){b.invoke("editor.insertNode",this)})})}).on("dragover",!1)}};m.hasCodeMirror&&(m.isSupportAmd?require(["codemirror"],function(a){K=a}):K=window.CodeMirror);var X=function(a){var b=a.layoutInfo.editor,c=a.layoutInfo.editable,d=a.layoutInfo.codable,e=a.options;this.sync=function(){var a=this.isActivated();a&&m.hasCodeMirror&&d.data("cmEditor").save()},this.isActivated=function(){return b.hasClass("codeview")},this.toggle=function(){this.isActivated()?this.deactivate():this.activate(),a.triggerEvent("codeview.toggled")},this.activate=function(){if(d.val(p.html(c,e.prettifyHtml)),d.height(c.height()),a.invoke("toolbar.updateCodeview",!0),b.addClass("codeview"),d.focus(),m.hasCodeMirror){var f=K.fromTextArea(d[0],e.codemirror);if(e.codemirror.tern){var g=new K.TernServer(e.codemirror.tern);f.ternServer=g,f.on("cursorActivity",function(a){g.updateArgHints(a)})}f.setSize(null,c.outerHeight()),d.data("cmEditor",f)}},this.deactivate=function(){if(m.hasCodeMirror){var f=d.data("cmEditor");d.val(f.getValue()),f.toTextArea()}var g=p.value(d,e.prettifyHtml)||p.emptyPara,h=c.html()!==g;c.html(g),c.height(e.height?d.height():"auto"),b.removeClass("codeview"),h&&a.triggerEvent("change",c.html(),c),c.focus(),a.invoke("toolbar.updateCodeview",!1)},this.destroy=function(){this.isActivated()&&this.deactivate()}},Y=24,Z=function(b){var c=a(document),d=b.layoutInfo.statusbar,e=b.layoutInfo.editable,f=b.options;this.initialize=function(){f.airMode||f.disableResizeEditor||d.on("mousedown",function(a){a.preventDefault(),a.stopPropagation();var b=e.offset().top-c.scrollTop();c.on("mousemove",function(a){var c=a.clientY-(b+Y);c=f.minheight>0?Math.max(c,f.minheight):c,c=f.maxHeight>0?Math.min(c,f.maxHeight):c,e.height(c)}).one("mouseup",function(){c.off("mousemove")})})},this.destroy=function(){d.off()}},$=function(b){var c=b.layoutInfo.editor,d=b.layoutInfo.toolbar,e=b.layoutInfo.editable,f=b.layoutInfo.codable,g=a(window),h=a("html, body");this.toggle=function(){var a=function(a){e.css("height",a.h),f.css("height",a.h),f.data("cmeditor")&&f.data("cmeditor").setsize(null,a.h)};c.toggleClass("fullscreen"),this.isFullscreen()?(e.data("orgHeight",e.css("height")),g.on("resize",function(){a({h:g.height()-d.outerHeight()})}).trigger("resize"),h.css("overflow","hidden")):(g.off("resize"),a({h:e.data("orgHeight")}),h.css("overflow","visible")),b.invoke("toolbar.updateFullscreen",this.isFullscreen())},this.isFullscreen=function(){return c.hasClass("fullscreen")}},_=function(b){var c=this,d=a(document),e=b.layoutInfo.editingArea,f=b.options;this.events={"summernote.mousedown":function(a,b){c.update(b.target)&&b.preventDefault()},"summernote.keyup summernote.scroll summernote.change summernote.dialog.shown":function(){c.update()}},this.initialize=function(){this.$handle=a(['<div class="note-handle">','<div class="note-control-selection">','<div class="note-control-selection-bg"></div>','<div class="note-control-holder note-control-nw"></div>','<div class="note-control-holder note-control-ne"></div>','<div class="note-control-holder note-control-sw"></div>','<div class="',f.disableResizeImage?"note-control-holder":"note-control-sizing",' note-control-se"></div>',f.disableResizeImage?"":'<div class="note-control-selection-info"></div>',"</div>","</div>"].join("")).prependTo(e),this.$handle.on("mousedown",function(a){if(p.isControlSizing(a.target)){a.preventDefault(),a.stopPropagation();var e=c.$handle.find(".note-control-selection").data("target"),f=e.offset(),g=d.scrollTop();d.on("mousemove",function(a){b.invoke("editor.resizeTo",{x:a.clientX-f.left,y:a.clientY-(f.top-g)},e,!a.shiftKey),c.update(e[0])}).one("mouseup",function(a){a.preventDefault(),d.off("mousemove"),b.invoke("editor.afterCommand")}),e.data("ratio")||e.data("ratio",e.height()/e.width())}})},this.destroy=function(){this.$handle.remove()},this.update=function(c){var d=p.isImg(c),e=this.$handle.find(".note-control-selection");if(b.invoke("imagePopover.update",c),d){var f=a(c),g=f.position(),h={w:f.outerWidth(!0),h:f.outerHeight(!0)};e.css({display:"block",left:g.left,top:g.top,width:h.w,height:h.h}).data("target",f);var i=h.w+"x"+h.h;e.find(".note-control-selection-info").text(i),b.invoke("editor.saveTarget",c)}else this.hide();return d},this.hide=function(){b.invoke("editor.clearTarget"),this.$handle.children().hide()}},aa=function(b){var c=this,e="http://",f=/^(https?:\/\/|ssh:\/\/|ftp:\/\/|file:\/|mailto:[A-Z0-9._%+-]+@)?(www\.)?(.+)$/i;this.events={"summernote.keyup":function(a,b){b.isDefaultPrevented()||c.handleKeyup(b)},"summernote.keydown":function(a,b){c.handleKeydown(b)}},this.initialize=function(){this.lastWordRange=null},this.destroy=function(){this.lastWordRange=null},this.replace=function(){if(this.lastWordRange){var c=this.lastWordRange.toString(),d=c.match(f);if(d&&(d[1]||d[2])){var g=d[1]?c:e+c,h=a("<a />").html(c).attr("href",g)[0];this.lastWordRange.insertNode(h),this.lastWordRange=null,b.invoke("editor.focus")}}},this.handleKeydown=function(a){if(d.contains([L.code.ENTER,L.code.SPACE],a.keyCode)){var c=b.invoke("editor.createRange").getWordRange();this.lastWordRange=c}},this.handleKeyup=function(a){d.contains([L.code.ENTER,L.code.SPACE],a.keyCode)&&this.replace()}},ba=function(a){var b=a.layoutInfo.note;this.events={"summernote.change":function(){b.val(a.invoke("code"))}},this.shouldInitialize=function(){return p.isTextarea(b[0])}},ca=function(b){var c=this,d=b.layoutInfo.editingArea,e=b.options;this.events={"summernote.init summernote.change":function(){c.update()},"summernote.codeview.toggled":function(){c.update()}},this.shouldInitialize=function(){return!!e.placeholder},this.initialize=function(){this.$placeholder=a('<div class="note-placeholder">'),this.$placeholder.on("click",function(){b.invoke("focus")}).text(e.placeholder).prependTo(d)},this.destroy=function(){this.$placeholder.remove()},this.update=function(){var a=!b.invoke("codeview.isActivated")&&b.invoke("editor.isEmpty");this.$placeholder.toggle(a)}},da=function(b){var e=this,f=a.summernote.ui,g=b.layoutInfo.toolbar,h=b.options,i=h.langInfo,j=c.invertObject(h.keyMap[m.isMac?"mac":"pc"]),k=this.representShortcut=function(a){var b=j[a];return m.isMac&&(b=b.replace("CMD","⌘").replace("SHIFT","⇧")),b=b.replace("BACKSLASH","\\").replace("SLASH","/").replace("LEFTBRACKET","[").replace("RIGHTBRACKET","]")," ("+b+")"};this.initialize=function(){this.addToolbarButtons(),this.addImagePopoverButtons(),this.addLinkPopoverButtons(),this.fontInstalledMap={}},this.destroy=function(){delete this.fontInstalledMap},this.isFontInstalled=function(a){return e.fontInstalledMap.hasOwnProperty(a)||(e.fontInstalledMap[a]=m.isFontInstalled(a)||d.contains(h.fontNamesIgnoreCheck,a)),e.fontInstalledMap[a]},this.addToolbarButtons=function(){b.memo("button.style",function(){return f.buttonGroup([f.button({className:"dropdown-toggle",contents:f.icon(h.icons.magic)+" "+f.icon(h.icons.caret,"span"),tooltip:i.style.style,data:{toggle:"dropdown"}}),f.dropdown({className:"dropdown-style",items:b.options.styleTags,template:function(a){"string"==typeof a&&(a={tag:a,title:a});var b=a.tag,c=a.title,d=a.style?' style="'+a.style+'" ':"",e=a.className?' className="'+a.className+'"':"";return"<"+b+d+e+">"+c+"</"+b+">"},click:b.createInvokeHandler("editor.formatBlock")})]).render()}),b.memo("button.bold",function(){return f.button({className:"note-btn-bold",contents:f.icon(h.icons.bold),tooltip:i.font.bold+k("bold"),click:b.createInvokeHandler("editor.bold")}).render()}),b.memo("button.italic",function(){return f.button({className:"note-btn-italic",contents:f.icon(h.icons.italic),tooltip:i.font.italic+k("italic"),click:b.createInvokeHandler("editor.italic")}).render()}),b.memo("button.underline",function(){return f.button({className:"note-btn-underline",contents:f.icon(h.icons.underline),tooltip:i.font.underline+k("underline"),click:b.createInvokeHandler("editor.underline")}).render()}),b.memo("button.clear",function(){return f.button({contents:f.icon(h.icons.eraser),tooltip:i.font.clear+k("removeFormat"),click:b.createInvokeHandler("editor.removeFormat")}).render()}),b.memo("button.strikethrough",function(){return f.button({className:"note-btn-strikethrough",contents:f.icon(h.icons.strikethrough),tooltip:i.font.strikethrough+k("strikethrough"),click:b.createInvokeHandler("editor.strikethrough")}).render()}),b.memo("button.superscript",function(){return f.button({className:"note-btn-superscript",contents:f.icon(h.icons.superscript),tooltip:i.font.superscript,click:b.createInvokeHandler("editor.superscript")}).render()}),b.memo("button.subscript",function(){return f.button({className:"note-btn-subscript",contents:f.icon(h.icons.subscript),tooltip:i.font.subscript,click:b.createInvokeHandler("editor.subscript")}).render()}),b.memo("button.fontname",function(){return f.buttonGroup([f.button({className:"dropdown-toggle",contents:'<span class="note-current-fontname"/> '+f.icon(h.icons.caret,"span"),tooltip:i.font.name,data:{toggle:"dropdown"}}),f.dropdownCheck({className:"dropdown-fontname",checkClassName:h.icons.menuCheck,items:h.fontNames.filter(e.isFontInstalled),template:function(a){return'<span style="font-family:'+a+'">'+a+"</span>"},click:b.createInvokeHandler("editor.fontName")})]).render()}),b.memo("button.fontsize",function(){return f.buttonGroup([f.button({className:"dropdown-toggle",contents:'<span class="note-current-fontsize"/>'+f.icon(h.icons.caret,"span"),tooltip:i.font.size,data:{toggle:"dropdown"}}),f.dropdownCheck({className:"dropdown-fontsize",checkClassName:h.icons.menuCheck,items:h.fontSizes,click:b.createInvokeHandler("editor.fontSize")})]).render()}),b.memo("button.color",function(){return f.buttonGroup({className:"note-color",children:[f.button({className:"note-current-color-button",contents:f.icon(h.icons.font+" note-recent-color"),tooltip:i.color.recent,click:function(c){var d=a(c.currentTarget);b.invoke("editor.color",{backColor:d.attr("data-backColor"),foreColor:d.attr("data-foreColor")})},callback:function(a){var b=a.find(".note-recent-color");b.css("background-color","#FFFF00"),a.attr("data-backColor","#FFFF00")}}),f.button({className:"dropdown-toggle",contents:f.icon(h.icons.caret,"span"),tooltip:i.color.more,data:{toggle:"dropdown"}}),f.dropdown({items:["<li>",'<div class="btn-group">','  <div class="note-palette-title">'+i.color.background+"</div>","  <div>",'    <button type="button" class="note-color-reset btn btn-default" data-event="backColor" data-value="inherit">',i.color.transparent,"    </button>","  </div>",'  <div class="note-holder" data-event="backColor"/>',"</div>",'<div class="btn-group">','  <div class="note-palette-title">'+i.color.foreground+"</div>","  <div>",'    <button type="button" class="note-color-reset btn btn-default" data-event="removeFormat" data-value="foreColor">',i.color.resetToDefault,"    </button>","  </div>",'  <div class="note-holder" data-event="foreColor"/>',"</div>","</li>"].join(""),callback:function(b){b.find(".note-holder").each(function(){var b=a(this);b.append(f.palette({colors:h.colors,eventName:b.data("event")}).render())})},click:function(c){var d=a(c.target),e=d.data("event"),f=d.data("value");if(e&&f){var g="backColor"===e?"background-color":"color",h=d.closest(".note-color").find(".note-recent-color"),i=d.closest(".note-color").find(".note-current-color-button");h.css(g,f),i.attr("data-"+e,f),b.invoke("editor."+e,f)}}})]}).render()}),b.memo("button.ul",function(){return f.button({contents:f.icon(h.icons.unorderedlist),tooltip:i.lists.unordered+k("insertUnorderedList"),click:b.createInvokeHandler("editor.insertUnorderedList")}).render()}),b.memo("button.ol",function(){return f.button({contents:f.icon(h.icons.orderedlist),tooltip:i.lists.ordered+k("insertOrderedList"),click:b.createInvokeHandler("editor.insertOrderedList")}).render()});var d=f.button({contents:f.icon(h.icons.alignLeft),tooltip:i.paragraph.left+k("justifyLeft"),click:b.createInvokeHandler("editor.justifyLeft")}),g=f.button({contents:f.icon(h.icons.alignCenter),tooltip:i.paragraph.center+k("justifyCenter"),click:b.createInvokeHandler("editor.justifyCenter")}),j=f.button({contents:f.icon(h.icons.alignRight),tooltip:i.paragraph.right+k("justifyRight"),click:b.createInvokeHandler("editor.justifyRight")}),l=f.button({contents:f.icon(h.icons.alignJustify),tooltip:i.paragraph.justify+k("justifyFull"),click:b.createInvokeHandler("editor.justifyFull")}),m=f.button({contents:f.icon(h.icons.outdent),tooltip:i.paragraph.outdent+k("outdent"),click:b.createInvokeHandler("editor.outdent")}),n=f.button({contents:f.icon(h.icons.indent),tooltip:i.paragraph.indent+k("indent"),click:b.createInvokeHandler("editor.indent")});b.memo("button.justifyLeft",c.invoke(d,"render")),b.memo("button.justifyCenter",c.invoke(g,"render")),b.memo("button.justifyRight",c.invoke(j,"render")),b.memo("button.justifyFull",c.invoke(l,"render")),b.memo("button.outdent",c.invoke(m,"render")),b.memo("button.indent",c.invoke(n,"render")),b.memo("button.paragraph",function(){return f.buttonGroup([f.button({className:"dropdown-toggle",contents:f.icon(h.icons.alignLeft)+" "+f.icon(h.icons.caret,"span"),tooltip:i.paragraph.paragraph,data:{toggle:"dropdown"}}),f.dropdown([f.buttonGroup({className:"note-align",children:[d,g,j,l]}),f.buttonGroup({className:"note-list",children:[m,n]})])]).render()}),b.memo("button.height",function(){return f.buttonGroup([f.button({className:"dropdown-toggle",contents:f.icon(h.icons.textHeight)+" "+f.icon(h.icons.caret,"span"),tooltip:i.font.height,data:{toggle:"dropdown"}}),f.dropdownCheck({items:h.lineHeights,checkClassName:h.icons.menuCheck,className:"dropdown-line-height",click:b.createInvokeHandler("editor.lineHeight")})]).render()}),b.memo("button.table",function(){return f.buttonGroup([f.button({className:"dropdown-toggle",contents:f.icon(h.icons.table)+" "+f.icon(h.icons.caret,"span"),tooltip:i.table.table,data:{toggle:"dropdown"}}),f.dropdown({className:"note-table",items:['<div class="note-dimension-picker">','  <div class="note-dimension-picker-mousecatcher" data-event="insertTable" data-value="1x1"/>','  <div class="note-dimension-picker-highlighted"/>','  <div class="note-dimension-picker-unhighlighted"/>',"</div>",'<div class="note-dimension-display">1 x 1</div>'].join("")})],{callback:function(a){var c=a.find(".note-dimension-picker-mousecatcher");c.css({width:h.insertTableMaxSize.col+"em",height:h.insertTableMaxSize.row+"em"}).mousedown(b.createInvokeHandler("editor.insertTable")).on("mousemove",e.tableMoveHandler)}}).render()}),b.memo("button.link",function(){return f.button({contents:f.icon(h.icons.link),tooltip:i.link.link,click:b.createInvokeHandler("linkDialog.show")}).render()}),b.memo("button.picture",function(){return f.button({contents:f.icon(h.icons.picture),tooltip:i.image.image,click:b.createInvokeHandler("imageDialog.show")}).render()}),b.memo("button.video",function(){return f.button({contents:f.icon(h.icons.video),tooltip:i.video.video,click:b.createInvokeHandler("videoDialog.show")}).render()}),b.memo("button.hr",function(){return f.button({contents:f.icon(h.icons.minus),tooltip:i.hr.insert+k("insertHorizontalRule"),click:b.createInvokeHandler("editor.insertHorizontalRule")}).render()}),b.memo("button.fullscreen",function(){return f.button({className:"btn-fullscreen",contents:f.icon(h.icons.arrowsAlt),tooltip:i.options.fullscreen,click:b.createInvokeHandler("fullscreen.toggle")}).render()}),b.memo("button.codeview",function(){return f.button({className:"btn-codeview",contents:f.icon(h.icons.code),tooltip:i.options.codeview,click:b.createInvokeHandler("codeview.toggle")}).render()}),b.memo("button.redo",function(){return f.button({contents:f.icon(h.icons.redo),tooltip:i.history.redo+k("redo"),click:b.createInvokeHandler("editor.redo")}).render()}),b.memo("button.undo",function(){return f.button({contents:f.icon(h.icons.undo),tooltip:i.history.undo+k("undo"),click:b.createInvokeHandler("editor.undo")}).render()}),b.memo("button.help",function(){return f.button({contents:f.icon(h.icons.question),tooltip:i.options.help,click:b.createInvokeHandler("helpDialog.show")
}).render()})},this.addImagePopoverButtons=function(){b.memo("button.imageSize100",function(){return f.button({contents:'<span class="note-fontsize-10">100%</span>',tooltip:i.image.resizeFull,click:b.createInvokeHandler("editor.resize","1")}).render()}),b.memo("button.imageSize50",function(){return f.button({contents:'<span class="note-fontsize-10">50%</span>',tooltip:i.image.resizeHalf,click:b.createInvokeHandler("editor.resize","0.5")}).render()}),b.memo("button.imageSize25",function(){return f.button({contents:'<span class="note-fontsize-10">25%</span>',tooltip:i.image.resizeQuarter,click:b.createInvokeHandler("editor.resize","0.25")}).render()}),b.memo("button.floatLeft",function(){return f.button({contents:f.icon(h.icons.alignLeft),tooltip:i.image.floatLeft,click:b.createInvokeHandler("editor.floatMe","left")}).render()}),b.memo("button.floatRight",function(){return f.button({contents:f.icon(h.icons.alignRight),tooltip:i.image.floatRight,click:b.createInvokeHandler("editor.floatMe","right")}).render()}),b.memo("button.floatNone",function(){return f.button({contents:f.icon(h.icons.alignJustify),tooltip:i.image.floatNone,click:b.createInvokeHandler("editor.floatMe","none")}).render()}),b.memo("button.removeMedia",function(){return f.button({contents:f.icon(h.icons.trash),tooltip:i.image.remove,click:b.createInvokeHandler("editor.removeMedia")}).render()})},this.addLinkPopoverButtons=function(){b.memo("button.linkDialogShow",function(){return f.button({contents:f.icon(h.icons.link),tooltip:i.link.edit,click:b.createInvokeHandler("linkDialog.show")}).render()}),b.memo("button.unlink",function(){return f.button({contents:f.icon(h.icons.unlink),tooltip:i.link.unlink,click:b.createInvokeHandler("editor.unlink")}).render()})},this.build=function(a,c){for(var d=0,e=c.length;e>d;d++){for(var g=c[d],h=g[0],i=g[1],j=f.buttonGroup({className:"note-"+h}).render(),k=0,l=i.length;l>k;k++){var m=b.memo("button."+i[k]);m&&j.append("function"==typeof m?m(b):m)}j.appendTo(a)}},this.updateCurrentStyle=function(){var c=b.invoke("editor.currentStyle");if(this.updateBtnStates({".note-btn-bold":function(){return"bold"===c["font-bold"]},".note-btn-italic":function(){return"italic"===c["font-italic"]},".note-btn-underline":function(){return"underline"===c["font-underline"]},".note-btn-subscript":function(){return"subscript"===c["font-subscript"]},".note-btn-superscript":function(){return"superscript"===c["font-superscript"]},".note-btn-strikethrough":function(){return"strikethrough"===c["font-strikethrough"]}}),c["font-family"]){var f=c["font-family"].split(",").map(function(a){return a.replace(/[\'\"]/g,"").replace(/\s+$/,"").replace(/^\s+/,"")}),h=d.find(f,e.isFontInstalled);g.find(".dropdown-fontname li a").each(function(){var b=a(this).data("value")+""==h+"";this.className=b?"checked":""}),g.find(".note-current-fontname").text(h)}if(c["font-size"]){var i=c["font-size"];g.find(".dropdown-fontsize li a").each(function(){var b=a(this).data("value")+""==i+"";this.className=b?"checked":""}),g.find(".note-current-fontsize").text(i)}if(c["line-height"]){var j=c["line-height"];g.find(".dropdown-line-height li a").each(function(){var b=a(this).data("value")+""==j+"";this.className=b?"checked":""})}},this.updateBtnStates=function(b){a.each(b,function(a,b){f.toggleBtnActive(g.find(a),b())})},this.tableMoveHandler=function(b){var c,d=18,e=a(b.target.parentNode),f=e.next(),g=e.find(".note-dimension-picker-mousecatcher"),i=e.find(".note-dimension-picker-highlighted"),j=e.find(".note-dimension-picker-unhighlighted");if(void 0===b.offsetX){var k=a(b.target).offset();c={x:b.pageX-k.left,y:b.pageY-k.top}}else c={x:b.offsetX,y:b.offsetY};var l={c:Math.ceil(c.x/d)||1,r:Math.ceil(c.y/d)||1};i.css({width:l.c+"em",height:l.r+"em"}),g.data("value",l.c+"x"+l.r),3<l.c&&l.c<h.insertTableMaxSize.col&&j.css({width:l.c+1+"em"}),3<l.r&&l.r<h.insertTableMaxSize.row&&j.css({height:l.r+1+"em"}),f.html(l.c+" x "+l.r)}},ea=function(b){var c=a.summernote.ui,d=b.layoutInfo.note,e=b.layoutInfo.toolbar,f=b.options;this.shouldInitialize=function(){return!f.airMode},this.initialize=function(){f.toolbar=f.toolbar||[],f.toolbar.length?b.invoke("buttons.build",e,f.toolbar):e.hide(),f.toolbarContainer&&e.appendTo(f.toolbarContainer),d.on("summernote.keyup summernote.mouseup summernote.change",function(){b.invoke("buttons.updateCurrentStyle")}),b.invoke("buttons.updateCurrentStyle")},this.destroy=function(){e.children().remove()},this.updateFullscreen=function(a){c.toggleBtnActive(e.find(".btn-fullscreen"),a)},this.updateCodeview=function(a){c.toggleBtnActive(e.find(".btn-codeview"),a),a?this.deactivate():this.activate()},this.activate=function(a){var b=e.find("button");a||(b=b.not(".btn-codeview")),c.toggleBtn(b,!0)},this.deactivate=function(a){var b=e.find("button");a||(b=b.not(".btn-codeview")),c.toggleBtn(b,!1)}},fa=function(b){var c=this,d=a.summernote.ui,e=b.layoutInfo.editor,f=b.options,g=f.langInfo;this.initialize=function(){var b=f.dialogsInBody?a(document.body):e,c='<div class="form-group"><label>'+g.link.textToDisplay+'</label><input class="note-link-text form-control" type="text" /></div><div class="form-group"><label>'+g.link.url+'</label><input class="note-link-url form-control" type="text" value="http://" /></div>'+(f.disableLinkTarget?"":'<div class="checkbox"><label><input type="checkbox" checked> '+g.link.openInNewWindow+"</label></div>"),h='<button href="#" class="btn btn-primary note-link-btn disabled" disabled>'+g.link.insert+"</button>";this.$dialog=d.dialog({className:"link-dialog",title:g.link.insert,fade:f.dialogsFade,body:c,footer:h}).render().appendTo(b)},this.destroy=function(){d.hideDialog(this.$dialog),this.$dialog.remove()},this.bindEnterKey=function(a,b){a.on("keypress",function(a){a.keyCode===L.code.ENTER&&b.trigger("click")})},this.showLinkDialog=function(e){return a.Deferred(function(a){var f=c.$dialog.find(".note-link-text"),g=c.$dialog.find(".note-link-url"),h=c.$dialog.find(".note-link-btn"),i=c.$dialog.find("input[type=checkbox]");d.onDialogShown(c.$dialog,function(){b.triggerEvent("dialog.shown"),f.val(e.text),f.on("input",function(){d.toggleBtn(h,f.val()&&g.val()),e.text=f.val()}),e.url||(e.url=e.text||"http://",d.toggleBtn(h,e.text)),g.on("input",function(){d.toggleBtn(h,f.val()&&g.val()),e.text||f.val(g.val())}).val(e.url).trigger("focus"),c.bindEnterKey(g,h),c.bindEnterKey(f,h),i.prop("checked",e.isNewWindow),h.one("click",function(b){b.preventDefault(),a.resolve({range:e.range,url:g.val(),text:f.val(),isNewWindow:i.is(":checked")}),c.$dialog.modal("hide")})}),d.onDialogHidden(c.$dialog,function(){f.off("input keypress"),g.off("input keypress"),h.off("click"),"pending"===a.state()&&a.reject()}),d.showDialog(c.$dialog)}).promise()},this.show=function(){var a=b.invoke("editor.getLinkInfo");b.invoke("editor.saveRange"),this.showLinkDialog(a).then(function(a){b.invoke("editor.restoreRange"),b.invoke("editor.createLink",a)}).fail(function(){b.invoke("editor.restoreRange")})},b.memo("help.linkDialog.show",f.langInfo.help["linkDialog.show"])},ga=function(b){var c=this,e=a.summernote.ui,f=b.options;this.events={"summernote.keyup summernote.mouseup summernote.change summernote.scroll":function(){c.update()},"summernote.dialog.shown":function(){c.hide()}},this.shouldInitialize=function(){return!d.isEmpty(f.popover.link)},this.initialize=function(){this.$popover=e.popover({className:"note-link-popover",callback:function(a){var b=a.find(".popover-content");b.prepend('<span><a target="_blank"></a>&nbsp;</span>')}}).render().appendTo("body");var a=this.$popover.find(".popover-content");b.invoke("buttons.build",a,f.popover.link)},this.destroy=function(){this.$popover.remove()},this.update=function(){if(!b.invoke("editor.hasFocus"))return void this.hide();var c=b.invoke("editor.createRange");if(c.isCollapsed()&&c.isOnAnchor()){var d=p.ancestor(c.sc,p.isAnchor),e=a(d).attr("href");this.$popover.find("a").attr("href",e).html(e);var f=p.posFromPlaceholder(d);this.$popover.css({display:"block",left:f.left,top:f.top})}else this.hide()},this.hide=function(){this.$popover.hide()}},ha=function(b){var c=this,d=a.summernote.ui,e=b.layoutInfo.editor,f=b.options,g=f.langInfo;this.initialize=function(){var b=f.dialogsInBody?a(document.body):e,c="";if(f.maximumImageFileSize){var h=Math.floor(Math.log(f.maximumImageFileSize)/Math.log(1024)),i=1*(f.maximumImageFileSize/Math.pow(1024,h)).toFixed(2)+" "+" KMGTP"[h]+"B";c="<small>"+g.image.maximumFileSize+" : "+i+"</small>"}var j='<div class="form-group note-group-select-from-files"><label>'+g.image.selectFromFiles+'</label><input class="note-image-input form-control" type="file" name="files" accept="image/*" multiple="multiple" />'+c+'</div><div class="form-group" style="overflow:auto;"><label>'+g.image.url+'</label><input class="note-image-url form-control col-md-12" type="text" /></div>',k='<button href="#" class="btn btn-primary note-image-btn disabled" disabled>'+g.image.insert+"</button>";this.$dialog=d.dialog({title:g.image.insert,fade:f.dialogsFade,body:j,footer:k}).render().appendTo(b)},this.destroy=function(){d.hideDialog(this.$dialog),this.$dialog.remove()},this.bindEnterKey=function(a,b){a.on("keypress",function(a){a.keyCode===L.code.ENTER&&b.trigger("click")})},this.show=function(){b.invoke("editor.saveRange"),this.showImageDialog().then(function(a){d.hideDialog(c.$dialog),b.invoke("editor.restoreRange"),"string"==typeof a?b.invoke("editor.insertImage",a):b.invoke("editor.insertImagesOrCallback",a)}).fail(function(){b.invoke("editor.restoreRange")})},this.showImageDialog=function(){return a.Deferred(function(a){var e=c.$dialog.find(".note-image-input"),f=c.$dialog.find(".note-image-url"),g=c.$dialog.find(".note-image-btn");d.onDialogShown(c.$dialog,function(){b.triggerEvent("dialog.shown"),e.replaceWith(e.clone().on("change",function(){a.resolve(this.files||this.value)}).val("")),g.click(function(b){b.preventDefault(),a.resolve(f.val())}),f.on("keyup paste",function(){var a=f.val();d.toggleBtn(g,a)}).val("").trigger("focus"),c.bindEnterKey(f,g)}),d.onDialogHidden(c.$dialog,function(){e.off("change"),f.off("keyup paste keypress"),g.off("click"),"pending"===a.state()&&a.reject()}),d.showDialog(c.$dialog)})}},ia=function(b){var c=a.summernote.ui,e=b.options;this.shouldInitialize=function(){return!d.isEmpty(e.popover.image)},this.initialize=function(){this.$popover=c.popover({className:"note-image-popover"}).render().appendTo("body");var a=this.$popover.find(".popover-content");b.invoke("buttons.build",a,e.popover.image)},this.destroy=function(){this.$popover.remove()},this.update=function(a){if(p.isImg(a)){var b=p.posFromPlaceholder(a);this.$popover.css({display:"block",left:b.left,top:b.top})}else this.hide()},this.hide=function(){this.$popover.hide()}},ja=function(b){var c=this,d=a.summernote.ui,e=b.layoutInfo.editor,f=b.options,g=f.langInfo;this.initialize=function(){var b=f.dialogsInBody?a(document.body):e,c='<div class="form-group row-fluid"><label>'+g.video.url+' <small class="text-muted">'+g.video.providers+'</small></label><input class="note-video-url form-control span12" type="text" /></div>',h='<button href="#" class="btn btn-primary note-video-btn disabled" disabled>'+g.video.insert+"</button>";this.$dialog=d.dialog({title:g.video.insert,fade:f.dialogsFade,body:c,footer:h}).render().appendTo(b)},this.destroy=function(){d.hideDialog(this.$dialog),this.$dialog.remove()},this.bindEnterKey=function(a,b){a.on("keypress",function(a){a.keyCode===L.code.ENTER&&b.trigger("click")})},this.createVideoNode=function(b){var c,d=/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,e=b.match(d),f=/\/\/instagram.com\/p\/(.[a-zA-Z0-9_-]*)/,g=b.match(f),h=/\/\/vine.co\/v\/(.[a-zA-Z0-9]*)/,i=b.match(h),j=/\/\/(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/,k=b.match(j),l=/.+dailymotion.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/,m=b.match(l),n=/\/\/v\.youku\.com\/v_show\/id_(\w+)=*\.html/,o=b.match(n),p=/^.+.(mp4|m4v)$/,q=b.match(p),r=/^.+.(ogg|ogv)$/,s=b.match(r),t=/^.+.(webm)$/,u=b.match(t);if(e&&11===e[1].length){var v=e[1];c=a("<iframe>").attr("frameborder",0).attr("src","//www.youtube.com/embed/"+v).attr("width","640").attr("height","360")}else if(g&&g[0].length)c=a("<iframe>").attr("frameborder",0).attr("src",g[0]+"/embed/").attr("width","612").attr("height","710").attr("scrolling","no").attr("allowtransparency","true");else if(i&&i[0].length)c=a("<iframe>").attr("frameborder",0).attr("src",i[0]+"/embed/simple").attr("width","600").attr("height","600").attr("class","vine-embed");else if(k&&k[3].length)c=a("<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen>").attr("frameborder",0).attr("src","//player.vimeo.com/video/"+k[3]).attr("width","640").attr("height","360");else if(m&&m[2].length)c=a("<iframe>").attr("frameborder",0).attr("src","//www.dailymotion.com/embed/video/"+m[2]).attr("width","640").attr("height","360");else if(o&&o[1].length)c=a("<iframe webkitallowfullscreen mozallowfullscreen allowfullscreen>").attr("frameborder",0).attr("height","498").attr("width","510").attr("src","//player.youku.com/embed/"+o[1]);else{if(!(q||s||u))return!1;c=a("<video controls>").attr("src",b).attr("width","640").attr("height","360")}return c.addClass("note-video-clip"),c[0]},this.show=function(){var a=b.invoke("editor.getSelectedText");b.invoke("editor.saveRange"),this.showVideoDialog(a).then(function(a){d.hideDialog(c.$dialog),b.invoke("editor.restoreRange");var e=c.createVideoNode(a);e&&b.invoke("editor.insertNode",e)}).fail(function(){b.invoke("editor.restoreRange")})},this.showVideoDialog=function(e){return a.Deferred(function(a){var f=c.$dialog.find(".note-video-url"),g=c.$dialog.find(".note-video-btn");d.onDialogShown(c.$dialog,function(){b.triggerEvent("dialog.shown"),f.val(e).on("input",function(){d.toggleBtn(g,f.val())}).trigger("focus"),g.click(function(b){b.preventDefault(),a.resolve(f.val())}),c.bindEnterKey(f,g)}),d.onDialogHidden(c.$dialog,function(){f.off("input"),g.off("click"),"pending"===a.state()&&a.reject()}),d.showDialog(c.$dialog)})}},ka=function(b){var c=this,d=a.summernote.ui,e=b.layoutInfo.editor,f=b.options,g=f.langInfo;this.createShortCutList=function(){var c=f.keyMap[m.isMac?"mac":"pc"];return Object.keys(c).map(function(d){var e=c[d],f=a('<div><div class="help-list-item"/></div>');return f.append(a("<label><kbd>"+d+"</kdb></label>").css({width:180,"margin-right":10})).append(a("<span/>").html(b.memo("help."+e)||e)),f.html()}).join("")},this.initialize=function(){var b=f.dialogsInBody?a(document.body):e,c=['<p class="text-center">','<a href="//summernote.org/" target="_blank">Summernote 0.8.1</a> · ','<a href="//github.com/summernote/summernote" target="_blank">Project</a> · ','<a href="//github.com/summernote/summernote/issues" target="_blank">Issues</a>',"</p>"].join("");this.$dialog=d.dialog({title:g.options.help,fade:f.dialogsFade,body:this.createShortCutList(),footer:c,callback:function(a){a.find(".modal-body").css({"max-height":300,overflow:"scroll"})}}).render().appendTo(b)},this.destroy=function(){d.hideDialog(this.$dialog),this.$dialog.remove()},this.showHelpDialog=function(){return a.Deferred(function(a){d.onDialogShown(c.$dialog,function(){b.triggerEvent("dialog.shown"),a.resolve()}),d.showDialog(c.$dialog)}).promise()},this.show=function(){b.invoke("editor.saveRange"),this.showHelpDialog().then(function(){b.invoke("editor.restoreRange")})}},la=function(b){var e=this,f=a.summernote.ui,g=b.options,h=20;this.events={"summernote.keyup summernote.mouseup summernote.scroll":function(){e.update()},"summernote.change summernote.dialog.shown":function(){e.hide()},"summernote.focusout":function(a,b){m.isFF||b.relatedTarget&&p.ancestor(b.relatedTarget,c.eq(e.$popover[0]))||e.hide()}},this.shouldInitialize=function(){return g.airMode&&!d.isEmpty(g.popover.air)},this.initialize=function(){this.$popover=f.popover({className:"note-air-popover"}).render().appendTo("body");var a=this.$popover.find(".popover-content");b.invoke("buttons.build",a,g.popover.air)},this.destroy=function(){this.$popover.remove()},this.update=function(){var a=b.invoke("editor.currentStyle");if(a.range&&!a.range.isCollapsed()){var e=d.last(a.range.getClientRects());if(e){var f=c.rect2bnd(e);this.$popover.css({display:"block",left:Math.max(f.left+f.width/2,0)-h,top:f.top+f.height})}}else this.hide()},this.hide=function(){this.$popover.hide()}},ma=function(b){var e=this,f=a.summernote.ui,g=5,h=b.options.hint||[],i=b.options.hintDirection||"bottom",j=a.isArray(h)?h:[h];this.events={"summernote.keyup":function(a,b){b.isDefaultPrevented()||e.handleKeyup(b)},"summernote.keydown":function(a,b){e.handleKeydown(b)},"summernote.dialog.shown":function(){e.hide()}},this.shouldInitialize=function(){return j.length>0},this.initialize=function(){this.lastWordRange=null,this.$popover=f.popover({className:"note-hint-popover",hideArrow:!0,direction:""}).render().appendTo("body"),this.$popover.hide(),this.$content=this.$popover.find(".popover-content"),this.$content.on("click",".note-hint-item",function(){e.$content.find(".active").removeClass("active"),a(this).addClass("active"),e.replace()})},this.destroy=function(){this.$popover.remove()},this.selectItem=function(a){this.$content.find(".active").removeClass("active"),a.addClass("active"),this.$content[0].scrollTop=a[0].offsetTop-this.$content.innerHeight()/2},this.moveDown=function(){var a=this.$content.find(".note-hint-item.active"),b=a.next();if(b.length)this.selectItem(b);else{var c=a.parent().next();c.length||(c=this.$content.find(".note-hint-group").first()),this.selectItem(c.find(".note-hint-item").first())}},this.moveUp=function(){var a=this.$content.find(".note-hint-item.active"),b=a.prev();if(b.length)this.selectItem(b);else{var c=a.parent().prev();c.length||(c=this.$content.find(".note-hint-group").last()),this.selectItem(c.find(".note-hint-item").last())}},this.replace=function(){var a=this.$content.find(".note-hint-item.active");if(a.length){var c=this.nodeFromItem(a);this.lastWordRange.insertNode(c),M.createFromNode(c).collapse().select(),this.lastWordRange=null,this.hide(),b.invoke("editor.focus")}},this.nodeFromItem=function(a){var b=j[a.data("index")],c=a.data("item"),d=b.content?b.content(c):c;return"string"==typeof d&&(d=p.createText(d)),d},this.createItemTemplates=function(b,c){var d=j[b];return c.map(function(c,e){var f=a('<div class="note-hint-item"/>');return f.append(d.template?d.template(c):c+""),f.data({index:b,item:c}),0===b&&0===e&&f.addClass("active"),f})},this.handleKeydown=function(a){this.$popover.is(":visible")&&(a.keyCode===L.code.ENTER?(a.preventDefault(),this.replace()):a.keyCode===L.code.UP?(a.preventDefault(),this.moveUp()):a.keyCode===L.code.DOWN&&(a.preventDefault(),this.moveDown()))},this.searchKeyword=function(a,b,c){var d=j[a];if(d&&d.match.test(b)&&d.search){var e=d.match.exec(b);d.search(e[1],c)}else c()},this.createGroup=function(b,c){var d=a('<div class="note-hint-group note-hint-group-'+b+'"/>');return this.searchKeyword(b,c,function(a){a=a||[],a.length&&(d.html(e.createItemTemplates(b,a)),e.show())}),d},this.handleKeyup=function(a){if(d.contains([L.code.ENTER,L.code.UP,L.code.DOWN],a.keyCode)){if(a.keyCode===L.code.ENTER&&this.$popover.is(":visible"))return}else{var f=b.invoke("editor.createRange").getWordRange(),h=f.toString();if(j.length&&h){this.$content.empty();var k=c.rect2bnd(d.last(f.getClientRects()));k&&(this.$popover.hide(),this.lastWordRange=f,j.forEach(function(a,b){a.match.test(h)&&e.createGroup(b,h).appendTo(e.$content)}),"top"===i?this.$popover.css({left:k.left,top:k.top-this.$popover.outerHeight()-g}):this.$popover.css({left:k.left,top:k.top+k.height+g}))}else this.hide()}},this.show=function(){this.$popover.show()},this.hide=function(){this.$popover.hide()}};a.summernote=a.extend(a.summernote,{version:"0.8.1",ui:J,plugins:{},options:{modules:{editor:U,clipboard:V,dropzone:W,codeview:X,statusbar:Z,fullscreen:$,handle:_,hintPopover:ma,autoLink:aa,autoSync:ba,placeholder:ca,buttons:da,toolbar:ea,linkDialog:fa,linkPopover:ga,imageDialog:ha,imagePopover:ia,videoDialog:ja,helpDialog:ka,airPopover:la},buttons:{},lang:"en-US",toolbar:[["style",["style"]],["font",["bold","underline","clear"]],["fontname",["fontname"]],["color",["color"]],["para",["ul","ol","paragraph"]],["table",["table"]],["insert",["link","picture","video"]],["view",["fullscreen","codeview","help"]]],popover:{image:[["imagesize",["imageSize100","imageSize50","imageSize25"]],["float",["floatLeft","floatRight","floatNone"]],["remove",["removeMedia"]]],link:[["link",["linkDialogShow","unlink"]]],air:[["color",["color"]],["font",["bold","underline","clear"]],["para",["ul","paragraph"]],["table",["table"]],["insert",["link","picture"]]]},airMode:!1,width:null,height:null,focus:!1,tabSize:4,styleWithSpan:!0,shortcuts:!0,textareaAutoSync:!0,direction:null,styleTags:["p","blockquote","pre","h1","h2","h3","h4","h5","h6"],fontNames:["Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana"],fontSizes:["8","9","10","11","12","14","18","24","36"],colors:[["#000000","#424242","#636363","#9C9C94","#CEC6CE","#EFEFEF","#F7F7F7","#FFFFFF"],["#FF0000","#FF9C00","#FFFF00","#00FF00","#00FFFF","#0000FF","#9C00FF","#FF00FF"],["#F7C6CE","#FFE7CE","#FFEFC6","#D6EFD6","#CEDEE7","#CEE7F7","#D6D6E7","#E7D6DE"],["#E79C9C","#FFC69C","#FFE79C","#B5D6A5","#A5C6CE","#9CC6EF","#B5A5D6","#D6A5BD"],["#E76363","#F7AD6B","#FFD663","#94BD7B","#73A5AD","#6BADDE","#8C7BC6","#C67BA5"],["#CE0000","#E79439","#EFC631","#6BA54A","#4A7B8C","#3984C6","#634AA5","#A54A7B"],["#9C0000","#B56308","#BD9400","#397B21","#104A5A","#085294","#311873","#731842"],["#630000","#7B3900","#846300","#295218","#083139","#003163","#21104A","#4A1031"]],lineHeights:["1.0","1.2","1.4","1.5","1.6","1.8","2.0","3.0"],tableClassName:"table table-bordered",insertTableMaxSize:{col:10,row:10},dialogsInBody:!1,dialogsFade:!1,maximumImageFileSize:null,callbacks:{onInit:null,onFocus:null,onBlur:null,onEnter:null,onKeyup:null,onKeydown:null,onSubmit:null,onImageUpload:null,onImageUploadError:null},codemirror:{mode:"text/html",htmlMode:!0,lineNumbers:!0},keyMap:{pc:{ENTER:"insertParagraph","CTRL+Z":"undo","CTRL+Y":"redo",TAB:"tab","SHIFT+TAB":"untab","CTRL+B":"bold","CTRL+I":"italic","CTRL+U":"underline","CTRL+SHIFT+S":"strikethrough","CTRL+BACKSLASH":"removeFormat","CTRL+SHIFT+L":"justifyLeft","CTRL+SHIFT+E":"justifyCenter","CTRL+SHIFT+R":"justifyRight","CTRL+SHIFT+J":"justifyFull","CTRL+SHIFT+NUM7":"insertUnorderedList","CTRL+SHIFT+NUM8":"insertOrderedList","CTRL+LEFTBRACKET":"outdent","CTRL+RIGHTBRACKET":"indent","CTRL+NUM0":"formatPara","CTRL+NUM1":"formatH1","CTRL+NUM2":"formatH2","CTRL+NUM3":"formatH3","CTRL+NUM4":"formatH4","CTRL+NUM5":"formatH5","CTRL+NUM6":"formatH6","CTRL+ENTER":"insertHorizontalRule","CTRL+K":"linkDialog.show"},mac:{ENTER:"insertParagraph","CMD+Z":"undo","CMD+SHIFT+Z":"redo",TAB:"tab","SHIFT+TAB":"untab","CMD+B":"bold","CMD+I":"italic","CMD+U":"underline","CMD+SHIFT+S":"strikethrough","CMD+BACKSLASH":"removeFormat","CMD+SHIFT+L":"justifyLeft","CMD+SHIFT+E":"justifyCenter","CMD+SHIFT+R":"justifyRight","CMD+SHIFT+J":"justifyFull","CMD+SHIFT+NUM7":"insertUnorderedList","CMD+SHIFT+NUM8":"insertOrderedList","CMD+LEFTBRACKET":"outdent","CMD+RIGHTBRACKET":"indent","CMD+NUM0":"formatPara","CMD+NUM1":"formatH1","CMD+NUM2":"formatH2","CMD+NUM3":"formatH3","CMD+NUM4":"formatH4","CMD+NUM5":"formatH5","CMD+NUM6":"formatH6","CMD+ENTER":"insertHorizontalRule","CMD+K":"linkDialog.show"}},icons:{align:"note-icon-align",alignCenter:"note-icon-align-center",alignJustify:"note-icon-align-justify",alignLeft:"note-icon-align-left",alignRight:"note-icon-align-right",indent:"note-icon-align-indent",outdent:"note-icon-align-outdent",arrowsAlt:"note-icon-arrows-alt",bold:"note-icon-bold",caret:"note-icon-caret",circle:"note-icon-circle",close:"note-icon-close",code:"note-icon-code",eraser:"note-icon-eraser",font:"note-icon-font",frame:"note-icon-frame",italic:"note-icon-italic",link:"note-icon-link",unlink:"note-icon-chain-broken",magic:"note-icon-magic",menuCheck:"note-icon-check",minus:"note-icon-minus",orderedlist:"note-icon-orderedlist",pencil:"note-icon-pencil",picture:"note-icon-picture",question:"note-icon-question",redo:"note-icon-redo",square:"note-icon-square",strikethrough:"note-icon-strikethrough",subscript:"note-icon-subscript",superscript:"note-icon-superscript",table:"note-icon-table",textHeight:"note-icon-text-height",trash:"note-icon-trash",underline:"note-icon-underline",undo:"note-icon-undo",unorderedlist:"note-icon-unorderedlist",video:"note-icon-video"}}})});
(function (factory) {
    /* global define */
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(window.jQuery);
    }
}(function ($) {
    $.extend(true, $.summernote.lang, {
        'en-US': {
            imageTitle: {
                edit: 'Редактировать заголовок',
                titleLabel: 'Заголовок',
                altLabel: 'Альтернативный текст',
            }
        },
        'fr-FR': {
            imageTitle: {
                edit: 'Modifier le titre',
                titleLabel: 'Titre',
                altLabel: 'Texte alternatif',
            },
        },
        'ko-KR': {
            imageTitle: {
                edit: '제목 바꾸기',
                titleLabel: '제목'
            },
        },
        'ru-RU': {
            imageTitle: {
                edit: 'Редактировать заголовок',
                titleLabel: 'Заголовок',
                altLabel: 'Альтернативный текст',
            }
        }
    });

    $.extend($.summernote.plugins, {
        'imageTitle': function (context) {
            var self = this;

            var ui = $.summernote.ui;
            var $note = context.layoutInfo.note;
            var $editor = context.layoutInfo.editor;
            var $editable = context.layoutInfo.editable;

            if (typeof context.options.imageTitle === 'undefined') {
                context.options.imageTitle = {};
            }

            if (typeof context.options.imageTitle.specificAltField === 'undefined') {
                context.options.imageTitle.specificAltField = false;
            }

            var options = context.options;
            var lang = options.langInfo;

            context.memo('button.imageTitle', function () {
                var button = ui.button({
                    contents: ui.icon(options.icons.pencil),
                    tooltip: lang.imageTitle.edit,
                    click: function (e) {
                        context.invoke('imageTitle.show');
                    }
                });

                return button.render();
            });

            this.initialize = function () {
                var $container = options.dialogsInBody ? $(document.body) : $editor;

                var body = '<div class="form-group">' +
                             '<label>' + lang.imageTitle.titleLabel + '</label>' +
                             '<input class="note-image-title-text form-control" type="text" />' +
                           '</div>';

                if (options.imageTitle.specificAltField) {
                    body += '<div class="form-group">' +
                               '<label>' + lang.imageTitle.altLabel + '</label>' +
                               '<input class="note-image-alt-text form-control" type="text" />' +
                             '</div>';
                }

                var footer = '<button href="#" class="btn btn-primary note-image-title-btn">' + lang.imageTitle.edit + '</button>';

                this.$dialog = ui.dialog({
                    title: lang.imageTitle.edit,
                    body: body,
                    footer: footer
                }).render().appendTo($container);
            };

            this.destroy = function () {
                ui.hideDialog(this.$dialog);
                this.$dialog.remove();
            };

            this.bindEnterKey = function ($input, $btn) {
                $input.on('keypress', function (event) {
                    if (event.keyCode === 13) {
                        $btn.trigger('click');
                    }
                });
            };

            this.show = function () {
                var $img = $($editable.data('target'));
                var imgInfo = {
                    imgDom: $img,
                    title: $img.attr('title'),
                    alt: $img.attr('alt'),
                };
                this.showLinkDialog(imgInfo).then(function (imgInfo) {
                    ui.hideDialog(self.$dialog);
                    var $img = imgInfo.imgDom;

                    if (imgInfo.alt) {
                        $img.attr('alt', imgInfo.alt);
                    }
                    else {
                        $img.removeAttr('alt');
                    }

                    if (imgInfo.title) {
                        $img.attr('title', imgInfo.title);
                    }
                    else {
                        $img.removeAttr('title');
                    }

                    $note.val(context.invoke('code'));
                    $note.change();
                });
            };

            this.showLinkDialog = function (imgInfo) {
                return $.Deferred(function (deferred) {
                    var $imageTitle = self.$dialog.find('.note-image-title-text'),
                        $imageAlt = (options.imageTitle.specificAltField) ? self.$dialog.find('.note-image-alt-text') : null,
                        $editBtn = self.$dialog.find('.note-image-title-btn');

                    ui.onDialogShown(self.$dialog, function () {
                        context.triggerEvent('dialog.shown');

                        $editBtn.click(function (event) {
                            event.preventDefault();
                            deferred.resolve({
                                imgDom: imgInfo.imgDom,
                                title: $imageTitle.val(),
                                alt: (options.imageTitle.specificAltField) ? $imageAlt.val() : $imageTitle.val(),
                            });
                        });

                        $imageTitle.val(imgInfo.title).trigger('focus');
                        self.bindEnterKey($imageTitle, $editBtn);

                        if (options.imageTitle.specificAltField) {
                            $imageAlt.val(imgInfo.alt);
                            self.bindEnterKey($imageAlt, $editBtn);
                        }
                    });

                    ui.onDialogHidden(self.$dialog, function () {
                        $editBtn.off('click');

                        if (deferred.state() === 'pending') {
                            deferred.reject();
                        }
                    });

                    ui.showDialog(self.$dialog);
                });
            };
        }
    });
}));
(function ($) {
  $.extend($.summernote.lang, {
    'ru-RU': {
      font: {
        bold: 'Полужирный',
        italic: 'Курсив',
        underline: 'Подчёркнутый',
        clear: 'Убрать стили шрифта',
        height: 'Высота линии',
        name: 'Шрифт',
        strikethrough: 'Зачёркнутый',
        subscript: 'Нижний индекс',
        superscript: 'Верхний индекс',
        size: 'Размер шрифта'
      },
      image: {
        image: 'Картинка',
        insert: 'Вставить картинку',
        resizeFull: 'Восстановить размер',
        resizeHalf: 'Уменьшить до 50%',
        resizeQuarter: 'Уменьшить до 25%',
        floatLeft: 'Расположить слева',
        floatRight: 'Расположить справа',
        floatNone: 'Расположение по-умолчанию',
        shapeRounded: 'Форма: Закругленная',
        shapeCircle: 'Форма: Круг',
        shapeThumbnail: 'Форма: Миниатюра',
        shapeNone: 'Форма: Нет',
        dragImageHere: 'Перетащите сюда картинку',
        dropImage: 'Перетащите картинку',
        selectFromFiles: 'Выбрать из файлов',
        url: 'URL картинки',
        remove: 'Удалить картинку'
      },
      video: {
        video: 'Видео',
        videoLink: 'Ссылка на видео',
        insert: 'Вставить видео',
        url: 'URL видео',
        providers: '(YouTube, Vimeo, Vine, Instagram, DailyMotion или Youku)'
      },
      link: {
        link: 'Ссылка',
        insert: 'Вставить ссылку',
        unlink: 'Убрать ссылку',
        edit: 'Редактировать',
        textToDisplay: 'Отображаемый текст',
        url: 'URL для перехода',
        openInNewWindow: 'Открывать в новом окне'
      },
      table: {
        table: 'Таблица'
      },
      hr: {
        insert: 'Вставить горизонтальную линию'
      },
      style: {
        style: 'Стиль',
        normal: 'Нормальный',
        blockquote: 'Цитата',
        pre: 'Код',
        h1: 'Заголовок 1',
        h2: 'Заголовок 2',
        h3: 'Заголовок 3',
        h4: 'Заголовок 4',
        h5: 'Заголовок 5',
        h6: 'Заголовок 6'
      },
      lists: {
        unordered: 'Маркированный список',
        ordered: 'Нумерованный список'
      },
      options: {
        help: 'Помощь',
        fullscreen: 'На весь экран',
        codeview: 'Исходный код'
      },
      paragraph: {
        paragraph: 'Параграф',
        outdent: 'Уменьшить отступ',
        indent: 'Увеличить отступ',
        left: 'Выровнять по левому краю',
        center: 'Выровнять по центру',
        right: 'Выровнять по правому краю',
        justify: 'Растянуть по ширине'
      },
      color: {
        recent: 'Последний цвет',
        more: 'Еще цвета',
        background: 'Цвет фона',
        foreground: 'Цвет шрифта',
        transparent: 'Прозрачный',
        setTransparent: 'Сделать прозрачным',
        reset: 'Сброс',
        resetToDefault: 'Восстановить умолчания'
      },
      shortcut: {
        shortcuts: 'Сочетания клавиш',
        close: 'Закрыть',
        textFormatting: 'Форматирование текста',
        action: 'Действие',
        paragraphFormatting: 'Форматирование параграфа',
        documentStyle: 'Стиль документа',
        extraKeys: 'Дополнительные комбинации'
      },
      history: {
        undo: 'Отменить',
        redo: 'Повтор'
      }
    }
  });
})(jQuery);

//  ----------------------------------------------------------------------------
//
//  bootstrap-typeahead.js  
//
//  Twitter Bootstrap Typeahead Plugin
//  v1.2.2
//  https://github.com/tcrosen/twitter-bootstrap-typeahead
//
//
//  Author
//  ----------
//  Terry Rosen
//  tcrosen@gmail.com | @rerrify | github.com/tcrosen/
//
//
//  Description
//  ----------
//  Custom implementation of Twitter's Bootstrap Typeahead Plugin
//  http://twitter.github.com/bootstrap/javascript.html#typeahead
//
//
//  Requirements
//  ----------
//  jQuery 1.7+
//  Twitter Bootstrap 2.0+
//
//  ----------------------------------------------------------------------------

!
function ($) {

    "use strict";

    //------------------------------------------------------------------
    //
    //  Constructor
    //
    var Typeahead = function (element, options) {
        this.$element = $(element);
        this.options = $.extend(true, {}, $.fn.typeahead.defaults, options);
        this.$menu = $(this.options.menu).appendTo('body');
        this.shown = false;

        // Method overrides    
        this.eventSupported = this.options.eventSupported || this.eventSupported;
        this.grepper = this.options.grepper || this.grepper;
        this.highlighter = this.options.highlighter || this.highlighter;
        this.lookup = this.options.lookup || this.lookup;
        this.matcher = this.options.matcher || this.matcher;
        this.render = this.options.render || this.render;
        this.select = this.options.select || this.select;
        this.sorter = this.options.sorter || this.sorter;
        this.source = this.options.source || this.source;        
                
        if (!this.source.length) {
            var ajax = this.options.ajax;

            if (typeof ajax === 'string') {
                this.ajax = $.extend({}, $.fn.typeahead.defaults.ajax, { url: ajax });
            } else {
                this.ajax = $.extend({}, $.fn.typeahead.defaults.ajax, ajax);
            }

            if (!this.ajax.url) {
                this.ajax = null;
            }
        }

        this.listen();        
    }

    Typeahead.prototype = {

        constructor: Typeahead,

        //=============================================================================================================
        //
        //  Utils
        //
        //=============================================================================================================

        //------------------------------------------------------------------
        //
        //  Check if an event is supported by the browser eg. 'keypress'
        //  * This was included to handle the "exhaustive deprecation" of jQuery.browser in jQuery 1.8
        //
        eventSupported: function(eventName) {         
            var isSupported = (eventName in this.$element);

            if (!isSupported) {
              this.$element.setAttribute(eventName, 'return;');
              isSupported = typeof this.$element[eventName] === 'function';
            }

            return isSupported;
        },

        //=============================================================================================================
        //
        //  AJAX
        //
        //=============================================================================================================

        //------------------------------------------------------------------
        //
        //  Handle AJAX source 
        //
        ajaxer: function () { 
            var that = this,
                query = that.$element.val();
            
            if (query === that.query) {
                return that;
            }
    
            // Query changed
            that.query = query;

            // Cancel last timer if set
            if (that.ajax.timerId) {
                clearTimeout(that.ajax.timerId);
                that.ajax.timerId = null;
            }
            
            if (!query || query.length < that.ajax.triggerLength) {
                // Cancel the ajax callback if in progress
                if (that.ajax.xhr) {
                    that.ajax.xhr.abort();
                    that.ajax.xhr = null;
                    that.ajaxToggleLoadClass(false);
                }

                return that.shown ? that.hide() : that;
            }
                    
            // Query is good to send, set a timer
            that.ajax.timerId = setTimeout(function() {
                $.proxy(that.ajaxExecute(query), that)
            }, that.ajax.timeout);
                    
            return that;
        },

        //------------------------------------------------------------------
        //
        //  Execute an AJAX request
        //
        ajaxExecute: function(query) {
            this.ajaxToggleLoadClass(true);
            
            // Cancel last call if already in progress
            if (this.ajax.xhr) this.ajax.xhr.abort();
            
            var params = this.ajax.preDispatch ? this.ajax.preDispatch(query) : { query : query };
            var jAjax = (this.ajax.method === "post") ? $.post : $.get;
            this.ajax.xhr = jAjax(this.ajax.url, params, $.proxy(this.ajaxLookup, this));
            this.ajax.timerId = null;
        },
    
        //------------------------------------------------------------------
        //
        //  Perform a lookup in the AJAX results
        //
        ajaxLookup: function (data) { 
            var items;
            
            this.ajaxToggleLoadClass(false);

            if (!this.ajax.xhr) return;
            
            if (this.ajax.preProcess) {
                data = this.ajax.preProcess(data);
            }

            // Save for selection retreival
            this.ajax.data = data;

            items = this.grepper(this.ajax.data);
    
            if (!items || !items.length) {
                return this.shown ? this.hide() : this;
            }

            this.ajax.xhr = null;

            return this.render(items.slice(0, this.options.items)).show();
        },
        
        //------------------------------------------------------------------
        //
        //  Toggle the loading class
        //
        ajaxToggleLoadClass: function (enable) {
            if (!this.ajax.loadingClass) return;
            this.$element.toggleClass(this.ajax.loadingClass, enable);
        },

        //=============================================================================================================
        //
        //  Data manipulation
        //
        //=============================================================================================================

        //------------------------------------------------------------------
        //
        //  Search source
        //
        lookup: function (event) {
            var that = this,
                items;

            if (that.ajax) {
                that.ajaxer();
            }
            else {
                that.query = that.$element.val();

                if (!that.query) {
                    return that.shown ? that.hide() : that;
                }
                
                items = that.grepper(that.source);
                
                if (!items || !items.length) {
                    return that.shown ? that.hide() : that;
                }

                return that.render(items.slice(0, that.options.items)).show();
            }
        },

        //------------------------------------------------------------------
        //
        //  Filters relevent results 
        //
        grepper: function(data) {
            var that = this,
                items;

            if (data && data.length && !data[0].hasOwnProperty(that.options.display)) {                
                return null;
            } 

            items = $.grep(data, function (item) {
                return that.matcher(item[that.options.display], item);
            });

            return this.sorter(items);                
        },

        //------------------------------------------------------------------
        //
        //  Looks for a match in the source
        //
        matcher: function (item) {
            return ~item.toLowerCase().indexOf(this.query.toLowerCase());
        },

        //------------------------------------------------------------------
        //
        //  Sorts the results
        //
        sorter: function (items) {
            var that = this,
                beginswith = [],
                caseSensitive = [],
                caseInsensitive = [],
                item;

            while (item = items.shift()) {
                if (!item[that.options.display].toLowerCase().indexOf(this.query.toLowerCase())) {
                    beginswith.push(item);
                }
                else if (~item[that.options.display].indexOf(this.query)) {
                    caseSensitive.push(item);
                }
                else {
                    caseInsensitive.push(item);
                }
            }

            return beginswith.concat(caseSensitive, caseInsensitive);
        },       

        //=============================================================================================================
        //
        //  DOM manipulation
        //
        //=============================================================================================================

        //------------------------------------------------------------------
        //
        //  Shows the results list
        //
        show: function () {
            var pos = $.extend({}, this.$element.offset(), {
                height: this.$element[0].offsetHeight
            });

            this.$menu.css({
                top: pos.top + pos.height,
                left: pos.left
            });

            this.$menu.show();
            this.shown = true;

            return this;
        },

        //------------------------------------------------------------------
        //
        //  Hides the results list
        //
        hide: function () {
            this.$menu.hide();
            this.shown = false;
            return this;
        },
        
        //------------------------------------------------------------------
        //
        //  Highlights the match(es) within the results
        //
        highlighter: function (item) {
            var query = this.query.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&');
            return item.replace(new RegExp('(' + query + ')', 'ig'), function ($1, match) {
                return '<strong>' + match + '</strong>';
            });
        },

        //------------------------------------------------------------------
        //
        //  Renders the results list
        //
        render: function (items) {
            var that = this;

            items = $(items).map(function (i, item) {
                i = $(that.options.item).attr('data-value', item[that.options.val]);
                i.find('a').html(that.highlighter(item[that.options.display], item));
                return i[0];
            });

            items.first().addClass('active');
            this.$menu.html(items);
            return this;
        },

        //------------------------------------------------------------------
        //
        //  Item is selected
        //
        select: function () {
            var $selectedItem = this.$menu.find('.active');
            this.$element.val($selectedItem.text()).change();
            this.options.itemSelected($selectedItem, $selectedItem.attr('data-value'), $selectedItem.text());
            return this.hide();
        },

        //------------------------------------------------------------------
        //
        //  Selects the next result
        //
        next: function (event) {
            var active = this.$menu.find('.active').removeClass('active');
            var next = active.next();

            if (!next.length) {
                next = $(this.$menu.find('li')[0]);
            }

            next.addClass('active');
        },

        //------------------------------------------------------------------
        //
        //  Selects the previous result
        //
        prev: function (event) {
            var active = this.$menu.find('.active').removeClass('active');
            var prev = active.prev();

            if (!prev.length) {
                prev = this.$menu.find('li').last();
            }

            prev.addClass('active');
        },

        //=============================================================================================================
        //
        //  Events
        //
        //=============================================================================================================

        //------------------------------------------------------------------
        //
        //  Listens for user events
        //
        listen: function () {
            this.$element.on('blur', $.proxy(this.blur, this))
                         .on('keyup', $.proxy(this.keyup, this));

    		if (this.eventSupported('keydown')) {
				this.$element.on('keydown', $.proxy(this.keypress, this));
			} else {
				this.$element.on('keypress', $.proxy(this.keypress, this));
			}

            this.$menu.on('click', $.proxy(this.click, this))
                      .on('mouseenter', 'li', $.proxy(this.mouseenter, this));
        },

        //------------------------------------------------------------------
        //
        //  Handles a key being raised up
        //
        keyup: function (e) {
            e.stopPropagation();
            e.preventDefault();

            switch (e.keyCode) {
                case 40:
                    // down arrow
                case 38:
                    // up arrow
                    break;
                case 9:
                    // tab
                case 13:
                    // enter
                    if (!this.shown) {
                        return;
                    }
                    this.select();
                    break;
                case 27:
                    // escape
                    this.hide();
                    break;
                default:
                    this.lookup();
            }
        },

        //------------------------------------------------------------------
        //
        //  Handles a key being pressed
        //
        keypress: function (e) {
            e.stopPropagation();
            if (!this.shown) {
                return;
            }

            switch (e.keyCode) {
                case 9:
                    // tab
                case 13:
                    // enter
                case 27:
                    // escape
                    e.preventDefault();
                    break;
                case 38:
                    // up arrow
                    e.preventDefault();
                    this.prev();
                    break;
                case 40:
                    // down arrow
                    e.preventDefault();
                    this.next();
                    break;
            }
        },

        //------------------------------------------------------------------
        //
        //  Handles cursor exiting the textbox
        //
        blur: function (e) {
            var that = this;
            e.stopPropagation();
            e.preventDefault();
            setTimeout(function () {
                if (!that.$menu.is(':focus')) {
                  that.hide();
                }
            }, 150)
        },

        //------------------------------------------------------------------
        //
        //  Handles clicking on the results list
        //
        click: function (e) {
            e.stopPropagation();
            e.preventDefault();
            this.select();
        },

        //------------------------------------------------------------------
        //
        //  Handles the mouse entering the results list
        //
        mouseenter: function (e) {
            this.$menu.find('.active').removeClass('active');
            $(e.currentTarget).addClass('active');
        }
    }

    //------------------------------------------------------------------
    //
    //  Plugin definition
    //
    $.fn.typeahead = function (option) {
        return this.each(function () {
            var $this = $(this),
                data = $this.data('typeahead'),
                options = typeof option === 'object' && option;

            if (!data) {
                $this.data('typeahead', (data = new Typeahead(this, options)));
            }

            if (typeof option === 'string') {
                data[option]();
            }
        });
    }

    //------------------------------------------------------------------
    //
    //  Defaults
    //
    $.fn.typeahead.defaults = {
        source: [],
        items: 8,
        menu: '<ul class="typeahead dropdown-menu"></ul>',
        item: '<li><a href="#"></a></li>',
        display: 'name',
        val: 'id',
        itemSelected: function () { },
        ajax: {
            url: null,
            timeout: 300,
            method: 'post',
            triggerLength: 3,
            loadingClass: null,
            displayField: null,
            preDispatch: null,
            preProcess: null
        }
    }

    $.fn.typeahead.Constructor = Typeahead;

    //------------------------------------------------------------------
    //
    //  DOM-ready call for the Data API (no-JS implementation)
    //    
    //  Note: As of Bootstrap v2.0 this feature may be disabled using $('body').off('.data-api')    
    //  More info here: https://github.com/twitter/bootstrap/tree/master/js
    //
    $(function () {
        $('body').on('focus.typeahead.data-api', '[data-provide="typeahead"]', function (e) {
            var $this = $(this);

            if ($this.data('typeahead')) {
                return;
            }

            e.preventDefault();
            $this.typeahead($this.data());
        })
    });

} (window.jQuery);

$(document).ready(function() {
    
     $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
        
     });
     
    $('.selectpicker').selectpicker();
    
     $('.icheck input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
     
    $('.wredaktor').summernote({
        lang: 'ru-RU',
        minHeight: 300,
        toolbar: [
            ['style', ['style']],
            ['style', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video', 'hr', 'readmore']],
            ['genixcms', ['elfinder']],
            ['view', ['fullscreen', 'codeview']],
            ['help', ['help']]
        ],
        imageTitle: {
          specificAltField: true,
        },
        popover: {
            image: [
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']],
                ['custom', ['imageTitle']],
            ],
        },
        callbacks: {
            onImageUpload: function(files) {
                var data = new FormData();
                data.append("file", files[0]);
                $.ajax({
                    url: '/admin/upload/filem',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(data){  
                        $('.wredaktor').summernote('insertImage', data.item, data.filename);     
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus+" "+errorThrown);
                   }
                });
            }
        }
    });
     
     function aclick() {
        $('a.ajax').on('click', function(){
        return false;	
        });
    } aclick();
    
    /*
     * function ajaxForm
     * @param $arg
     */
    function ajaxForm()
    {
        $('.ajax-form').on('click', function(){
        var self 	= $(this);
        var method 	= self.data('method');
        var href 	= self.attr('href');
        var token 	= $('meta[name="_token"]').attr('content');
        
        var $form 		= $('<form/>', {action: href, method: 'post'});
            var $inputMethod 	= $('<input/>', {type: 'hidden', name: '_method', value: method});
            var $inputToken 	= $('<input/>', {type: 'hidden', name: '_token', value: token});
            $form.append($inputMethod, $inputToken).hide().appendTo('body').submit();
        });
        
    } ajaxForm();
     
    
    function postAjax(classid) {
        
        $(classid).on('click', function(e){
            e.preventDefault();
            
            var method 	= $(this).data('method');
            var title 	= $(this).data('title');
            var message = $(this).data('message');
            var href 	= $(this).data('href');
            var yes 	= $(this).data('button1');
            var no 	= $(this).data('button2');
            
            var token 	= $('meta[name="_token"]').attr('content');
            
            bootbox.dialog({
            message: message,
            title: title,
            buttons: {
              success: {
               label: no,
               className: "btn-success",
               callback: function() {
                    bootbox.hideAll();
               }
              },
              danger: {
                label: yes,
                className: "btn-danger",
                callback: function() {
                  
                    var $form = $('<form/>', {action: href, method: 'post'});
                    var $inputMethod = $('<input/>', {type: 'hidden', name: '_method', value: method});
                    var $inputToken = $('<input/>', {type: 'hidden', name: '_token', value: token});
                    $form.append($inputMethod, $inputToken).hide().appendTo('body').submit();
                    
                }
              }
             
            }
          });
             
        });
        
        
    }
    postAjax('.delete');
    
    

    $('.dd-menu').nestable({
        collapseAll: true,
        expandBtnHTML: '<button data-action="expand" class="btn btn-link"><i class="fa fa-fw fa-plus-square"></i></button>',    
        collapseBtnHTML: '<button data-action="collapse" class="btn btn-link"><i class="fa fa-fw fa-minus-square"></i></button>',    
    }).on('change', function(e){
        
        var dani = $('.dd-menu').nestable('serialize');
        sortMenu(dani);
    });
    
    $('.dd-term').nestable({
        collapseAll: true,
        expandBtnHTML: '<button data-action="expand" class="btn btn-link"><i class="fa fa-fw fa-plus-square"></i></button>',    
        collapseBtnHTML: '<button data-action="collapse" class="btn btn-link"><i class="fa fa-fw fa-minus-square"></i></button>',    
    }).on('change', function(e){
        
        var dani = $('.dd-term').nestable('serialize');
        sortTerm(dani);
    });
    
    
    $('.anes').on('click', function(e){
	var action = $(this).data('action');
	
	if (action === 'expandAll') {
            $('.dd').nestable('expandAll');
        }
	
        if (action === 'collapseAll') {
            $('.dd').nestable('collapseAll');
        }
    });
    
    function sortMenu(dani) {
          $.ajax({
               url: '/admin/menu/item/sort',
               type: 'POST',
               data: {sort : dani},
          });
    }
    
    function sortTerm(dani) {
          $.ajax({
               url: '/admin/taxonomy/term/sort',
               type: 'POST',
               data: {sort : dani},
          });
    }
    
//     var el = document.getElementById('photo-widget');
//     var sortable = new Sortable(el, {
//          group: "sorting",
//          sort: true,
//          animation: 150,
//          onEnd: function (evt) {
//               var order = sortable.toArray();
//               $.post('/photography/sort', {order:order});
//          }
//     });

     function getType() {
          $('select[name=marka]').on('change', function(){
               
               var form = $('.form-product');
               var data = form.serializeArray();
              
               $.ajax({
                    url: '/admin/get/type',
                    type: 'POST',
                    data: data,
                    beforeSend:function(data){
                         
                    },
                    success: function(data){
                         form.find('.type').html(data.result);
                         form.find('.model select').html('');
                         $('.selectpicker').selectpicker('refresh');
                         getModel();
                    },
                    error: function(data){
                       
                    }
               });
               
          });
     } getType();
     
     function getModel() {
          
          $('select[name=type]').on('change', function(){
               
               var form = $('.form-product');
               var data = form.serializeArray();
              
               $.ajax({
                    url: '/admin/get/model',
                    type: 'POST',
                    data: data,
                    beforeSend:function(before){
                         
                    },
                    success: function(data){
                         form.find('.model').html(data.result);
                         $('.selectpicker').selectpicker('refresh');
                    },
                    error: function(errors){
                       
                    }
               });
          });
     } //getModel();
     
     function uploadImages() {
          var uri = $('#input-image').data('uri');
          
          $('#input-image').fileinput({
               allowedFileExtensions : ['png', 'jpg', 'jpeg'],
               uploadUrl: '/admin/upload/images',
               uploadAsync: true,
               showUpload: true,
               dropZoneEnabled: false,
               showPreview: false,
               language: 'ru',
               uploadExtraData:{uri:uri},
               slugCallback: function (filename){
                   return filename;
               }
          }).on('filebatchuploadsuccess', function(event, data, previewId, index) {
               var wrap = $('#image-widget ul');
               wrap.append(data.response.result);
               wrap.find('li').each(function(k, v){
                    $(this).find('.sort').val(k);
                    $(this).find('.fid').attr('name', 'image['+k+'][fid]');
                    $(this).find('.sort').attr('name', 'image['+k+'][sort]');
               });
               deleteImages();
               sortImage();
          });
          
     } uploadImages();
     
     function deleteImages() {
          
          $('.delim').on('click', function(){
               
               var image_id = $(this).data('image-id');
               
               $(this).parent().remove();
               
               $.post('/admin/delete/images', {image_id:image_id});
               
          });
          
     } deleteImages();
     
     function sortImage() {
          var el = document.getElementById('image-widget-drag');
          if (el) {
               var sortable = new Sortable(el, {
                    group: "sorting",
                    sort: true,
                    handle: '.drag',
                    //filter: '.modal',
                    animation: 150,
                    onEnd: function (evt) {
                         var order = sortable.toArray();
                         var wrap = $('#image-widget ul');
                         wrap.find('li').each(function(k, v){
                              $(this).find('.sort').val(k);
                              $(this).find('.fid').attr('name', 'image['+k+'][fid]');
                              $(this).find('.sort').attr('name', 'image['+k+'][sort]');
                         });
                         //$.post('/photography/sort', {order:order});
                         console.log(order);
                    }
               });
          }
     } sortImage();
     
     
     function geoCity() {
         $('.geocity')
         .selectpicker({
             liveSearch: true
         })
         .ajaxSelectPicker({
             ajax: {
                 url: '/geo/city',
                 //data: function () {
                 //   q: '{{{q}}}'
                 //}
             },
             locale: {
                 emptyTitle: 'Поиск города...'
             },
             preprocessData: function(data){
                 var city = [];
                 var len = data.length;
                 for(var i = 0; i < len; i++){
                     var curr = data[i];
                     city.push({
                         value: curr.city_id,
                         text: curr.city,
                         data: {
                             subtext: curr.region
                         },
                     });
                 }
                 return city;
             },
             preserveSelected: false
         });
         $('select').trigger('change');
     } geoCity();
    
});

/*-----------TREE-----------------*/
$.fn.extend({
    treed: function (o) {
      
      var openedClass = 'glyphicon-minus-sign';
      var closedClass = 'glyphicon-plus-sign';
      
      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };
      
        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a.cl').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});


/*-----------TREE-----------------*/












