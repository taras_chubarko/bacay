// JavaScript Document

jQuery(function($){

  $('input[placeholder], textarea[placeholder]').placeholder();

 // $('input, select').styler();

  $(".scrollbar").scroller({
    customClass: "advanced",
    handleSize: 56
  });

  $('div.tabs').on('click', 'li:not(.current)', function() {
		$(this).addClass('current').siblings().removeClass('current')
			.parents('div.section').find('div.box').eq($(this).index()).fadeIn(150).siblings('div.box').hide();
	})

});//end ready

// slider responsive
if(typeof window.matchMedia != "undefined" && window.matchMedia('(max-width: 479px)').matches) {
	jQuery(function($) {
		$('.more-prod-carousel').flexslider({
			animation: "slide",
			useCSS: false,
			slideshow: false,
			animationLoop: true,
			itemWidth: 260,
			move: 1,
			minItems: 1,
			maxItems: 1
		});
	});
}
else {
	jQuery(function($) {
		$('.more-prod-carousel').flexslider({
			animation: "slide",
			useCSS: false,
			slideshow: false,
			animationLoop: true,
			itemWidth: 260,
			move: 1,
			minItems: 3,
			maxItems: 3
		});
	});
}


//Plugin placeholder
(function(b,f,i){function l(){b(this).find(c).each(j)}function m(a){for(var a=a.attributes,b={},c=/^jQuery\d+/,e=0;e<a.length;e++)if(a[e].specified&&!c.test(a[e].name))b[a[e].name]=a[e].value;return b}function j(){var a=b(this),d;a.is(":password")||(a.data("password")?(d=a.next().show().focus(),b("label[for="+a.attr("id")+"]").attr("for",d.attr("id")),a.remove()):a.realVal()==a.attr("placeholder")&&(a.val(""),a.removeClass("placeholder")))}function k(){var a=b(this),d,c;placeholder=a.attr("placeholder");
b.trim(a.val()).length>0||(a.is(":password")?(c=a.attr("id")+"-clone",d=b("<input/>").attr(b.extend(m(this),{type:"text",value:placeholder,"data-password":1,id:c})).addClass("placeholder"),a.before(d).hide(),b("label[for="+a.attr("id")+"]").attr("for",c)):(a.val(placeholder),a.addClass("placeholder")))}var g="placeholder"in f.createElement("input"),h="placeholder"in f.createElement("textarea"),c=":input[placeholder]";b.placeholder={input:g,textarea:h};!i&&g&&h?b.fn.placeholder=function(){}:(!i&&g&&
!h&&(c="textarea[placeholder]"),b.fn.realVal=b.fn.val,b.fn.val=function(){var a=b(this),d;if(arguments.length>0)return a.realVal.apply(this,arguments);d=a.realVal();a=a.attr("placeholder");return d==a?"":d},b.fn.placeholder=function(){this.filter(c).each(k);return this},b(function(a){var b=a(f);b.on("submit","form",l);b.on("focus",c,j);b.on("blur",c,k);a(c).placeholder()}))})(jQuery,document,window.debug);


// Photo gallery > Vertical
//jQuery(".photosgallery-vertical").sliderkit({
//	circular:true,
//	mousewheel:true,
////	shownavitems:6,
//	verticalnav:true,
//	navclipcenter:true,
//	auto:false
//});
$(document).ready(function() {
	$(".fancyslider").fancybox({
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	
});