$(document).ready(function() {
    
     $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
        
     });
     
    $('.selectpicker').selectpicker();
    
     $('.icheck input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
     
    $('.wredaktor').summernote({
        lang: 'ru-RU',
        minHeight: 300,
        toolbar: [
            ['style', ['style']],
            ['style', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video', 'hr', 'readmore']],
            ['genixcms', ['elfinder']],
            ['view', ['fullscreen', 'codeview']],
            ['help', ['help']]
        ],
        imageTitle: {
          specificAltField: true,
        },
        popover: {
            image: [
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']],
                ['custom', ['imageTitle']],
            ],
        },
        callbacks: {
            onImageUpload: function(files) {
                var data = new FormData();
                data.append("file", files[0]);
                $.ajax({
                    url: '/admin/upload/filem',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(data){  
                        $('.wredaktor').summernote('insertImage', data.item, data.filename);     
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                    console.log(textStatus+" "+errorThrown);
                   }
                });
            }
        }
    });
     
     function aclick() {
        $('a.ajax').on('click', function(){
        return false;	
        });
    } aclick();
    
    /*
     * function ajaxForm
     * @param $arg
     */
    function ajaxForm()
    {
        $('.ajax-form').on('click', function(){
        var self 	= $(this);
        var method 	= self.data('method');
        var href 	= self.attr('href');
        var token 	= $('meta[name="_token"]').attr('content');
        
        var $form 		= $('<form/>', {action: href, method: 'post'});
            var $inputMethod 	= $('<input/>', {type: 'hidden', name: '_method', value: method});
            var $inputToken 	= $('<input/>', {type: 'hidden', name: '_token', value: token});
            $form.append($inputMethod, $inputToken).hide().appendTo('body').submit();
        });
        
    } ajaxForm();
     
    
    function postAjax(classid) {
        
        $(classid).on('click', function(e){
            e.preventDefault();
            
            var method 	= $(this).data('method');
            var title 	= $(this).data('title');
            var message = $(this).data('message');
            var href 	= $(this).data('href');
            var yes 	= $(this).data('button1');
            var no 	= $(this).data('button2');
            
            var token 	= $('meta[name="_token"]').attr('content');
            
            bootbox.dialog({
            message: message,
            title: title,
            buttons: {
              success: {
               label: no,
               className: "btn-success",
               callback: function() {
                    bootbox.hideAll();
               }
              },
              danger: {
                label: yes,
                className: "btn-danger",
                callback: function() {
                  
                    var $form = $('<form/>', {action: href, method: 'post'});
                    var $inputMethod = $('<input/>', {type: 'hidden', name: '_method', value: method});
                    var $inputToken = $('<input/>', {type: 'hidden', name: '_token', value: token});
                    $form.append($inputMethod, $inputToken).hide().appendTo('body').submit();
                    
                }
              }
             
            }
          });
             
        });
        
        
    }
    postAjax('.delete');
    
    

    $('.dd-menu').nestable({
        collapseAll: true,
        expandBtnHTML: '<button data-action="expand" class="btn btn-link"><i class="fa fa-fw fa-plus-square"></i></button>',    
        collapseBtnHTML: '<button data-action="collapse" class="btn btn-link"><i class="fa fa-fw fa-minus-square"></i></button>',    
    }).on('change', function(e){
        
        var dani = $('.dd-menu').nestable('serialize');
        sortMenu(dani);
    });
    
    $('.dd-term').nestable({
        collapseAll: true,
        expandBtnHTML: '<button data-action="expand" class="btn btn-link"><i class="fa fa-fw fa-plus-square"></i></button>',    
        collapseBtnHTML: '<button data-action="collapse" class="btn btn-link"><i class="fa fa-fw fa-minus-square"></i></button>',    
    }).on('change', function(e){
        
        var dani = $('.dd-term').nestable('serialize');
        sortTerm(dani);
    });
    
    
    $('.anes').on('click', function(e){
	var action = $(this).data('action');
	
	if (action === 'expandAll') {
            $('.dd').nestable('expandAll');
        }
	
        if (action === 'collapseAll') {
            $('.dd').nestable('collapseAll');
        }
    });
    
    function sortMenu(dani) {
          $.ajax({
               url: '/admin/menu/item/sort',
               type: 'POST',
               data: {sort : dani},
          });
    }
    
    function sortTerm(dani) {
          $.ajax({
               url: '/admin/taxonomy/term/sort',
               type: 'POST',
               data: {sort : dani},
          });
    }
    
//     var el = document.getElementById('photo-widget');
//     var sortable = new Sortable(el, {
//          group: "sorting",
//          sort: true,
//          animation: 150,
//          onEnd: function (evt) {
//               var order = sortable.toArray();
//               $.post('/photography/sort', {order:order});
//          }
//     });

     function getType() {
          $('select[name=marka]').on('change', function(){
               
               var form = $('.form-product');
               var data = form.serializeArray();
              
               $.ajax({
                    url: '/admin/get/type',
                    type: 'POST',
                    data: data,
                    beforeSend:function(data){
                         
                    },
                    success: function(data){
                         form.find('.type').html(data.result);
                         form.find('.model select').html('');
                         $('.selectpicker').selectpicker('refresh');
                         getModel();
                    },
                    error: function(data){
                       
                    }
               });
               
          });
     } getType();
     
     function getModel() {
          
          $('select[name=type]').on('change', function(){
               
               var form = $('.form-product');
               var data = form.serializeArray();
              
               $.ajax({
                    url: '/admin/get/model',
                    type: 'POST',
                    data: data,
                    beforeSend:function(before){
                         
                    },
                    success: function(data){
                         form.find('.model').html(data.result);
                         $('.selectpicker').selectpicker('refresh');
                    },
                    error: function(errors){
                       
                    }
               });
          });
     } //getModel();
     
     function uploadImages() {
          var uri = $('#input-image').data('uri');
          
          $('#input-image').fileinput({
               allowedFileExtensions : ['png', 'jpg', 'jpeg'],
               uploadUrl: '/admin/upload/images',
               uploadAsync: true,
               showUpload: true,
               dropZoneEnabled: false,
               showPreview: false,
               language: 'ru',
               uploadExtraData:{uri:uri},
               slugCallback: function (filename){
                   return filename;
               }
          }).on('filebatchuploadsuccess', function(event, data, previewId, index) {
               var wrap = $('#image-widget ul');
               wrap.append(data.response.result);
               wrap.find('li').each(function(k, v){
                    $(this).find('.sort').val(k);
                    $(this).find('.fid').attr('name', 'image['+k+'][fid]');
                    $(this).find('.sort').attr('name', 'image['+k+'][sort]');
               });
               deleteImages();
               sortImage();
          });
          
     } uploadImages();
     
     function deleteImages() {
          
          $('.delim').on('click', function(){
               
               var image_id = $(this).data('image-id');
               
               $(this).parent().remove();
               
               $.post('/admin/delete/images', {image_id:image_id});
               
          });
          
     } deleteImages();
     
     function sortImage() {
          var el = document.getElementById('image-widget-drag');
          if (el) {
               var sortable = new Sortable(el, {
                    group: "sorting",
                    sort: true,
                    handle: '.drag',
                    //filter: '.modal',
                    animation: 150,
                    onEnd: function (evt) {
                         var order = sortable.toArray();
                         var wrap = $('#image-widget ul');
                         wrap.find('li').each(function(k, v){
                              $(this).find('.sort').val(k);
                              $(this).find('.fid').attr('name', 'image['+k+'][fid]');
                              $(this).find('.sort').attr('name', 'image['+k+'][sort]');
                         });
                         //$.post('/photography/sort', {order:order});
                         console.log(order);
                    }
               });
          }
     } sortImage();
     
     
     function geoCity() {
         $('.geocity')
         .selectpicker({
             liveSearch: true
         })
         .ajaxSelectPicker({
             ajax: {
                 url: '/geo/city',
                 //data: function () {
                 //   q: '{{{q}}}'
                 //}
             },
             locale: {
                 emptyTitle: 'Поиск города...'
             },
             preprocessData: function(data){
                 var city = [];
                 var len = data.length;
                 for(var i = 0; i < len; i++){
                     var curr = data[i];
                     city.push({
                         value: curr.city_id,
                         text: curr.city,
                         data: {
                             subtext: curr.region
                         },
                     });
                 }
                 return city;
             },
             preserveSelected: false
         });
         $('select').trigger('change');
     } geoCity();
    
});

/*-----------TREE-----------------*/
$.fn.extend({
    treed: function (o) {
      
      var openedClass = 'glyphicon-minus-sign';
      var closedClass = 'glyphicon-plus-sign';
      
      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };
      
        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class='indicator glyphicon " + closedClass + "'></i>");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a.cl').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});


/*-----------TREE-----------------*/












