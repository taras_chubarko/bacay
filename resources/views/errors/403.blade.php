@extends('site::layouts.master')

@section('content')
    <div id="p403">
        <h1>403</h1>
        <p>{{ $exception->getMessage() }}</p> 
    </div>
@stop