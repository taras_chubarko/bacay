@extends('site::layouts.master')

@section('content')
    <div id="p404">
        <h1>404</h1>
        <p>Такой страници не существует.</p>
    </div>
@stop