<label for="brand" class="col-lg-4 label-30">Марка *</label>
<div class="col-lg-8">
    {!! Form::select('brand', $brands, null, array('class' => 'form-control selectpicker', 'title' => 'Выбрать марку')) !!}
</div>