<label for="model" class="col-lg-4 label-30">Модель *</label>
<div class="col-lg-8">
    {!! Form::select('model', $models, null, array('class' => 'form-control selectpicker', 'title' => 'Выбрать модель')) !!}
</div>