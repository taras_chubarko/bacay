@extends('admin::layouts.master')

@section('content')

<section class="content-header">
    <h1>{{ MetaTag::set('title', 'Платные услуги') }}</h1>
    <ol class="breadcrumb">
        <li><a href="/admin"><i class="fa fa-dashboard"></i> Главная</a></li>
        <li class="active">Платные услуги</li>
    </ol>
</section>
  
<section class="content">

    <div class="box">
        <div class="box-header with-border">
            <a class="btn btn-success btn-flat" href="{{ route('admin.services.create') }}">Добавить</a>
        </div>
        
        <div class="box-body">
            @if($services->count())
            <table class="table table-bordered b-c-222D32">
                <thead>
                    <tr>
                        <th width="10">#</th>
                        <th>Название</th>
                        <th>Цена</th>
                        <th width="150">Дата</th>
                        <th width="150">Статус</th>
                        <th width="100">Действие</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($services as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->price }} руб.</td>
                        <td>{{ $item->date }}</td>
                        <td>{{ $item->statuss }}</td>
                        <td>
                            <a href="{{ route('admin.services.edit', $item->id) }}" class="btn btn-sm bg-navy btn-flat"><i class="fa fa-fw fa-edit"></i></a>
                            @include('cms::admin.delete', ['route' => 'admin.services.destroy', 'id' => $item->id])
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
           
            @else
                <p>Нет услуг.</p>
            @endif
        </div><!-- /.box-body -->
        
        <div class="box-footer">
         
        </div><!-- /.box-footer-->
    </div>

</section>
@stop