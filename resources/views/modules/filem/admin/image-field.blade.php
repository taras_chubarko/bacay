<div id="image-widget">
    <ul id="image-widget-drag">
        @include('filem::admin.image-widget', ['images' => isset($images) ? $images : null]) 
    </ul>
    <div class="clearfix"></div>
</div>

{!! Form::file('images[]', array('class' => 'files', 'id' => 'input-image', 'multiple' => true, 'data-uri' => $uri)) !!}