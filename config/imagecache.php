<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Name of route
    |--------------------------------------------------------------------------
    |
    | Enter the routes name to enable dynamic imagecache manipulation.
    | This handle will define the first part of the URI:
    | 
    | {route}/{template}/{filename}
    | 
    | Examples: "images", "img/cache"
    |
    */
   
    'route' => 'imagecache',

    /*
    |--------------------------------------------------------------------------
    | Storage paths
    |--------------------------------------------------------------------------
    |
    | The following paths will be searched for the image filename, submited 
    | by URI. 
    | 
    | Define as many directories as you like.
    |
    */
    
    'paths' => array(
        public_path('uploads/zhivotnye'),
        public_path('uploads/nedvizhimost'),
        public_path('uploads/transport'),
        public_path('uploads/elektronika'),
        public_path('uploads/detskiy'),
        public_path('uploads/baners'),
        public_path('uploads/sport-otdykh-khobbi'),
        public_path('uploads/otdam-darom'),
        public_path('uploads/avatar'),
        public_path('uploads/drugoe'),
        public_path('uploads/obmen-i-barakholka'),
        public_path('uploads/detskiy-mir'),
        public_path('uploads/krasota-i-zdorove'),
        public_path('uploads/odezhda-i-aksesuary'),
        public_path('uploads/zhivotnye-i-rasteniya'),
        public_path('uploads/dom-i-sad'),
        public_path('uploads/biznes-i-partnerstvo'),
        public_path('uploads/uslugi-i-deyatelnost'),
        public_path('uploads/odezhda'),
        public_path('uploads/rabota-i-obrazovanie')
       // public_path('products')
    ),

    /*
    |--------------------------------------------------------------------------
    | Manipulation templates
    |--------------------------------------------------------------------------
    |
    | Here you may specify your own manipulation filter templates.
    | The keys of this array will define which templates 
    | are available in the URI:
    |
    | {route}/{template}/{filename}
    |
    | The values of this array will define which filter class
    | will be applied, by its fully qualified name.
    |
    */
   
    'templates' => array(
        'small' => 'Intervention\Image\Templates\Small',
        'medium' => 'Intervention\Image\Templates\Medium',
        'large' => 'Intervention\Image\Templates\Large',
        '150x150' => 'Modules\Filem\Filters\Fit150x150',
        '154x113' => 'Modules\Filem\Filters\Fit154x113',
        '167x127' => 'Modules\Filem\Filters\Fit167x127',
    ),

    /*
    |--------------------------------------------------------------------------
    | Image Cache Lifetime
    |--------------------------------------------------------------------------
    |
    | Lifetime in minutes of the images handled by the imagecache route.
    |
    */
   
    'lifetime' => 43200,

);
