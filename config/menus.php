<?php

return [

    'styles' => [
        'navbar'       => 'Pingpong\Menus\Presenters\Bootstrap\NavbarPresenter',
        'navbar-right' => 'Pingpong\Menus\Presenters\Bootstrap\NavbarRightPresenter',
        'nav-pills'    => 'Pingpong\Menus\Presenters\Bootstrap\NavPillsPresenter',
        'nav-tab'      => 'Pingpong\Menus\Presenters\Bootstrap\NavTabPresenter',
        'sidebar'      => 'Pingpong\Menus\Presenters\Bootstrap\SidebarMenuPresenter',
        'navmenu'      => 'Pingpong\Menus\Presenters\Bootstrap\NavMenuPresenter',
        'adminLeft'    => 'Modules\Menu\Presenter\AdminLeftMenuPresenter',
        'admin-top'    => 'Modules\Menu\Presenter\AdminTopMenuPresenter',
        'top'          => 'Modules\Menu\Presenter\TopMenuPresenter',
        
    ],

    'ordering' => false,

];
