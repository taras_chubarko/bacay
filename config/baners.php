<?php

return [
    'name' => 'Baners',
    'tables' => [
        'baners_category',
        'baners_location',
        'baners_position',
        'baners_size',
    ],
];