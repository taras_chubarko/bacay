<?php

return [
    'name' => 'Wallet',
    'no_csrf' => array('wallet/robokassa/result', 'wallet/robokassa/success', 'wallet/robokassa/fail'),
];