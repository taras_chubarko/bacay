<?php

return [
    'entity' => [
        'pages'     => 'Страницы',
        'any'       => 'Произвольная'
    ],
    
    'robots' => [
        'index,follow'      => 'index,follow',
        'noindex,follow'    => 'noindex,follow',
        'index,nofollow'    => 'index,nofollow',
        'noindex,nofollow'  => 'noindex,nofollow'
    ],
];