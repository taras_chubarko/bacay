<?php

return [
    'name' => 'User',
    'permissions' => [
        
        'admin' => [
            'name' => 'Панель администратора',
            'permissions' => [
                'admin'             => 'Доступ к панели администратора',
                'admin.doc'         => 'Доступ к Документам',
                'admin.struc'       => 'Доступ к Структуре',
                'admin.user'        => 'Доступ к Пользователям',
                'admin.sett'        => 'Доступ к Настройкам',
                'admin.stat'        => 'Доступ к Статистике',
                'admin.services'    => 'Доступ к Платным услугам',
            ],
        ],
        'pages' => [
            'name' => 'Страницы',
            'permissions' => [
                'pages.create'  => 'Создание',
                'pages.edit'    => 'Редактирование',
                'pages.delete'  => 'Удаление',
                'pages.view'    => 'Просмотр',
                'pages.control' => 'Управление (дает видимость в меню администратора)',
            ],
        ],
        'advert' => [
            'name' => 'Обявления',
            'permissions' => [
                'advert.create'  => 'Создание',
                'advert.edit'    => 'Редактирование',
                'advert.delete'  => 'Удаление',
                'advert.view'    => 'Просмотр',
                'advert.control' => 'Управление (дает видимость в меню администратора)',
            ],
        ],
        'baners' => [
            'name' => 'Банерная реклама',
            'permissions' => [
                'baners.create'  => 'Создание',
                'baners.edit'    => 'Редактирование',
                'baners.delete'  => 'Удаление',
                'baners.control' => 'Управление (дает видимость в меню администратора)',
            ],
        ],
        'block' => [
            'name' => 'Блоки',
            'permissions' => [
                'block.create'  => 'Создание',
                'block.edit'    => 'Редактирование',
                'block.delete'  => 'Удаление',
                'block.control' => 'Управление (дает видимость в меню администратора)',
            ],
        ],
        'menu' => [
            'name' => 'Меню',
            'permissions' => [
                'menu.create'  => 'Создание',
                'menu.edit'    => 'Редактирование',
                'menu.delete'  => 'Удаление',
                'menu.control' => 'Управление (дает видимость в меню администратора)',
            ],
        ],
        'taxonomy' => [
            'name' => 'Таксономия',
            'permissions' => [
                'taxonomy.create'  => 'Создание',
                'taxonomy.edit'    => 'Редактирование',
                'taxonomy.delete'  => 'Удаление',
                'taxonomy.control' => 'Управление (дает видимость в меню администратора)',
            ],
        ],
        'users' => [
            'name' => 'Пользователи',
            'permissions' => [
                'users.create'  => 'Создание',
                'users.edit'    => 'Редактирование',
                'users.delete'  => 'Удаление',
                'users.view'    => 'Просмотр',
                'users.ban'     => 'Блокировка / Разблокировка',
                'users.control' => 'Управление (дает видимость в меню администратора)',
            ],
        ],
        'roles' => [
            'name' => 'Роли',
            'permissions' => [
                'roles.create'  => 'Создание',
                'roles.edit'    => 'Редактирование',
                'roles.delete'  => 'Удаление',
                'roles.control' => 'Управление (дает видимость в меню администратора)',
            ],
        ],
        'newslatter' => [
            'name' => 'Рассылка',
            'permissions' => [
                'newslatter.create'  => 'Создание',
                'newslatter.edit'    => 'Редактирование',
                'newslatter.delete'  => 'Удаление',
                'newslatter.control' => 'Управление (дает видимость в меню администратора)',
            ],
        ],
        'settings' => [
            'name' => 'Настройки',
            'permissions' => [
                'settings.advert'       => 'Настройка объявлений',
                //'settings.services'     => 'Платные услуги',
                'settings.robokassa'    => 'Настройки робокассы',
                'settings.cache'        => 'Очистить кеш',
                'settings.meta'         => 'Мета-теги',
                'settings.mat'          => 'Запрещенные слова',
            ],
        ],
        'discount' => [
            'name' => 'Скидка',
            'permissions' => [
                'discount.create'  => 'Создание',
                'discount.edit'    => 'Редактирование',
                'discount.delete'  => 'Удаление',
                'discount.control' => 'Управление (дает видимость в меню администратора)',
            ],
        ],
        'services' => [
            'name' => 'Платные услуги',
            'permissions' => [
                'services.create'  => 'Создание',
                'services.edit'    => 'Редактирование',
                'services.delete'  => 'Удаление',
                'services.control' => 'Управление (дает видимость в меню администратора)',
                'services.billing' => 'Билинговая система',
            ],
        ],
        
    ],
];