var elixir = require('laravel-elixir');
elixir.config.sourcemaps = false;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
  //mix.copy('resources/assets/lib/jquery-2.1.4.min.js', 'resources/assets/js/jquery-2.1.4.min.js');
  //mix.copy('resources/assets/lib/bootstrap/dist/js/bootstrap.min.js', 'resources/assets/js/bootstrap.min.js');
  //mix.copy('resources/assets/lib/bootstrap/dist/css/bootstrap.min.css', 'resources/assets/css/bootstrap.min.css');
  //mix.copy('resources/assets/lib/bootstrap/dist/fonts', 'public_html/themes/admin/assets/fonts');
  //mix.copy('resources/assets/lib/adminLTE/dist/js/app.min.js', 'resources/assets/js/app.min.js');
  //mix.copy('resources/assets/lib/adminLTE/dist/img', 'public_html/themes/admin/assets/img');
  //mix.copy('resources/assets/lib/adminLTE/dist/css/AdminLTE.min.css', 'resources/assets/css/AdminLTE.min.css');
  //mix.copy('resources/assets/lib/adminLTE/dist/css/skins/skin-blue.min.css', 'resources/assets/css/skin-blue.min.css');
  //mix.copy('resources/assets/lib/ionic/css/ionic.min.css', 'resources/assets/css/ionic.min.css');
  //mix.copy('resources/assets/lib/ionic/fonts', 'public_html/themes/admin/assets/fonts');
  //mix.copy('resources/assets/lib/FontAwesome/css/font-awesome.min.css', 'resources/assets/css/font-awesome.min.css');
  //mix.copy('resources/assets/lib/FontAwesome/fonts', 'public_html/themes/admin/assets/fonts');
  //mix.copy('resources/assets/lib/plugins/iCheck/icheck.min.js', 'resources/assets/js/icheck.min.js');
  //mix.copy('resources/assets/lib/plugins/iCheck/square/blue.css', 'resources/assets/css/icheck_blue.css');
  ////-----site-----//
  //mix.copy('resources/assets/bacay/fonts', 'public_html/themes/site/assets/fonts');
  //mix.copy('resources/assets/bacay/img', 'public_html/themes/site/assets/img');
  //mix.copy('resources/assets/bacay/css/main.css', 'resources/assets/css/site.css');
  //mix.copy('resources/assets/bacay/js/main.js', 'resources/assets/js/site.js');
  //mix.copy('resources/assets/bacay/js/jquery.sliderkit.1.4.min.js', 'resources/assets/js/jquery.sliderkit.1.4.min.js');
  //mix.copy('resources/assets/bacay/js/jquery.mousewheel.min.js', 'resources/assets/js/jquery.mousewheel.min.js');
  //mix.copy('resources/assets/bacay/js/jquery.fs.scroller.js', 'resources/assets/js/jquery.fs.scroller.js');
  //mix.copy('resources/assets/bacay/js/jquery.formstyler.min.js', 'resources/assets/js/jquery.formstyler.min.js');
  //mix.copy('resources/assets/bacay/js/jquery.flexslider-min.js', 'resources/assets/js/jquery.flexslider-min.js');
  //mix.copy('resources/assets/bacay/js/jquery.fancybox.pack.js', 'resources/assets/js/jquery.fancybox.pack.js');
  //mix.copy('resources/assets/bacay/js/jquery.fancybox.js', 'resources/assets/js/jquery.fancybox.js');
  //
});

elixir(function(mix) {
  mix.scripts([
  'app.min.js',
  'bootbox.min.js',
  'bootstrap-select.min.js',
  'ajax-bootstrap-select.min.js',
  'ajax-bootstrap-select.en-US.min.js',
  'Sortable.min.js',
  'fileinput.min.js',
  'fileinput_locale_ru.js',
  'icheck.min.js',
  'summernote.min.js',
  'summernote-image-title.js',
  'summernote-ru-RU.js',
  'bootstrap-typeahead.js',
  'admin.js'
  ], 'public_html/themes/admin/assets/js/all.js')
});

elixir(function(mix) {
  mix.styles([
  'bootstrap.min.css',
  'AdminLTE.min.css',
  'skin-blue.min.css',
  'font-awesome.min.css',
  'bootstrap-select.min.css',
  'fileinput.min.css',
  'icheck_blue.css',
  'summernote.css',
  'admin.css'
  ], 'public_html/themes/admin/assets/css/all.css')
});

//-------------------------------------------------
//
//-------------------------------------------------
elixir(function(mix) {
  mix.scripts([
  'jquery.sliderkit.1.4.min.js',
  'bootstrap-select.min.js',
  'ajax-bootstrap-select.min.js',
  'ajax-bootstrap-select.en-US.min.js',
  'jquery.mousewheel.min.js',
  'jquery.fs.scroller.js',
  'jquery.formstyler.min.js',
  'jquery.flexslider-min.js',
  'jquery.fancybox.pack.js',
  'jquery.fancybox.js',
  'fileinput.min.js',
  'fileinput_locale_ru.js',
  'bootstrap-slider.min.js',
  'bootstrap-typeahead.js',
  'grids.js',
  'bootbox.min.js',
  'jquery.maskedinput.min.js',
  'jquery.textchange.min.js',
  'site.js'
  ], 'public_html/themes/site/assets/js/all.js')
});

elixir(function(mix) {
  mix.styles([
  'bootstrap.min.css',
  'bootstrap-select.min.css',
  'font-awesome.min.css',
  'fileinput.min.css',
  'ajax-bootstrap-select.css',
  'bootstrap-slider.min.css',
  'flaticon.css',
  'site.css'
  ], 'public_html/themes/site/assets/css/all.css')
});
