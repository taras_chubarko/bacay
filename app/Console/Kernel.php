<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('inspire')
        //         ->hourly();
        
        $schedule->call(function () {
            
            $adverts = \Advert::all();
            foreach($adverts as $advert)
            {
                if(isset($advert->pivotEnd[0]))
                {
                    $end = $advert->pivotEnd[0]->pivot->end;
                    $days = timeLeft($end);
                    if($days->days <= 0)
                    {
                       \DB::table('advert')
                        ->where('id', $advert->id)
                        ->update(['status' => 0]);
                    }
                }
            }
            
        })->cron('* * * * *');//->daily();
        
        
        $schedule->call(function () {
            \NewsLatter::send();
        })->cron('* * * * *');//->everyFiveMinutes();//->cron('* * * * *');
    }
}
