<?php namespace App;

use Landish\Pagination\Pagination as BasePagination;

class Pagination extends BasePagination {

        /**
     * Pagination wrapper HTML.
     *
     * @var string
     */
    protected $paginationWrapper = '<ul class="pagination">%s %s %s</ul>';

    /**
     * Available page wrapper HTML.
     *
     * @var string
     */
    protected $availablePageWrapper = '<li><a href="%s">%s</a></li>';

    /**
     * Get active page wrapper HTML.
     *
     * @var string
     */
    protected $activePageWrapper = '<li class="pagin-active"><a href="javascript:">%s</a></li>';

    /**
     * Get disabled page wrapper HTML.
     *
     * @var string
     */
    protected $disabledPageWrapper = '<li>%s</li>';

    /**
     * Previous button text.
     *
     * @var string
     */
    protected $previousButtonText = '<i class="fa fa-arrow-left"></i>&emsp;Назад';

    /**
     * Next button text.
     *
     * @var string
     */
    protected $nextButtonText = 'Вперед&emsp;<i class="fa fa-arrow-right"></i>';

    /***
     * "Dots" text.
     *
     * @var string
     */
    protected $dotsText = '...';

}