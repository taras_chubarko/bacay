<?php

Breadcrumbs::register('adverts', function($breadcrumbs, $advertsCount)
{
    $segments = Request::segments();
    //dd($segments);
    $term = TaxonomyTerm::findBySlug($segments[1]);
    
    $breadcrumbs->push('Все объявления в '.session('location.city.name_ru'), route('adverts.category', $segments[1]));
    $breadcrumbs->push($term->name.' <span>'.$advertsCount.'</span>');
    
});

Breadcrumbs::register('advert', function($breadcrumbs, $advert)
{
    $term = TaxonomyTerm::findBySlug($advert->type);
    $breadcrumbs->push('Все объявления в '.session('location.city.name_ru'), route('adverts.category', $advert->type));
    $breadcrumbs->push($term->name, route('adverts.category', $advert->type));
    $breadcrumbs->push($advert->name);
});