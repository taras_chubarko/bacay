<?php
/*
 * Helpers
 */
function cuteDate($date)
{
    $today = date('d.m.Y', time());
    $yesterday = date('d.m.Y', time() - 86400);
    $dbDate = date('d.m.Y', strtotime($date));
    $dbTime = date('H:i', strtotime($date));
  
    switch ($dbDate)
    {
        case $today : $output = 'Сегодня в '. $dbTime; break;
        case $yesterday : $output = 'Вчера в '. $dbTime; break;
        default : $output = $dbDate .' в ' . $dbTime;
    }
    return $output;
}
/*
 * function name
 * @param $arg
 */
function cuteDate2($date)
{
    $today = date('d.m.Y', time());
    $yesterday = date('d.m.Y', time() - 86400);
    $dbDate = date('d.m.Y', strtotime($date));
    $dbTime = date('H:i', strtotime($date));
  
    switch ($dbDate)
    {
        case $today : $output = 'Размещено сегодня в '. $dbTime; break;
        case $yesterday : $output = 'Размещено вчера в '. $dbTime; break;
        default : $output = 'Размещено '.$dbDate .' в ' . $dbTime;
    }
    return $output;
}

function cmp($a, $b) {
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

function timeLeft($mysqlDate) {
    $seconds = strtotime($mysqlDate) - time();

    $days = floor($seconds / 86400);
    $seconds %= 86400;

    $hours = floor($seconds / 3600);
    $seconds %= 3600;

    $minutes = floor($seconds / 60);
    $seconds %= 60;

    $data = array();
    $data['days']       = number_format($days, 0, '.', '');
    $data['hours']      = number_format($hours, 0, '.', '');
    $data['minutes']    = number_format($minutes, 0, '.', '');
    $data['seconds']    = number_format($seconds, 0, '.', '');
    
    return (object) $data;
}

function num2word($num, $words){
    
    $num = $num % 100;
    
    if ($num > 19)
    {
        $num = $num % 10;
    }
    
    switch ($num)
    {
        case 1: 
            return($words[0]);
        break;
            
        case 2: case 3: case 4:
            return($words[1]);
        break;
            
        default:
            return($words[2]);
    }
}



