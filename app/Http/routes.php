<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
$scss = new scssc();
$scss->setFormatter("scss_formatter_compressed");
$scssIn = file_get_contents(public_path().'/themes/site/assets/scss/site.scss');
$cssOut = $scss->compile($scssIn);
file_put_contents(public_path().'/themes/site/assets/css/site.css', $cssOut);

\User::set_location();

\Visitor::log();

Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin'], function()
{
   Route::get('/', function () {
      return 'cvxc';
   });
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => ['web']], function () {

   Route::get('captchas', function() {
      Captcha::create(null);
   });
    
});

/*
 * Макросы новые
 */

Form::macro('checkboxes', function($name, $values, $default = array(), $attributes = array())
{
   $out = '';
   if(!empty($values))
   {
      $out .= '<ul class="filter-ch-list">';
      foreach($values as $key => $value)
      {
         $chk = false;
         
         if(!is_null($default))
         {
            $chk =  (in_array($key, $default)) ? true : false;
         }
         
         $out .= '<li>
            '.Form::checkbox($name.'['.$key.']', $key, $chk, array('id' => $name.'-'.$key, 'class' => $attributes['class'])).'
            <label for="'.$name.'-'.$key.'">'. $value. '</label>
         </li>';
         
      }
      $out .= '</ul>';
   }
    
   return $out;
});

Form::macro('radios', function($name, $values, $default = null)
{
   $out = '';
   if(!empty($values))
   {
      foreach($values as $key => $value)
      {
         $out .= '<div class="radio">';
            $out .= '<label for="'.$name.'-'.$key.'" class="icheck">' . Form::radio($name, $key, ($key == $default) ? true : false) . '&ensp;'. $value. '</label>';
         $out .= '</div>';
      }
   }
    
   return $out;
});

Form::macro('radios2', function($name, $values, $default = array(), $attributes = array())
{
   $out = '';
   if(!empty($values))
   {
      $out .= '<ul class="filter-ch-list">';
      foreach($values as $key => $value)
      {
         $chk = false;
         
         if(!is_null($default))
         {
            $chk =  (in_array($key, $default)) ? true : false;
         }
         
         $out .= '<li>
            '.Form::radio($name, $key, $chk, array('id' => $name.'-'.$key, 'class' => $attributes['class'])).'
            <label for="'.$name.'-'.$key.'">'. $value. '</label>
         </li>';
         
      }
      $out .= '</ul>';
   }
    
   return $out;
});

Form::macro('selectMetro', function($name = 'name', $value = array(), $empty = null, $settings = array())
{
   $html = '';
    
    if(isset($value))
    {
        $class = null;
        $first = null;
        $attr = array();
        
        foreach($settings as $k => $opt)
        {
            switch($k)
            {
                case 'class':
                    $class = ' '.$opt;
                break;
                
                //case 'title':
                //    $first = '<option value="All">'.$opt.'</option>';
                //break;
                
                default:
                $attr[] = $k.'="'.$opt.'"';
            }
            
        }
        $attributes = implode(' ', $attr);
        
        $html .= '<select name="'.$name.'" class="form-control'.$class.'" '.$attributes.' data-live-search="true">';
        $html .= $first;
        foreach($value as $key => $item)
        {
            $selected = ($key == $empty) ? 'selected=selected' : null;
            $cl = 'class=glyph-icon flaticon-symbols';
            $atr['data-content'] = '<span><i style="color:'.$item->color.'" class="glyph-icon flaticon-symbols"></i> '.$item->name.'</span>';
            //$dataCont = e('data-content="<span><i class="dsfasd">1</i>sfdgsfg</span>');
            $html .= sprintf('<option %s value="%s" %s>%s</option>', Html::attributes($atr), $key, $selected, $item->name);
        }
        $html .= '</select>';
    }
    return $html;
});

Blade::extend(function($value) {
   return preg_replace('/\@var(.+)/', '<?php ${1}; ?>', $value);
});

Validator::extend('phone', function ($attribute, $value, $parameters) {
   return preg_match('/[+][7][(][0-9]{3}[)] [0-9]{3}[-][0-9]{2}[-][0-9]{2}$/i', $value);
});




